
import UIKit
import ReactorKit

final class <# Controller Name #>: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
//    /// 뒤로가기 (푸시)
//    @IBOutlet weak var backButton: UIButton!
//    /// 뒤로가기 (모달)
//    @IBOutlet weak var backModalButton: UIButton!
//
//    /// 상단 타이틀 라벨
//    @IBOutlet weak var topTitleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // ViewController 초기 설정
        initialize()
    }

    func bind(reactor: <# Reactor Name #>) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension <# Controller Name #> {
    func bindAction(_ reactor: <# Reactor Name #>) {
//        backButton.rx.tap
//            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
//            .map { Reactor.Action.dismiss }
//            .bind(to: reactor.action)
//            .disposed(by: disposeBag)


    }
}

// MARK: bindState For Reactor
private extension <# Controller Name #> {
    func bindState(_ reactor: <# Reactor Name #>) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension <# Controller Name #> {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }

    /// 레이아웃 초기 설정
    func initializeLayout() {}

    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension <# Template Main View Controller #> {
//    /// Reactor Action, <# 액션명 #>
//    func commandReactorAction<# 액션명 #>() {
//        guard let reactor = reactor else { return }
//
//        Observable.just(Reactor.Action.<# 액션명 #>)
//            .observeOn(MainScheduler.asyncInstance)
//            .bind(to: reactor.action)
//            .disposed(by: disposeBag)
//    }
}

// MARK: -
