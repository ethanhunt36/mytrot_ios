//
//  TemplateResponseModelType.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct <#className#>: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: <#className#>, rhs: <#className#>) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    // PROPERTY
    var data: <#MainDataModel#>?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct <#MainDataModel#>: Codable {
    var list_data: [<#ListDataModel#>]
}

struct <#ListDataModel#>: Codable {
    var no: Int64
    var name: String
}
