//
//  IAPDonateCell.swift
//  MyTrot
//
//  Created by hclim on 2021/08/05.
//

import Foundation
import RxSwift
protocol IAPPayDelegate: AnyObject {
    func clickPayment(infoItem: ProductListInfoModel)
}
class IAPDonateCell: BaseTableViewCell {
    var disposeBag = DisposeBag()
    weak var delegate: IAPPayDelegate?

    @IBOutlet weak var iapTitleLabel: UILabel!
    @IBOutlet weak var iapPriceLabel: UILabel!
    @IBOutlet weak var iapDescLabel: UILabel!
    @IBOutlet weak var backView: UIView!{
        didSet {
            backView.layer.cornerRadius = 10
            backView.layer.masksToBounds = true
            backView.layer.borderWidth = 0.5
            backView.layer.borderColor = Asset.brownishGrey.color.cgColor
        }
    }
    @IBOutlet weak var payButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    func initializeEvent() {
        payButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let item = self?.item else { return }
                self?.delegate?.clickPayment(infoItem: item)

            }).disposed(by: disposeBag)

    }

    var item: ProductListInfoModel? {
        didSet {
            iapTitleLabel.text = item?.description ?? ""
            let price = "\(item?.price_i.commaString ?? "0") 원"
            iapPriceLabel.text = price
            let point = "\(item?.point.commaString ?? "0")P"
            let voteCnt = "\(item?.vote_cnt.commaString ?? "0")장"

            let attributedString = NSMutableAttributedString(string: "투표권 \(voteCnt)과 포인트 \(point)를 선물로 드려요.", attributes: [
                .font: UIFont.systemFont(ofSize: 15.0, weight: .semibold),
                .foregroundColor: Asset.purpley.color,
              .kern: 0.0
            ])

            attributedString.addAttribute(.foregroundColor, value: Asset.fadedRed.color, range: NSRange(location: 4, length: voteCnt.count))
            let nextCount = 4 + voteCnt.count + 6

            attributedString.addAttribute(.foregroundColor, value: Asset.fadedRed.color, range: NSRange(location: nextCount, length: point.count))

            iapDescLabel.attributedText = attributedString
        }
    }
}
