//
//  IAPDeactivateAdCell.swift
//  MyTrot
//
//  Created by hclim on 2021/08/05.
//

import Foundation
import RxSwift
class IAPDeactivateAdCell: BaseTableViewCell {
    var disposeBag = DisposeBag()
    weak var delegate: IAPPayDelegate?

    @IBOutlet weak var iapTitleLabel: UILabel!
    @IBOutlet weak var iapPriceLabel: UILabel!
    @IBOutlet weak var backView: UIView!{
        didSet {
            backView.layer.cornerRadius = 10
            backView.layer.masksToBounds = true
            backView.layer.borderWidth = 0.5
            backView.layer.borderColor = Asset.brownishGrey.color.cgColor
        }
    }
    @IBOutlet weak var payButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    func initializeEvent() {
        payButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let item = self?.item else { return }
                self?.delegate?.clickPayment(infoItem: item)

            }).disposed(by: disposeBag)

    }

    var item: ProductListInfoModel? {
        didSet {
            iapTitleLabel.text = item?.description ?? ""
            let price = "\(item?.price_i.commaString ?? "0") 원"
            iapPriceLabel.text = price
        }
    }
}
