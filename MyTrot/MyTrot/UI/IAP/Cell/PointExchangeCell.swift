//
//  PointExchangeCell.swift
//  MyTrot
//
//  Created by hclim on 2021/08/05.
//

import Foundation
import RxSwift
import UIKit

protocol PointExchangeDelegate: AnyObject {
    func clickExchange(point_amount: Int)
}

class PointExchangeCell: BaseTableViewCell {
    var disposeBag = DisposeBag()
    weak var delegate: PointExchangeDelegate?

    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var voteLabel: UILabel!
    @IBOutlet weak var backView: UIView!{
        didSet {
            backView.layer.cornerRadius = 10
            backView.layer.masksToBounds = true
            backView.layer.borderWidth = 0.5
            backView.layer.borderColor = Asset.brownishGrey.color.cgColor
        }
    }
    @IBOutlet weak var exchangeButton: UIButton! {
        didSet {
            exchangeButton.layer.cornerRadius = 10
            exchangeButton.layer.masksToBounds = true

        }
    }
    @IBOutlet weak var pointSlider: UISlider!
    
    @IBOutlet weak var usagePointLabel: UILabel!
    
    @IBOutlet weak var pointTextField: UITextField!
    @IBOutlet weak var pointRoundView: UIView! {
        didSet {
            pointRoundView.layer.cornerRadius = 5.0
            pointRoundView.layer.borderColor = Asset.purpley.color.cgColor
            pointRoundView.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var pointInputButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.updateLayout()
    }
    
    func updateLayout(){
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }


    var currentPoint: Int? {
        didSet {
            pointLabel.text = "\(currentPoint ?? 0)P"
            let votePoint = (currentPoint ?? 0) * 2
            voteLabel.text = "\(votePoint)장"
        }
    }
    var usagePoint: Int? {
        didSet {
            pointSlider.maximumValue = Float(usagePoint ?? 0)
            usagePointLabel.text = "\(Float(usagePoint ?? 0))P"
        }
    }
    func initializeEvent() {
        pointTextField.delegate = self
        pointSlider.minimumValue = 0
        pointSlider.rx.value
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] currentPoint in
                self?.currentPoint = Int(currentPoint)
                self?.pointTextField.text = "\(Int(currentPoint))"
                if Int(currentPoint) == 0 {
                    self?.pointTextField.text = ""
                }
            }).disposed(by: disposeBag)
        
        pointInputButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let point_amount = self?.currentPoint else { return }
                self?.delegate?.clickExchange(point_amount: point_amount)

            }).disposed(by: disposeBag)

        exchangeButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let point_amount = self?.currentPoint else { return }
                self?.delegate?.clickExchange(point_amount: point_amount)

            }).disposed(by: disposeBag)
        let bus = RxBus.shared
        bus.asObservable(event: Events.RefreshMyInfo.self, sticky: true).subscribe {[weak self] event in
            print("RefreshMyInfo!!!")
            let usagePoint = Int(User.shared.myInfo?.point ?? 0)
            self?.usagePoint = usagePoint
            self?.currentPoint = 0
            self?.pointSlider.value = 0
//            self?.usagePointLabel.text = "\(Float(usagePoint ))P"

        }.disposed(by: disposeBag)


    }
}

extension PointExchangeCell: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        print("textFieldDidBeginEditing")
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool // return NO to not change text
    {
        let max = pointSlider.maximumValue
        let current = Float("\(pointTextField.text ?? "0")\(string)") ?? 0
        if string == "" {
            if current < 10 {
                currentPoint = 0
                pointSlider.value = 0
                return true

            }
            let realCurrent = current / 10
            currentPoint = Int(realCurrent)
            pointSlider.value = realCurrent

            return true
        }
    
        if max >= current {
            currentPoint = Int(current)
            pointSlider.value = current
            return true
        } else {
            return false
        }
    }

}
