//
//  IAPViewModel.swift
//  MyTrot
//
//  Created by hclim on 2021/08/05.
//

import Foundation
struct IAPViewModel: Hashable {
    static func == (lhs: IAPViewModel, rhs: IAPViewModel) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(createdTime)
    }

    // 모델 생성시간, hashValue 생성을 위한 값으로 사용
    let createdTime: Double = Date().timeIntervalSince1970

    init(_ items: [IAPCellItem] = []) {
        self.items = items
    }

    // 리스트 데이터
    let items: [IAPCellItem]

    // item 갯수
    var count: Int { items.count }

    // item 요소에 접근 정의
    subscript(index: Int) -> IAPCellItem { return items[index] }
}

enum IAPType {
    case donateHeader
    case donate
    case autoChargeHeader
    case autoCharge
    case boosterHeader
    case booster
    case deactivateAdHeader
    case deactivateAd
    case exchangePoint
}
protocol IAPCellItem {
    // 타입
    var type: IAPType { get }
}
extension IAPCellItem where Self: IAPDonateHeaderCellItem {
    var type: IAPType { return .donateHeader }
}
final class IAPDonateHeaderCellItem: IAPCellItem {}

extension IAPCellItem where Self: IAPDonateCellItem {
    var type: IAPType { return .donate }
}
final class IAPDonateCellItem: IAPCellItem {
    
    var item: ProductListInfoModel
    init(responseItem item: ProductListInfoModel) {
        self.item = item
    }
}

extension IAPCellItem where Self: IAPAutoChargeHeaderCellItem {
    var type: IAPType { return .autoChargeHeader }
}
final class IAPAutoChargeHeaderCellItem: IAPCellItem {}

extension IAPCellItem where Self: IAPAutoChargeCellItem {
    var type: IAPType { return .autoCharge }
}
final class IAPAutoChargeCellItem: IAPCellItem {
    
    var item: ProductListInfoModel
    init(responseItem item: ProductListInfoModel) {
        self.item = item
    }
}

extension IAPCellItem where Self: IAPBoosterHeaderCellItem {
    var type: IAPType { return .boosterHeader }
}
final class IAPBoosterHeaderCellItem: IAPCellItem {}

extension IAPCellItem where Self: IAPBoosterCellItem {
    var type: IAPType { return .booster }
}
final class IAPBoosterCellItem: IAPCellItem {
    
    var item: ProductListInfoModel
    init(responseItem item: ProductListInfoModel) {
        self.item = item
    }
}

extension IAPCellItem where Self: IAPDeactivateAdHeaderCellItem {
    var type: IAPType { return .deactivateAdHeader }
}
final class IAPDeactivateAdHeaderCellItem: IAPCellItem {}

extension IAPCellItem where Self: IAPDeactivateAdCellItem {
    var type: IAPType { return .deactivateAd }
}
final class IAPDeactivateAdCellItem: IAPCellItem {
    
    var item: ProductListInfoModel
    init(responseItem item: ProductListInfoModel) {
        self.item = item
    }
}

extension IAPCellItem where Self: IAPExchangePointCellItem {
    var type: IAPType { return .exchangePoint }
}
final class IAPExchangePointCellItem: IAPCellItem {}
