//
//  IAPViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/08/05.
//

import Foundation

import UIKit
import ReactorKit
import StoreKit

final class IAPViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!

    
    var data = IAPViewModel() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }

        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: IAPViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension IAPViewController {
    func bindAction(_ reactor: IAPViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        reactor.action.onNext(.getList)
    }
}

// MARK: bindState For Reactor
private extension IAPViewController {
    func bindState(_ reactor: IAPViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.productIds }
            .filterNil()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] ids in
                self?.requestProducts(ids: ids)
            }).disposed(by: disposeBag)
        
        reactor.state.map { $0.mytrotOrder_id }
            .filterNil()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] order_id in
                guard let transItem = reactor.currentState.transactionItem else { return }
                self?.paymentToApple(infoItem: transItem)
            }).disposed(by: disposeBag)
        reactor.state.map { $0.transaction }
            .filterNil()
            .subscribe(onNext: { [weak self] transaction in
                self?.finishTransaction(transaction: transaction)
            }).disposed(by: disposeBag)


    }
}
// MARK: -
// MARK: private IAP Function
private extension IAPViewController {
    func finishTransaction(transaction: SKPaymentTransaction) {
        SKPaymentQueue.default().finishTransaction(transaction)
        guard let reactor = reactor else { return }
        reactor.action.onNext(.finishTransaction)
    }
    // API Response 후 -> IAP 불러오기
    func requestProducts(ids: [String]) {
        InAppProducts.shared.addProducts(productIds: ids)
        IAPProducts.store.requestProducts { [weak self] (success, iapProducts) in
            if success, let strongProducts = iapProducts {
                guard let reactor = self?.reactor else { return }
                guard let response = reactor.currentState.iapResponse else { return }
                InAppProducts.shared.addSKProducts(sendProducts: strongProducts)

                var section: [IAPCellItem] = []
                
                let donateList = response.data?.filter({ model -> Bool in
                    return model.sku.hasPrefix("donation")
                })
                if donateList?.count ?? 0 > 1 {
                    section.append(IAPDonateHeaderCellItem())
                    let strongList = donateList ?? []
                    for donateItem in strongList {
                        if strongProducts.filter({ p in
                            return p.productIdentifier == donateItem.sku
                        }).count > 0 {
                            section.append(IAPDonateCellItem(responseItem: donateItem))
                        }
                        
                    }
                }
                
                let autoChargeList = response.data?.filter({ model -> Bool in
                    return model.sku.hasPrefix("auto_ticket")
                })
                if autoChargeList?.count ?? 0 > 1 {
                    section.append(IAPAutoChargeHeaderCellItem())
                    let strongList = autoChargeList ?? []
                    for autoChargeItem in strongList {
                        if strongProducts.filter({ p in
                            return p.productIdentifier == autoChargeItem.sku
                        }).count > 0 {
                            section.append(IAPAutoChargeCellItem(responseItem: autoChargeItem))
                        }

                    }
                }
                let boostList = response.data?.filter({ model -> Bool in
                    return model.sku.hasPrefix("booster")
                })
                if boostList?.count ?? 0 > 1 {
                    section.append(IAPBoosterHeaderCellItem())
                    let strongList = boostList ?? []
                    for boostItem in strongList {
                        if strongProducts.filter({ p in
                            return p.productIdentifier == boostItem.sku
                        }).count > 0 {
                            section.append(IAPBoosterCellItem(responseItem: boostItem))
                        }

                    }
                }
                let removeAdList = response.data?.filter({ model -> Bool in
                    return model.sku.hasPrefix("remove_ad")
                })
                if removeAdList?.count ?? 0 > 1 {
                    section.append(IAPDeactivateAdHeaderCellItem())
                    let strongList = removeAdList ?? []
                    for removeAdItem in strongList {
                        if strongProducts.filter({ p in
                            return p.productIdentifier == removeAdItem.sku
                        }).count > 0 {
                            section.append(IAPDeactivateAdCellItem(responseItem: removeAdItem))
                        }

                    }
                }
                section.append(IAPExchangePointCellItem())

                self?.data = IAPViewModel(section)

            } else {
                DispatchQueue.main.async { [weak self] in
                    self?.subscribeOnNextBaseAlertPushBack(alertData: "결제 데이터 호출중 오류가 발생하였습니다.\n잠시 후 다시 시도해 주세요.")
                }
            }

        }
        
    }
    @objc func handleIAPPurchase(_ notification: Notification) {

    }
}
// MARK: -
// MARK: private initialize
private extension IAPViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.MyInfo.self, sticky: true).subscribe {[weak self] event in
            print("IAPScreenRefresh!!!")
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)

    }
}


// MARK: - Reactor Action
private extension IAPViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}


// MARK: -
extension IAPViewController {
    func initializeTableView() {
        self.tableView.register(cellType: IAPDonateHeaderCell.self)
        self.tableView.register(cellType: IAPDonateCell.self)
        self.tableView.register(cellType: IAPAuthChargeHeaderCell.self)
        self.tableView.register(cellType: IAPAutoChargeCell.self)
        self.tableView.register(cellType: IAPBoosterHeaderCell.self)
        self.tableView.register(cellType: IAPBoosterCell.self)
        self.tableView.register(cellType: IAPDeactivateAdHeaderCell.self)
        self.tableView.register(cellType: IAPDeactivateAdCell.self)
        self.tableView.register(cellType: PointExchangeCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        let singleTabClose = UITapGestureRecognizer(target: self, action: #selector(tapKeyboardClose))
        singleTabClose.numberOfTapsRequired = 1
        singleTabClose.isEnabled = true
        singleTabClose.cancelsTouchesInView = false
        view.addGestureRecognizer(singleTabClose)
    }
    @objc private func tapKeyboardClose() {
        self.view.endEditing(true)
    }
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        bottomAnchorConstraint.constant = ((view.convert(keyboardFrame.cgRectValue, from: nil).size.height)  )
        print("keyboard height : \((view.convert(keyboardFrame.cgRectValue, from: nil).size.height))")
        print("view.safeAreaInsets.bottom : \(view.safeAreaInsets.bottom)")

    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        bottomAnchorConstraint.constant = 0
    }

}
// MARK: -
// MARK: UITableViewDelegate
extension IAPViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let type = data[indexPath.row].type
        switch type {
        case .donateHeader:
            return 115
        case .donate:
            return 83
        case .autoChargeHeader:
            return 153
        case .autoCharge:
            return 51
        case .boosterHeader:
            return 133
        case .booster:
            return 51
        case .deactivateAdHeader:
            return 176
        case .deactivateAd:
            return 51
        case .exchangePoint:
            return 450

        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = data[indexPath.row].type
        switch type {
        case .donateHeader:
            let cell = tableView.dequeueReusableCell(for: indexPath) as IAPDonateHeaderCell
            return cell
        case .donate:
            let cell = tableView.dequeueReusableCell(for: indexPath) as IAPDonateCell
            let cellItem = data[indexPath.row] as? IAPDonateCellItem
            
            cell.item = cellItem?.item
            cell.delegate = self
            return cell
        case .autoChargeHeader:
            let cell = tableView.dequeueReusableCell(for: indexPath) as IAPAuthChargeHeaderCell
            return cell
        case .autoCharge:
            let cell = tableView.dequeueReusableCell(for: indexPath) as IAPAutoChargeCell
            let cellItem = data[indexPath.row] as? IAPAutoChargeCellItem
            cell.delegate = self

            cell.item = cellItem?.item

            return cell
        case .boosterHeader:
            let cell = tableView.dequeueReusableCell(for: indexPath) as IAPBoosterHeaderCell
            return cell
        case .booster:
            let cell = tableView.dequeueReusableCell(for: indexPath) as IAPBoosterCell
            let cellItem = data[indexPath.row] as? IAPBoosterCellItem
            cell.delegate = self

            cell.item = cellItem?.item

            return cell
        case .deactivateAdHeader:
            let cell = tableView.dequeueReusableCell(for: indexPath) as IAPDeactivateAdHeaderCell
            return cell
        case .deactivateAd:
            let cell = tableView.dequeueReusableCell(for: indexPath) as IAPDeactivateAdCell
            let cellItem = data[indexPath.row] as? IAPDeactivateAdCellItem
            cell.delegate = self

            cell.item = cellItem?.item

            return cell
        case .exchangePoint:
            let cell = tableView.dequeueReusableCell(for: indexPath) as PointExchangeCell
            let usagePoint = Int(User.shared.myInfo?.point ?? 0)
            cell.usagePoint = usagePoint
            cell.delegate = self
            print("load exchangePoint")
            return cell

        }
    }

}
extension IAPViewController {
    func paymentToApple(infoItem: ProductListInfoModel) {
        print("paymentToApple")
        guard let product = InAppProducts.shared.products.filter({ $0.productIdentifier == infoItem.sku }).first else { return }
        guard let reactor = self.reactor else { return }

        guard let mytrotOrder_id = reactor.currentState.mytrotOrder_id else { return }

        IAPProducts.store.buyProductWithOrderId(product, order_id: mytrotOrder_id) { [weak self] (success, transaction) in
            // 애플 결제 완료 -> 아이템 지급 호출
            if success {
                print("productsPaymentCompletionHandler")
                guard let transactionItem = reactor.currentState.transactionItem else { return }
                guard let transactionOrder_id = transaction.payment.applicationUsername else {
                    SKPaymentQueue.default().finishTransaction(transaction)
                    return
                }
                let receipt = self?.getReceiptData() ?? ""
                if receipt != "" {
                    reactor.action.onNext(.completeOrder(infoItem: transactionItem, transactionId: transaction.transactionIdentifier ?? "", order_id: transactionOrder_id, token: receipt, transaction: transaction))

                    let preference = PreferencesService()
                    print("paid products : \(preference.getIAPProducts())")

                } else {
                    Utils.AlertShow(msg: "구매영수증데이터 준비중입니다.\n잠시후 다시 시도해 주세요.")

                }

            }else {
                if let transactionError = transaction.error as NSError?,
                    let localizedDescription = transaction.error?.localizedDescription,
                    transactionError.code != SKError.paymentCancelled.rawValue {
                    print("Transaction Error: \(localizedDescription)")
                    Utils.AlertShow(msg: "실패 : \(localizedDescription)")
                }
            }

        }

//        IAPProducts.store.buyProduct(product) { [weak self] (success, transaction) in
//            // 애플 결제 완료 -> 아이템 지급 호출
//            if success {
//                print("productsPaymentCompletionHandler")
//                guard let transactionItem = reactor.currentState.transactionItem else { return }
//                guard let mytrotOrder_id = reactor.currentState.mytrotOrder_id else { return }
//                guard let transactionOrder_id = transaction.payment.applicationUsername else { return }
//                print("mytrotOrder_id : \(mytrotOrder_id)")
//                print("transactionOrder_id : \(transactionOrder_id)")
//                let receipt = self?.getReceiptData() ?? ""
//                reactor.action.onNext(.completeOrder(infoItem: transactionItem, transactionId: transaction.transactionIdentifier ?? "", order_id: mytrotOrder_id, token: receipt, transaction: transaction))
//
//                let preference = PreferencesService()
//                print("paid products : \(preference.getIAPProducts())")
//
//            }else {
//                if let transactionError = transaction.error as NSError?,
//                    let localizedDescription = transaction.error?.localizedDescription,
//                    transactionError.code != SKError.paymentCancelled.rawValue {
//                    print("Transaction Error: \(localizedDescription)")
//                    Utils.AlertShow(msg: "실패 : \(localizedDescription)")
//                }
//            }
//
//        }

    }
    
    public func getReceiptData() -> String? {
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                let receiptString = receiptData.base64EncodedString(options: [])
                print("receiptString : \(receiptString)")
                return receiptString
            }
            catch {
                print("Couldn't read receipt data with error: " + error.localizedDescription)
                return nil
            }
        }
        return nil
    }

}
extension IAPViewController: IAPPayDelegate {
    func clickPayment(infoItem: ProductListInfoModel) {
        print("clickPayment : \(infoItem.sku)")
        guard let reactor = reactor else { return }
        reactor.action.onNext(.addOrder(infoItem: infoItem))
    }
    
    
}

extension IAPViewController: PointExchangeDelegate {
    func clickExchange(point_amount: Int) {
        print("clickExchange : \(point_amount)")
        if point_amount < 10 {
            Utils.AlertShow(msg: "10P 이상 부터 교환할 수 있습니다.")
            return 
        }
        let controller = UIAlertController.init(title: "", message: "포인트 \(point_amount)P를 투표권 \(point_amount * 2)장으로\n교환하시겠습니까?", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "확인", style: .default, handler: { [weak self] _ in
            
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.exchangePoint(point_amount))
        })
        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)

        controller.addAction(defaultAction)
        controller.addAction(cancel)

        self.present(controller, animated: true)

    }
    
    
}
