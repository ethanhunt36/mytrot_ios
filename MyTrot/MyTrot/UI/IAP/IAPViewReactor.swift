//
//  IAPViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/08/05.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift
import StoreKit

final class IAPViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case getList
        case exchangePoint(Int)
        
        // 상품 주문(클릭)
        case addOrder(infoItem: ProductListInfoModel)
        
        // 상품 결제
        case completeOrder(infoItem: ProductListInfoModel, transactionId: String, order_id: String, token: String, transaction: SKPaymentTransaction)
        // 결제 후 Apple Transaction 완료
        case finishTransaction
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(ResponseProductList)
        case empty
        // 결제 진행중인 선택된 Item과 Order_id
        case setPayment(ProductListInfoModel, String)
        case setTransaction(SKPaymentTransaction?)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var productIds: [String]?
        var iapResponse: ResponseProductList?
        var transactionItem: ProductListInfoModel?
        var mytrotOrder_id: String?
        var transaction: SKPaymentTransaction?
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false
        )
    }
    
    func mutate(action: IAPViewReactor.Action) -> Observable<IAPViewReactor.Mutation> {
        switch action {
        case .finishTransaction:
            return .just(.setTransaction(nil))
        case .addOrder(let infoItem):
            return networkService.addOrder(product_no: infoItem.no, price: infoItem.price_i).asObservable().map { res in
                print("networkService.addOrder : \(res)")
                if res.isResponseStatusSuccessful() {
                    return Mutation.setPayment(infoItem, res.data ?? "")

                } else {
                    Utils.AlertShow(msg: res.msg ?? Constants.Network.ErrorMessage.networkError)
                    return .empty
                }
//                return .empty
            }
        case .completeOrder(let infoModel, let transactionId, let order_id, let token, let transaction):
            return networkService.completeOrder(order_no: Int(order_id) ?? 0, price: infoModel.price_i, pay_type: "I", sku: infoModel.sku, order_id: transactionId, token: token).asObservable().map { [weak self] res in
                let preference = PreferencesService()
                let productInfo = ProductInfo(receipt: token, mytrotOrderId: order_id, appleTransactionId: transactionId)

                if res.isResponseStatusSuccessful() {
                    RxBus.shared.post(event: Events.RefreshMyInfo(), sticky: true)

                    Utils.AlertShow(msg: self?.completePayMessage(sku: infoModel.sku) ?? "결제 완료 되었습니다.")
                    preference.finishIapProduct(productInfo: productInfo)
                } else {
                    preference.setUnPaidIAPProduct(productInfo: productInfo)

                    Utils.AlertShow(msg: res.msg ?? Constants.Network.ErrorMessage.networkError)

                }
                print("networkService.completeOrder : \(res)")
                return .setTransaction(transaction)
            }
        case .exchangePoint(let point):
            return networkService.exchangePoint(point_amount: point).asObservable().map { response in
                if response.isResponseStatusSuccessful() {
                    RxBus.shared.post(event: Events.RefreshMyInfo(), sticky: true)
                    Utils.AlertShow(msg: "교환이 완료 되었습니다.")
                    return .empty

                }
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)

                return .empty

            }
        case .getList:
            return networkService.getIAPList().asObservable().map { Mutation.setResponse($0)}
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: IAPViewReactor.State, mutation: IAPViewReactor.Mutation) -> IAPViewReactor.State {
        var state = state
        switch mutation {
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.iapResponse = response
                let iapIdLists: [String]? = response.data?.map{ $0.sku }
                state.productIds = iapIdLists
                
            } else {
                RxBus.shared.post(event: Events.AlertShow(message: response.msg ?? ""))
            }
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty: return state
        case .setPayment(let infoModel, let order_id):
            state.transactionItem = infoModel
            state.mytrotOrder_id = order_id
        case .setTransaction(let transaction):
            state.transaction = transaction
        }
        return state
    }
    
    func completePayMessage(sku: String) -> String {
        if (sku.contains("donation")) {
            return "감사합니다.\n후원하기 완료 되었습니다."
        }
        if (sku.contains("remove_ad") || sku.contains("ios.test")) {
            return "감사합니다.\n앞으로 광고는 모두 제거됩니다."
        }
        if (sku.contains("auto_ticket")) {
            return "감사합니다.\n\n앞으로 1시간 마다 자동으로 투표권이 충전됩니다."
        }
        if (sku.contains("booster")) {
            return "감사합니다.\n\n'광고보고 투표권&포인트 무료 충전' 시에 투표권을 2배로 충전해드립니다."
        }

        return "결제 완료 되었습니다."
    }
}
