//
//  ChartFlow.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
import RxFlow
import RxRelay

final class SplashFlow: Flow {
    var root: Presentable {
        return self.rootViewController
    }

    private lazy var rootViewController: BaseNavigationController = {
        let viewController = BaseNavigationController()
        viewController.setNavigationBarHidden(true, animated: false)
        return viewController
    }()

    private let myStepper: SplashStepper

    init(withStepper stepper: SplashStepper) {
        self.myStepper = stepper
    }

    deinit { print("\(type(of: self)): \(#function)") }

    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? SplashStep else { return FlowContributors.none }

        switch step {
        case .main:
            return navigateToMainScreen()
        
        }
    }
}

// MARK: -
// MARK: 이동 (navigate)
// MARK: -
extension SplashFlow {
    /// 메인화면 이동
    private func navigateToMainScreen() -> FlowContributors {
        let controller = SplashViewController.instantiate()
        let reactor = SplashViewReactor()
        controller.reactor = reactor

        self.rootViewController.setViewControllers([controller], animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
}

// MARK: 닫기 (dismiss, pop)
private extension SplashFlow {
    func dismissScreen(type: NavigationType = .push, animated: Bool = true) -> FlowContributors {
        switch type {
        case .push:
            if self.rootViewController.viewControllers.count == 1 {
                return dismissScreen(type: .modal, animated: true)
            }
            self.rootViewController.popViewController(animated: animated)
        case .modal:
            self.rootViewController.dismiss(animated: animated, completion: {
            })
        }
        return .none
    }
}

class SplashStepper: Stepper {
    var steps = PublishRelay<Step>()
    var step: SplashStep?
    convenience init(step: SplashStep) {
        self.init()
        self.step = step
    }
    var initialStep: Step {
        if let step = self.step {
            return step
        }
        return SplashStep.main
    }
}

