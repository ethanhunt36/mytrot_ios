//
//  SplashViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//


import UIKit
import ReactorKit
import WebKit

final class SplashViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
//    /// 뒤로가기 (푸시)
//    @IBOutlet weak var backButton: UIButton!
//    /// 뒤로가기 (모달)
//    @IBOutlet weak var backModalButton: UIButton!
//
//    /// 상단 타이틀 라벨
//    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var agreementView: UIView!
    @IBOutlet weak var agreementWeb: WKWebView!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // ViewController 초기 설정
        initialize()
    }

    func bind(reactor: SplashViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension SplashViewController {
    func bindAction(_ reactor: SplashViewReactor) {
        agreeButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.setAppState(.agreeAccepted) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        cancelButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                let controller = UIAlertController.init(title: "", message: Constants.String.AlertMessage.agreementDeniedMessage, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "확인", style: .cancel, handler: { _ in
                    reactor.action.onNext(.setAppState(.agreeDenied))
                })
                
                controller.addAction(defaultAction)

                self?.present(controller, animated: true)

            }).disposed(by: disposeBag)

    }
}

// MARK: bindState For Reactor
private extension SplashViewController {
    func bindState(_ reactor: SplashViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.appState }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] appState in
                self?.commandReactorActionAppState(appState: appState)
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension SplashViewController {
    /// 초기 설정
    func initialize() {
//        delay(delay: 2.0, closure: { [weak self] in
//            self?.commandReactorActionMoveMain()
//
//        })
        initializeLayout()
        initializeEventBus()
    }

    /// 레이아웃 초기 설정
    func initializeLayout() {
        self.agreementWeb.load(URLRequest.init(url: URL(string: "http://www.mytrot.co.kr/agree/agree_use.html")!))

    }

    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension SplashViewController {
//    /// Reactor Action, <# 액션명 #>
    func commandReactorActionMoveMain() {
        guard let reactor = reactor else { return }

        Observable.just(Reactor.Action.moveToScreen(.mainScreen))
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
    }
    func commandReactorActionAppState(appState: MyAppState) {
        guard let reactor = reactor else { return }
        let pref = PreferencesService()

        switch appState {
        case .needToCheckAgreeTerms:
            print("pref.getIsUserAgreeTerms() === \(pref.getIsUserAgreeTerms())")
            if pref.getIsUserAgreeTerms() == true {
                reactor.action.onNext(.setAppState(.agreeAccepted))
            } else {
                // 약관 오픈
                self.agreementView.isHidden = false
            }
        // 버전 필수 업데이트
        case .updateRequired:
            updateRequiredAlert()
        // 버전 선택 업데이트
        case .updateSelection:
            updateSelectionAlert()
        // 버전 최신
        case .recentVersion:
            if pref.getMember_no() != "" {
                Observable.just(Reactor.Action.setAppState(.needToLoginAfter))
                    .observeOn(MainScheduler.asyncInstance)
                    .bind(to: reactor.action)
                    .disposed(by: disposeBag)

            } else {
                Observable.just(Reactor.Action.setAppState(.needToAddUser))
                    .observeOn(MainScheduler.asyncInstance)
                    .bind(to: reactor.action)
                    .disposed(by: disposeBag)
            }

        case .agreeDenied:
            pref.setIsUserAgreeTerms(isUserAgreeTerms: false)
            self.agreementView.isHidden = true
            Observable.just(Reactor.Action.versionCheck)
                .observeOn(MainScheduler.asyncInstance)
                .bind(to: reactor.action)
                .disposed(by: disposeBag)

        case .agreeAccepted:
            pref.setIsUserAgreeTerms(isUserAgreeTerms: true)
            self.agreementView.isHidden = true
            Observable.just(Reactor.Action.versionCheck)
                .observeOn(MainScheduler.asyncInstance)
                .bind(to: reactor.action)
                .disposed(by: disposeBag)
        case .needToAddUser:
            Observable.just(Reactor.Action.addUser)
                .observeOn(MainScheduler.asyncInstance)
                .bind(to: reactor.action)
                .disposed(by: disposeBag)
        case .needToLogin:
            Observable.just(Reactor.Action.userLogin)
                .observeOn(MainScheduler.asyncInstance)
                .bind(to: reactor.action)
                .disposed(by: disposeBag)
        case .readyToUse:
            Observable.just(Reactor.Action.moveToScreen(.mainScreen))
                .observeOn(MainScheduler.asyncInstance)
                .bind(to: reactor.action)
                .disposed(by: disposeBag)
        case .needToLoginAfter:
            Observable.just(Reactor.Action.loginAfter)
                .observeOn(MainScheduler.asyncInstance)
                .bind(to: reactor.action)
                .disposed(by: disposeBag)
        case .needToUserInfo:
            Observable.just(Reactor.Action.getUserInfo)
                .observeOn(MainScheduler.asyncInstance)
                .bind(to: reactor.action)
                .disposed(by: disposeBag)

        }
    }
    
    func updateSelectionAlert() {
        let controller = UIAlertController.init(title: "", message: Constants.String.AlertMessage.updateSelectionMessage, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "업데이트", style: .default, handler: { [weak self] _ in
            if let appstoreUrl = URL(string: Constants.String.AppStore.appstoreUrl) {
                let application = UIApplication.shared
                if application.canOpenURL(appstoreUrl) {
                    application.open(appstoreUrl, options: [:], completionHandler: nil)
                }

            }
            guard let reactor = self?.reactor else { return }

            reactor.action.onNext(.setAppState(.recentVersion))

        })
        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: { [weak self] _ in
            guard let reactor = self?.reactor else { return }

            reactor.action.onNext(.setAppState(.recentVersion))
            
        })

        controller.addAction(defaultAction)
        controller.addAction(cancel)

        present(controller, animated: true)

    }
    
    func updateRequiredAlert() {

        let controller = UIAlertController.init(title: "", message: Constants.String.AlertMessage.updateRequiredMessage, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "업데이트", style: .cancel, handler: { [weak self] _ in
            if let appstoreUrl = URL(string: Constants.String.AppStore.appstoreUrl) {
                let application = UIApplication.shared
                if application.canOpenURL(appstoreUrl) {
                    application.open(appstoreUrl, options: [:], completionHandler: nil)
                }

            }

            guard let reactor = self?.reactor else { return }

            reactor.action.onNext(.setAppState(.needToCheckAgreeTerms))
        })
        
        controller.addAction(defaultAction)

        self.present(controller, animated: true)

    }
}

// MARK: -
