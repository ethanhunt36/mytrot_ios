//
//  SplashViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

enum MyAppState {
    // 약관 동의 체크
    case needToCheckAgreeTerms
    // 최초 약관 동의 완료
    case agreeAccepted
    // 최초 약관 동의 취소
    case agreeDenied
    
    // 버전 필수 업데이트
    case updateRequired
    // 버전 선택 업데이트
    case updateSelection
    // 버전 최신
    case recentVersion
    
    // 회원 자동 추가
    case needToAddUser
    
    // 회원 로그인
    case needToLogin
    // 자동 로그인
    case needToLoginAfter
    // 회원 정보 획득
    case needToUserInfo
    // 앱 사용 준비 -> 메인
    case readyToUse
}

final class SplashViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()

    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        
        case versionCheck
        case userLogin
        case addUser
        case loginAfter
        case getUserInfo
        case setAppState(MyAppState)

    }

    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponseMyInfo(ResponseMyInfo)
        case setEmpty
        case setAppState(MyAppState)
    }

    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var responseMyInfo: ResponseMyInfo?
        var appState: MyAppState
    }

    /// 초기 상태값
    let initialState: State
    let networkService = MyTrotService()

    init() {
        self.initialState = State(
            isDismiss: false,
            appState: .needToCheckAgreeTerms
        )
    }

    func mutate(action: SplashViewReactor.Action) -> Observable<SplashViewReactor.Mutation> {
        switch action {
        case .setAppState(let state):
            return .just(.setAppState(state))
        case .loginAfter:
            return networkService.loginAfter().asObservable()
                .map {
                    res in
                    return Mutation.setAppState(.needToUserInfo)
                }

        case .versionCheck:
            return networkService.versionCheck().asObservable()
                .map {
                    res in
                    if res.isResponseStatusSuccessful() , let keywordList = res.data?.config_info?.keyword_list {
                        // keyword List 저장 !
                        SearchKeyword.shared.updateKeyword(data: keywordList)
                    }
                    if let blogUrl = res.data?.config_info?.blog_url {
                        User.shared.blog_url = blogUrl
                    }
                    if let boardPromotion = res.data?.config_info?.board_promo_info {
                        BoardPromotion.shared.updatePromotionInfo(data: boardPromotion)
                    }
                    let mustUpdate = res.data?.new_must_update ?? ""
                    print("mustUpdate : \(mustUpdate)")
                    switch mustUpdate{
                    case "Y":
                        return .setAppState(.updateRequired)
                    case "N":
                        return .setAppState(.updateSelection)
                        default : break ;
                    }
                    let preference = PreferencesService()
                    if preference.getMember_no() != "" {
                        return Mutation.setAppState(.needToLoginAfter)

                    } else {
                        return Mutation.setAppState(.needToAddUser)
                    }
                }
        case .userLogin:
            return networkService.memberLogin().asObservable()
                .map {
                    res in
                    let resCode = res.err ?? 0
                    if resCode == 1 {
                        return Mutation.setAppState(.needToAddUser)
                    } else {
                        return Mutation.setResponseMyInfo(res)
                    }
                    
                }
        case .addUser:
            return networkService.addMember().asObservable()
                .map {
                    res in
                    let resCode = res.err ?? 0
                    if resCode == 0 {
                        return Mutation.setAppState(.needToLogin)
                    } else {
                        RxBus.shared.post(event: Events.AlertShow(message: res.msg ?? Constants.Network.ErrorMessage.networkError))
                        return .setEmpty
                    }
                    
                }
        case .getUserInfo:
            return networkService.getMemberInfo().asObservable()
                .map {
                    res in
                    let resCode = res.err ?? 0
                    if resCode == 0 {
                        return Mutation.setResponseMyInfo(res)
                    } else {
                        RxBus.shared.post(event: Events.AlertShow(message: res.msg ?? Constants.Network.ErrorMessage.networkError))
                        return .setEmpty
                    }
                    
                }

        case .dismiss:
            //            self.navigate(step: <# AptnerStep #>)
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }

    func reduce(state: SplashViewReactor.State, mutation: SplashViewReactor.Mutation) -> SplashViewReactor.State {
        var state = state
        switch mutation {
        case .setResponseMyInfo(let myInfo):
            state.responseMyInfo = myInfo
            User.shared.updateMyInformation(data: myInfo.data)
            state.appState = .readyToUse
        case .setEmpty:
            return state
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setAppState(let appState):
            state.appState = appState
        }
        return state
    }
}
