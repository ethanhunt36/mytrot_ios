//
//  MoreMainViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//


import UIKit
import ReactorKit
import GoogleMobileAds
import UnityAds

extension CALayer {
    func addBorder(_ arr_edge: [UIRectEdge], color: UIColor, width: CGFloat) {
        for edge in arr_edge {
            let border = CALayer()
            switch edge {
            case UIRectEdge.top:
                border.frame = CGRect.init(x: 0, y: 0, width: frame.width, height: width)
                break
            case UIRectEdge.bottom:
                border.frame = CGRect.init(x: 0, y: frame.height - width, width: frame.width, height: width)
                break
            case UIRectEdge.left:
                border.frame = CGRect.init(x: 0, y: 0, width: width, height: frame.height)
                break
            case UIRectEdge.right:
                border.frame = CGRect.init(x: frame.width - width, y: 0, width: width, height: frame.height)
                break
            default:
                break
            }
            

            border.backgroundColor = color.cgColor;
            self.addSublayer(border)
        }
        self.cornerRadius = 5.0
        self.masksToBounds = true
//        self.shadowRadius = 20.0
//        self.shadowOpacity = 0.07
//        self.shadowOffset = CGSize(width: 1, height: 5)

        

    }
}

final class MoreMainViewController: BaseViewController, StoryboardView, HasPreferencesService, GADFullScreenContentDelegate {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    var preferencesService = PreferencesService()
    let networkService = MyTrotService()
    
    private var ad_full: GADInterstitialAd?
    private var ad_reward_full: GADRewardedInterstitialAd?
    private var ad_reward: GADRewardedAd?
    
    // UnityAds 광고 로딩 여부
    private var _isReadyUnityAds = false
    
    
    private var ad_show_type = ""
    private var ad_next_action = ""
    
    var leftPpobkiTime: Double = 0
    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var topStackView: UIView! {
        didSet {
            topStackView.layer.addBorder([.top,.left,.right], color: Asset.lightPurpley.color, width: 2.0)
        }
    }
    @IBOutlet weak var bottomStackView: UIView! {
        didSet {
            bottomStackView.layer.addBorder([.bottom,.left,.right], color: Asset.lightPurpley.color, width: 2.0)
        }
    }
    @IBOutlet weak var bottomHideStackView: UIView! {
        didSet {
            bottomHideStackView.layer.addBorder([.bottom,.left,.right], color: Asset.lightPurpley.color, width: 2.0)
        }
    }
    // 닫기 버튼이 있는 뷰
    @IBOutlet weak var hideStackView: UIView!
    // 더보기 버튼이 있는 뷰
    @IBOutlet weak var moreStackView: UIView!
    
    // 카울리 배너
    @IBOutlet weak var adCaulyView: UIView!
    
    @IBOutlet weak var ppobkiDescLabel: UILabel!
    @IBOutlet weak var ppobkiNextCountLabel: UILabel!
    @IBOutlet weak var ppobkiView: UIView!
    @IBOutlet weak var ppobkiRoundView: UIView!{
        didSet {
            ppobkiRoundView.layer.borderWidth = 1
            ppobkiRoundView.layer.borderColor = Asset.purpley.color.cgColor
            ppobkiRoundView.layer.cornerRadius = 15

        }
    }
    @IBOutlet weak var ppobkiButton: UIButton!

    @IBOutlet weak var iapRoundView: UIView!{
        didSet {
            iapRoundView.layer.borderWidth = 1
            iapRoundView.layer.borderColor = Asset.purpley.color.cgColor
            iapRoundView.layer.cornerRadius = 15

        }
    }
    @IBOutlet weak var iapButton: UIButton!


    @IBOutlet weak var promotionRoundView: UIView!{
        didSet {
            
            promotionRoundView.layer.borderWidth = 1
            promotionRoundView.layer.borderColor = Asset.darkGrey.color.cgColor
            promotionRoundView.layer.cornerRadius = 15
            promotionRoundView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var promotionButton: UIButton!
    @IBOutlet weak var promotionLabel: UILabel! {
        didSet {
            let reward_vote = BoardPromotion.shared.promotionInfo?.reward_vote ?? 30
            let reward_point = BoardPromotion.shared.promotionInfo?.reward_point ?? 20
            
            let attributedString = NSMutableAttributedString(string: "마이트롯을 홍보하고 인증샷을 등록해 주시면\n투표권\(reward_vote)장, 포인트 \(reward_point)P를 드려요", attributes: [
                .font: UIFont.systemFont(ofSize: 16.0, weight: .regular),
                .foregroundColor: Asset.brownishGrey.color,
              .kern: 0.0
            ])
            attributedString.addAttribute(.foregroundColor, value: Asset.fadedRed.color, range: NSRange(location: 27, length: "\(reward_vote)".count + 1))
            
            let nextCount = 27 + "\(reward_vote)".count + 7
            attributedString.addAttribute(.foregroundColor, value: Asset.fadedRed.color, range: NSRange(location: nextCount, length: "\(reward_point)".count + 1))

            promotionLabel.attributedText = attributedString
        }
    }


    // 카울리 전면
    var _caulyInterstitialAd:CaulyInterstitialAd? = nil
        
    // 배너
    var _caulyView: CaulyAdView? = nil
    @IBOutlet weak var freePointLabel: UILabel! {
        didSet {
//            광고보고 투표권 2장과 포인트 0.5P를 받으세요
            let point_amount = "\(User.shared.point_amount)"
            let amount_ticket_ad = User.shared.amount_ticket_ad
            let attributedString = NSMutableAttributedString(string: "광고보고 투표권 \(amount_ticket_ad)장과 포인트 \(point_amount)P를 받으세요", attributes: [
                .font: UIFont.systemFont(ofSize: 15.0, weight: .semibold),
                .foregroundColor: Asset.black.color,
              .kern: 0.0
            ])
            attributedString.addAttribute(.foregroundColor, value: Asset.fadedRed.color, range: NSRange(location: 9, length: amount_ticket_ad.count + 1))
            
            let nextCount = 9 + amount_ticket_ad.count + 7
            attributedString.addAttribute(.foregroundColor, value: Asset.fadedRed.color, range: NSRange(location: nextCount, length: point_amount.count + 1))

            freePointLabel.attributedText = attributedString


        }
    }

    @IBOutlet weak var hideButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var nickChangeButton: UIButton! {
        didSet {
            nickChangeButton.layer.cornerRadius = 5.0
            nickChangeButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var phoneAuthButton: UIButton!{
        didSet {
            phoneAuthButton.layer.cornerRadius = 5.0
            phoneAuthButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var activityCertificationButton: UIButton!{
        didSet {
            activityCertificationButton.layer.cornerRadius = 20.0
            activityCertificationButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var levelRankButton: UIButton!{
        didSet {
            levelRankButton.layer.cornerRadius = 20.0
            levelRankButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var askAddtionalVideoButton: UIButton!
    @IBOutlet weak var voteHistoryButton: UIButton!{
        didSet {
            voteHistoryButton.layer.cornerRadius = 20.0
            voteHistoryButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var pointHistoryButton: UIButton!{
        didSet {
            pointHistoryButton.layer.cornerRadius = 20.0
            pointHistoryButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var noticeButton: UIButton!{
        didSet {
            noticeButton.layer.cornerRadius = 20.0
            noticeButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var faqButton: UIButton! {
        didSet {
            faqButton.layer.cornerRadius = 20.0
            faqButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var agreementButton: UIButton!{
        didSet {
            if preferencesService.getIsUserAgreeTerms() == false {
                agreementButton.setTitle("약관동의하기", for: .normal)
            } else {
                agreementButton.setTitle("이용약관", for: .normal)

            }
            agreementButton.layer.cornerRadius = 20.0
            agreementButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var movieStorageButton: UIButton!{
        didSet {
            movieStorageButton.layer.cornerRadius = 20.0
            movieStorageButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var versionLabel: UILabel!{
        didSet {
            guard let dictionary = Bundle.main.infoDictionary,
                let version = dictionary["CFBundleShortVersionString"] as? String else { return }
            let attrString = "버전 정보 : \(version)"
            let attributedString = NSMutableAttributedString(string: attrString, attributes: [
                .font: UIFont.systemFont(ofSize: 19.0, weight: .regular),
                .foregroundColor: Asset.black.color,
              .kern: 0.0
            ])
            attributedString.addAttribute(.foregroundColor, value: Asset.tangerine.color, range: NSRange(location: 8, length: version.count))
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 19.0, weight: .regular), range: NSRange(location: 8, length: version.count))

            versionLabel.attributedText = attributedString
        }
    }

    @IBOutlet weak var versionButton: UIButton!{
        didSet {
            versionButton.layer.cornerRadius = 20.0
            versionButton.layer.masksToBounds = true

        }
    }

    @IBOutlet weak var pointRankButton: UIButton!{
        didSet {
            pointRankButton.layer.cornerRadius = 20.0
            pointRankButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var voteRankButton: UIButton!{
        didSet {
            voteRankButton.layer.cornerRadius = 20.0
            voteRankButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var moreAppstoreButton: UIButton!
    
    @IBOutlet weak var nickLabel: UILabel! {
        didSet {
            guard let nick = User.shared.myInfo?.nick else {
                return
            }
            guard let lv = User.shared.myInfo?.lv else {
                return
            }
            nickLabel.text = "\(nick) Lv.\(lv)"
        }
    }
    @IBOutlet weak var phoneLabel: UILabel! {
        didSet {
            if let phone = User.shared.myInfo?.phone, phone != "" {
                phoneLabel.text = "핸드폰 인증 되어있습니다."
                phoneAuthButton.isHidden = true

            } else {
                phoneLabel.text = "미인증 상태 입니다."
                phoneAuthButton.isHidden = false

            }
            
        }
    }
    @IBOutlet weak var favSingerButton: UIButton! {
        didSet {
            favSingerButton.setTitle("선택", for: .normal)
            favSingerButton.layer.cornerRadius = 5.0
            favSingerButton.layer.masksToBounds = true

            if let artist_name = User.shared.myInfo?.artist_name {
                if artist_name != "" {
                    favSingerButton.setTitle("초기화", for: .normal)
                }
            }
        }
    }

    @IBOutlet weak var favArtistLabel: UILabel! {
        didSet {
            favArtistLabel.text = "최애 가수를 선택해주세요."

            if let artist_name = User.shared.myInfo?.artist_name {
                if artist_name != "" {
                    if let artist_pan_count = User.shared.myInfo?.artist_pan_count {
                        favArtistLabel.text = "\(artist_name),  팬: \(artist_pan_count.commaString)명"
                    }
                }
            }
        }
    }
    @IBOutlet weak var voteLabel: UILabel! {
        didSet {
            let voteCount = User.shared.myInfo?.vote_cnt.commaString ?? "0"
            voteLabel.text = "\(voteCount)장"
        }
    }
    @IBOutlet weak var pointLabel: UILabel! {
        didSet {
            let pointCount = Utils.roundByPointIntValue(input: User.shared.myInfo?.point ?? 0, someBelow: 1).commaString
            pointLabel.text = "\(pointCount) P"

        }
    }
    @IBOutlet weak var levelLabel: UILabel! {
        didSet {
            let lv = User.shared.myInfo?.lv
            levelLabel.text = "Lv.\(lv ?? 0)"

        }
    }
    @IBOutlet weak var expLabel: UILabel! {
        didSet {
            let lv_pnt = User.shared.myInfo?.lv_pnt
            let exp_end = User.shared.myInfo?.exp_end
            expLabel.text = "(\(lv_pnt ?? 0) / \(exp_end ?? 0) )"

        }
    }
    @IBOutlet weak var vodPlayLabel: UILabel! {
        didSet {

        }
    }
    @IBOutlet weak var vodPlayTimeLabel: UILabel! {
        didSet {

        }
    }
    @IBOutlet weak var savedPointLabel: UILabel!
    @IBOutlet weak var donatePointLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var cheerUploadCount: UILabel!
    @IBOutlet weak var likeGiveLabel: UILabel!
    @IBOutlet weak var likeGetLabel: UILabel!

    @IBOutlet weak var adVotePointButton: UIButton!
    var timer = Timer()

    // 광고보고 무료 충전
    @IBOutlet weak var adFreePointGetView: UIView! {
        didSet {
            adFreePointGetView.layer.borderWidth = 1
            adFreePointGetView.layer.borderColor = Asset.purpley.color.cgColor
            adFreePointGetView.layer.cornerRadius = 15
        }
    }

    @IBOutlet weak var unRegisterButton: UIButton!
    @IBOutlet weak var unRegisterLabel: UILabel! {
        didSet {
            unRegisterLabel.attributedText = NSAttributedString(string: unRegisterLabel.text!, attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])

        }
    }
    @IBOutlet weak var mytrotBlogButton: UIButton!
    @IBOutlet weak var recommendCodeView: UIView!

    @IBOutlet weak var recommendCodeButton: UIButton! {
        didSet {
            recommendCodeButton.layer.cornerRadius = 20.0
            recommendCodeButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var recommendCodeLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
        
    }
    
    
    
    
    
    
    
    func complte_ad(ad_type: String){
        guard let reactor = reactor else { return }
        
        print("complte_ad, ad_next_action : \(ad_next_action)")
        
        if(self.ad_next_action != Constants.AdNextAction.for_default) {
            return
        }
        
        
        reactor.action.onNext(.votePutTicketAdPointApi(ad_type))
        
        if(ad_type == Constants.AdType.g_full_screen) {
            initAd_google_full()
        } else if(ad_type == Constants.AdType.g_reward_full) {
            initAd_google_reward_full()
        } else if(ad_type == Constants.AdType.g_reward) {
            initAd_google_reward()
        } else if(ad_type == Constants.AdType.cauly_full) {
            initAd_Cauly()
        } else if(ad_type == Constants.AdType.UnityAds) {
            initAd_UnityAds()
        } else if(ad_type == Constants.AdType.VungleFull || ad_type == Constants.AdType.VungleReward) {
            //initAd_UnityAds()
        }
    }
    
    func bind(reactor: MoreMainViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let reactor = reactor else { return }

        if(ad_show_type == "") {
            reactor.action.onNext(.getMoreMainApi)
        }
        
        if(ad_next_action == Constants.AdNextAction.for_move_ppobkki) {
            reactor.action.onNext(.moveToStepScreen(.ppobKiPage))
            ad_next_action = ""
            return
        }

        
        reactor.action.onNext(.getPpobkiInfo)

    }
        
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        print("Ad did fail to present full screen content. \(ad), error : \(error)")
        self.ad_full = nil
    }

    /// Tells the delegate that the ad presented full screen content.
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Ad did present full screen content. \(ad)")
        
        
    }

    /// Tells the delegate that the ad dismissed full screen content.
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Ad did dismiss full screen content. \(ad)")
        
        self.ad_full = nil
        self.complte_ad(ad_type: self.ad_show_type)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension MoreMainViewController {
    
    func showAd(adNextAction : String){
        print("showAd, ad_next_action : \(ad_next_action)")

        self.ad_next_action = adNextAction
        
        print("adVotePointButton, _isReadyUnityAds : ", self._isReadyUnityAds)
        
        print("adVotePointButton, ad_reward_full : ", (self.ad_reward_full == nil ? "nil" : "ok"))
        print("adVotePointButton, ad_reward : ", (self.ad_reward == nil ? "nil" : "ok"))
        
        print("adVotePointButton, ad_full : ", (self.ad_full == nil ? "nil" : "ok"))
        
        print("adVotePointButton, _caulyInterstitialAd : ", (self._caulyInterstitialAd == nil ? "nil" : "ok"))
        print("adVotePointButton, UnityAds.isReady(video): ", (UnityAds.isReady("video") != false ? "nil" : "ok"))
        print("adVotePointButton, UnityAds.isReady(rewardedVideo): ", (UnityAds.isReady("rewardedVideo") != false ? "nil" : "ok"))
        
        if self.ad_reward_full != nil && self.ad_show_type != Constants.AdType.g_reward_full {
            print("try g_reward_full")
            self.ad_show_type = Constants.AdType.g_reward_full
            self.ad_reward_full?.present(fromRootViewController: self, userDidEarnRewardHandler: {
                self.ad_reward_full = nil
                print("ad_reward_full reward!!!")
                self.complte_ad(ad_type: self.ad_show_type)
            })
        }
        else if self.ad_reward != nil && self.ad_show_type != Constants.AdType.g_reward {
            print("try g_reward")
            self.ad_show_type = Constants.AdType.g_reward
            self.ad_reward?.present(fromRootViewController: self, userDidEarnRewardHandler: {
                self.ad_reward_full = nil
                print("ad_reward reward!!!")
                self.complte_ad(ad_type: self.ad_show_type)
            })
        }
        else if self.ad_full != nil && self.ad_show_type != Constants.AdType.g_full_screen {
            print("try g_full_screen")
            self.ad_show_type = Constants.AdType.g_full_screen
            self.ad_full?.present(fromRootViewController: self)
        }
        else if self._caulyInterstitialAd != nil && self.ad_show_type != Constants.AdType.cauly_full {
            print("try cauly")
            self.ad_show_type = Constants.AdType.cauly_full
            self._caulyInterstitialAd?.show()
        }
        else if UnityAds.isReady("video") && self.ad_show_type != Constants.AdType.UnityAds {
            print("try UnityAds")
            self.ad_show_type = Constants.AdType.UnityAds
            UnityAds.show(self, placementId: "video", showDelegate: nil)
        } else if UnityAds.isReady("rewardedVideo") && self.ad_show_type != Constants.AdType.UnityAds {
            print("try UnityAds")
            self.ad_show_type = Constants.AdType.UnityAds
            UnityAds.show(self, placementId: "rewardedVideo", showDelegate: nil)
        }
        else {
            print("Ad wasn't ready")
            self.initAd()
            Utils.AlertShow(msg: "광고 수신 중입니다.\n잠시 쉬었다가 다시 클릭해주세요.")
        }

    }
    
    
    func bindAction(_ reactor: MoreMainViewReactor) {
        iapButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(.iapPage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        moreButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.showHide}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        hideButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.showMore}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        nickChangeButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.nickChangePage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        phoneAuthButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.phoneAuthPage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        levelRankButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.levelRankListPage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        favSingerButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ _ in
                if let artist_name = User.shared.myInfo?.artist_name {
                    if artist_name != "" {
                        return Reactor.Action.removeFavSinger
                    }
                }
                return Reactor.Action.moveToStepScreen(CommonStep.favSingerSelectPage(type: .choiceFavSinger))
            }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        activityCertificationButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.favSingerSelectPage(type: .activityCertification))}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        askAddtionalVideoButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.openAlert()
            }).disposed(by: disposeBag)
        voteRankButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.voteRankPage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        voteHistoryButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.voteHistoryPage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        pointHistoryButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.pointHistoryPage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        noticeButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.noticeListPage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        faqButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.faqPage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        agreementButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.termsPage(type: .agreement, url: "http://www.mytrot.co.kr/agree/agree_use.html"))}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        movieStorageButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.videoStoragePage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        pointRankButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.pointRankPage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        ppobkiButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            //.map{ Reactor.Action.moveToStepScreen(CommonStep.ppobKiPage)}
            .map({ [weak self] _ in
            
                // TODO 남은 시간 체크해서 아직 시간이 남은 상태라면 alert 리턴 처리
                // :: 잠시 후에 뽑기를 선택하실 수 있어요.\n\n잠시 후에 이용해주세요.
                if self?.leftPpobkiTime ?? 0 > 0 {
                    Utils.AlertShow(msg: "잠시 후에 뽑기를 선택하실 수 있어요.\n\n잠시 후에 이용해주세요.")
                    return Reactor.Action.empty
                } else {
                    self?.showAd(adNextAction: Constants.AdNextAction.for_move_ppobkki)
                    
                    return Reactor.Action.viewAd

                }
                
            })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        moreAppstoreButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map({ _ in
                UIApplication.shared.open(URL(string: "")!, completionHandler: { success in
                    print("scheme opened: \(success)")
                })
                return Reactor.Action.getMoreMainApi
            })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        adVotePointButton.rx.tap
              .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
              .map({ _ in
                  
                self.showAd(adNextAction: Constants.AdNextAction.for_default)
                
                return Reactor.Action.viewAd
              })
              .bind(to: reactor.action)
              .disposed(by: disposeBag)
        
        promotionButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.promotionListPage)}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        unRegisterButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.unregisterAlertPrev1()
            }).disposed(by: disposeBag)

        versionButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.openAppstore()
            }).disposed(by: disposeBag)
        
        
        mytrotBlogButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map{ Reactor.Action.moveToStepScreen(CommonStep.noticeWebDetailPage(type: .blog, url: User.shared.blog_url))}
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        recommendCodeButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in

                self?.recommendAlert()
            }).disposed(by: disposeBag)

    }
}

// MARK: bindState For Reactor
private extension MoreMainViewController {
    func recommendAlert() {
        let pointMy = reactor?.currentState.responseRCMD?.data?.point ?? 0
        let pointFriend = reactor?.currentState.responseRCMD?.data?.point_target ?? 0
        let rcmdCode = reactor?.currentState.responseRCMD?.data?.rcmd_code ?? ""
        let msg = "추천인 코드와 간단한 소개 문구가 클립보드에 복사되었습니다.\n\nSMS, 카카오톡 등을 통해서 지인에게 전달해주세요.\n\n초대받은 친구가 '마이트롯'에 가입하면 두분 모두에게 포인트를 드립니다.\n\n(친구: \(pointFriend)P, 본인 : \(pointMy)P)"
        let controller = UIAlertController.init(title: "", message: msg, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "확인", style: .default, handler: { [weak self]_ in
            let shareText: String = "트로트가 매일 추가되는 마이트롯!\n\n평생 완전 무료!\n\n광고만 봐도 투표권, 포인트 적립!\n\n가장 좋아하는 가수에게 투표하고, 기부도 참여해요!"
                                    + "\n\n" +
                                    "앱 설치 후 추천인코드 입력시 \(pointFriend)P 지급!!\n" +
                                    "\n다른 사람을 초대하면 추가 \(pointMy)P 지급!!\n\n단 추천인 코드를 반드시 입력 바랍니다.\n\n추천인 코드 : \(rcmdCode)\n\n다운로드 : http://www.mytrot.co.kr/ad.php?from=mytrot_ios";
            UIPasteboard.general.string = shareText
            var shareObject = [Any]()
            shareObject.append(shareText)

            let activityController = UIActivityViewController(activityItems: shareObject, applicationActivities: nil)
            activityController.popoverPresentationController?.sourceView = self?.view
            self?.present(activityController, animated: true)
        })

        controller.addAction(defaultAction)

        self.present(controller, animated: true)

    }
    func bindState(_ reactor: MoreMainViewReactor) {
        
        
        reactor.state.map { $0.can_time_cheat }
        .filterNil()
        .filter{ $0 == "Y" }
        .subscribe(onNext: { [weak self] _ in
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.cheatTimeApi)
        })
        .disposed(by: disposeBag)
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.isMoreShowing }
            .distinctUntilChanged()
            .bind(to: hideStackView.rx.isHidden)
            .disposed(by: disposeBag)
        reactor.state.map { $0.isHideShowing }
            .distinctUntilChanged()
            .bind(to: moreStackView.rx.isHidden)
            .disposed(by: disposeBag)
        reactor.state.map { $0.ppobkiInfo }
            .filterNil()
            .map { $0.data }
            .map { $0?.list }
            .filterNil()
            .map { ($0.count, Utils.getPpobKiCount(list: $0))}
            .map { "총 \($0.0)장, 지금 \($0.1)장 남았어요"}
            .bind(to: ppobkiDescLabel.rx.text)
            .disposed(by: disposeBag)
        reactor.state.map { $0.ppobkiInfo }
            .filterNil()
            .map { $0.data }
            .map { $0?.list }
            .filterNil()
            .map { Utils.getPpobKiCount(list: $0) == 0}
            .bind(to: ppobkiView.rx.isHidden)
            .disposed(by: disposeBag)
        reactor.state.map { $0.nextPpobkiString }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] nextPpobkiString in
                self?.bindPpobkiLabelAttribute(string: nextPpobkiString)
            }).disposed(by: disposeBag)
        reactor.state.map { $0.can_apply_dttm}
            .filter{ $0 == "" }
            .subscribe(onNext: { [weak self] _ in
                self?.leftPpobkiTime = 0
            }).disposed(by: disposeBag)

        reactor.state.map { $0.can_apply_dttm}
            .filter{ $0 != "" }
            .map { Utils.getLeftPpobkiTime(compareTime: $0)}
            .filter{ $0 > 0 }
            .subscribe(onNext: { [weak self] leftTime in
                guard let strongPpobkiTime = self?.leftPpobkiTime else { return }
                if strongPpobkiTime <= 0 {
                    self?.leftPpobkiTime = leftTime
                }
                
            }).disposed(by: disposeBag)

          
        reactor.state.map { $0.unregister}
            .distinctUntilChanged()
            .filter{ $0 == true }
            .subscribe(onNext: { [weak self] _ in
                self?.unregistAlert()
            }).disposed(by: disposeBag)

        // leftPpobkiTime
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(repeatPpobkiCount), userInfo: nil, repeats: true)

        reactor.action.onNext(.getRCMDCode)
        
        reactor.state.map { $0.responseRCMD }
        .filterNil()
        .distinctUntilChanged()
        .subscribe(onNext: { [weak self] response in
            guard let recommendCode = response.data?.rcmd_code else {
                self?.recommendCodeView.isHidden = true
                return
                
            }
            guard let recommendCount = response.data?.rcmd_cnt else {
                self?.recommendCodeView.isHidden = true
                return
                
            }
            self?.recommendCodeView.isHidden = false
            let attributedString = NSMutableAttributedString(string: "초대하기 (추천인 코드 : \(recommendCode))\n내 추천인 코드를 입력한 회원 수 : \(recommendCount)명", attributes: [
                .font: UIFont.systemFont(ofSize: 16.0, weight: .regular),
                .foregroundColor: Asset.brownishGrey.color,
              .kern: 0.0
            ])
            attributedString.addAttribute(.foregroundColor, value: Asset.fadedRed.color, range: NSRange(location: 15, length: "\(recommendCode)".count))
            
            let nextCount = 15 + "\(recommendCode)".count + 23
            attributedString.addAttribute(.foregroundColor, value: Asset.brownishGrey.color, range: NSRange(location: nextCount, length: "\(recommendCount)".count + 1))

            self?.recommendCodeLabel.attributedText = attributedString

        }).disposed(by: disposeBag)
    }
}

// MARK: -
// MARK: private initialize
private extension MoreMainViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initAd()
    }
    
    func initAd(){
        self.ad_show_type = ""
        initAd_google_full()
        initAd_google_reward_full()
        initAd_google_reward()
        initAd_UnityAds()
        initAd_Cauly()
    }
    
    func initAd_Cauly(){
        self._caulyView=CaulyAdView.init()
        self._caulyView?.delegate = self                       //  delegate 설정

        //let frame = CGRect.init(x: 0, y: strongSelf.view.bounds.height - (85 + (strongSelf.tabBarController?.tabBar.frame.size.height ?? 40)), width: strongSelf.view.frame.width, height: 100)
        //self.caulyView?.bounds = frame
        
        self.adCaulyView.addSubview(self._caulyView!)
        self._caulyView?.startBannerAdRequest()                //배너광고요청
         
        
        self._caulyInterstitialAd = CaulyInterstitialAd.init()
        self._caulyInterstitialAd?.delegate = self;    //  전면 delegate 설정
        self._caulyInterstitialAd?.startRequest();     //  전면광고 요청
    }
    
    func initAd_UnityAds(){
        UnityAds.initialize(Constants.Ad.unity_game_id)
        UnityAds.add(self)
    }
    
    func initAd_google_full(){
        
        ad_full = nil
        let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID: Constants.Ad.google_interstitial,
                               request: request,
                               completionHandler: { [self] ad, error in
                                if let error = error {
                                  print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                                  return
                                }
                                ad_full = ad
                                ad_full?.fullScreenContentDelegate = self
                                print("loaded, ad_full")
                              }
        )
    }
    
    func initAd_google_reward_full(){
        ad_reward_full = nil
        let request = GADRequest()
        GADRewardedInterstitialAd.load(withAdUnitID: Constants.Ad.google_reward_full,
                                       request: request,
                                       completionHandler: { [self] ad, error in
                                        if let error = error {
                                          print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                                          return
                                        }
                                        ad_reward_full = ad
                                        //ad_reward_full?.fullScreenContentDelegate = self
                                        print("loaded, ad_reward_full")
                                       })
    }
    
    func initAd_google_reward(){
        ad_reward = nil
        let request = GADRequest()
        GADRewardedAd.load(withAdUnitID: Constants.Ad.google_reward,
                           request: request,
                           completionHandler: {[self] ad, error in
                            if let error = error {
                              print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                              return
                            }
                            ad_reward = ad
                            print("loaded, ad_reward")
                           })
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.RefreshAdInfo.self, sticky: true).subscribe { [weak self] _ in
            let point_amount = "\(User.shared.point_amount)"
            let amount_ticket_ad = User.shared.amount_ticket_ad
            let attributedString = NSMutableAttributedString(string: "광고보고 투표권 \(amount_ticket_ad)장과 포인트 \(point_amount)P를 받으세요", attributes: [
                .font: UIFont.systemFont(ofSize: 15.0, weight: .semibold),
                .foregroundColor: Asset.black.color,
              .kern: 0.0
            ])
            attributedString.addAttribute(.foregroundColor, value: Asset.fadedRed.color, range: NSRange(location: 9, length: amount_ticket_ad.count + 1))
            
            let nextCount = 9 + amount_ticket_ad.count + 7
            attributedString.addAttribute(.foregroundColor, value: Asset.fadedRed.color, range: NSRange(location: nextCount, length: point_amount.count + 1))

            self?.freePointLabel.attributedText = attributedString

        }.disposed(by: disposeBag)

        
        bus.asObservable(event: Events.MyInfo.self, sticky: true).subscribe { [weak self] _ in
            if self?.preferencesService.getIsUserAgreeTerms() == false {
                self?.agreementButton.setTitle("약관동의하기", for: .normal)
            } else {
                self?.agreementButton.setTitle("이용약관", for: .normal)

            }
            self?.favSingerButton.setTitle("선택", for: .normal)

            if let artist_name = User.shared.myInfo?.artist_name {
                if artist_name != "" {
                    self?.favSingerButton.setTitle("초기화", for: .normal)
                } 
            }

            guard let nick = User.shared.myInfo?.nick else {
                return
            }
            guard let lv = User.shared.myInfo?.lv else {
                return
            }
            self?.nickLabel.text = "\(nick) Lv.\(lv)"

            if let phone = User.shared.myInfo?.phone, phone != "" {
                self?.phoneLabel.text = phone
                self?.phoneAuthButton.isHidden = true

            } else {
                self?.phoneLabel.text = "미인증 상태 입니다."
                self?.phoneAuthButton.isHidden = false

            }

            self?.favArtistLabel.text = "최애 가수를 선택해주세요."
            if let artist_name = User.shared.myInfo?.artist_name {
                if artist_name != "" {
                    if let artist_pan_count = User.shared.myInfo?.artist_pan_count {
                        self?.favArtistLabel.text = "\(artist_name),  팬: \(artist_pan_count.commaString)명"
                    }
                }
            }
            let voteCount = User.shared.myInfo?.vote_cnt ?? 0
            self?.voteLabel.text = "\(voteCount.commaString)장"
            let pointCount = Utils.roundByPointIntValue(input: User.shared.myInfo?.point ?? 0, someBelow: 1).commaString
            self?.pointLabel.text = "\(pointCount) P"

            self?.vodPlayLabel.text = "\(User.shared.myInfo?.vod_cnt ?? 0) 회"
            let vodTime = User.shared.myInfo?.vod_time ?? 0 == 0 ? "00시간00초" : Utils.converSecToTimeString(time: User.shared.myInfo?.vod_time ?? 0)
            self?.vodPlayTimeLabel.text = vodTime
            
            let pointGet: Double = User.shared.myInfo?.point_get ?? 0
            self?.savedPointLabel.text = "\("\(Utils.roundByPointIntValue(input: pointGet, someBelow: 2))".insertComma) P"
            self?.donatePointLabel.text = "\(User.shared.myInfo?.point_use?.commaString ?? "0") P"
            self?.voteCountLabel.text = "\(User.shared.myInfo?.voted_cnt?.insertComma ?? "0") 장"
            self?.cheerUploadCount.text = "\(User.shared.myInfo?.board_write_cnt ?? 0) 회"
            self?.likeGiveLabel.text = "\(User.shared.myInfo?.put_like_cnt ?? 0) 회"
            self?.likeGetLabel.text = "\(User.shared.myInfo?.get_like_cnt ?? "0") 회"


            self?.levelLabel.text = "Lv.\(lv)"
            let lv_pnt = User.shared.myInfo?.lv_pnt
            let exp_end = User.shared.myInfo?.exp_end
            self?.expLabel.text = "(\(lv_pnt ?? 0) / \(exp_end ?? 0) )"

        }.disposed(by: disposeBag)

    }
}


// MARK: - Reactor Action
private extension MoreMainViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
    @objc func repeatPpobkiCount() {
        guard let reactor = reactor else { return }
        // 남은 시간 표현되면,
        if leftPpobkiTime <= 0 {
            if reactor.currentState.nextPpobkiString.hasPrefix("남") {
                reactor.action.onNext(.getPpobkiInfo)
            }
        } else {
            leftPpobkiTime = Utils.getLeftPpobkiTime(compareTime: reactor.currentState.can_apply_dttm)
            let bindString = "남은시간 : \(Utils.calculateLeftPpobkiTime(time: leftPpobkiTime))"
            bindPpobkiLabelAttribute(string: bindString)
        }
        
    }
    func bindPpobkiLabelAttribute(string: String) {
        if string.hasPrefix("남") {
            let attributedString = NSMutableAttributedString(string: string, attributes: [
                .font: UIFont.systemFont(ofSize: 14.0, weight: .medium),
                .foregroundColor: Asset.fadedRed.color,
              .kern: 0.0
            ])
            attributedString.addAttribute(.foregroundColor, value: Asset.brownishGrey.color, range: NSRange(location: 0, length: 6))
            
            ppobkiNextCountLabel.attributedText = attributedString

        } else {
            let attributedString = NSMutableAttributedString(string: string, attributes: [
                .font: UIFont.systemFont(ofSize: 14.0, weight: .medium),
                .foregroundColor: Asset.fadedRed.color,
              .kern: 0.0
            ])
            ppobkiNextCountLabel.attributedText = attributedString

        }

    }
    func openAlert() {
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        let addtionalPage =  UIAlertAction(title: "영상 추가 요청", style: .default) { [weak self] (action) in
            print("영상 추가 요청")
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.moveToStepScreen(CommonStep.askAdditionalVideoPage))
        }
        let reportPage =  UIAlertAction(title: "문의하기", style: .default) { [weak self] (action) in
            print("문의하기")
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.moveToStepScreen(CommonStep.askQuestionPage))
        }


        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        alert.addAction(addtionalPage)
        alert.addAction(reportPage)
        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)

    }

    func unregisterAlertPrev1() {
        let controller = UIAlertController.init(title: "", message: "마이트롯을 사용하시면서 불편하셨던 내용은 문의하기를 통해 글을 남겨주세요.\n불편해소를 위해 운영팀이 최대한 지원하겠습니다.\n\n탈퇴하시겠습니까?", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "확인", style: .destructive, handler: { [weak self]_ in
            self?.unregisterAlertPrev2()
        })
        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: { _ in
        })

        controller.addAction(defaultAction)
        controller.addAction(cancel)

        self.present(controller, animated: true)

    }
    func unregisterAlertPrev2() {
        let controller = UIAlertController.init(title: "", message: "탈퇴를 하시면 모든 데이터가 삭제됩니다.\n\n투표권/포인트를 모두 사용하셨는지 확인해주세요.\n\n정말 탈퇴하시겠습니까?", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "탈퇴하기", style: .destructive, handler: { [weak self] _ in
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.unregister)
        })
        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: { _ in
        })

        controller.addAction(defaultAction)
        controller.addAction(cancel)

        self.present(controller, animated: true)

    }
    func unregistAlert() {
        let controller = UIAlertController.init(title: "", message: "탈퇴 요청이 처리되었습니다.", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "확인", style: .cancel, handler: { [weak self] _ in
            self?.preferencesService.removeAllSavedData()

            delay(delay: 2) {
                exit(0)
            }
        })
        
        controller.addAction(defaultAction)
        self.present(controller, animated: true)

    }
    
    func openAppstore() {
        if let appstoreUrl = URL(string: Constants.String.AppStore.appstoreUrl) {
            let application = UIApplication.shared
            if application.canOpenURL(appstoreUrl) {
                application.open(appstoreUrl, options: [:], completionHandler: nil)
            }

        }

    }
}

// MARK: -




// 유니티 광고 Delegate
extension MoreMainViewController: UnityAdsDelegate {
    func unityAdsReady(_ placementId: String) {
        print("UnityAdsDelegate Ready placementId : \(placementId)")
        _isReadyUnityAds = true;
        print("UnityAdsDelegate Ready placementId : \(placementId), _isReadyUnityAds : ", _isReadyUnityAds)
    }

    func unityAdsDidStart(_ placementId: String) {
        print("UnityAdsDelegate DidStart placementId : \(placementId)")

    }
    func unityAdsDidError(_ error: UnityAdsError, withMessage message: String) {
        print("UnityAdsDelegate DidError withMessage : \(message)")
        _isReadyUnityAds = false

    }
    func unityAdsDidFinish(_ placementId: String, with state: UnityAdsFinishState) {
        print("UnityAdsDelegate DidFinish placementId : \(placementId)")
        _isReadyUnityAds = false
        
        print("unityAdsDidFinish reward!!!")
        self.complte_ad(ad_type: self.ad_show_type)
    }
}

// Cauly 광고 Delegate (전면)
extension MoreMainViewController: CaulyInterstitialAdDelegate {
    // 광고 정보 수신 성공
    func didReceive(_ interstitialAd: CaulyInterstitialAd!, isChargeableAd: Bool) {
        NSLog("Cauly Interstitial Recevie intersitial");
        //_caulyIinterstitialAd?.show(withParentViewController: self)
    }

    func didFail(toReceive interstitialAd: CaulyInterstitialAd!, errorCode: Int32, errorMsg: String!) {
        print("Cauly Interstitial Recevie fail intersitial errorCode:\(errorCode) errorMsg:\(errorMsg!)");
        _caulyInterstitialAd = nil
    }
    //Interstitial 형태의 광고가 보여지기 직전
    func willShow(_ interstitialAd: CaulyInterstitialAd!) {
        print("Cauly Interstitial willShow")
    }
    // Interstitial 형태의 광고가 닫혔을 때
    func didClose(_ interstitialAd: CaulyInterstitialAd!) {
        print("Cauly Interstitial didClose")
        _caulyInterstitialAd=nil
    }

}

// Cauly 광고 Delegate (배너)
extension MoreMainViewController: CaulyAdViewDelegate {
    //광고 정보 수신 성공
    func didReceiveAd(_ adView: CaulyAdView!, isChargeableAd: Bool) {
        print("Cauly Banner Loaded didReceiveAd callback")
        adView?.show(withParentViewController: self, target: self.view)
        
    }
    //광고 정보 수신 실패
    func didFail(toReceiveAd adView: CaulyAdView!, errorCode: Int32, errorMsg: String!) {
         print("Cauly Banner didFailToReceiveAd: \(errorCode)(\(errorMsg!))");
    }
    // 랜딩 화면 표시
    func willShowLanding(_ adView: CaulyAdView!) {
        print("Cauly Banner willShowLanding")
    }
    // 랜딩 화면이 닫혔을 때
    func didCloseLanding(_ adView: CaulyAdView!) {
        print("Cauly Banner didCloseLanding")
    }

}
