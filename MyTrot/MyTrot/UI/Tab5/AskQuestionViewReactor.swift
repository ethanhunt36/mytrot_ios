//
//  AskQuestionViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class AskQuestionViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        
        case setImage1Data(Data)
        case setImage2Data(Data)
        case setImage3Data(Data)

        case uploadReport(String)
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setImage1Data(Data)
        case setImage2Data(Data)
        case setImage3Data(Data)
        case setResponse(CommonResponse)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var image1Data: Data?
        var image2Data: Data?
        var image3Data: Data?
        var alertBackMessage: String?

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false
        )
    }
    
    func mutate(action: AskQuestionViewReactor.Action) -> Observable<AskQuestionViewReactor.Mutation> {
        switch action {
        case .uploadReport(let cont):
            if cont.count == 0 {
                Utils.AlertShow(msg: "문의내용을 입력해 주세요.")
                return .empty()
            }

            var sendDatas: [Data] = []
            if let data1 = currentState.image1Data {
                sendDatas.append(data1)
            }
            if let data2 = currentState.image2Data {
                sendDatas.append(data2)
            }
            if let data3 = currentState.image3Data {
                sendDatas.append(data3)
            }
            return networkService.uploadQuestion(cont: cont, pics: sendDatas.count == 0 ? nil : sendDatas ).asObservable().map { Mutation.setResponse($0)}

        case .setImage1Data(let data):
            return .just(.setImage1Data(data))
        case .setImage2Data(let data):
            return .just(.setImage2Data(data))
        case .setImage3Data(let data):
            return .just(.setImage3Data(data))

        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: AskQuestionViewReactor.State, mutation: AskQuestionViewReactor.Mutation) -> AskQuestionViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setImage1Data(let data):
            state.image1Data = data
        case .setImage2Data(let data):
            state.image2Data = data
        case .setImage3Data(let data):
            state.image3Data = data
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.alertBackMessage = response.msg?.replaceSlashString ?? "문의 접수가 완료되었습니다.\n최대한 빨리 확인/처리해서 회신 드리겠습니다."
                
            } else {
                RxBus.shared.post(event: Events.AlertShow(message: response.msg ?? ""))
            }

        }
        return state
    }
}
