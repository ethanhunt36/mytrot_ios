//
//  MoreHistoryCell.swift
//  MyTrot
//
//  Created by hclim on 2021/07/09.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

class VoteAccHistoryCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {

    }
    
    var item: VoteHistoryDataListModel? {
        didSet {
            titleLabel.text = item?.subject ?? ""
            dateLabel.text = item?.reg_dttm ?? ""
            contLabel.text = "\(item?.amount ?? 0) 장"
        }
    }

    var pointItem: PointHistoryDataListModel? {
        didSet {
            titleLabel.text = pointItem?.subject ?? ""
            dateLabel.text = pointItem?.reg_dttm ?? ""
            contLabel.text = "\(pointItem?.point_amount ?? 0) P"
        }
    }

}
