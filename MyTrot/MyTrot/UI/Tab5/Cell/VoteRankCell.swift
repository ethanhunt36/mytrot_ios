//
//  VoteRankCell.swift
//  MyTrot
//
//  Created by hclim on 2021/07/12.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

class VoteRankCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contLabel: UILabel!
    @IBOutlet weak var cntNumber: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {

    }
    var ranking: Int? {
        didSet {
            cntNumber.text = "\(ranking ?? 0)"
        }
    }
    var item: VoteRankDataListModel? {
        didSet {
            titleLabel.text = item?.nick ?? ""
            let voteSum: String = item?.vote_sum.commaString ?? ""
            contLabel.text = "\(voteSum) 장"
        }
    }

    var pointItem: PointRankDataListModel? {
        didSet {
            titleLabel.text = pointItem?.nick ?? ""
            let pointSum: String = pointItem?.point_sum.commaString ?? ""

            contLabel.text = "\(pointSum) P"
        }
    }

}
