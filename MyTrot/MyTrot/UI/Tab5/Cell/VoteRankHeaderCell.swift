//
//  VoteRankHeaderCell.swift
//  MyTrot
//
//  Created by hclim on 2021/07/12.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
protocol VoteRankHeaderCellDelegate: AnyObject {
    func sortList(type: VoteRankTermListType)
    func pointSortList(type: PointRankTermListType)
}
class VoteRankHeaderCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()
    weak var delegate: VoteRankHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    
    @IBOutlet weak var myDataLabel: UILabel!
    @IBOutlet weak var updateDateLabel: UILabel!
    @IBOutlet weak var weekButton: UIButton! {
        didSet {
            weekButton.customCornerRadius = 10.0

            weekButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            weekButton.setBackgroundColor(UIColor.clear, for: .normal)
            weekButton.setTitleColor(Asset.tangerine.color, for: .normal)
            weekButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var monthButton: UIButton! {
        didSet {
            monthButton.customCornerRadius = 10.0

            monthButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            monthButton.setBackgroundColor(UIColor.clear, for: .normal)
            monthButton.setTitleColor(Asset.tangerine.color, for: .normal)
            monthButton.setTitleColor(Asset.tangerine.color, for: .selected)

        }
    }

    var selectTerms: String? {
        didSet {
            monthButton.isSelected = false
            weekButton.isSelected = false

            switch selectTerms {
            case VoteRankTermListType.month.rawValue:
                monthButton.isSelected = true
            case VoteRankTermListType.week.rawValue:
                weekButton.isSelected = true
            case PointRankTermListType.month.rawValue:
                monthButton.isSelected = true
            case PointRankTermListType.week.rawValue:
                weekButton.isSelected = true
            default:
                break;
            }
        }
    }
    var pointItem: PointRankDataMyModel? {
        didSet {
            guard let cellItem = pointItem else { return }
            updateDateLabel.text = "순위는 하루 한번씩 업데이트 됩니다.\n최근 업데이트 : \(cellItem.date)"
            if cellItem.rank == 0 {
                let myDataString = "\(cellItem.nick) 등록된 순위가 없습니다."
                let attributedString = NSMutableAttributedString(string: myDataString, attributes: [
                    .font: UIFont.systemFont(ofSize: 13.0, weight: .regular),
                    .foregroundColor: Asset.fadedRed.color,
                  .kern: 0.0
                ])
                attributedString.addAttribute(.foregroundColor, value: Asset.dark.color, range: NSRange(location: 0, length: cellItem.nick.count + 1))

                myDataLabel.attributedText = attributedString
            } else {
                let myDataString = "\(cellItem.nick) \(cellItem.rank)위"
                let attributedString = NSMutableAttributedString(string: myDataString, attributes: [
                    .font: UIFont.systemFont(ofSize: 13.0, weight: .regular),
                    .foregroundColor: Asset.fadedRed.color,
                  .kern: 0.0
                ])
                attributedString.addAttribute(.foregroundColor, value: Asset.dark.color, range: NSRange(location: 0, length: cellItem.nick.count + 1))

                myDataLabel.attributedText = attributedString

            }
        }
    }

    var item: VoteRankDataMyModel? {
        didSet {
            guard let cellItem = item else { return }
            updateDateLabel.text = "순위는 하루 한번씩 업데이트 됩니다.\n최근 업데이트 : \(cellItem.date)"
            if cellItem.rank == 0 {
                let myDataString = "\(cellItem.nick) 등록된 순위가 없습니다."
                let attributedString = NSMutableAttributedString(string: myDataString, attributes: [
                    .font: UIFont.systemFont(ofSize: 13.0, weight: .regular),
                    .foregroundColor: Asset.fadedRed.color,
                  .kern: 0.0
                ])
                attributedString.addAttribute(.foregroundColor, value: Asset.dark.color, range: NSRange(location: 0, length: cellItem.nick.count + 1))

                myDataLabel.attributedText = attributedString
            } else {
                let myDataString = "\(cellItem.nick) \(cellItem.rank)위"
                let attributedString = NSMutableAttributedString(string: myDataString, attributes: [
                    .font: UIFont.systemFont(ofSize: 13.0, weight: .regular),
                    .foregroundColor: Asset.fadedRed.color,
                  .kern: 0.0
                ])
                attributedString.addAttribute(.foregroundColor, value: Asset.dark.color, range: NSRange(location: 0, length: cellItem.nick.count + 1))

                myDataLabel.attributedText = attributedString

            }
        }
    }
    
    func initializeEvent() {
        
        weekButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                if (self?.item) != nil {
                    self?.delegate?.sortList(type: .week)
                } else {
                    self?.delegate?.pointSortList(type: .week)
                }
                
            }).disposed(by: disposeBag)
        monthButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                if (self?.item) != nil {
                    self?.delegate?.sortList(type: .month)

                } else {
                    self?.delegate?.pointSortList(type: .month)

                }
            }).disposed(by: disposeBag)

    }
}
