//
//  PromotionListCell.swift
//  MyTrot
//
//  Created by hclim on 2021/07/06.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

class PromotionListCell: BaseTableViewCell {
    var disposeBag = DisposeBag()

    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    @IBOutlet weak var nickLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var promotionImageView: UIImageView!{
        didSet {
            promotionImageView.layer.cornerRadius = Define.cornerRadius
            promotionImageView.layer.masksToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    func initializeEvent() {

    }
    var item: PromotionListInfoModel? {
        didSet {
            guard let cellItem = item else { return }
            titleLabel.text = cellItem.title
            nickLabel.text = cellItem.nick
            let readCnt = cellItem.read_cnt
            let likeCnt = cellItem.like_cnt
            let commentCnt = cellItem.comment_cnt
            
            countLabel.text = "좋아요 \(likeCnt) | 댓글 \(commentCnt) | 조회 \(readCnt)"
            dateLabel.text = cellItem.reg_dttm
            promotionImageView.kf.setImage(with: URL(string: cellItem.img01.validateHostImageUrl))
        }
    }
}
