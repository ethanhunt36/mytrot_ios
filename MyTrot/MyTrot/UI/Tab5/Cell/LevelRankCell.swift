//
//  LevelRankCell.swift
//  MyTrot
//
//  Created by hclim on 2021/07/12.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

class LevelRankCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contLabel: UILabel!
    @IBOutlet weak var cntNumber: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {

    }
    var ranking: Int? {
        didSet {
            cntNumber.text = "\(ranking ?? 0)"
        }
    }
    var item: LevelRankListInfoModel? {
        didSet {
            let nickString = "\(item?.nick ?? "") Lv.\(item?.lv ?? 0)"
            titleLabel.text = nickString
            let lv_pnt: String = item?.lv_pnt.commaString ?? ""
            contLabel.text = "\(lv_pnt)점"
        }
    }

}
