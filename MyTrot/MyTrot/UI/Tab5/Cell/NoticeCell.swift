//
//  NoticeCell.swift
//  MyTrot
//
//  Created by hclim on 2021/04/08.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

class NoticeCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {

    }

    var item: NoticeDataListModel? {
        didSet {
            let dateDt = item?.reg_dttm ?? ""

            if dateDt == "" {
                titleLabel.text = "\(item?.subject ?? "")"
            } else {
                let subject = item?.subject ?? ""
                let fullString = "\(subject)\n\(dateDt)"

                let attrString = fullString
                let attributedString = NSMutableAttributedString(string: attrString, attributes: [
                    .font: UIFont.systemFont(ofSize: 15.0, weight: .semibold),
                    .foregroundColor: Asset.black.color,
                  .kern: 0.0
                ])
                attributedString.addAttribute(.foregroundColor, value: Asset.darkGrey.color, range: NSRange(location: subject.count, length: dateDt.count + 1))
                attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 11.0, weight: .light), range: NSRange(location: subject.count, length: dateDt.count + 1))
                titleLabel.attributedText = attributedString

            }
            
        }
    }
    
}
