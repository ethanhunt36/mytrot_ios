//
//  FavSingerCollectionCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/29.
//

import Foundation
import Reusable
import Kingfisher

class FavSingerCollectionCell: UICollectionViewCell, NibReusable {
    @IBOutlet weak var singerImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    var item: ArtistSelectDataListModel? {
        didSet {
            descLabel.text = item?.name ?? "" 
            guard let imgPath = item?.pic.validateHostImageUrl else { return }

            if let imgUrl = URL(string: imgPath) {
                singerImageView.kf.setImage(with: imgUrl)
            }

        }
    }
}
