//
//  VideoStorageSingerCell.swift
//  MyTrot
//
//  Created by hclim on 2021/04/08.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

class VideoStorageSingerCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var artistImageView: UIImageView!{
        didSet {
            artistImageView.layer.cornerRadius = 25.0
            artistImageView.layer.masksToBounds = true
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {

    }

    var item: VideoStorageDataListModel? {
        didSet {
            titleLabel.text = "\(item?.name ?? "")"
            countLabel.text = "\(item?.cnt?.commaString ?? "0")"
            artistImageView.kf.setImage(with: URL(string: item?.artist_pic?.validateHostImageUrl ?? ""))

        }
    }
}
