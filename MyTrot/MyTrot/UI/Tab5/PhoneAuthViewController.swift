//
//  PhoneAuthViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit

final class PhoneAuthViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var authTextField: UITextField!
    @IBOutlet weak var phoneAuthButton: UIButton! {
        didSet {
            phoneAuthButton.layer.cornerRadius = 3.0
        }
    }
    @IBOutlet weak var authConfirmButton: UIButton!{
        didSet {
            authConfirmButton.layer.cornerRadius = 3.0
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: PhoneAuthViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension PhoneAuthViewController {
    func bindAction(_ reactor: PhoneAuthViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        phoneAuthButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { [weak self] _ in
                Reactor.Action.getAuthNumber(self?.phoneTextField.text ?? "") }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        authConfirmButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { [weak self] _ in
                Reactor.Action.confirmAuthNumber(self?.phoneTextField.text ?? "", self?.authTextField.text ?? "") }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        
    }
}

// MARK: bindState For Reactor
private extension PhoneAuthViewController {
    func bindState(_ reactor: PhoneAuthViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.confirmMessage }
            .filterNil()
            .subscribe(onNext: { [weak self] message in
                self?.subscribeOnNextBaseAlertPushBack(alertData: message)

            }).disposed(by: disposeBag)
        reactor.state.map { $0.choiceMember }
            .filter{ $0 == .callChoiceMember}
            .take(1)
            .map { [weak self] _ in Reactor.Action.callChoiceMember(self?.phoneTextField.text ?? "") }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
                    

    }
}

// MARK: -
// MARK: private initialize
private extension PhoneAuthViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureScrollView))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(singleTapGestureRecognizer)
    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension PhoneAuthViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
    @objc func tapGestureScrollView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

}

// MARK: -
