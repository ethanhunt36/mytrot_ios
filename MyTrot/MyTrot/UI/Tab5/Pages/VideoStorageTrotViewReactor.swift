//
//  VideoStorageTrotViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class VideoStorageTrotViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case getList
        case addBookmark(artist_no: Int64, vod_no: Int64)
        case removeBookmark(artist_no: Int64, vod_no: Int64)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(ResponseArtistVideoList)
        case setAddBookMarkingStatus(BookMarkingStatus, vod_no: Int64)
        case setRemoveBookMarkingStatus(BookMarkingStatus, vod_no: Int64)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var items: [ArtistVideoListModel]
        var bookmarkingStatus: BookMarkingStatus


    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            items: [],
            bookmarkingStatus: .none
        )
    }
    
    func mutate(action: VideoStorageTrotViewReactor.Action) -> Observable<VideoStorageTrotViewReactor.Mutation> {
        switch action {
        case .removeBookmark(let artist_no, let vod_no):
            if artist_no == 0, vod_no == 0 {
                return .just(.setRemoveBookMarkingStatus(.none, vod_no: 0))
            }
            return .concat([
                .just(.setRemoveBookMarkingStatus(.processing, vod_no: 0)),
                networkService.removeBookmark(artist_no: artist_no, vodNo: vod_no).asObservable().map { response in
                    if response.isResponseStatusSuccessful() {

                        return .setRemoveBookMarkingStatus(.doneSuccess, vod_no: vod_no)
                    } else {
                        return .setRemoveBookMarkingStatus(.doneFail, vod_no: 0)
                    }
                    
                }
            ])

        case .addBookmark(let artist_no, let vod_no):
            if artist_no == 0, vod_no == 0 {
                return .just(.setAddBookMarkingStatus(.none, vod_no: 0))
            }
            return .concat([
                .just(.setAddBookMarkingStatus(.processing, vod_no: 0)),
                networkService.addBookmark(artist_no: artist_no, vodNo: vod_no).asObservable().map { response in
                    if response.isResponseStatusSuccessful() {
                        RxBus.shared.post(event: Events.AddBookMark(no: vod_no), sticky: true)

                        return .setAddBookMarkingStatus(.doneSuccess, vod_no: vod_no)
                    } else {
                        return .setAddBookMarkingStatus(.doneFail, vod_no: 0)
                    }
                    
                }
            ])

        case .getList:
            return networkService.videoStorageList(gb: .trot, artist_no: -1).asObservable().map { Mutation.setResponse($0)}

        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: VideoStorageTrotViewReactor.State, mutation: VideoStorageTrotViewReactor.Mutation) -> VideoStorageTrotViewReactor.State {
        var state = state
        switch mutation {
        case .setRemoveBookMarkingStatus(let status, let vod_no):
            var tempList: [ArtistVideoListModel] = []
            tempList.append(contentsOf: state.items)
            state.items.removeAll()
            for item in tempList {
                if vod_no != item.no {
                    
                    state.items.append(item)
                }
            }
            
            state.bookmarkingStatus = status

        case .setAddBookMarkingStatus(let status, let vod_no):
            
            var tempList: [ArtistVideoListModel] = []
            tempList.append(contentsOf: state.items)
            state.items.removeAll()
            for var item in tempList {
                if vod_no == item.no {
                    
                    item.is_bookmark = 1
                }
                state.items.append(item)
            }
            
            state.bookmarkingStatus = status

        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                if let datalist = response.data?.list {
                    for var item in datalist {
                        item.is_bookmark = 1
                        state.items.append(item)
                    }

                }
//                state.items = response.data?.list ?? []
            }

        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
