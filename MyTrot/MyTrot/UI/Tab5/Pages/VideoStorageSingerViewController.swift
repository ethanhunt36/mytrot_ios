//
//  VideoStorageSingerViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit
import XLPagerTabStrip

final class VideoStorageSingerViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    var itemInfo = IndicatorInfo(title: "View")

    weak var pagerDelegate: VideoStoragePagerDelegate?
    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var noListLabel: UILabel!

    var tableViewList: [VideoStorageDataListModel] = []
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: VideoStorageSingerViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension VideoStorageSingerViewController {
    func bindAction(_ reactor: VideoStorageSingerViewReactor) {
        //        backButton.rx.tap
        //            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
        //            .map { Reactor.Action.dismiss }
        //            .bind(to: reactor.action)
        //            .disposed(by: disposeBag)
        
        reactor.action.onNext(.getList)
        
    }
}

// MARK: bindState For Reactor
private extension VideoStorageSingerViewController {
    func bindState(_ reactor: VideoStorageSingerViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .filter{ $0.count > 0 }
            .subscribe(onNext: { [weak self] items in
                self?.tableViewList.append(contentsOf: items)
                self?.tableView.reloadData()
                self?.noListLabel.isHidden = true

            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension VideoStorageSingerViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension VideoStorageSingerViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
// MARK: - IndicatorInfoProvider
extension VideoStorageSingerViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}
// MARK: -
extension VideoStorageSingerViewController {
    func initializeTableView() {
        self.tableView.register(cellType: VideoStorageSingerCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension VideoStorageSingerViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        let item = tableViewList[indexPath.row]
        let artist_no = item.no
        let name = item.name
        if let delegate = pagerDelegate {
            delegate.moveToVideoPage(artist_no: artist_no, name: name)
        }
        
    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as VideoStorageSingerCell
        cell.item = self.tableViewList[indexPath.row]

        return cell

    }
}

