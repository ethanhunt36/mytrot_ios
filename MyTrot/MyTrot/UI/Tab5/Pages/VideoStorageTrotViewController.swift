//
//  VideoStorageTrotViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit
import XLPagerTabStrip

final class VideoStorageTrotViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    var itemInfo = IndicatorInfo(title: "View")
    weak var pagerDelegate: VideoStoragePagerDelegate?

    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    var tableViewList: [ArtistVideoListModel] = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noListLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: VideoStorageTrotViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension VideoStorageTrotViewController {
    func bindAction(_ reactor: VideoStorageTrotViewReactor) {
        //        backButton.rx.tap
        //            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
        //            .map { Reactor.Action.dismiss }
        //            .bind(to: reactor.action)
        //            .disposed(by: disposeBag)
        reactor.action.onNext(.getList)

        
    }
}

// MARK: bindState For Reactor
private extension VideoStorageTrotViewController {
    func bindState(_ reactor: VideoStorageTrotViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .filter{ $0.count > 0 }
            .subscribe(onNext: { [weak self] items in
                self?.tableViewList = items
                self?.tableView.reloadData()
                self?.noListLabel.isHidden = true

            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension VideoStorageTrotViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()

    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.YoutubeMyCountUpdate.self, sticky: true).subscribe { [weak self] event in
            guard let model = event.element?.model else { return }
            var tempList: [ArtistVideoListModel] = []
            for var item in self?.tableViewList ?? [] {
                if item.youtube_id == model.youtube_id {
                    print("match! YoutubeMyCountUpdate 0")
                    print("match! YoutubeMyCountUpdate 1: \(item.my_play_cnt)")
                    item.my_play_cnt = item.my_play_cnt + 1
                    print("match! YoutubeMyCountUpdate 2: \(item.my_play_cnt)")

                }
                tempList.append(item)

            }
            self?.tableViewList = tempList
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.AddBookMark.self, sticky: false).subscribe { [weak self] event in
            let strongList = self?.tableViewList ?? []
            var tempTableList: [ArtistVideoListModel] = []
            for var item in strongList {
                if item.no == event.element?.no {
                    item.is_bookmark = 1
                }
                tempTableList.append(item)
            }
            self?.tableViewList = tempTableList
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.RemoveBookMark.self, sticky: false).subscribe { [weak self] event in
            let strongList = self?.tableViewList ?? []
            var tempTableList: [ArtistVideoListModel] = []
            for var item in strongList {
                if item.no == event.element?.no {
                    item.is_bookmark = 0
                }
                tempTableList.append(item)
            }
            self?.tableViewList = tempTableList
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)

    }
}


// MARK: - Reactor Action
private extension VideoStorageTrotViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
// MARK: - IndicatorInfoProvider
extension VideoStorageTrotViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}
extension VideoStorageTrotViewController {
    func initializeTableView() {
        self.tableView.register(cellType: VideoMainCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension VideoStorageTrotViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")

        let link = self.tableViewList[indexPath.row].youtube_id

        if let delegate = pagerDelegate {
            delegate.moveToYoutubePlayerPage(link: link, list: tableViewList)
        }

    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as VideoMainCell
        cell.item = self.tableViewList[indexPath.row]
        cell.delegate = self

        return cell

    }
}
extension VideoStorageTrotViewController: VideoMainCellDelegate{
    func moveToYoutube(link: String) {

        guard let urlLink = URL(string: "https://www.youtube.com/watch?v=\(link)") else { return}
        UIApplication.shared.open(urlLink)
    }
    
    func bookMarking(model: ArtistVideoListModel) {
        //
        print("bookMarking")
        guard let reactor = reactor else { return }
        let isBookmarking = model.is_bookmark

        if isBookmarking == 0 {
            reactor.action.onNext(.addBookmark(artist_no: model.artist_no, vod_no: model.no))
        } else {
            reactor.action.onNext(.removeBookmark(artist_no: model.artist_no, vod_no: model.no))

        }

    }
    
}

