//
//  VoteRankAccumulateViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/07/09.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class VoteRankAccumulateViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case getList(term: VoteRankTermListType)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponseVoteRankList(ResponseVoteRankList)
        case setTerm(VoteRankTermListType)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var items: [VoteRankDataListModel]
        var gubun: VoteRankListType
        var term: VoteRankTermListType
        var myModel: VoteRankDataMyModel
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(gubun: VoteRankListType, term: VoteRankTermListType) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            items: [],
            gubun: gubun,
            term: term,
            myModel: VoteRankDataMyModel(rank: 0, nick: "", date: Date().description)
        )
    }

    func mutate(action: VoteRankAccumulateViewReactor.Action) -> Observable<VoteRankAccumulateViewReactor.Mutation> {
        switch action {
        case .getList(let term):
            return .concat([
                .just(.setTerm(term)),
                networkService.voteRankList(gubun: currentState.gubun, terms: term).asObservable().map { Mutation.setResponseVoteRankList($0)}
            ])

        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: VoteRankAccumulateViewReactor.State, mutation: VoteRankAccumulateViewReactor.Mutation) -> VoteRankAccumulateViewReactor.State {
        var state = state
        switch mutation {
        case .setTerm(let term):
            state.term = term
        case .setResponseVoteRankList(let response):
            if response.isResponseStatusSuccessful() {
                let nick = User.shared.myInfo?.nick ?? ""
                let list = response.data?.list ?? []
                let date = list.first?.reg_dttm ?? Date().description
                if let rankingModel = response.data?.my_ranking.value as? Bool, rankingModel == false  {
                    state.myModel = VoteRankDataMyModel(rank: 0, nick: nick, date: date)
                } else if let rankingModel = response.data?.my_ranking.value as? [ String : Any], let rank =  rankingModel["rank"] as? Int{
                    
                    state.myModel = VoteRankDataMyModel(rank: rank, nick: nick, date: date)

                }
                state.items = list
            } else {
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)
            }

        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
