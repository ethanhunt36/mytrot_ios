//
//  PointRankAccumulateViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class PointRankAccumulateViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case getList(term: PointRankTermListType)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponsePointRankList(ResponsePointRankList)
        case setTerm(PointRankTermListType)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var items: [PointRankDataListModel]
        var gubun: PointRankListType
        var term: PointRankTermListType
        var myModel: PointRankDataMyModel

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(gubun: PointRankListType, term: PointRankTermListType) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            items: [],
            gubun: gubun,
            term: term,
            myModel: PointRankDataMyModel(rank: 0, nick: "", date: Date().description)
        )
    }

    func mutate(action: PointRankAccumulateViewReactor.Action) -> Observable<PointRankAccumulateViewReactor.Mutation> {
        switch action {
        case .getList(let term):
            return .concat([
                .just(.setTerm(term)),
                networkService.pointRankList(gubun: currentState.gubun, terms: term).asObservable().map { Mutation.setResponsePointRankList($0)}
            ])

        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: PointRankAccumulateViewReactor.State, mutation: PointRankAccumulateViewReactor.Mutation) -> PointRankAccumulateViewReactor.State {
        var state = state
        switch mutation {
        case .setTerm(let term):
            state.term = term
        case .setResponsePointRankList(let response):
            if response.isResponseStatusSuccessful() {
                let nick = User.shared.myInfo?.nick ?? ""
                let list = response.data?.list ?? []
                let date = list.first?.reg_dttm ?? Date().description
                if let rankingModel = response.data?.my_ranking.value as? Bool, rankingModel == false  {
                    state.myModel = PointRankDataMyModel(rank: 0, nick: nick, date: date)
                } else if let rankingModel = response.data?.my_ranking.value as? [ String : Any], let rank =  rankingModel["rank"] as? Int{
                    
                    state.myModel = PointRankDataMyModel(rank: rank, nick: nick, date: date)

                }
                state.items = list
            } else {
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)
            }


        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
