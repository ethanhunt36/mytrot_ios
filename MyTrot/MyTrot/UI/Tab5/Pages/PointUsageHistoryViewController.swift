//
//  PointUsageHistoryViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit
import XLPagerTabStrip

final class PointUsageHistoryViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    var itemInfo = IndicatorInfo(title: "View")

    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    var tableViewList: [PointHistoryDataListModel] = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noListLabel: UILabel!
    var isListLoadFinish: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: PointAccumulateHistoryViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension PointUsageHistoryViewController {
    func bindAction(_ reactor: PointAccumulateHistoryViewReactor) {
        //        backButton.rx.tap
        //            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
        //            .map { Reactor.Action.dismiss }
        //            .bind(to: reactor.action)
        //            .disposed(by: disposeBag)
        
        reactor.action.onNext(.getList)

    }
}

// MARK: bindState For Reactor
private extension PointUsageHistoryViewController {
    func bindState(_ reactor: PointAccumulateHistoryViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .filter{ $0.count > 0 }
            .subscribe(onNext: { [weak self] items in
                self?.noListLabel.isHidden = true
                self?.isListLoadFinish = true
                self?.tableViewList = items
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension PointUsageHistoryViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()

    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension PointUsageHistoryViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
// MARK: - IndicatorInfoProvider
extension PointUsageHistoryViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}
extension PointUsageHistoryViewController {
    func initializeTableView() {
        self.tableView.register(cellType: VoteAccHistoryCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension PointUsageHistoryViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 67
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as VoteAccHistoryCell
        cell.pointItem = self.tableViewList[indexPath.row]

        return cell

    }
}

extension PointUsageHistoryViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.height

        // 스크롤이 테이블 뷰 Offset의 끝에 가게 되면 다음 페이지를 호출
        let didScrollGoToEndOfTableView: Bool = offsetY > (contentHeight - height)
        if didScrollGoToEndOfTableView, isListLoadFinish == true {
            isListLoadFinish = false
            print("호출")
            guard let reactor = reactor else { return }
            if reactor.currentState.last_no != 0 {
                reactor.action.onNext(.getList)
            }
        }
    }
}

