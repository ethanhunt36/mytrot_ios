//
//  VoteAccumulateHistoryViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class VoteAccumulateHistoryViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        
        case getList
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponseVoteHistoryList(ResponseVoteHistoryList)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var items: [VoteHistoryDataListModel]
        var gubun: VoteHistoryListType
        var last_no: Int64
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(gubun: VoteHistoryListType) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            items: [],
            gubun: gubun,
            last_no: 0
        )
    }
    
    func mutate(action: VoteAccumulateHistoryViewReactor.Action) -> Observable<VoteAccumulateHistoryViewReactor.Mutation> {
        switch action {
        case .getList:
            return networkService.voteHistoryList(gubun: currentState.gubun, lastNo: currentState.last_no).asObservable().map{ Mutation.setResponseVoteHistoryList($0)}
            
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: VoteAccumulateHistoryViewReactor.State, mutation: VoteAccumulateHistoryViewReactor.Mutation) -> VoteAccumulateHistoryViewReactor.State {
        var state = state
        switch mutation {
        case .setResponseVoteHistoryList(let response):
            if response.isResponseStatusSuccessful() {
                let list = response.data?.list ?? []
                state.last_no = response.data?.last_no ?? 0
                state.items.append(contentsOf: list)
            } else {
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)
            }
        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
