//
//  MoreMainViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class MoreMainViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    var preferencesService = PreferencesService()
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)

        case addFavSinger(String)
        case removeFavSinger
        
        case getMoreMainApi
        case viewAd
        case votePutTicketAdPointApi(String)
        case showMore
        case showHide
        
        /// 뽑기 API
        case getPpobkiInfo
        case cheatTimeApi
        case empty
        
        case unregister
        
        case getRCMDCode
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponseMyInfo(ResponseMyInfo)
        case setMoreShowing(Bool)
        case setHideShowing(Bool)
        case setResponsePpobkiInfo(ResponsePpobkiInfo)
        case setunregister(Bool)
        case setRcmdCode(String)
        case setCheat(String)
        case setResponseMyRCMDCode(ResponseMyRCMDCode)
        case empty
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var favSinger: String
        var isMoreShowing: Bool
        var isHideShowing: Bool
        var ppobkiInfo: ResponsePpobkiInfo?
        var nextPpobkiString: String
        var can_apply_dttm: String
        
        var unregister: Bool
        var rcmdCode: String?
        var can_time_cheat: String?
        
        var responseRCMD: ResponseMyRCMDCode?
    }
    
    /// 초기 상태값
    let initialState: State
    let networkService = MyTrotService()

    init() {
        self.initialState = State(
            isDismiss: false,
            favSinger: preferencesService.getFavSinger(),
            isMoreShowing: true, // 더 보기 버튼이 보이는가?
            isHideShowing: false, // 닫기 버튼이 보이는가?
            nextPpobkiString: "지금 바로 뽑을 수 있어요!!!",
            can_apply_dttm: "",
            unregister: false
        )
    }
    
    func mutate(action: MoreMainViewReactor.Action) -> Observable<MoreMainViewReactor.Mutation> {
        switch action {
        case .cheatTimeApi:
            return .concat([
                .just(.setCheat("N")),
                networkService.cheatTimeApi().asObservable().map { _ in Mutation.empty }
            ])
        case .getRCMDCode:
            return networkService.getMyRCDMCode().asObservable().map { Mutation.setResponseMyRCMDCode($0)}
        case .unregister:
            return networkService.unregister().asObservable().map { res in
                if res.isResponseStatusSuccessful() {
                    return .setunregister(true)
                }
                return .empty

            }
        case .empty:
            return .empty()
        case .getPpobkiInfo:
            
            return networkService.getPpobkkiList().asObservable().map { Mutation.setResponsePpobkiInfo($0) }
            
        case .removeFavSinger:
            preferencesService.setIsSelectdMyFavSinger(isSelectdMyFavSinger: false)
            return networkService.uploadMyArtist(artist_no: -1).asObservable().map { res in
                if res.isResponseStatusSuccessful() {
                    User.shared.updateMyInformation(data: res.data)
                    RxBus.shared.post(event: Events.AlertShow(message: "최애 삭제 되었습니다"))
                    return .empty
                }
                return .empty
            }
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .showMore:
            return .concat([
                .just(.setMoreShowing(true)),
                .just(.setHideShowing(false))
            ])
        case .showHide:
            return .concat([
                .just(.setMoreShowing(false)),
                .just(.setHideShowing(true))
            ])

        case .getMoreMainApi:
            return networkService.getMyInfoByNo().asObservable().map {
                Mutation.setResponseMyInfo($0)
            }
        case .addFavSinger(let singer):
            preferencesService.setFavSinger(singer: singer)
            return .empty()
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        case .viewAd:
            return .empty()
        
        case .votePutTicketAdPointApi(let ad_type):
            return networkService.votePutTicketAdPoint(ad_type: ad_type).asObservable().map { res in
                if res.isResponseStatusSuccessful() == true {
                    if let memberModel = res.data?.member {
                        User.shared.updateMyInformation(data: memberModel)
                    }
                    
                    if let point_amount = res.data?.point_amount {
                        User.shared.updatePointAmount(point_amount: point_amount)
                    }
                    if let amount_ticket_ad = res.data?.amount_ticket_ad {
                        User.shared.updateAmountTicketAd(amount_ticket_ad: amount_ticket_ad)
                    }
                }
                //    Utils.AlertShow(msg: res.msg ?? Constants.Network.ErrorMessage.networkError)
                Toast(text: res.msg ?? Constants.Network.ErrorMessage.networkError,
                      duration: 0.9).show()
                return .empty
            }
        }
    }
    
    func reduce(state: MoreMainViewReactor.State, mutation: MoreMainViewReactor.Mutation) -> MoreMainViewReactor.State {
        var state = state
        switch mutation {
        case .setResponseMyRCMDCode(let response):
            state.responseRCMD = response
        case .setCheat(let flag):
            state.can_time_cheat = flag
        case .setRcmdCode(let code):
            state.rcmdCode = code
        case .setunregister(let flag):
            state.unregister = flag
        case .setResponseMyInfo(let response):
            User.shared.updateMyInformation(data: response.data)

            return state
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setMoreShowing(let flag):
            state.isMoreShowing = flag
        case .setHideShowing(let flag):
            state.isHideShowing = flag
        case .setResponsePpobkiInfo(let info):
            state.ppobkiInfo = info
            var can_apply_dttm = info.data?.can_apply_dttm ?? ""
            if Utils.isAvailablePpobKi(compareTime: can_apply_dttm) == false {
                let bindTime = Utils.calculateLeftPpobkiTimeFromDate(compareTime: state.can_apply_dttm)
                state.nextPpobkiString = "남은시간 : \(bindTime)"
            } else {
                can_apply_dttm = ""
                state.nextPpobkiString = "지금 바로 뽑을 수 있어요!!!"
            }
            state.can_apply_dttm = can_apply_dttm
            state.can_time_cheat = info.data?.can_time_cheat
        case .empty: return state
        }
        return state
    }
}
