//
//  NickChangeViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class NickChangeViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case changeNick(String)
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case empty
        case setUpdateMessage(String)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var nickName: String
        var updateMessage: String?
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            nickName: User.shared.myInfo?.nick ?? ""
        )
    }
    
    func mutate(action: NickChangeViewReactor.Action) -> Observable<NickChangeViewReactor.Mutation> {
        switch action {
        case .changeNick(let nick):
            return networkService.updateNickname(nick: nick).asObservable().map { res in
                if res.isResponseStatusSuccessful() == true {
                    return .setUpdateMessage("변경 완료 되었습니다.")
                } else {
                    
                    Utils.AlertShow(msg: res.msg ?? Constants.Network.ErrorMessage.networkError)
                }
                return .empty
            }
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: NickChangeViewReactor.State, mutation: NickChangeViewReactor.Mutation) -> NickChangeViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty:
            return state
        case .setUpdateMessage(let msg):
            state.updateMessage = msg
        }
        return state
    }
}
