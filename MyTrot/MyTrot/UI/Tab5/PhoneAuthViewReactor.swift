//
//  PhoneAuthViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift
enum ChoiceMemberStatus {
    case none
    case callChoiceMember
}
final class PhoneAuthViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        
        case getAuthNumber(String)
        case confirmAuthNumber(String, String)
        case callChoiceMember(String)
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case empty
        case setConfirmMessage(String)
        case setChoiceMemberStatus(ChoiceMemberStatus)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var confirmMessage: String?
        var choiceMember: ChoiceMemberStatus
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            choiceMember: .none
        )
    }
    
    func mutate(action: PhoneAuthViewReactor.Action) -> Observable<PhoneAuthViewReactor.Mutation> {
        switch action {
        case .callChoiceMember(let phone):
            return networkService.choiceMember(phoneNo: phone).asObservable().map { res in
                if res.isResponseStatusSuccessful() == true {
                    RxBus.shared.post(event: Events.RefreshMyInfo(), sticky: true)
                    
                    return .setConfirmMessage(res.msg ?? "완료 되었습니다.")
                } else {
                    Utils.AlertShow(msg: res.msg ?? Constants.Network.ErrorMessage.networkError)
                }
                return .empty
            }
        case .getAuthNumber(let phone):
            if phone.count == 0 {
                Utils.AlertShow(msg: "핸드폰 번호를 입력해주세요.")
                return .empty()
            }
            return networkService.getAuthNumber(phoneNo: phone).asObservable().map { res in
                if res.isResponseStatusSuccessful() == true {
                    Utils.AlertShow(msg: res.msg ?? "입력하신 핸드폰 번호로 문자가 발송되었습니다.")
                } else {
                    Utils.AlertShow(msg: res.msg ?? Constants.Network.ErrorMessage.networkError)
                }
                return .empty
            }
        
        case .confirmAuthNumber(let phone, let pin):
            if phone.count == 0 {
                Utils.AlertShow(msg: "핸드폰 번호를 입력해주세요.")
                return .empty()
            }
            if pin.count == 0 {
                Utils.AlertShow(msg: "인증 번호를 입력해주세요.")
                return .empty()
            }

            return networkService.confirmAuthNumber(phoneNo: phone, pin: pin).asObservable().map { res in
                if res.isResponseStatusSuccessful() == true {
                    // old_member_info 가 있으면 다른 계정으로 변경하고 업데이트
                    if let oldMemberInfo = res.data?.old_member_info {
                        User.shared.updateMyInformation(data: oldMemberInfo)
                    }
                    return .setChoiceMemberStatus(.callChoiceMember)
//                    RxBus.shared.post(event: Events.RefreshMyInfo(), sticky: true)
                    
//                    return .setConfirmMessage(res.msg ?? "완료 되었습니다.")
                } else {
                    Utils.AlertShow(msg: res.msg ?? Constants.Network.ErrorMessage.networkError)
                }
                return .empty
            }


        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: PhoneAuthViewReactor.State, mutation: PhoneAuthViewReactor.Mutation) -> PhoneAuthViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty: return state
        case .setConfirmMessage(let msg):
            state.confirmMessage = msg
        case .setChoiceMemberStatus(let status):
            state.choiceMember = status

        }
        return state
    }
}
