//
//  VoteRankPagerViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/07/09.
//

import Foundation
import XLPagerTabStrip
import ReactorKit
import Reusable

class VoteRankPagerViewController: ButtonBarPagerTabStripViewController, StoryboardView , StoryboardBased{
    var disposeBag = DisposeBag()

    @IBOutlet weak var backButton: UIButton!

    func bind(reactor: VoteRankPagerViewReactor) {
        bindAction(reactor)
        bindState(reactor)

    }
    
    override func viewDidLoad() {
        // change selected bar color
        print("VoteHistoryMainPagerViewController viewDidLoad")
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = Asset.custard.color
        settings.style.buttonBarItemFont =  UIFont.boldSystemFont(ofSize: 16)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true

        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = Asset.bottomTabDisabled.color
            newCell?.label.textColor = Asset.black.color
        }
        super.viewDidLoad()
    }

    // MARK: - PagerTabStripDataSource

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let voteRankAccViewController = VoteRankAccumulateViewController.instantiate()
        let voteRankAccViewReactor = VoteRankAccumulateViewReactor(gubun: .get, term: .week)
        voteRankAccViewController.reactor = voteRankAccViewReactor
        voteRankAccViewController.itemInfo = IndicatorInfo(title: "적립")
        
        let voteRankUsageViewController = VoteRankUsageViewController.instantiate()
        let voteRankUsageViewReactor = VoteRankAccumulateViewReactor(gubun: .use, term: .week)
        voteRankUsageViewController.reactor = voteRankUsageViewReactor
        voteRankUsageViewController.itemInfo = IndicatorInfo(title: "사용")
        
        return [voteRankAccViewController, voteRankUsageViewController]
    }

    // MARK: - Actions

}

// MARK: -
// MARK: bindAction For Reactor
private extension VoteRankPagerViewController {
    func bindAction(_ reactor: VoteRankPagerViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        
    }
}

// MARK: bindState For Reactor
private extension VoteRankPagerViewController {
    func bindState(_ reactor: VoteRankPagerViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        
    }
}

// MARK: -
// MARK: private initialize
private extension VoteRankPagerViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}
