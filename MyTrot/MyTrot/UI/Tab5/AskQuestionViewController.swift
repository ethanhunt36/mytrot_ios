//
//  AskQuestionViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit
import MobileCoreServices

final class AskQuestionViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var reportTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var pic1Button: UIButton!
    @IBOutlet weak var pic2Button: UIButton!
    @IBOutlet weak var pic3Button: UIButton!
    @IBOutlet weak var pic1ImageView: UIImageView! {
        didSet {
            pic1ImageView.customCornerRadius = 5.0
        }
    }
    @IBOutlet weak var pic2ImageView: UIImageView!{
        didSet {
            pic2ImageView.customCornerRadius = 5.0
        }
    }
    @IBOutlet weak var pic3ImageView: UIImageView!{
        didSet {
            pic3ImageView.customCornerRadius = 5.0
        }
    }
    @IBOutlet weak var placeHolderLabel: UILabel!
    
    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!

    lazy var selectedImageIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: AskQuestionViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension AskQuestionViewController {
    func bindAction(_ reactor: AskQuestionViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        cancelButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        pic1Button.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.openPhotoAlert(picIndex: 1)
            }).disposed(by: disposeBag)
        pic2Button.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.openPhotoAlert(picIndex: 2)
            }).disposed(by: disposeBag)
        pic3Button.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.openPhotoAlert(picIndex: 3)
            }).disposed(by: disposeBag)
        sendButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { [weak self] _ in
                return Reactor.Action.uploadReport(self?.reportTextView.text ?? "")
            }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

    }
}

// MARK: bindState For Reactor
private extension AskQuestionViewController {
    func bindState(_ reactor: AskQuestionViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.alertBackMessage}
            .distinctUntilChanged()
            .filterNil()
            .subscribe(onNext: { [weak self] alertBackMessage in
                self?.subscribeOnNextBaseAlertPushBack(alertData: alertBackMessage)
                
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension AskQuestionViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureScrollView))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(singleTapGestureRecognizer)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension AskQuestionViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: Private Function
private extension AskQuestionViewController {
    func openPhotoAlert(picIndex: Int) {
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        selectedImageIndex = picIndex
        let library =  UIAlertAction(title: "사진앨범", style: .default) { [weak self] (action) in
            self?.openLibrary()
        }


        let camera =  UIAlertAction(title: "카메라", style: .default) { [weak self] (action) in
            self?.openCamera()

        }
        let cancel = UIAlertAction(title: "취소", style: .cancel) { [weak self] (action) in
            self?.selectedImageIndex = 0

        }
        alert.addAction(library)

        alert.addAction(camera)

        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)


    }
    
    func openCamera() {
        let pickerController = UIImagePickerController()
        // Part 1: File origin
        pickerController.sourceType = .camera
        
        // Must import `MobileCoreServices`
        // Part 2: Define if photo or/and video is going to be captured by camera
        pickerController.mediaTypes = [kUTTypeImage as String]
        
        // Part 3: camera settings
        pickerController.cameraCaptureMode = .photo // Default media type .photo vs .video
        pickerController.cameraDevice = .rear // rear Vs front
        // Part 4: User can optionally crop only a certain part of the image or video with iOS default tools
        pickerController.allowsEditing = true
        
        // Part 5: For callback of user selection / cancellation
        pickerController.delegate = self

        // Part 6: Present the UIImagePickerViewController
        present(pickerController, animated: true, completion: nil)

    }
    func openLibrary() {
        let pickerController = UIImagePickerController()
        
        /*
        * Part 1: Select the origin of media source
        Either one of the belows:
        1. .photoLibrary     -> Go to album selection page
        2. .savedPhotosAlbum -> Go to Moments directly
        */
        pickerController.sourceType = .photoLibrary
        
        // Must import `MobileCoreServices`
        // Part 2: Allow user to select both image and video as source
        pickerController.mediaTypes = [kUTTypeImage as String]

        // Part 3: User can optionally crop only a certain part of the image or video with iOS default tools
        pickerController.allowsEditing = true
        
        // Part 4: For callback of user selection / cancellation
        pickerController.delegate = self
        
        // Part 5: Show UIImagePickerViewController to user
        present(pickerController, animated: true, completion: nil)

    }
    @objc func tapGestureScrollView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        bottomAnchorConstraint.constant = -1 * ((view.convert(keyboardFrame.cgRectValue, from: nil).size.height) - view.safeAreaInsets.bottom )
        placeHolderLabel.isHidden = true
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        bottomAnchorConstraint.constant = 0
        if reportTextView.hasText == false {
            placeHolderLabel.isHidden = false
        }
    }

}

// MARK: -
extension AskQuestionViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! CFString
        switch mediaType {
        case kUTTypeImage:
          // Handle image selection result
            print("Selected media is image")
            let editedImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
            guard let reactor = reactor else { return }
            guard let imgData = editedImage.jpegData(compressionQuality: 0.5) else { return }

            switch selectedImageIndex {
            case 1:
                pic1ImageView.image = editedImage
                reactor.action.onNext(.setImage1Data(imgData))
            case 2:
                pic2ImageView.image = editedImage
                reactor.action.onNext(.setImage2Data(imgData))
            case 3:
                pic3ImageView.image = editedImage
                reactor.action.onNext(.setImage3Data(imgData))

            default:
                break;
            }
            
            

        case kUTTypeMovie:
            print("Selected media is video === > false")
            
          
        default:
            print("Mismatched type: \(mediaType)")
        }
        picker.dismiss(animated: true, completion: nil)

    }

}
