//
//  AskAddtionalVideoViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class AskAddtionalVideoViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case uploadReport(String)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(CommonResponse)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var alertBackMessage: String?

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false
        )
    }
    
    func mutate(action: AskAddtionalVideoViewReactor.Action) -> Observable<AskAddtionalVideoViewReactor.Mutation> {
        switch action {
        case .uploadReport(let cont):
            if cont.count == 0 {
                Utils.AlertShow(msg: "내용을 입력해 주세요.")
                return .empty()
            }

            return networkService.uploadAdditionalVideo(cont: cont).asObservable().map { Mutation.setResponse($0)}
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: AskAddtionalVideoViewReactor.State, mutation: AskAddtionalVideoViewReactor.Mutation) -> AskAddtionalVideoViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.alertBackMessage = response.msg?.replaceSlashString ?? "영상요청 접수가 완료되었습니다.\n최대한 빨리 확인/처리해서 회신 드리겠습니다."
                
            } else {
                RxBus.shared.post(event: Events.AlertShow(message: response.msg ?? ""))
            }
        }
        return state
    }
}
