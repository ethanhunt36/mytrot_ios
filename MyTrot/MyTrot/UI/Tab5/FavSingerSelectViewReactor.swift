//
//  FavSingerSelectViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

enum FavSingerMyArtistType {
    case nothing
    case nonCheck
    case topCheck
}
enum FavSingerScreenType {
    /// 내 최애가수 설정
    case choiceFavSinger
    /// 글 등록시 설정
    case choiceUploadSinger
    /// 리스트에서 가수 선택
    case choiceSinger
    /// 활동 인증서 선택
    case activityCertification
}
final class FavSingerSelectViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        
        case getArtistList
        
        case uploadMyArtist(Int64)
        case checkMyFav
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(ResponseArtistSelectList)
        case setFavType(FavSingerMyArtistType)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var items: [ArtistSelectDataListModel]
        var screenType: FavSingerScreenType
        var titleString: String
        var myFavType: FavSingerMyArtistType
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(screenType type: FavSingerScreenType) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            items: [],
            screenType: type,
            titleString: Utils.getFavSingerTitle(screenType: type),
            myFavType: Utils.getMyFavType()
        )
    }
    func mutate(action: FavSingerSelectViewReactor.Action) -> Observable<FavSingerSelectViewReactor.Mutation> {
        switch action {
        case .checkMyFav:
            if currentState.myFavType == .topCheck {
                return .concat([
                    .just(.setFavType(currentState.myFavType)),
                    networkService.artistSelectList().asObservable().map { Mutation.setResponse($0)}
                ])
            } else {
                
                return .just(.setFavType(currentState.myFavType))

            }
        case .uploadMyArtist(let artist_no):
            return networkService.uploadMyArtist(artist_no: artist_no).asObservable().map { [weak self] res in
                if res.isResponseStatusSuccessful() {
                    if self?.currentState.screenType == .choiceFavSinger {
                        User.shared.updateMyInformation(data: res.data)
                    }

                    self?.navigate(step: CommonStep.pushDismiss(animated: true))
                }
                return Mutation.setDismiss(false)
            }
        case .getArtistList:
            return networkService.artistSelectList().asObservable().map { Mutation.setResponse($0)}
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: FavSingerSelectViewReactor.State, mutation: FavSingerSelectViewReactor.Mutation) -> FavSingerSelectViewReactor.State {
        var state = state
        switch mutation {
        case .setFavType(let type):
            switch type {
            case .nonCheck:
                state.myFavType = .topCheck
                var tempList: [ArtistSelectDataListModel] = []
                tempList.append(contentsOf: state.items)
                state.items.removeAll()
                let myArtistNo = User.shared.myInfo?.artist_no ?? 0
                for item in tempList {
                    if myArtistNo == item.no {
                        state.items.insert(item, at: 0)
                    } else {
                        state.items.append(item)
                    }
                }
                preferencesService.setIsSelectdMyFavSinger(isSelectdMyFavSinger: true)
            case .topCheck:
                state.myFavType = .nonCheck
                preferencesService.setIsSelectdMyFavSinger(isSelectdMyFavSinger: false)
            case .nothing:
                Utils.AlertShow(msg: "더보기 화면에서 최애 가수를 설정해주세요.")
            }
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.items = response.data?.list_data ?? []

                if currentState.myFavType == .topCheck {
                    var tempList: [ArtistSelectDataListModel] = []
                    tempList.append(contentsOf: state.items)
                    state.items.removeAll()
                    let myArtistNo = User.shared.myInfo?.artist_no ?? 0
                    for item in tempList {
                        if myArtistNo == item.no {
                            state.items.insert(item, at: 0)
                        } else {
                            state.items.append(item)
                        }
                    }

                }
                
            }
        }
        return state
    }
}
