//
//  NoticeListViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import UIKit
import ReactorKit

final class NoticeListViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var isListLoadFinish: Bool = false
    var tableViewList: [NoticeDataListModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: NoticeListViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension NoticeListViewController {
    func bindAction(_ reactor: NoticeListViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        reactor.action.onNext(.getNotice)
        
    }
}

// MARK: bindState For Reactor
private extension NoticeListViewController {
    func bindState(_ reactor: NoticeListViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.item }
            .filter{ $0.count > 0}
            .subscribe(onNext: { [weak self] item in
                self?.tableViewList.append(contentsOf: item)
                self?.isListLoadFinish = true
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension NoticeListViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension NoticeListViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
extension NoticeListViewController {
    func initializeTableView() {
        self.tableView.register(cellType: NoticeCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension NoticeListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        guard let reactor = reactor else { return }
        let b_no = tableViewList[indexPath.row].b_no ?? ""
        let url = tableViewList[indexPath.row].url
        if b_no != "" {
            reactor.action.onNext(.moveToStepScreen(CommonStep.noticeWebDetailPage(type: .blog, url: url)))
            
        } else if url != "" {
            reactor.action.onNext(.moveToStepScreen(CommonStep.noticeWebDetailPage(type: .notice, url: url)))

        } else {
            reactor.action.onNext(.moveToStepScreen(CommonStep.noticeDetailPage(model: tableViewList[indexPath.row])))
            reactor.action.onNext(.readArticle(tableViewList[indexPath.row].no ?? 0))
        }
    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension


    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as NoticeCell
        cell.item = self.tableViewList[indexPath.row]
        return cell

    }
}
extension NoticeListViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.height

        // 스크롤이 테이블 뷰 Offset의 끝에 가게 되면 다음 페이지를 호출
        let didScrollGoToEndOfTableView: Bool = offsetY > (contentHeight - height)
        if didScrollGoToEndOfTableView, isListLoadFinish == true {
            isListLoadFinish = false
            print("호출")
            guard let reactor = reactor else { return }
            if reactor.currentState.last_no != "" {
                reactor.action.onNext(.getNoticeAddtionalList)
            }
        }
    }
}
