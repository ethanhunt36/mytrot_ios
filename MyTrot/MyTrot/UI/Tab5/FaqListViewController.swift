//
//  FaqListViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/04/07.
//

import Foundation
import UIKit
import ReactorKit

final class FaqListViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var isListLoadFinish: Bool = false
    var tableViewList: [FAQDataListModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: FaqListViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension FaqListViewController {
    func bindAction(_ reactor: FaqListViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        reactor.action.onNext(.getNotice)

    }
}

// MARK: bindState For Reactor
private extension FaqListViewController {
    func bindState(_ reactor: FaqListViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.item }
            .filter{ $0.count > 0}
            .subscribe(onNext: { [weak self] item in
                self?.tableViewList.append(contentsOf: item)
                self?.isListLoadFinish = true
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension FaqListViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension FaqListViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
extension FaqListViewController {
    func initializeTableView() {
        self.tableView.register(cellType: FaqCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension FaqListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        guard let reactor = reactor else { return }
//        let b_no = tableViewList[indexPath.row].b_no ?? ""
        if tableViewList[indexPath.row].url != "" {
            reactor.action.onNext(.moveToStepScreen(CommonStep.noticeWebDetailPage(type: .blog, url: tableViewList[indexPath.row].url)))

        } else {
            reactor.action.onNext(.moveToStepScreen(CommonStep.faqDetailPage(model: tableViewList[indexPath.row])))

        }
//        if b_no != "" {
//            let url = tableViewList[indexPath.row].url
//            reactor.action.onNext(.moveToStepScreen(CommonStep.noticeWebDetailPage(type: .blog, url: url)))
//
//
//        } else {
//            reactor.action.onNext(.moveToStepScreen(CommonStep.faqDetailPage(model: tableViewList[indexPath.row])))
//        }
    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension


    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as FaqCell
        cell.item = self.tableViewList[indexPath.row]
        return cell

    }
}
extension FaqListViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.height

        // 스크롤이 테이블 뷰 Offset의 끝에 가게 되면 다음 페이지를 호출
        let didScrollGoToEndOfTableView: Bool = offsetY > (contentHeight - height)
        if didScrollGoToEndOfTableView, isListLoadFinish == true {
            isListLoadFinish = false
            print("호출")
            guard let reactor = reactor else { return }
            if reactor.currentState.last_no != "" {
                reactor.action.onNext(.getNoticeAddtionalList)
            }
        }
    }
}
