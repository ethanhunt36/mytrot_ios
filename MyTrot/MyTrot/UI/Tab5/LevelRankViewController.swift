//
//  LevelRankViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/07/12.
//

import Foundation

import UIKit
import ReactorKit

final class LevelRankViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    var tableViewList: [LevelRankListInfoModel] = []
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var myRankLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: LevelRankViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension LevelRankViewController {
    func bindAction(_ reactor: LevelRankViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        reactor.action.onNext(.getList)

    }
}

// MARK: bindState For Reactor
private extension LevelRankViewController {
    func bindState(_ reactor: LevelRankViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .filter{ $0.count > 0 }
            .subscribe(onNext: { [weak self] items in
                self?.tableViewList = items
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)

        reactor.state.map { $0.myRank}
            .subscribe(onNext: { [weak self] myRank in
                let nick = User.shared.myInfo?.nick ?? ""
                if myRank == 0 {
                    let myDataString = "\(nick) 등록된 순위가 없습니다."
                    let attributedString = NSMutableAttributedString(string: myDataString, attributes: [
                        .font: UIFont.systemFont(ofSize: 13.0, weight: .regular),
                        .foregroundColor: Asset.fadedRed.color,
                      .kern: 0.0
                    ])
                    attributedString.addAttribute(.foregroundColor, value: Asset.dark.color, range: NSRange(location: 0, length: nick.count + 1))

                    self?.myRankLabel.attributedText = attributedString
                } else {
                    let myDataString = "\(nick) \(myRank.commaString)위"
                    let attributedString = NSMutableAttributedString(string: myDataString, attributes: [
                        .font: UIFont.systemFont(ofSize: 13.0, weight: .regular),
                        .foregroundColor: Asset.fadedRed.color,
                      .kern: 0.0
                    ])
                    attributedString.addAttribute(.foregroundColor, value: Asset.dark.color, range: NSRange(location: 0, length: nick.count + 1))

                    self?.myRankLabel.attributedText = attributedString

                }

            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension LevelRankViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()

    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension LevelRankViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
extension LevelRankViewController {
    func initializeTableView() {
        self.tableView.register(cellType: LevelRankCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension LevelRankViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as LevelRankCell
        cell.item = self.tableViewList[indexPath.row]
        cell.ranking = indexPath.row + 1
        return cell

    }
}

