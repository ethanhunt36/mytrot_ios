//
//  PromotionUploadViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/07/07.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class PromotionUploadViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case uploadBoard(sns: String, url: String, cont: String)
        case setImageData1(Data)
        case setImageData2(Data)
        case setImageData3(Data)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(CommonResponse)
        case empty
        case setImageData1(Data)
        case setImageData2(Data)
        case setImageData3(Data)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var alertBackMessage: String?
        var imageData1: Data?
        var imageData2: Data?
        var imageData3: Data?

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false
        )
    }
    
    func mutate(action: PromotionUploadViewReactor.Action) -> Observable<PromotionUploadViewReactor.Mutation> {
        switch action {
        case .setImageData1(let data):
            return .just(.setImageData1(data))
        case .setImageData2(let data):
            return .just(.setImageData2(data))
        case .setImageData3(let data):
            return .just(.setImageData3(data))
        case .uploadBoard(let sns, let url, let cont):
            if preferencesService.getIsUserAgreeTerms() == false {
                Utils.AlertShow(msg: Constants.String.AlertMessage.agreementDeniedMessage)
                
                return .empty()
            }
            if cont == "" {
                Utils.AlertShow(msg: Constants.String.AlertMessage.promotionValidateMessage)
                
                return .empty()

            }
            let nick = User.shared.myInfo?.nick ?? ""
            let sendData1 = currentState.imageData1 ?? Data()
            let sendData2 = currentState.imageData2 ?? Data()
            let sendData3 = currentState.imageData3 ?? Data()
            return  networkService.uploadPromotion(nick: nick, title: sns, cont: cont, link: url, image1: sendData1, image2: sendData2, image3: sendData3)
                .asObservable().map { Mutation.setResponse($0)}

        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: PromotionUploadViewReactor.State, mutation: PromotionUploadViewReactor.Mutation) -> PromotionUploadViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.alertBackMessage = response.msg?.replaceSlashString ?? ""
                RxBus.shared.post(event: Events.GalleryUploadFinish())

            } else {
                RxBus.shared.post(event: Events.AlertShow(message: response.msg ?? ""))
            }
        case .empty:
            return state
        case .setImageData1(let data):
            state.imageData1 = data
        case .setImageData2(let data):
            state.imageData2 = data
        case .setImageData3(let data):
            state.imageData3 = data

        }
        return state
    }
}
