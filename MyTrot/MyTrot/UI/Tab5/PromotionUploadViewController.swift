//
//  PromotionUploadViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/07/07.
//

import Foundation

import UIKit
import ReactorKit
import MobileCoreServices

final class PromotionUploadViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var snsField: UITextField!

    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!
    @IBOutlet weak var uploadSendButton: UIButton!
    @IBOutlet weak var uploadImageButton1: UIButton!
    @IBOutlet weak var uploadImageView1: UIImageView!
    @IBOutlet weak var uploadImageButton2: UIButton!
    @IBOutlet weak var uploadImageView2: UIImageView!
    @IBOutlet weak var uploadImageButton3: UIButton!
    @IBOutlet weak var uploadImageView3: UIImageView!
    @IBOutlet weak var urlField: UITextField!

    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var uploadTextView: UITextView!

    @IBOutlet weak var scrollView: UIScrollView!

    var imageSelectIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: PromotionUploadViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension PromotionUploadViewController {
    func bindAction(_ reactor: PromotionUploadViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        uploadSendButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { [weak self] _ in
                Reactor.Action.uploadBoard(sns: self?.snsField.text ?? "", url: self?.urlField.text ?? "", cont: self?.uploadTextView.text ?? "") }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        uploadImageButton1.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.openPhotoAlert(index: 1)
            }).disposed(by: disposeBag)
        uploadImageButton2.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.openPhotoAlert(index: 2)
            }).disposed(by: disposeBag)
        uploadImageButton3.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.openPhotoAlert(index: 3)
            }).disposed(by: disposeBag)

    }
}

// MARK: bindState For Reactor
private extension PromotionUploadViewController {
    func bindState(_ reactor: PromotionUploadViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.alertBackMessage}
            .distinctUntilChanged()
            .filterNil()
            .subscribe(onNext: { [weak self] alertBackMessage in
                self?.subscribeOnNextBaseAlertPushBack(alertData: alertBackMessage)
                
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension PromotionUploadViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureScrollView))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(singleTapGestureRecognizer)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension PromotionUploadViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
// MARK: Private Function
private extension PromotionUploadViewController {
    func openPhotoAlert(index: Int) {
        imageSelectIndex = index
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        let library =  UIAlertAction(title: "사진앨범", style: .default) { [weak self] (action) in
            self?.openLibrary()
        }


        let camera =  UIAlertAction(title: "카메라", style: .default) { [weak self] (action) in
            self?.openCamera()

        }
        let cancel = UIAlertAction(title: "취소", style: .cancel) { [weak self]
            _ in
            self?.imageSelectIndex = 0
        }
        alert.addAction(library)

        alert.addAction(camera)

        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)


    }
    
    func openCamera() {
        let pickerController = UIImagePickerController()
        // Part 1: File origin
        pickerController.sourceType = .camera
        
        // Must import `MobileCoreServices`
        // Part 2: Define if photo or/and video is going to be captured by camera
        pickerController.mediaTypes = [kUTTypeImage as String]
        
        // Part 3: camera settings
        pickerController.cameraCaptureMode = .photo // Default media type .photo vs .video
        pickerController.cameraDevice = .rear // rear Vs front
        // Part 4: User can optionally crop only a certain part of the image or video with iOS default tools
//        pickerController.allowsEditing = true
        
        // Part 5: For callback of user selection / cancellation
        pickerController.delegate = self

        // Part 6: Present the UIImagePickerViewController
        present(pickerController, animated: true, completion: nil)

    }
    func openLibrary() {
        let pickerController = UIImagePickerController()
        
        /*
        * Part 1: Select the origin of media source
        Either one of the belows:
        1. .photoLibrary     -> Go to album selection page
        2. .savedPhotosAlbum -> Go to Moments directly
        */
        pickerController.sourceType = .photoLibrary
        
        // Must import `MobileCoreServices`
        // Part 2: Allow user to select both image and video as source
        pickerController.mediaTypes = [kUTTypeImage as String]

        // Part 3: User can optionally crop only a certain part of the image or video with iOS default tools
//        pickerController.allowsEditing = true
        
        // Part 4: For callback of user selection / cancellation
        pickerController.delegate = self
        
        // Part 5: Show UIImagePickerViewController to user
        present(pickerController, animated: true, completion: nil)

    }
    @objc func tapGestureScrollView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
//        bottomAnchorConstraint.constant = ((view.convert(keyboardFrame.cgRectValue, from: nil).size.height) - view.safeAreaInsets.bottom - 80 )
    
        if uploadTextView.isFirstResponder {
            scrollView.setContentOffset(CGPoint(x: 0, y: 400), animated: true)

        }
        if urlField.isFirstResponder {
            scrollView.setContentOffset(CGPoint(x: 0, y: 350), animated: true)

        }
        print("keyboard height : \((view.convert(keyboardFrame.cgRectValue, from: nil).size.height))")
        print("view.safeAreaInsets.bottom : \(view.safeAreaInsets.bottom)")

//        commentView.isHidden = false
//
//        print("view.safeAreaInsets.top  :  \(view.safeAreaInsets.top )")
//        print("view.safeAreaInsets.bottom  :  \(view.safeAreaInsets.bottom )")
//        UIView.animate(withDuration: 0.30) { [weak self] in
//            self?.view.layoutIfNeeded()
//        }
        placeHolderLabel.isHidden = true
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        bottomAnchorConstraint.constant = 0
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)

        if uploadTextView.hasText == false {
            placeHolderLabel.isHidden = false
        }
    }

}

// MARK: -
extension PromotionUploadViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! CFString
        switch mediaType {
        case kUTTypeImage:
          // Handle image selection result
            print("Selected media is image")
            let editedImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            guard let reactor = reactor else { return }
            guard let imgData = editedImage.jpegData(compressionQuality: 0.5) else { return }

            switch self.imageSelectIndex {
            case 1:
                uploadImageView1.image = editedImage
                reactor.action.onNext(.setImageData1(imgData))

            case 2:
                uploadImageView2.image = editedImage
                reactor.action.onNext(.setImageData2(imgData))

            case 3:
                uploadImageView3.image = editedImage
                reactor.action.onNext(.setImageData3(imgData))

            default:
                break ;
            }
            
            
            
//          let originalImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
//          fooOriginalImageView.image = originalImage

        case kUTTypeMovie:
          // Handle video selection result
            print("Selected media is video === > false")
            
//          let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as! URL
          
        default:
            print("Mismatched type: \(mediaType)")
        }
        picker.dismiss(animated: true, completion: nil)

    }

}

