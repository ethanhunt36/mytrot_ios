//
//  LevelRankViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/07/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class LevelRankViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case getList

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponseLevelRankList(ResponseLevelRankList)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var items: [LevelRankListInfoModel]
        var myRank: Int

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            items: [],
            myRank: 0
        )
    }
    
    func mutate(action: LevelRankViewReactor.Action) -> Observable<LevelRankViewReactor.Mutation> {
        switch action {
        case .getList:
            return networkService.levelRankList().asObservable().map{ Mutation.setResponseLevelRankList($0)}
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: LevelRankViewReactor.State, mutation: LevelRankViewReactor.Mutation) -> LevelRankViewReactor.State {
        var state = state
        switch mutation {
        case .setResponseLevelRankList(let response):
            if response.isResponseStatusSuccessful() {
                let list = response.data?.list ?? []
                state.items = list
                
                if let rankingModel = response.data?.my_ranking.value as? [ String : Any], let rank =  rankingModel["rank"] as? Int{
                    
                    state.myRank = rank
                }

            } else {
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)
            }
        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
