//
//  ActivityCertViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/06/08.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

enum ActivityCertState {
    /// 최초 화면 진입 상태
    case none
    /// 인증서 조회 완료 상태
    case finishToGetCert
    /// 인증서 발급 완료 상태
    case finishToGetMission
}
final class ActivityCertViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case getCertInfo
        case getCertMission
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setScreenState(ActivityCertState)
        case setResponseActivityCertInfo(ResponseActivityCertInfo)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var artistModel: ArtistSelectDataListModel
        var screenState: ActivityCertState
        var certInfo: ResponseActivityCertInfo?
        var nickName: String
        var today: String
        var isYesterDay: Bool
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(withArtistModel model: ArtistSelectDataListModel, isYesterDay: Bool ) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            artistModel: model,
            screenState: .none,
            nickName: User.shared.myInfo?.nick ?? "",
            today: Utils.getTodayString(),
            isYesterDay: isYesterDay
        )
    }
    
    func mutate(action: ActivityCertViewReactor.Action) -> Observable<ActivityCertViewReactor.Mutation> {
        switch action {
        case .getCertInfo:
            return networkService.getCertInfo(artist_no: currentState.artistModel.no, isYesterDay: currentState.isYesterDay).asObservable().map { Mutation.setResponseActivityCertInfo($0) }
        case .getCertMission:
            return networkService.checkCertInfo().asObservable().map { _ in return .setScreenState(.finishToGetMission)}
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.popDismissRoot(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: ActivityCertViewReactor.State, mutation: ActivityCertViewReactor.Mutation) -> ActivityCertViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setScreenState(let flag):
            state.screenState = flag
        case .setResponseActivityCertInfo(let response):
            state.screenState = .finishToGetCert
            state.certInfo = response
        }
        return state
    }
}
