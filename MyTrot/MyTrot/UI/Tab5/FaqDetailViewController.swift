//
//  FaqDetailViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/04/08.
//

import Foundation
import UIKit
import ReactorKit
import Kingfisher

final class FaqDetailViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var contLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imageStackContainerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: FaqDetailViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension FaqDetailViewController {
    func bindAction(_ reactor: FaqDetailViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
    }
}

// MARK: bindState For Reactor
private extension FaqDetailViewController {
    func bindState(_ reactor: FaqDetailViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.model }
            .subscribe(onNext: { [weak self] model in
                let imgUrl = model.img_path ?? ""
                if imgUrl == "" {
                    self?.imageStackContainerView.isHidden = true
                } else {
                    self?.imageStackContainerView.isHidden = false
                    self?.imgView.kf.setImage(with: URL(string: imgUrl))
                }
//                let cont = model.cont?.htmlToString ?? ""
                self?.contLabel.text = model.cont
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension FaqDetailViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension FaqDetailViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
