//
//  ChartFlow.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
import RxFlow
import RxRelay

final class Tab5Flow: Flow {
    var root: Presentable {
        return self.rootViewController
    }

    private lazy var rootViewController: BaseNavigationController = {
        let viewController = BaseNavigationController()
        viewController.setNavigationBarHidden(true, animated: false)
        return viewController
    }()

    private let myStepper: Tab5Stepper

    init(withStepper stepper: Tab5Stepper) {
        self.myStepper = stepper
    }

    deinit { print("\(type(of: self)): \(#function)") }

    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? CommonStep else { return FlowContributors.none }

        switch step {
        case .tab5Main:
            return navigateToMainScreen()
            
        /// 닉네임 변경
        case .nickChangePage:
            return nickChangePage()
        /// 핸드폰 인증
        case .phoneAuthPage:
            return phoneAuthPage()
        /// 최애가서 선택
        case .favSingerSelectPage(let type):
            return favSingerSelectPage(type: type)
        /// 영상 추가요청
        case .askAdditionalVideoPage:
            return askAdditionalVideoPage()
        /// 영상 문의
        case .askQuestionPage:
            return askQuestionPage()

        /// 공지사항 리스트
        case .noticeListPage:
            return noticeListPage()
        /// 공지사항 웹이동
        case .noticeWebDetailPage(let type, let url):
            return noticeWebDetailPage(type: type, url: url)
        /// FAQ
        case .faqPage:
            return faqPage()
        case .faqDetailPage(let model):
            return faqDetailPage(model: model)
        /// 이용약관
        case .termsPage(let type, let url):
            return termsPage(type: type, url: url)
        /// 영상보관함
        case .videoStoragePage:
            return videoStoragePage()
        /// 포인트 순위
        case .pointRankPage:
            return pointRankPage()
        case .voteRankPage:
            return voteRankPage()
        /// 투표권 내역 리스트
        case .voteHistoryPage:
            return voteHistoryPage()
        /// 포인트 내역 리스트
        case .pointHistoryPage:
            return pointHistoryPage()
            
        case .noticeDetailPage(let model):
            return noticeDetailPage(model: model)
        case .videoListByArtistNo(let artistNo, let artistName):
            return navigateToVideoListByArtist(artist_no: artistNo, artist_name: artistName)
        case .playYoutube(let link, let list):
            return playYoutube(link: link, list: list)
        case .modalDismiss(let animated):
            return dismissScreen(type: .modal, animated: animated)
        case .pushDismiss(let animated):
            return dismissScreen(type: .push, animated: animated)
        case .searchPage:
            return navigateToSearchScreen()
        case .videoSearchResultPage(let keyword):
            return videoSearchResultPage(keyword: keyword)
        case .activityCertPage(let model, let isYesterDay):
            return activityCertPage(model: model, isYesterDay: isYesterDay)
        case .ppobKiPage:
            return ppobKiPage()
        case .popDismissRoot(let animated):
            return popDismissRoot(animated: animated)
        case .galleryDetailPage(let no):
            return galleryDetailPage(boardNo: no)
        case .promotionListPage:
            return promotionListPage()
        case .promotionUploadPage:
            return promotionUploadPage()
        case .levelRankListPage:
            return levelRankListPage()
        case .iapPage:
            return iapPage()
        default: return .none
        }
    }
}

// MARK: -
// MARK: 이동 (navigate)
// MARK: -
extension Tab5Flow {
    /// 메인화면 이동
    private func navigateToMainScreen() -> FlowContributors {
        let controller = MoreMainViewController.instantiate()
        let reactor = MoreMainViewReactor()
        controller.reactor = reactor

        self.rootViewController.setViewControllers([controller], animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))
    }
    private func playYoutube(link: String, list: [ArtistVideoListModel]) -> FlowContributors {
        let controller = CommonYTPlayerViewController.instantiate()
        let reactor = CommonYTPlayerViewReactor(withLink: link, list: list)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }

    private func iapPage() -> FlowContributors {
        let controller = IAPViewController.instantiate()
        let reactor = IAPViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))
        
    }
    private func levelRankListPage() -> FlowContributors {
        let controller = LevelRankViewController.instantiate()
        let reactor = LevelRankViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))
        
    }

    private func promotionUploadPage() -> FlowContributors {
        let controller = PromotionUploadViewController.instantiate()
        let reactor = PromotionUploadViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))
        
    }

    private func promotionListPage() -> FlowContributors {
        let controller = PromotionViewController.instantiate()
        let reactor = PromotionViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))
        
    }
    private func galleryDetailPage(boardNo: Int64) -> FlowContributors {
        let controller = GalleryDetailViewController.instantiate()
        let reactor = GalleryDetailViewReactor(promotion: boardNo)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }

    private func navigateToSearchScreen() -> FlowContributors {
        let controller = SearchVideoListViewController.instantiate()
        let reactor = SearchVideoListViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func navigateToVideoListByArtist(artist_no: Int64, artist_name: String) -> FlowContributors {
        let controller = CommonVideoListViewController.instantiate()
        let reactor = CommonVideoListViewReactor(name: artist_name)
        controller.reactor = reactor
        
        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }

    private func nickChangePage() ->FlowContributors {
        let controller = NickChangeViewController.instantiate()
        let reactor = NickChangeViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func phoneAuthPage() -> FlowContributors {
        let controller = PhoneAuthViewController.instantiate()
        let reactor = PhoneAuthViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func favSingerSelectPage(type: FavSingerScreenType) -> FlowContributors {
        let controller = FavSingerSelectViewController.instantiate()
        let reactor = FavSingerSelectViewReactor(screenType: type)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))


    }
    /// 영상 추가요청
    private func askAdditionalVideoPage() -> FlowContributors {
        let controller = AskAddtionalVideoViewController.instantiate()
        let reactor = AskAddtionalVideoViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// 영상 문의
    private func askQuestionPage() -> FlowContributors {
        let controller = AskQuestionViewController.instantiate()
        let reactor = AskQuestionViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// 공지사항 리스트
    private func noticeListPage() -> FlowContributors {
        let controller = NoticeListViewController.instantiate()
        let reactor = NoticeListViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// 공지사항 리스트
    private func noticeDetailPage(model: NoticeDataListModel) -> FlowContributors {
        let controller = NoticeDetailViewController.instantiate()
        let reactor = NoticeDetailViewReactor(withModel: model)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// 공지사항 리스트
    private func faqDetailPage(model: FAQDataListModel) -> FlowContributors {
        let controller = FaqDetailViewController.instantiate()
        let reactor = FaqDetailViewReactor(withModel: model)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// 공지사항 웹이동
    private func noticeWebDetailPage(type: WebType, url: String) -> FlowContributors {
        let controller = CommonWebViewController.instantiate()
        let reactor = CommonWebViewReactor(withWebType: type, url: url)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// FAQ
    private func faqPage() -> FlowContributors {
        let controller = FaqListViewController.instantiate()
        let reactor = FaqListViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// 이용약관
    private func termsPage(type: WebType, url: String) -> FlowContributors {
        let controller = CommonWebViewController.instantiate()
        let reactor = CommonWebViewReactor(withWebType: type, url: url)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// 영상보관함
    private func videoStoragePage() -> FlowContributors {
        let controller = VideoStoragePagerViewController.instantiate()
        let reactor = VideoStoragePagerViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func videoSearchResultPage(keyword: String) ->FlowContributors {
        let controller = CommonVideoListViewController.instantiate()
        let reactor = CommonVideoListViewReactor(keyword: keyword)
        controller.reactor = reactor
        
        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func activityCertPage(model: ArtistSelectDataListModel, isYesterDay: Bool) ->FlowContributors {
        let controller = ActivityCertViewController.instantiate()
        let reactor = ActivityCertViewReactor(withArtistModel: model, isYesterDay: isYesterDay)
        controller.reactor = reactor
        
        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func ppobKiPage() ->FlowContributors {
        let controller = PpobKiViewController.instantiate()
        let reactor = PpobKiViewReactor()
        controller.reactor = reactor
        
        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// 포인트 순위
    private func pointRankPage() -> FlowContributors {
        let controller = PointRankPagerViewController.instantiate()
        let reactor = PointRankPagerViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }

    /// 투표권 순위
    func voteRankPage() -> FlowContributors {
        let controller = VoteRankPagerViewController.instantiate()
        let reactor = VoteRankPagerViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// 투표권 내역 리스트
    private func voteHistoryPage() -> FlowContributors {
        let controller = VoteHistoryMainPagerViewController.instantiate()
        let reactor = VoteHistoryMainPagerViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    /// 포인트 내역 리스트
    private func pointHistoryPage() -> FlowContributors {
        let controller = PointHistoryMainPagerViewController.instantiate()
        let reactor = PointHistoryMainPagerViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }

}

// MARK: 닫기 (dismiss, pop)
private extension Tab5Flow {
    func popDismissRoot(animated: Bool = true) -> FlowContributors{
        self.rootViewController.popToRootViewController(animated: animated)
        return .none

    }
    func dismissScreen(type: NavigationType = .push, animated: Bool = true) -> FlowContributors {
        switch type {
        case .push:
            if self.rootViewController.viewControllers.count == 1 {
                return dismissScreen(type: .modal, animated: true)
            }
            self.rootViewController.popViewController(animated: animated)
        case .modal:
            self.rootViewController.dismiss(animated: animated, completion: {
            })
        }
        return .none
    }
}

class Tab5Stepper: Stepper {
    var steps = PublishRelay<Step>()
    var step: CommonStep?
    convenience init(step: CommonStep) {
        self.init()
        self.step = step
    }
    var initialStep: Step {
        if let step = self.step {
            return step
        }
        return CommonStep.tab5Main
    }
}

