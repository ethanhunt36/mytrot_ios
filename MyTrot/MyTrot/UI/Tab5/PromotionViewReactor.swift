//
//  PromotionViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/07/06.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class PromotionViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case getPromotionList
        case getPromotionListByDefault

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(ResponsePromotionList)
        case setResponseDefault(ResponsePromotionList)
        case setAddtionalLoading(Bool)
        case setLastNo(Int64)
        case empty

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var last_no: Int64
        var isAddtionalLoading: Bool
        var item: [PromotionListInfoModel]

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            last_no: 0,
            isAddtionalLoading: false,
            item: []
        )
    }
    
    func mutate(action: PromotionViewReactor.Action) -> Observable<PromotionViewReactor.Mutation> {
        switch action {
        case .getPromotionListByDefault:
            return networkService.getPromotionList(last_no: 0).asObservable().map { Mutation.setResponseDefault($0)}
        case .getPromotionList:
            return networkService.getPromotionList(last_no: currentState.last_no).asObservable().map { Mutation.setResponse($0)}

        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: PromotionViewReactor.State, mutation: PromotionViewReactor.Mutation) -> PromotionViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setResponseDefault(let response):
            if response.isResponseStatusSuccessful() {
                state.last_no = response.data?.last_no ?? 0
                let listData = response.data?.list_data ?? []

                state.item = listData

            }

            
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.last_no = response.data?.last_no ?? 0
                let listData = response.data?.list_data ?? []

                state.item.append(contentsOf: listData)
            } else {
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)

            }
        case .setLastNo(let lastNo):
            state.last_no = lastNo
        case .setAddtionalLoading(let flag):
            state.isAddtionalLoading = flag
        case .empty: return state

        }
        return state
    }
}
