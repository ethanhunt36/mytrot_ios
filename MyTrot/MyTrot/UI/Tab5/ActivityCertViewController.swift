//
//  ActivityCertViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/06/08.
//

import Foundation

import UIKit
import ReactorKit
import Kingfisher

final class ActivityCertViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
        @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var playCountLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var donateCountLabel: UILabel!
    @IBOutlet weak var cheerCountLabel: UILabel!
    @IBOutlet weak var galleryCountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var fanCountLabel: UILabel!

    @IBOutlet weak var downloadButton: UIButton!{
        didSet {
            downloadButton.layer.borderWidth = 1.0
            downloadButton.layer.borderColor = Asset.purpley.color.cgColor
            downloadButton.layer.cornerRadius = 20.0
            downloadButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var artistImageView: UIImageView!

    @IBOutlet weak var capturedFrameView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: ActivityCertViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension ActivityCertViewController {
    func bindAction(_ reactor: ActivityCertViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        downloadButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                UIGraphicsBeginImageContextWithOptions(self?.capturedFrameView.bounds.size ?? CGSize.zero, self?.capturedFrameView.isOpaque ?? false, 0.0)
                defer { UIGraphicsEndImageContext() }
                if let context = UIGraphicsGetCurrentContext() {
                    self?.capturedFrameView.layer.render(in: context)
                    guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return }
                    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                    Toast(text: "앨범에 저장 되었습니다.", duration: 1.0).show()
                }
            }).disposed(by: disposeBag)

        
    }
}

// MARK: bindState For Reactor
private extension ActivityCertViewController {
    func bindState(_ reactor: ActivityCertViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.screenState}
            .distinctUntilChanged()
            .filter{ $0 == .none }
            .map { _ in return Reactor.Action.getCertInfo }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        reactor.state.map { $0.screenState}
            .distinctUntilChanged()
            .filter{ $0 == .finishToGetCert }
            .map { _ in return Reactor.Action.getCertMission }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        reactor.state.map { $0.artistModel }
            .map { $0.name}
            .bind(to: artistNameLabel.rx.text)
            .disposed(by: disposeBag)
        
        reactor.state.map { $0.nickName }
            .bind(to: fanCountLabel.rx.text)
            .disposed(by: disposeBag)
        
        reactor.state.map { $0.today }
            .bind(to: dateLabel.rx.text)
            .disposed(by: disposeBag)
        
        reactor.state.map { $0.artistModel }
            .map { $0.pic}
            .filter{ $0 != ""}
            .take(1)
            .subscribe(onNext: { [weak self] data in
                if let imgUrl = URL(string: data) {
                    self?.artistImageView.kf.setImage(with: imgUrl)
                    self?.artistImageView.layer.cornerRadius = 30.0
                    self?.artistImageView.layer.masksToBounds = true
                }
            }).disposed(by: disposeBag)


        
        reactor.state.map { $0.certInfo}
            .filterNil()
            .map { $0.data }
            .subscribe(onNext: { [weak self] data in
                self?.playCountLabel.text = "\(data?.pnt_play ?? "0")회"
                self?.voteCountLabel.text = "\(data?.pnt_vote ?? "0")장"
                self?.donateCountLabel.text = "\(data?.pnt_donate ?? "0")P"
                self?.cheerCountLabel.text = "\(data?.pnt_board1 ?? 0)회"
                self?.galleryCountLabel.text = "\(data?.pnt_board2 ?? 0)회"
                

            }).disposed(by: disposeBag)

            
    }
}

// MARK: -
// MARK: private initialize
private extension ActivityCertViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension ActivityCertViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
