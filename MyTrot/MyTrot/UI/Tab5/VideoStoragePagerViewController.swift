//
//  VideoStorageViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import XLPagerTabStrip
import ReactorKit
import Reusable
protocol VideoStoragePagerDelegate: AnyObject {
    func moveToVideoPage(artist_no: Int64, name: String)
    func moveToYoutubePlayerPage(link: String, list: [ArtistVideoListModel])
}
class VideoStoragePagerViewController: ButtonBarPagerTabStripViewController, StoryboardView , StoryboardBased{
    var disposeBag = DisposeBag()

    @IBOutlet weak var backButton: UIButton!

    func bind(reactor: VideoStoragePagerViewReactor) {
        bindAction(reactor)
        bindState(reactor)

    }
    
    override func viewDidLoad() {
        // change selected bar color
        print("VideoStoragePagerViewController viewDidLoad")
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = Asset.custard.color
        settings.style.buttonBarItemFont =  UIFont.boldSystemFont(ofSize: 16)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true

        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = Asset.bottomTabDisabled.color
            newCell?.label.textColor = Asset.black.color
        }
        super.viewDidLoad()
    }

    // MARK: - PagerTabStripDataSource

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let videoStorageSingerViewController = VideoStorageSingerViewController.instantiate()
        let videoStorageSingerViewReactor = VideoStorageSingerViewReactor()
        videoStorageSingerViewController.reactor = videoStorageSingerViewReactor
        videoStorageSingerViewController.itemInfo = IndicatorInfo(title: "가수")
        videoStorageSingerViewController.pagerDelegate = self
        
        let videoStorageTrotViewController = VideoStorageTrotViewController.instantiate()
        let videoStorageTrotViewReactor = VideoStorageTrotViewReactor()
        videoStorageTrotViewController.reactor = videoStorageTrotViewReactor
        videoStorageTrotViewController.itemInfo = IndicatorInfo(title: "트롯")
        videoStorageTrotViewController.pagerDelegate = self

        return [videoStorageSingerViewController, videoStorageTrotViewController]
    }

    // MARK: - Actions

}

// MARK: -
// MARK: bindAction For Reactor
private extension VideoStoragePagerViewController {
    func bindAction(_ reactor: VideoStoragePagerViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        
    }
}

// MARK: bindState For Reactor
private extension VideoStoragePagerViewController {
    func bindState(_ reactor: VideoStoragePagerViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        
    }
}

// MARK: -
// MARK: private initialize
private extension VideoStoragePagerViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}

extension VideoStoragePagerViewController: VideoStoragePagerDelegate {
    func moveToVideoPage(artist_no: Int64, name: String) {
        guard let reactor = reactor else { return }
        reactor.action.onNext(.moveToVideoPage(artistNo: artist_no))
    }
    
    func moveToYoutubePlayerPage(link: String, list: [ArtistVideoListModel]) {
        guard let reactor = reactor else { return }

        reactor.action.onNext(.moveToStepScreen(CommonStep.playYoutube(link: link, list: list)))

    }

}
