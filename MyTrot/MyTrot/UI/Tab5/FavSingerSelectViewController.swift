//
//  FavSingerSelectViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit

final class FavSingerSelectViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    enum Define {
        static let screenWidth: CGFloat = UIScreen.main.bounds.width
    }

    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var favView: UIView!
    
    @IBOutlet weak var collectionViewTopAnchor: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    var singerList: [ArtistSelectDataListModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: FavSingerSelectViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension FavSingerSelectViewController {
    func bindAction(_ reactor: FavSingerSelectViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        favButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.checkMyFav }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        reactor.action.onNext(.getArtistList)
        print("my fav artist no : \(User.shared.myInfo?.artist_no) ")
    }
}

// MARK: bindState For Reactor
private extension FavSingerSelectViewController {
    func bindState(_ reactor: FavSingerSelectViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .subscribe(onNext: { [weak self] items in
                self?.singerList.removeAll()
                self?.singerList.append(contentsOf: items)
                self?.collectionView.reloadData()
            }).disposed(by: disposeBag)

        reactor.state.map { $0.titleString }
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
        
        reactor.state.map { $0.screenType }
            .subscribe(onNext: { [weak self] screenType in
                switch screenType {
                case .choiceFavSinger:
                    self?.collectionViewTopAnchor.constant = -50
                    self?.favView.isHidden = true
                case .choiceSinger,
                     .choiceUploadSinger,
                     .activityCertification:
                    self?.collectionViewTopAnchor.constant = 0
                    self?.favView.isHidden = false
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.myFavType }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] myFavType in
                switch myFavType {
                case .nothing:
                    self?.favButton.isSelected = false
                case .nonCheck:
                    self?.favButton.isSelected = false
                case .topCheck:
                    self?.favButton.isSelected = true
                }
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension FavSingerSelectViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeCollectionLayout()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension FavSingerSelectViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
extension FavSingerSelectViewController {
    func initializeCollectionLayout() {
        // ios 10
        let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        
        let totalMargine: CGFloat = 30.0
        let labelHeight: CGFloat = 30
        let collectionViewCellwidth: CGFloat = (Define.screenWidth - totalMargine )/3
        let collectionViewCellheight = collectionViewCellwidth + labelHeight
        print("collectionViewCellwidth : \(collectionViewCellwidth)")
        print("collectionViewCellheight : \(collectionViewCellheight)")

        // 고정된 크기
        flowLayout?.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)

        flowLayout?.estimatedItemSize = CGSize(width: collectionViewCellwidth, height: collectionViewCellheight)
        flowLayout?.estimatedItemSize = CGSize(width: collectionViewCellwidth, height: 170)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(cellType: FavSingerCollectionCell.self)



    }
}
// MARK: -
extension FavSingerSelectViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //위아래 라인 간격

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return 1

    }

    //옆 라인 간격

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        return 0.5

    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath) as FavSingerCollectionCell
        cell.item = singerList[indexPath.row]
        return cell
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return singerList.count
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAt: \(singerList[indexPath.row])")

        guard let reactor = reactor else { return }
        if reactor.currentState.screenType == .choiceFavSinger {
            reactor.action.onNext(.uploadMyArtist(singerList[indexPath.row].no))
        } else if reactor.currentState.screenType == .choiceUploadSinger {
            RxBus.shared.post(event: Events.UploadSelectArtistInfo(artist_no: singerList[indexPath.row].no, artist_name: singerList[indexPath.row].name))
            reactor.action.onNext(.dismiss)
        } else if reactor.currentState.screenType == .choiceSinger {
            RxBus.shared.post(event: Events.SelectArtistInfo(artist_no: singerList[indexPath.row].no, artist_name: singerList[indexPath.row].name))
            reactor.action.onNext(.dismiss)

        } else if reactor.currentState.screenType == .activityCertification {
            let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

            let today =  UIAlertAction(title: "오늘", style: .default) { [weak self] (action) in
                print("신고하기")
                guard let reactor = self?.reactor else { return }
                guard let singlerModel = self?.singerList[indexPath.row] else { return }
                reactor.action.onNext(.moveToStepScreen(.activityCertPage(artistModel: singlerModel, isYesterDay: false)))

            }
            let yesterday =  UIAlertAction(title: "어제", style: .default) { [weak self] (action) in
                print("신고하기")
                guard let reactor = self?.reactor else { return }
                guard let singlerModel = self?.singerList[indexPath.row] else { return }

                reactor.action.onNext(.moveToStepScreen(.activityCertPage(artistModel: singlerModel, isYesterDay: true)))

            }


            let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
            alert.addAction(yesterday)
            alert.addAction(today)
            alert.addAction(cancel)

            present(alert, animated: true, completion: nil)


        }
    }
}
