//
//  GalleryDetailViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift
final class GalleryDetailViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case getDetail
        case sendComment(String)
        case setUploadStatus(UploadStatus)
        case deleteBoard
        case deleteComment(Int64)
        case like
        case unLike
        case savePhoto(UIImage)
        
        case blockAllBoard

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponseBoardDetail(ResponseBoardDetail)
        case setResponseBoardComment(CommonStringResponse)
        case setUploadStatus(UploadStatus)
        case empty
        case setDeleteMessage(String)
        case setCommentDeleteMessage(String?)
        case setLike(Bool)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var board_no: Int64
        var viewModel: BoardViewModel?
        var categoryType: BoardCategoryType
        var uploadStatus: UploadStatus
        var del_yn: String
        var deleteMessage: String?
        var commentDeleteMessage: String?
        var isLike: Bool?
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(no board_no: Int64) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            board_no: board_no,
            categoryType: .gallery,
            uploadStatus: .none,
            del_yn: "N"
        )
    }
    init(promotion board_no: Int64) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            board_no: board_no,
            categoryType: .hongbo,
            uploadStatus: .none,
            del_yn: "N"

        )
    }

    func mutate(action: GalleryDetailViewReactor.Action) -> Observable<GalleryDetailViewReactor.Mutation> {
        switch action {
        case .blockAllBoard:
            return networkService.blockAllBoard(boardNo: currentState.board_no).asObservable().map { response in
                if response.isResponseStatusSuccessful() {
                    RxBus.shared.post(event: Events.GalleryUploadFinish())

                    return .setDeleteMessage(response.msg ?? Constants.String.AlertMessage.blockAllUserSuccess)
                } else {
                    Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)

                    return .empty
                }
            }

        case .savePhoto(let image):
            if preferencesService.getIsUserAgreeTerms() == false {
                Utils.AlertShow(msg: Constants.String.AlertMessage.agreementDeniedMessage)
                
                return .empty()
            }

            return networkService.imageDownload(board_no: currentState.board_no).asObservable().map {
                res in
                if res.isResponseStatusSuccessful() == false {
                    Utils.AlertShow(msg: res.msg ?? Constants.Network.ErrorMessage.networkError)
                    return Mutation.empty
                } else {

                    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                    Toast(text: "앨범에 저장 되었습니다.", duration: 1.0).show()
                    return Mutation.setUploadStatus(.done)
                }
                
            }
        case .like:
            return networkService.boardLike(board_no: currentState.board_no).asObservable().map { _ in Mutation.setLike(true)}
        case .unLike:
            return networkService.boardUnLike(board_no: currentState.board_no).asObservable().map { _ in Mutation.setLike(false)}

        case .deleteComment(let cmt_no):
            return networkService.deleteComment(comment_no: cmt_no).asObservable().map { response in
                if response.isResponseStatusSuccessful() {
                    Utils.AlertShow(msg: response.msg ?? "삭제되었습니다.")

                    return .setCommentDeleteMessage(response.msg ?? "삭제되었습니다.")
                } else {
                    return .empty
                }
                
            }

        case .deleteBoard:
            return networkService.deleteBoard(board_no: currentState.board_no).asObservable().map { response in
                if response.isResponseStatusSuccessful() {
                    RxBus.shared.post(event: Events.GalleryUploadFinish())

                    return .setDeleteMessage(response.msg ?? "삭제되었습니다.")
                } else {
                    return .empty
                }
                
            }

        case .setUploadStatus(let status):
            return .just(.setUploadStatus(status))
        case .sendComment(let cont):
            if preferencesService.getIsUserAgreeTerms() == false {
                Utils.AlertShow(msg: Constants.String.AlertMessage.agreementDeniedMessage)
                
                return .empty()
            }

            if cont.count == 0 {
                RxBus.shared.post(event: Events.AlertShow(message: "댓글을 입력해 주세요"))
                return .just(.empty)
            }
            return .concat([
                .just(.setUploadStatus(.processing)),
                networkService.uploadComment(board_no: currentState.board_no, cont: cont).asObservable().map { Mutation.setResponseBoardComment($0)}
            ])
        case .getDetail:
            return networkService.boardDetail(board_no: currentState.board_no, lastNo: -1, category: currentState.categoryType).asObservable().map { Mutation.setResponseBoardDetail($0)}

        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }

    func reduce(state: GalleryDetailViewReactor.State, mutation: GalleryDetailViewReactor.Mutation) -> GalleryDetailViewReactor.State {
        var state = state
        switch mutation {
        case .setLike(let isLike):
            state.isLike = isLike

        case .setResponseBoardComment(let response):
            if response.isResponseStatusSuccessful() {
                state.uploadStatus = .done
            }
            Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError )

        case .setResponseBoardDetail(let response):
            if response.isResponseStatusSuccessful() {
            
                var sectionList: [BoardDetailCellItem] = []
                // 공통 상단 아이템
                guard var topItem = response.data?.view_data else { return state}

                if currentState.categoryType == .gallery {
                    topItem.is_like = (response.data?.is_ilike ?? 0) == 1 ? "Y" : "N"
                    let topContentsItem = BoardTopItem(withResponse: topItem)
                    sectionList.append(topContentsItem)


                } else if currentState.categoryType == .hongbo{

                    // 홍보인경우
                    let img01 = topItem.img01 ?? ""
                    let img02 = topItem.img02 ?? ""
                    let img03 = topItem.img03 ?? ""
                    var bottomImageUrl = ""
                    if img03 != "" {
                        let imgItem1 = BoardImageItem(withResponse: img01)
                        let imgItem2 = BoardImageItem(withResponse: img02)
                        sectionList.append(imgItem1)
                        sectionList.append(imgItem2)

                        bottomImageUrl = img03
                    } else if img02 != "" {
                        let imgItem1 = BoardImageItem(withResponse: img01)
                        sectionList.append(imgItem1)

                        bottomImageUrl = img02
                    } else {
                        bottomImageUrl = img01
                    }
                    topItem.is_like = (response.data?.is_ilike ?? 0) == 1 ? "Y" : "N"
                    let topContentsItem = BoardTopItem(withResponse: topItem, imgUrl: bottomImageUrl)
                    sectionList.append(topContentsItem)

                }
                
                if let member_no = topItem.member_info.member_no?.value as? String , member_no == User.shared.myInfo?.member_no {
                    state.del_yn = "Y"
                } else {
                    state.del_yn = "N"
                }

                if let commentData = response.data?.comment_data, commentData.list_data?.count ?? 0 > 0 {
                    if let listData = commentData.list_data {
                        for commentItem in listData {
                            let replyItem = BoardReplyItem(withResponse: commentItem)
                            sectionList.append(replyItem)
                        }

                    }
                }
                let mainModel = BoardViewModel(sectionList)
                state.viewModel = mainModel


            }
        case .setUploadStatus(let status):
            state.uploadStatus = status

        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty: return state
        case .setDeleteMessage(let message):
            state.deleteMessage = message
        case .setCommentDeleteMessage(let message):
            state.commentDeleteMessage = message

        }
        return state
    }
}
