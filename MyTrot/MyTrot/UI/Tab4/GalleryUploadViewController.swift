//
//  GalleryUploadViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit
import MobileCoreServices

final class GalleryUploadViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var singerLabel: UILabel!
    @IBOutlet weak var singerSelectButton: UIButton!

    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var uploadTextView: UITextView!
    @IBOutlet weak var uploadSendButton: UIButton!
    @IBOutlet weak var uploadImageButton: UIButton!
    @IBOutlet weak var uploadImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: GalleryUploadViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension GalleryUploadViewController {
    func bindAction(_ reactor: GalleryUploadViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        uploadSendButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { [weak self] _ in
                Reactor.Action.uploadBoard(cont: self?.uploadTextView.text ?? "") }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        singerSelectButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.moveToStepScreen(.favSingerSelectPage(type: .choiceUploadSinger)) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        uploadImageButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.openPhotoAlert()
            }).disposed(by: disposeBag)

    }
}

// MARK: bindState For Reactor
private extension GalleryUploadViewController {
    func bindState(_ reactor: GalleryUploadViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.singerName}
            .bind(to: singerLabel.rx.text)
            .disposed(by: disposeBag)
        
        reactor.state.map { $0.alertBackMessage}
            .distinctUntilChanged()
            .filterNil()
            .subscribe(onNext: { [weak self] alertBackMessage in
                self?.subscribeOnNextBaseAlertPushBack(alertData: alertBackMessage)
                
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension GalleryUploadViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureScrollView))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(singleTapGestureRecognizer)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }

    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.UploadSelectArtistInfo.self, sticky: true).subscribe { [weak self] event in
            
            if let artist_no = event.element?.artist_no , let artist_name = event.element?.artist_name {
                guard let reactor = self?.reactor else { return }
                reactor.action.onNext(.setSingerName(artist_name))
                reactor.action.onNext(.setSingerNo(artist_no))
            }
            

        }.disposed(by: disposeBag)
    }
}

// MARK: Private Function
private extension GalleryUploadViewController {
    func openPhotoAlert() {
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        let library =  UIAlertAction(title: "사진앨범", style: .default) { [weak self] (action) in
            self?.openLibrary()
        }


        let camera =  UIAlertAction(title: "카메라", style: .default) { [weak self] (action) in
            self?.openCamera()

        }
        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        alert.addAction(library)

        alert.addAction(camera)

        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)


    }
    
    func openCamera() {
        let pickerController = UIImagePickerController()
        // Part 1: File origin
        pickerController.sourceType = .camera
        
        // Must import `MobileCoreServices`
        // Part 2: Define if photo or/and video is going to be captured by camera
        pickerController.mediaTypes = [kUTTypeImage as String]
        
        // Part 3: camera settings
        pickerController.cameraCaptureMode = .photo // Default media type .photo vs .video
        pickerController.cameraDevice = .rear // rear Vs front
        // Part 4: User can optionally crop only a certain part of the image or video with iOS default tools
        pickerController.allowsEditing = true
        
        // Part 5: For callback of user selection / cancellation
        pickerController.delegate = self

        // Part 6: Present the UIImagePickerViewController
        present(pickerController, animated: true, completion: nil)

    }
    func openLibrary() {
        let pickerController = UIImagePickerController()
        
        /*
        * Part 1: Select the origin of media source
        Either one of the belows:
        1. .photoLibrary     -> Go to album selection page
        2. .savedPhotosAlbum -> Go to Moments directly
        */
        pickerController.sourceType = .photoLibrary
        
        // Must import `MobileCoreServices`
        // Part 2: Allow user to select both image and video as source
        pickerController.mediaTypes = [kUTTypeImage as String]

        // Part 3: User can optionally crop only a certain part of the image or video with iOS default tools
        pickerController.allowsEditing = true
        
        // Part 4: For callback of user selection / cancellation
        pickerController.delegate = self
        
        // Part 5: Show UIImagePickerViewController to user
        present(pickerController, animated: true, completion: nil)

    }
    @objc func tapGestureScrollView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        bottomAnchorConstraint.constant = ((view.convert(keyboardFrame.cgRectValue, from: nil).size.height) - view.safeAreaInsets.bottom - 80 )
        print("keyboard height : \((view.convert(keyboardFrame.cgRectValue, from: nil).size.height))")
        print("view.safeAreaInsets.bottom : \(view.safeAreaInsets.bottom)")

//        commentView.isHidden = false
//
//        print("view.safeAreaInsets.top  :  \(view.safeAreaInsets.top )")
//        print("view.safeAreaInsets.bottom  :  \(view.safeAreaInsets.bottom )")
//        UIView.animate(withDuration: 0.30) { [weak self] in
//            self?.view.layoutIfNeeded()
//        }
        placeHolderLabel.isHidden = true
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        bottomAnchorConstraint.constant = 0
        if uploadTextView.hasText == false {
            placeHolderLabel.isHidden = false
        }
    }

}

// MARK: - Reactor Action
private extension GalleryUploadViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
extension GalleryUploadViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! CFString
        switch mediaType {
        case kUTTypeImage:
          // Handle image selection result
            print("Selected media is image")
            let editedImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
            uploadImageView.image = editedImage
            
            guard let reactor = reactor else { return }
            guard let imgData = editedImage.jpegData(compressionQuality: 0.5) else { return }
            reactor.action.onNext(.setImageData(imgData))
            
//          let originalImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
//          fooOriginalImageView.image = originalImage

        case kUTTypeMovie:
          // Handle video selection result
            print("Selected media is video === > false")
            
//          let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as! URL
          
        default:
            print("Mismatched type: \(mediaType)")
        }
        picker.dismiss(animated: true, completion: nil)

    }

}
