//
//  ChartFlow.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
import RxFlow
import RxRelay

final class Tab4Flow: Flow {
    var root: Presentable {
        return self.rootViewController
    }

    private lazy var rootViewController: BaseNavigationController = {
        let viewController = BaseNavigationController()
        viewController.setNavigationBarHidden(true, animated: false)
        return viewController
    }()

    private let myStepper: Tab4Stepper

    init(withStepper stepper: Tab4Stepper) {
        self.myStepper = stepper
    }

    deinit { print("\(type(of: self)): \(#function)") }

    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? CommonStep else { return FlowContributors.none }

        switch step {
        case .tab4Main:
            return navigateToMainScreen()
        /// 갤러리 등록
        case .galleryUploadPage:
            return galleryUploadPage()
        /// 갤러리 상세
        case .galleryDetailPage(let boardNo):
            return galleryDetailPage(boardNo: boardNo)
        case .favSingerSelectPage(let type):
            return favSingerSelectPage(type: type)

        /// 신고하기
        case .reportPage(let board_no):
            return reportPage(boardNo: board_no)

        case .modalDismiss(let animated):
            return dismissScreen(type: .modal, animated: animated)
        case .pushDismiss(let animated):
            return dismissScreen(type: .push, animated: animated)
        default: return .none
        }
    }
}

// MARK: -
// MARK: 이동 (navigate)
// MARK: -
extension Tab4Flow {
    /// 메인화면 이동
    private func navigateToMainScreen() -> FlowContributors {
        let controller = GalleryMainViewController.instantiate()
        let reactor = GalleryMainViewReactor()
        controller.reactor = reactor

        self.rootViewController.setViewControllers([controller], animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))
    }
    private func galleryUploadPage() -> FlowContributors {
        let controller = GalleryUploadViewController.instantiate()
        let reactor = GalleryUploadViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func galleryDetailPage(boardNo: Int64) -> FlowContributors {
        let controller = GalleryDetailViewController.instantiate()
        let reactor = GalleryDetailViewReactor(no: boardNo)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func favSingerSelectPage(type: FavSingerScreenType) -> FlowContributors {
        let controller = FavSingerSelectViewController.instantiate()
        let reactor = FavSingerSelectViewReactor(screenType: type)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))


    }

    private func reportPage(boardNo: Int64) -> FlowContributors {
        let controller = CommonReportViewController.instantiate()
        let reactor = CommonReportViewReactor(withBoardNo: boardNo)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }

}

// MARK: 닫기 (dismiss, pop)
private extension Tab4Flow {
    func dismissScreen(type: NavigationType = .push, animated: Bool = true) -> FlowContributors {
        switch type {
        case .push:
            if self.rootViewController.viewControllers.count == 1 {
                return dismissScreen(type: .modal, animated: true)
            }
            self.rootViewController.popViewController(animated: animated)
        case .modal:
            self.rootViewController.dismiss(animated: animated, completion: {
            })
        }
        return .none
    }
}

class Tab4Stepper: Stepper {
    var steps = PublishRelay<Step>()
    var step: CommonStep?
    convenience init(step: CommonStep) {
        self.init()
        self.step = step
    }
    var initialStep: Step {
        if let step = self.step {
            return step
        }
        return CommonStep.tab4Main
    }
}

