//
//  GalleryUploadViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class GalleryUploadViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case setSingerName(String)
        case setSingerNo(Int64)
        case uploadBoard(cont: String)
        case setImageData(Data)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setSingerName(String)
        case setSingerNo(Int64)
        case setResponse(CommonResponse)
        case empty
        case setImageData(Data)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var singerName: String
        var singerNo: Int64
        var alertBackMessage: String?
        var imageData: Data?
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            singerName: User.shared.myInfo?.artist_name ?? "" ,
            singerNo: User.shared.myInfo?.artist_no ?? 0

        )
    }
    
    func mutate(action: GalleryUploadViewReactor.Action) -> Observable<GalleryUploadViewReactor.Mutation> {
        switch action {
        case .setImageData(let data):
            return .just(.setImageData(data))
        case .uploadBoard(let cont):
            if preferencesService.getIsUserAgreeTerms() == false {
                Utils.AlertShow(msg: Constants.String.AlertMessage.agreementDeniedMessage)
                
                return .empty()
            }

            guard let sendData = currentState.imageData else { return .empty()}
            return networkService.uploadGalleryBoard(artist_no: currentState.singerNo, name: User.shared.myInfo?.nick ?? "", cont: cont, image: sendData).asObservable().map { Mutation.setResponse($0)}
        case .setSingerName(let name):
            return .just(.setSingerName(name))

        case .setSingerNo(let no):
            return .just(.setSingerNo(no))
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: GalleryUploadViewReactor.State, mutation: GalleryUploadViewReactor.Mutation) -> GalleryUploadViewReactor.State {
        var state = state
        switch mutation {
        case .setSingerName(let name):
            state.singerName = name
        case .setSingerNo(let no):
            state.singerNo = no
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.alertBackMessage = response.msg?.replaceSlashString ?? ""
                RxBus.shared.post(event: Events.GalleryUploadFinish())

            } else {
                RxBus.shared.post(event: Events.AlertShow(message: response.msg ?? ""))
            }
        case .empty:
            return state
        case .setImageData(let data):
            state.imageData = data
        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
