//
//  GalleryDetailViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit

final class GalleryDetailViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var deleteButton: UIButton! {
        didSet {
            deleteButton.layer.cornerRadius = 3.0
        }
    }

    var data = BoardViewModel() {
        didSet {
            tableView.reloadData()
        }
    }
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentViewBottomAnchorConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var commentSendButton: UIButton!
    var isDelete: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: GalleryDetailViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension GalleryDetailViewController {
    func bindAction(_ reactor: GalleryDetailViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        commentSendButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { [weak self] in
                return Reactor.Action.sendComment(self?.commentTextView.text ?? "") }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        deleteButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] model in
                self?.openDeleteAlert()
            }).disposed(by: disposeBag)

        reactor.action.onNext(.getDetail)

    }
}

// MARK: bindState For Reactor
private extension GalleryDetailViewController {
    func bindState(_ reactor: GalleryDetailViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.viewModel }
            .filter({ $0 != nil })
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] model in
                guard let strongModel = model else { return }
                self?.data = strongModel
            }).disposed(by: disposeBag)

        reactor.state.map { $0.uploadStatus}
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] status in
                switch status {
                case .done:
                    self?.commentTextView.text = ""
                    self?.placeHolderLabel.isHidden = false
                    self?.commentTextView.resignFirstResponder()
                    reactor.action.onNext(.setUploadStatus(.none))
                    reactor.action.onNext(.getDetail)
                default: break;
                }
                
            }).disposed(by: disposeBag)
        reactor.state.map { $0.deleteMessage }
            .filterNil()
            .subscribe(onNext: { [weak self] message in
                self?.subscribeOnNextBaseAlertPushBack(alertData: message)

            }).disposed(by: disposeBag)

        reactor.state.map { $0.commentDeleteMessage }
            .filterNil()
            .distinctUntilChanged()
            .subscribe(onNext: { _ in

                reactor.action.onNext(.getDetail)
            }).disposed(by: disposeBag)
        reactor.state.map { $0.isLike }
            .filterNil()
            .distinctUntilChanged()
            .subscribe(onNext: { _ in

                reactor.action.onNext(.getDetail)
            }).disposed(by: disposeBag)
        reactor.state.map { $0.del_yn }
            
            .subscribe(onNext: { [weak self] del_yn in
                self?.isDelete = del_yn == "Y"
            }).disposed(by: disposeBag)
        
        reactor.state.map { $0.categoryType}
            .filter{ $0 == .hongbo }
            .subscribe(onNext: { [weak self] _ in
                self?.topTitleLabel.text = "홍보 인증 상세"
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension GalleryDetailViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()

    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureScrollView))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(singleTapGestureRecognizer)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
    
    func openDeleteAlert() {
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        let deleteArticle =  UIAlertAction(title: "삭제", style: .default) { [weak self] (action) in
            print("삭제!!")
            let controller = UIAlertController.init(title: "", message: "삭제하시겠습니까?", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "확인", style: .default, handler: { [weak self] _ in
                guard let reactor = self?.reactor else { return }
                reactor.action.onNext(.deleteBoard)
            })
            let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)

            controller.addAction(defaultAction)
            controller.addAction(cancel)

            self?.present(controller, animated: true)

        }
        let reportArticle =  UIAlertAction(title: "신고하기", style: .default) { [weak self] (action) in
            print("신고하기")
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.moveToStepScreen(.reportPage(boardNo: reactor.currentState.board_no)))

        }
        let blockArticle =  UIAlertAction(title: "작성자의 글 모두 차단하기", style: .destructive) { [weak self] (action) in
            print("차단하기")
            
            let controller = UIAlertController.init(title: "", message: "작성자의 글 모두를 차단 하시겠습니까?", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "확인", style: .default, handler: { [weak self] _ in
                guard let reactor = self?.reactor else { return }
                reactor.action.onNext(.blockAllBoard)
            })
            let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)

            controller.addAction(defaultAction)
            controller.addAction(cancel)

            self?.present(controller, animated: true)


        }


        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        if isDelete == true {
            alert.addAction(deleteArticle)
        } else {
            alert.addAction(reportArticle)
            alert.addAction(blockArticle)

        }

        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)


    }
    func openCommentDeleteAlert(item: BoardDetailCommentDataListModel) {
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        let deleteArticle =  UIAlertAction(title: "삭제", style: .default) { [weak self] (action) in
            print("삭제!!")
            let controller = UIAlertController.init(title: "", message: "삭제하시겠습니까?", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "확인", style: .default, handler: { [weak self] _ in
                guard let reactor = self?.reactor else { return }
                reactor.action.onNext(.deleteComment(item.no ?? 0))
            })
            let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)

            controller.addAction(defaultAction)
            controller.addAction(cancel)

            self?.present(controller, animated: true)

        }


        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        alert.addAction(deleteArticle)

        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)


    }

}


// MARK: Private Function
private extension GalleryDetailViewController {
    @objc func tapGestureScrollView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        commentViewBottomAnchorConstraint.constant = -1 * ((view.convert(keyboardFrame.cgRectValue, from: nil).size.height) - view.safeAreaInsets.bottom )
//        commentView.isHidden = false
//
//        print("view.safeAreaInsets.top  :  \(view.safeAreaInsets.top )")
//        print("view.safeAreaInsets.bottom  :  \(view.safeAreaInsets.bottom )")
//        UIView.animate(withDuration: 0.30) { [weak self] in
//            self?.view.layoutIfNeeded()
//        }
        placeHolderLabel.isHidden = true
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        commentViewBottomAnchorConstraint.constant = 0
        if commentTextView.hasText == false {
            placeHolderLabel.isHidden = false
        }
    }

}

// MARK: - Reactor Action
private extension GalleryDetailViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
// MARK: -
extension GalleryDetailViewController {
    func initializeTableView() {
        self.tableView.register(cellType: CheerDetailReplyCell.self)
        self.tableView.register(cellType: GalleryDetailTopCell.self)
        self.tableView.register(cellType: GalleryDetailImageCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension GalleryDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        if indexPath.row == 0 { return }
//        if let delegate = pagerDelegate {
//            delegate.moveToVoteMainPage(model: tableViewList[indexPath.row])
//        }
    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = data[indexPath.row]
        switch item.type {
        case .top:
            let cell = tableView.dequeueReusableCell(for: indexPath) as GalleryDetailTopCell
            let replyItem = item as? BoardTopItem
            cell.imgString = replyItem?.url
            cell.item = replyItem?.response
            cell.delegate = self
            return cell

        case .image:
            
            let cell = tableView.dequeueReusableCell(for: indexPath) as GalleryDetailImageCell
            let imageItem = item as? BoardImageItem
            cell.item = imageItem?.url
            return cell
        case .reply:
            let cell = tableView.dequeueReusableCell(for: indexPath) as CheerDetailReplyCell
            let replyItem = item as? BoardReplyItem
            cell.item = replyItem?.response
            cell.delegate = self
            return cell

        }
        
    }
}

extension GalleryDetailViewController: CheerDetailReplyCellDelegate {
    func replyDeleteButton(item: BoardDetailCommentDataListModel) {
        print("delete!")
        openCommentDeleteAlert(item: item)
    }
    
    
}

extension GalleryDetailViewController: GalleryDetailTopCellDelegate {
    func savePhoto(image: UIImage) {
        print("savePhoto")
        guard let reactor = reactor else { return }

        reactor.action.onNext(.savePhoto(image))

    }

    func likeButton(isLikeOn: Bool) {
        guard let reactor = reactor else { return }
        if isLikeOn {
            reactor.action.onNext(.like)
        } else {
            reactor.action.onNext(.unLike)
        }
    }
    
    
}
