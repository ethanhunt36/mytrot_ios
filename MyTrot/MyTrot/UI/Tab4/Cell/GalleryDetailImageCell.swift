//
//  GalleryDetailImageCell.swift
//  MyTrot
//
//  Created by hclim on 2021/07/12.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

class GalleryDetailImageCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
    }



    @IBOutlet weak var galleryImageView: UIImageView!


    var item: String? {
        didSet {
            galleryImageView.kf.setImage(with: URL(string: item?.validateHostImageUrl ?? ""))

        }
    }

}
