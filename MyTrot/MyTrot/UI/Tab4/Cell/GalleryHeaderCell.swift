//
//  GalleryHeaderCell.swift
//  MyTrot
//
//  Created by hclim on 2021/04/04.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
protocol GalleryHeaderCellDelegate: AnyObject {
    func sortList(type: GalleryListType)
    func checkFavTopSinger(isCheck: Bool)
    func checkTotalSinger(isCheck: Bool)
}
class GalleryHeaderCell: UICollectionReusableView {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()
    weak var delegate: GalleryHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    @IBOutlet weak var recentButton: UIButton! {
        didSet {
            recentButton.customCornerRadius = 10.0

            recentButton.setBackgroundColor(UIColor.clear, for: .normal)
            recentButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)

            recentButton.setTitleColor(Asset.tangerine.color, for: .normal)
            recentButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var popularButton: UIButton! {
        didSet {
            popularButton.customCornerRadius = 10.0

            popularButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            popularButton.setBackgroundColor(UIColor.clear, for: .normal)
            popularButton.setTitleColor(Asset.tangerine.color, for: .normal)
            popularButton.setTitleColor(Asset.tangerine.color, for: .selected)

        }
    }
    @IBOutlet weak var commentTotalButton: UIButton! {
        didSet {
            commentTotalButton.customCornerRadius = 10.0

            commentTotalButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            commentTotalButton.setBackgroundColor(UIColor.clear, for: .normal)
            commentTotalButton.setTitleColor(Asset.tangerine.color, for: .normal)
            commentTotalButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var totalSingerButton: UIButton!
    @IBOutlet weak var favSingerButton: UIButton!

    var item: GalleryHeaderDataModel? {
        didSet {
            guard let cellModel = item else { return }
            favSingerButton.setTitle("\(cellModel.favSingerName) X", for: .normal)

            if cellModel.isSelectedFavSinger == true {
                totalSingerButton.isHidden = true
                favSingerButton.isHidden = false
            } else {
                totalSingerButton.isHidden = false
                favSingerButton.isHidden = true
            }
            recentButton.isSelected = false
            popularButton.isSelected = false
            commentTotalButton.isSelected = false

            switch cellModel.viewListType {
            case CheerListType.recent.rawValue:
                recentButton.isSelected = true
            case CheerListType.popular.rawValue:
                popularButton.isSelected = true
            case CheerListType.total.rawValue:
                commentTotalButton.isSelected = true
                totalSingerButton.isHidden = true
                favSingerButton.isHidden = true

            default:
                break;
            }
        }
    }
    
    func initializeEvent() {
        favSingerButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.isSelectedFavSinger = true
                self?.delegate?.checkFavTopSinger(isCheck: true)
            }).disposed(by: disposeBag)
        totalSingerButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.isSelectedFavSinger = false
                self?.delegate?.checkTotalSinger(isCheck: false)
            }).disposed(by: disposeBag)
        recentButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.viewListType = CheerListType.recent.rawValue
                self?.delegate?.sortList(type: .recent)
            }).disposed(by: disposeBag)
        popularButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.viewListType = CheerListType.popular.rawValue
                self?.delegate?.sortList(type: .popular)
            }).disposed(by: disposeBag)
        commentTotalButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.viewListType = CheerListType.total.rawValue
                self?.delegate?.sortList(type: .comment)
            }).disposed(by: disposeBag)

    }
}
