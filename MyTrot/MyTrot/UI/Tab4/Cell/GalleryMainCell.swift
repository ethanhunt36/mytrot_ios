//
//  GalleryMainCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/23.
//

import UIKit
import Reusable
import Kingfisher
protocol GalleryMainCellDelegate: AnyObject {
    func block(boardNo: Int64)
}

class GalleryMainCell: UICollectionViewCell, NibReusable {
    @IBOutlet weak var galleryImageView: UIImageView! 
    @IBOutlet weak var descLabel: UILabel!
    weak var delegate: GalleryMainCellDelegate?

    var item: GalleryDataListModel? {
        didSet {
            let desc = "좋아요 \(item?.like_cnt ?? 0) | 댓글 \(item?.comment_cnt ?? 0 ) | 조회 \(item?.read_cnt ?? 0)"
            descLabel.text = desc
            guard let imgPath = item?.img01.validateHostImageUrl else { return }
            if let imgUrl = URL(string: imgPath) {
                galleryImageView.kf.setImage(with: imgUrl)
            }

        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    func initializeEvent() {
        let longpressgesture = UILongPressGestureRecognizer(target: self, action: #selector(showResetMenu))
        longpressgesture.isEnabled = true
        longpressgesture.minimumPressDuration = 2.0
        self.addGestureRecognizer(longpressgesture)

    }
    @IBAction func showResetMenu(sender: UILongPressGestureRecognizer) {
        guard let model = self.item else { return }

        self.delegate?.block(boardNo: model.no)

        print("showResetMenu")
    }

}

