//
//  GalleryDetailTopCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/31.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
protocol GalleryDetailTopCellDelegate: AnyObject {
    func savePhoto(image: UIImage)
    func likeButton(isLikeOn: Bool)
}
class GalleryDetailTopCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()
    weak var delegate: GalleryDetailTopCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {
        likeButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let isLike = self?.isLike else {
                    self?.delegate?.likeButton(isLikeOn: true)
                    return
                }
                self?.delegate?.likeButton(isLikeOn: isLike == "N")
            }).disposed(by: disposeBag)
        saveButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let img = self?.galleryImageView.image else { return }
                self?.delegate?.savePhoto(image: img)

            }).disposed(by: disposeBag)

    }

    var isLike: String? {
        didSet {
            if isLike == "Y" {
                likeImageView.image = Asset.btnHeartOn.image
            } else {
                likeImageView.image = Asset.btnHeartOff.image

            }
        }
    }


    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var writerLabel: UILabel!
    @IBOutlet weak var regDateLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var readCountLabel: UILabel!
    @IBOutlet weak var galleryImageView: UIImageView!

    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeImageView: UIImageView!

    @IBOutlet weak var saveButton: UIButton! {
        didSet {
            saveButton.layer.cornerRadius = 5.0
        }
    }
    var imgString: String? {
        didSet {
            if imgString != nil, imgString != "" {
                galleryImageView.kf.setImage(with: URL(string: imgString?.validateHostImageUrl ?? ""))
                saveButton.isHidden = true
            }
        }
    }

    var item: BoardDetailViewDataModel? {
        didSet {
            contentLabel.text = item?.cont.htmlToString ?? ""
            writerLabel.text = "\(item?.member_info.nick ?? "") Lv. \(item?.member_info.lv ?? 0)"
            regDateLabel.text = item?.reg_dttm ?? ""
            likeCountLabel.text = "\(item?.like_cnt.commaString ?? "")명 좋아요"
            readCountLabel.text = "저장 \(item?.download_cnt.commaString ?? "0") | 조회 \(item?.read_cnt.commaString ?? "0")"
            if imgString == nil || imgString == "" {
                galleryImageView.kf.setImage(with: URL(string: item?.img01?.validateHostImageUrl ?? ""))
                saveButton.isHidden = false
            }
            
            isLike = item?.is_like

        }
    }

}
