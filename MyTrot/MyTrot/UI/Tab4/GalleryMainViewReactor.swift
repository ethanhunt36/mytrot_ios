//
//  GalleryMainViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class GalleryMainViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case getGalleryList
        case getGalleryListByType(GalleryListType)
        case getGalleryListByArtistNo(Int64, String)

        case moveToStepScreen(CommonStep)
        case getGalleryListByDefault
        
        case reloadWithoutNo(boardNo: Int64)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setListType(GalleryListType)
        case setLastNo(String)
        case setArtistNo(Int64)
        case setArtistName(String)
        case setResponseGalleryList(ResponseGalleryList)
        case setAddtionalLoading(Bool)
        case setBlockNo(Int64)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var last_no: String
        var type: GalleryListType
        var artist_no: Int64
        var artist_name: String
        var item: [GalleryDataListModel]
        var isAddtionalLoading: Bool
        var headerModel: GalleryHeaderDataModel
        var blockArticles: [Int64]

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService

    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()

        self.initialState = State(
            isDismiss: false,
            last_no: "",
            type: .recent,
            artist_no: 0,
            artist_name: "",
            item: [],
            isAddtionalLoading: false,
            headerModel: GalleryHeaderDataModel(isSelectedFavSinger: false, favSingerName: "", viewListType: GalleryListType.recent.rawValue),
            blockArticles: []
            
        )
    }
    
    func mutate(action: GalleryMainViewReactor.Action) -> Observable<GalleryMainViewReactor.Mutation> {
        switch action {
        case .reloadWithoutNo(let boardNo):
            return .just(.setBlockNo(boardNo))

//            return .concat([
//                .just(.setAddtionalLoading(currentState.isAddtionalLoading)),
//                .just(.setBlockNo(boardNo)),
//                .just(.setArtistNo(currentState.artist_no)),
//                .just(.setArtistName(currentState.artist_name)),
//                .just(.setListType(currentState.type)),
//                networkService.galleryList(sort: currentState.type, lastNo: currentState.last_no, artistNo: currentState.artist_no).asObservable().map { Mutation.setResponseGalleryList($0)}
//
//            ])

        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .getGalleryListByDefault:
            return .concat([
                .just(.setAddtionalLoading(false)),
                .just(.setArtistNo(-1)),
                .just(.setArtistName("")),
                .just(.setListType(.recent)),
                networkService.galleryList(sort: .recent, lastNo: "", artistNo: -1).asObservable().map { Mutation.setResponseGalleryList($0)}
            ])

        case .getGalleryList:
            return .concat([
                .just(.setAddtionalLoading(true)),
                networkService.galleryList(sort: currentState.type, lastNo: currentState.last_no, artistNo: currentState.artist_no).asObservable().map { Mutation.setResponseGalleryList($0)}
            ])

        case .getGalleryListByArtistNo(let artist_no, let artist_name):
            return .concat([
                .just(.setAddtionalLoading(false)),
                .just(.setArtistNo(artist_no)),
                .just(.setArtistName(artist_name)),
                networkService.galleryList(sort: currentState.type, lastNo: "", artistNo: artist_no).asObservable().map { Mutation.setResponseGalleryList($0)}
            ])
        case .getGalleryListByType(let type):
            return .concat([
                .just(.setAddtionalLoading(false)),
                .just(.setListType(type)),
                networkService.galleryList(sort: type, lastNo: "", artistNo: currentState.artist_no).asObservable().map { Mutation.setResponseGalleryList($0)}
            ])
            
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: GalleryMainViewReactor.State, mutation: GalleryMainViewReactor.Mutation) -> GalleryMainViewReactor.State {
        var state = state
        switch mutation {
        case .setBlockNo(let boardNo):
            state.blockArticles.append(boardNo)
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setListType(let type):
            state.type = type
            state.headerModel.viewListType = type.rawValue
        case .setLastNo(let lastNo):
            state.last_no = lastNo
        case .setArtistNo(let artistNo):
            state.artist_no = artistNo
            if artistNo < 1 {
                state.headerModel.isSelectedFavSinger = false
            } else {
                state.headerModel.isSelectedFavSinger = true
            }
        case .setArtistName(let artistName):
            state.headerModel.favSingerName = artistName
            state.artist_name = artistName
        case .setResponseGalleryList(let response):
            if response.isResponseStatusSuccessful() {
                state.last_no = response.data?.last_no ?? ""
                state.item = response.data?.list_data ?? []
                
            }
        case .setAddtionalLoading(let flag):
            state.isAddtionalLoading = flag

        }
        return state
    }
    

}
