//
//  GalleryMainViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//


import UIKit
import ReactorKit

final class GalleryMainViewController: BaseViewController, StoryboardView, HasPreferencesService {
    deinit { print("\(type(of: self)): \(#function)") }
    
    enum Define {
        static let screenWidth: CGFloat = UIScreen.main.bounds.width
    }
    var disposeBag = DisposeBag()
    var preferencesService = PreferencesService()
    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var galleryUploadButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var galleryList: [GalleryDataListModel] = []
    var isListLoadFinish: Bool = false

    @IBOutlet weak var recentButton: UIButton! {
        didSet {
            recentButton.setBackgroundColor(UIColor.clear, for: .normal)
            recentButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)

            recentButton.setTitleColor(Asset.tangerine.color, for: .normal)
            recentButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var popularButton: UIButton! {
        didSet {
            popularButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            popularButton.setBackgroundColor(UIColor.clear, for: .normal)
            popularButton.setTitleColor(Asset.tangerine.color, for: .normal)
            popularButton.setTitleColor(Asset.tangerine.color, for: .selected)

        }
    }
    @IBOutlet weak var commentTotalButton: UIButton! {
        didSet {
            commentTotalButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            commentTotalButton.setBackgroundColor(UIColor.clear, for: .normal)
            commentTotalButton.setTitleColor(Asset.tangerine.color, for: .normal)
            commentTotalButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var totalSingerButton: UIButton!
    @IBOutlet weak var favSingerButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: GalleryMainViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension GalleryMainViewController {
    func bindAction(_ reactor: GalleryMainViewReactor) {
        galleryUploadButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.moveToStepScreen(.galleryUploadPage) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        favSingerButton.rx.tap
            .map { Reactor.Action.getGalleryListByArtistNo(-1, "") }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        totalSingerButton.rx.tap
            .map { Reactor.Action.moveToStepScreen(.favSingerSelectPage(type: .choiceSinger)) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        recentButton.rx.tap
            .map { Reactor.Action.getGalleryListByType(.recent) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        popularButton.rx.tap
            .map { Reactor.Action.getGalleryListByType(.popular) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        commentTotalButton.rx.tap
            .map { Reactor.Action.getGalleryListByType(.comment) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        reactor.action.onNext(.getGalleryListByType(.recent))
    }
}

// MARK: bindState For Reactor
private extension GalleryMainViewController {
    func bindState(_ reactor: GalleryMainViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        
        reactor.state.map { $0.item }
            .distinctUntilChanged({ (resList1, resList2) -> Bool in
                guard let firstObj1 = resList1.first else { return false }
                guard let firstObj2 = resList2.first else { return false }
                return firstObj1.no == firstObj2.no
            })
            .filter{ $0.count > 0 }
            .subscribe(onNext: { [weak self] item in
                self?.isListLoadFinish = true
                let filteredList = self?.blockArrangeModel(targetList: item) ?? []
                if reactor.currentState.isAddtionalLoading == true {
                    self?.galleryList.append(contentsOf: filteredList)

                } else {
                    self?.galleryList.removeAll()
                    self?.galleryList.append(contentsOf: filteredList)

                }
                self?.collectionView.reloadData()
            }).disposed(by: disposeBag)
        reactor.state.map { $0.blockArticles}
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] item in
                if let tempList = self?.galleryList {
                    self?.galleryList = self?.blockArrangeModel(targetList: tempList) ?? []
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.headerModel }
            .subscribe(onNext: { [weak self] cellModel in
                self?.favSingerButton.setTitle("\(cellModel.favSingerName) X", for: .normal)

                if cellModel.isSelectedFavSinger == true {
                    self?.totalSingerButton.isHidden = true
                    self?.favSingerButton.isHidden = false
                } else {
                    self?.totalSingerButton.isHidden = false
                    self?.favSingerButton.isHidden = true
                }
                self?.recentButton.isSelected = false
                self?.popularButton.isSelected = false
                self?.commentTotalButton.isSelected = false

                switch cellModel.viewListType {
                case GalleryListType.recent.rawValue:
                    self?.recentButton.isSelected = true
                case GalleryListType.popular.rawValue:
                    self?.popularButton.isSelected = true
                case GalleryListType.comment.rawValue:
                    self?.commentTotalButton.isSelected = true

                default:
                    break;
                }
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension GalleryMainViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeCollectionLayout()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.SelectArtistInfo.self, sticky: true).subscribe { [weak self] event in
            
            if let artist_no = event.element?.artist_no , let artist_name = event.element?.artist_name {
                guard let reactor = self?.reactor else { return }
                Observable.just(Reactor.Action.getGalleryListByArtistNo(artist_no, artist_name))
                    .observeOn(MainScheduler.asyncInstance)
                    .bind(to: reactor.action)
                    .disposed(by: self?.disposeBag ?? DisposeBag())

            }
            

        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.GalleryUploadFinish.self, sticky: true).subscribe { [weak self] _ in
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.getGalleryListByDefault)

        }.disposed(by: disposeBag)

    }
    
}


// MARK: - Reactor Action
private extension GalleryMainViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
    
    func blockArrangeModel(targetList: [GalleryDataListModel]) -> [GalleryDataListModel] {
        return targetList.filter({ [weak self] model -> Bool in
            guard let reactor = self?.reactor else { return false }
            return reactor.currentState.blockArticles.contains(model.no) == false
        })

    }

}

// MARK: -
extension GalleryMainViewController {
    func initializeCollectionLayout() {
        // ios 10
        let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        
        let totalMargine: CGFloat = 5.0
        let labelHeight: CGFloat = 0
        let collectionViewCellwidth: CGFloat = (Define.screenWidth - totalMargine )/2
        let collectionViewCellheight = collectionViewCellwidth + labelHeight
        print("collectionViewCellwidth : \(collectionViewCellwidth)")
        print("collectionViewCellheight : \(collectionViewCellheight)")

        // 고정된 크기
        flowLayout?.estimatedItemSize = CGSize(width: collectionViewCellwidth, height: 200)
        
        collectionView.register(cellType: GalleryMainCell.self)



    }
}
// MARK: -
extension GalleryMainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return 1

    }

    //옆 라인 간격

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        return 0.5

    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath) as GalleryMainCell
        cell.item = galleryList[indexPath.row]
        cell.delegate = self
        return cell
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryList.count
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAt: \(galleryList[indexPath.row])")
        guard let reactor = reactor else { return }
        let boardNo = galleryList[indexPath.row].no
        reactor.action.onNext(.moveToStepScreen(.galleryDetailPage(boardNo: boardNo)))
    }
}
extension GalleryMainViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.height

        // 스크롤이 테이블 뷰 Offset의 끝에 가게 되면 다음 페이지를 호출
        let didScrollGoToEndOfTableView: Bool = offsetY > (contentHeight - height)
        if didScrollGoToEndOfTableView, isListLoadFinish == true {
            isListLoadFinish = false
            print("호출")
            guard let reactor = reactor else { return }
            if reactor.currentState.last_no != "" {
                reactor.action.onNext(.getGalleryList)
            }
        }
    }
}
extension GalleryMainViewController: GalleryHeaderCellDelegate {
    func sortList(type: GalleryListType) {
        print("sortList : \(type)")
        guard let reactor = reactor else { return }

        switch type {
        case .recent:
            reactor.action.onNext(.getGalleryListByType(.recent))
        case .popular:
            reactor.action.onNext(.getGalleryListByType(.popular))
        case .comment:
            reactor.action.onNext(.getGalleryListByType(.comment))
        }

    }
    
    func checkFavTopSinger(isCheck: Bool) {
        print("checkFavTopSinger : \(isCheck)")
        guard let reactor = reactor else { return }
        reactor.action.onNext(.getGalleryListByArtistNo(-1, ""))

    }
    func checkTotalSinger(isCheck: Bool) {
        print("checkTotalSinger : \(isCheck)")
        guard let reactor = reactor else { return }
        reactor.action.onNext(.moveToStepScreen(.favSingerSelectPage(type: .choiceSinger)))

    }
    
}
extension GalleryMainViewController: GalleryMainCellDelegate {

    func block(boardNo: Int64) {
        openDeleteAlert(boardNo: boardNo)
    }
    func openDeleteAlert(boardNo: Int64) {
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        let deleteArticle =  UIAlertAction(title: "블라인드", style: .default) { [weak self] (action) in
            print("블라인드!!")
            let controller = UIAlertController.init(title: "", message: "해당 글을 블라인드 처리 하시겠습니까?", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "확인", style: .default, handler: { [weak self] _ in
                guard let reactor = self?.reactor else { return }
                reactor.action.onNext(.reloadWithoutNo(boardNo: boardNo))
            })
            let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)

            controller.addAction(defaultAction)
            controller.addAction(cancel)

            self?.present(controller, animated: true)

        }

        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        alert.addAction(deleteArticle)

        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)


    }

}
