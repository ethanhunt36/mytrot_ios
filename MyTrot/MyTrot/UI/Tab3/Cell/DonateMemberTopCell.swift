//
//  DonateMemberTopCell.swift
//  MyTrot
//
//  Created by hclim on 2021/04/04.
//

import Foundation
import UIKit
import Kingfisher

class DonateMemberTopCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var img: UIImageView!

    var item: DonateMemberDataListModel? {
        didSet {
            guard let cellItem = item else { return }
            img.kf.setImage(with: URL(string: cellItem.imgUrl?.validateHostImageUrl ?? "")) { result in
                print("img complete")
            }
            img.kf.setImage(with: URL(string: cellItem.imgUrl?.validateHostImageUrl ?? ""))
            descLabel.text = "총 \(cellItem.search_cnt ?? 0)명의 팬분들이 참여해주셨습니다.\n여러분의 많은 참여에 감사드립니다."
        }
    }
}
