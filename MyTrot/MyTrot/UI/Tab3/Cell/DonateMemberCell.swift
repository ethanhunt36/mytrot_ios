//
//  DonateMemberCell.swift
//  MyTrot
//
//  Created by hclim on 2021/04/04.
//

import Foundation
import UIKit
import Kingfisher

class DonateMemberCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var indexLabel: UILabel!

    var rowIndex: Int? {
        didSet {
            indexLabel.text = "\(rowIndex ?? 0)"
        }
    }
    var item: DonateMemberDataListModel? {
        didSet {
            guard let cellItem = item else { return }
            descLabel.text = cellItem.member_nick
            pointLabel.text = "\(cellItem.sum_point.insertComma) P"
        }
    }
}
