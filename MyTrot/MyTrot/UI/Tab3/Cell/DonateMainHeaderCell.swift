//
//  DonateMainHeaderCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/22.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

protocol DonateMainHeaderCellDelegate: AnyObject {
    func sortList(type: DonateListType)
    func checkFavTopSinger(isCheck: Bool)
}
class DonateMainHeaderCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()
    weak var delegate: DonateMainHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    @IBOutlet weak var bottomView: UIView!{
        didSet {
            bottomView.layer.borderWidth = 1
            bottomView.layer.borderColor = Asset.purpley.color.cgColor
        }
    }
    @IBOutlet weak var bottomLabel: UILabel!

    @IBOutlet weak var descLabel: UILabel!{
        didSet {
            let voteCount = "\(User.shared.myInfo?.vote_cnt ?? 0)"
            let pointCount = "\(Utils.roundByPointIntValue(input: User.shared.myInfo?.point ?? 0, someBelow: 2) )"
            descLabel.text = "투표권 : \(voteCount.insertComma)장 | 포인트 : \(pointCount.insertComma) P"
        }
    }

    @IBOutlet weak var ingButton: UIButton! {
        didSet {
            ingButton.customCornerRadius = 10.0

            ingButton.setBackgroundColor(UIColor.clear, for: .normal)
            ingButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)

            ingButton.setTitleColor(Asset.tangerine.color, for: .normal)
            ingButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var doneButton: UIButton! {
        didSet {
            doneButton.customCornerRadius = 10.0

            doneButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            doneButton.setBackgroundColor(UIColor.clear, for: .normal)
            doneButton.setTitleColor(Asset.tangerine.color, for: .normal)
            doneButton.setTitleColor(Asset.tangerine.color, for: .selected)

        }
    }
    @IBOutlet weak var totalButton: UIButton! {
        didSet {
            totalButton.customCornerRadius = 10.0

            totalButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            totalButton.setBackgroundColor(UIColor.clear, for: .normal)
            totalButton.setTitleColor(Asset.tangerine.color, for: .normal)
            totalButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var favTopButton: UIButton!

    var item: DonateDataListModel? {
        didSet {
            guard let cellItem = item else { return }
            guard let cellModel = cellItem.donateHeaderModel else { return }
            favTopButton.isSelected = cellModel.isSelectedFavSinger
            ingButton.isSelected = false
            doneButton.isSelected = false
            totalButton.isSelected = false
            bottomLabel.text = "\(cellModel.done_data?.donate_price.insertComma ?? "")원 (\(cellModel.done_data?.donate_count ?? 0)회)"
            switch cellModel.viewListType {
            case DonateListType.doing.rawValue:
                ingButton.isSelected = true
                favTopButton.isHidden = false
            case DonateListType.done.rawValue:
                doneButton.isSelected = true
                favTopButton.isHidden = true
            case DonateListType.total.rawValue:
                totalButton.isSelected = true
                favTopButton.isHidden = true
            default:
                break;
            }
        }
    }
    
    func initializeEvent() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.MyInfo.self, sticky: true).subscribe { [weak self] event in
            let voteCount = "\(User.shared.myInfo?.vote_cnt ?? 0)"
            let pointCount = "\(Utils.roundByPointIntValue(input: User.shared.myInfo?.point ?? 0, someBelow: 2) )"
            self?.descLabel.text = "투표권 : \(voteCount.insertComma)장 | 포인트 : \(pointCount.insertComma) P"

        }.disposed(by: disposeBag)
        favTopButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                let isSelect = self?.favTopButton.isSelected ?? false
                let artist_no = User.shared.myInfo?.artist_no ?? 0
                if artist_no == 0 || artist_no == -1 {
                    // 아무 변화 없습니다!
                } else {
                    self?.item?.donateHeaderModel?.isSelectedFavSinger = !isSelect
                }

                self?.delegate?.checkFavTopSinger(isCheck: !isSelect)
            }).disposed(by: disposeBag)
        ingButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.donateHeaderModel?.viewListType = DonateListType.doing.rawValue
                self?.delegate?.sortList(type: .doing)
            }).disposed(by: disposeBag)
        doneButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.donateHeaderModel?.viewListType = DonateListType.done.rawValue
                self?.delegate?.sortList(type: .done)
            }).disposed(by: disposeBag)
        totalButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.donateHeaderModel?.viewListType = DonateListType.total.rawValue
                self?.delegate?.sortList(type: .total)
            }).disposed(by: disposeBag)

    }
}
