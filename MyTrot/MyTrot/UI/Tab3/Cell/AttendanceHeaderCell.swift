//
//  AttendanceHeaderCell.swift
//  MyTrot
//
//  Created by hclim on 2021/04/13.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

class AttendanceHeaderCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {

    }
    
    var item: AttendanceMyDataListModel? {
        didSet {
            let stringTitle = "현재 \(item?.combo ?? 0)일 연속 출석 기록중입니다.\n10일 연속 출석하시면 \(item?.bonus ?? 0)P를 보너스 지급합니다."
            titleLabel.text = stringTitle
        }
    }
    var rankItem: AttendanceRankDataListModel? {
        didSet {
            titleLabel.text = rankItem?.message ?? ""
        }

    }
}
