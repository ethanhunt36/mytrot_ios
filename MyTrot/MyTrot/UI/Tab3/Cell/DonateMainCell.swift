//
//  DonateMainCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/16.
//

import Foundation
import UIKit
import Kingfisher

class DonateMainCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }

    @IBOutlet weak var myDonateLabel: PaddingLabel!{
        didSet {
            myDonateLabel.layer.cornerRadius = Define.cornerRadius
            myDonateLabel.layer.masksToBounds = true
        }
    }

    @IBOutlet weak var donateLabel: PaddingLabel!{
        didSet {
            donateLabel.layer.cornerRadius = Define.cornerRadius
            donateLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var singerImageView: UIImageView!{
        didSet {
            singerImageView.layer.borderWidth = 1.0
            singerImageView.layer.borderColor = Asset.normalGrey.color.cgColor
            singerImageView.layer.cornerRadius = 35.0
            singerImageView.layer.masksToBounds = true
        }
    }

    @IBOutlet weak var singerLabel: UILabel!

    @IBOutlet weak var donateThLabel: UILabel!
    @IBOutlet weak var donatePointLabel: UILabel!
    @IBOutlet weak var donatePercentLabel: UILabel!
    @IBOutlet weak var doingBottomView: UIView!
    @IBOutlet weak var doneBottomView: UIView!
    @IBOutlet weak var doneThLabel: UILabel!
    @IBOutlet weak var doneDateLabel: UILabel!
    @IBOutlet weak var totalThLabel: UILabel!
    @IBOutlet weak var totalDateLabel: UILabel!
    @IBOutlet weak var totalBottomView: UIView!
    var item: DonateDataListModel? {
        didSet {
            guard let cellItem = item else { return }
            myDonateLabel.text = "\(cellItem.donate_my_point.insertComma) P"
            donateLabel.text = "\(cellItem.donate_cur_point.commaString) P"
            singerLabel.text = cellItem.name
            singerImageView.kf.setImage(with: URL(string: cellItem.pic.validateHostImageUrl))
            
            donateThLabel.text = "\(cellItem.donate_nth ?? 0)차 목표"
            doneThLabel.text = "\(cellItem.donate_nth ?? 0)차 모금기간"
            totalThLabel.text = "\(cellItem.counts ?? 0)회"
            
            doneDateLabel.text = "\(cellItem.donate_reg_dttm.removeDateFormatStyle) ~ \(cellItem.donate_end_dttm.removeDateFormatStyle)"
            totalDateLabel.text = "\(cellItem.donate_reg_dttm.removeDateFormatStyle) ~ \(cellItem.donate_end_dttm.removeDateFormatStyle)"
            donatePointLabel.text = "\(cellItem.donate_goal_point?.commaString ?? "0") P"

            let curPoint: Double = (Double)(cellItem.donate_cur_point)
            let goalPoint: Double = (Double)(cellItem.donate_goal_point ?? 0)
            let currentPercent = (curPoint / goalPoint) * 100

            donatePercentLabel.text = "\(round(currentPercent*100)/100)%"
            doingBottomView.isHidden = true
            doneBottomView.isHidden = true
            totalBottomView.isHidden = true
            let type = cellItem.donateType?.rawValue
            switch type {
            case DonateListType.doing.rawValue:
                doingBottomView.isHidden = false
            case DonateListType.done.rawValue:
                doneBottomView.isHidden = false
            case DonateListType.total.rawValue:
                totalBottomView.isHidden = false
            default:
                break;
            }
        }
    }
}
