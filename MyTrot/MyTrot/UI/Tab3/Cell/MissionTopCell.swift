//
//  MissionTopCell.swift
//  MyTrot
//
//  Created by hclim on 2021/04/05.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
class MissionTopCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()


    @IBOutlet weak var contentLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    var item: MissionDataListModel? {
        didSet {
            let voteCount = item?.voteCount ?? 0
            let pointCount = item?.pointCount ?? 0
            let peopleCount = item?.completeCount ?? 0
            let attrString = "오늘의 미션을 모두 100% 달성시\n투표권 \(voteCount)장과 포인트 \(pointCount)P를 선물로 드려요~\n오늘 \(peopleCount)명이 미션 달성 보상을 받았습니다."
            let attributedString = NSMutableAttributedString(string: attrString, attributes: [
                .font: UIFont.systemFont(ofSize: 15.0),
                .foregroundColor: Asset.black.color,
              .kern: 0.0
            ])
            let voteLength = "\(voteCount)".count + 1
            let pointLength = "\(pointCount)".count + 1
            let peopleLength = "\(peopleCount)".count + 1
            let votePosition = 24
            let pointPosition = votePosition + voteLength + 6
            let peoplePosition = pointPosition + pointLength + 14
            attributedString.addAttribute(.foregroundColor, value: UIColor.red, range: NSRange(location: votePosition, length: voteLength))
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 15.0), range: NSRange(location: votePosition, length: voteLength))
            attributedString.addAttribute(.foregroundColor, value: UIColor.red, range: NSRange(location: pointPosition, length: pointLength))
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 15.0), range: NSRange(location: pointPosition, length: pointLength))

            attributedString.addAttribute(.foregroundColor, value: UIColor.red, range: NSRange(location: peoplePosition, length: peopleLength))
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 15.0), range: NSRange(location: peoplePosition, length: peopleLength))

            contentLabel.attributedText = attributedString
        }
    }
}
