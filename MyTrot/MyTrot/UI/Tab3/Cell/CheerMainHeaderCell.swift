//
//  CheerMainHeaderCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/22.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

protocol CheerMainHeaderCellDelegate: AnyObject {
    func sortList(type: CheerListType)
    func checkFavTopSinger(isCheck: Bool)
    func checkTotalSinger(isCheck: Bool)
}
class CheerMainHeaderCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()
    weak var delegate: CheerMainHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    @IBOutlet weak var recentButton: UIButton! {
        didSet {
            recentButton.customCornerRadius = 10.0

            recentButton.setBackgroundColor(UIColor.clear, for: .normal)
            recentButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)

            recentButton.setTitleColor(Asset.tangerine.color, for: .normal)
            recentButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var popularButton: UIButton! {
        didSet {
            popularButton.customCornerRadius = 10.0

            popularButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            popularButton.setBackgroundColor(UIColor.clear, for: .normal)
            popularButton.setTitleColor(Asset.tangerine.color, for: .normal)
            popularButton.setTitleColor(Asset.tangerine.color, for: .selected)

        }
    }
    @IBOutlet weak var totalButton: UIButton! {
        didSet {
            totalButton.customCornerRadius = 10.0

            totalButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            totalButton.setBackgroundColor(UIColor.clear, for: .normal)
            totalButton.setTitleColor(Asset.tangerine.color, for: .normal)
            totalButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var totalSingerButton: UIButton!
    @IBOutlet weak var favSingerButton: UIButton!

    var item: CheerDataListModel? {
        didSet {
            guard let cellItem = item else { return }
            guard let cellModel = cellItem.cheerHeaderModel else { return }
            favSingerButton.setTitle("\(cellModel.favSingerName) X", for: .normal)

            print("cellModel.viewListType : \(cellModel.viewListType)")
            if cellModel.isSelectedFavSinger == true {
                totalSingerButton.isHidden = true
                favSingerButton.isHidden = false
            } else {
                totalSingerButton.isHidden = false
                favSingerButton.isHidden = true
            }
            recentButton.isSelected = false
            popularButton.isSelected = false
            totalButton.isSelected = false

            switch cellModel.viewListType {
            case CheerListType.recent.rawValue:
                recentButton.isSelected = true
            case CheerListType.popular.rawValue:
                popularButton.isSelected = true
            case CheerListType.total.rawValue:
                totalButton.isSelected = true
                totalSingerButton.isHidden = true
                favSingerButton.isHidden = true

            default:
                break;
            }
        }
    }
    
    func initializeEvent() {
        favSingerButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.cheerHeaderModel?.isSelectedFavSinger = true
                self?.delegate?.checkFavTopSinger(isCheck: true)
            }).disposed(by: disposeBag)
        totalSingerButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.cheerHeaderModel?.isSelectedFavSinger = false
                self?.delegate?.checkTotalSinger(isCheck: false)
            }).disposed(by: disposeBag)
        recentButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.cheerHeaderModel?.viewListType = CheerListType.recent.rawValue
                self?.delegate?.sortList(type: .recent)
            }).disposed(by: disposeBag)
        popularButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.cheerHeaderModel?.viewListType = CheerListType.popular.rawValue
                self?.delegate?.sortList(type: .popular)
            }).disposed(by: disposeBag)
        totalButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.cheerHeaderModel?.viewListType = CheerListType.total.rawValue
                self?.delegate?.sortList(type: .total)
            }).disposed(by: disposeBag)

    }
}
