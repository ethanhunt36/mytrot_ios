//
//  AttendanceCell.swift
//  MyTrot
//
//  Created by hclim on 2021/04/13.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

class AttendanceCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var regDtLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {

    }
    
    var item: AttendanceMyDataListModel? {
        didSet {
            regDtLabel.text = "\(item?.reg_dttm ?? "")"
            pointLabel.text = "\(item?.point ?? 0)P"
            titleLabel.text = "\(item?.message ?? "")"

        }
    }
    var rankItem: AttendanceRankDataListModel? {
        didSet {
            regDtLabel.isHidden = true
            pointLabel.text = rankItem?.message ?? ""
            titleLabel.text = rankItem?.nick ?? ""

        }
    }

}
