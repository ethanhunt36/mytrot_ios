//
//  VoteMainHeaderCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/18.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
protocol VoteHeaderDelegate: AnyObject {
    func sortList(type: VoteListType)
    func checkFavTopSinger(isCheck: Bool)
    func adverClick()
    
    func clickVoteGender()
    func clickVoteType()
}
class VoteMainHeaderCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()
    weak var delegate: VoteHeaderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    
    @IBOutlet weak var voteDescLabel: UILabel!{
        didSet {
            let voteCount = "\(User.shared.myInfo?.vote_cnt ?? 0)"
            let pointCount = "\(Utils.roundByPointIntValue(input: User.shared.myInfo?.point ?? 0, someBelow: 2) )"
            voteDescLabel.text = "투표권 : \(voteCount.insertComma)장 | 포인트 : \(pointCount.insertComma) P"
        }
    }

    @IBOutlet weak var voteTypeButton: UIButton! {
        didSet {
            voteTypeButton.customCornerRadius = 10.0

            voteTypeButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            voteTypeButton.setBackgroundColor(Asset.lightTangerine.color, for: .normal)
            voteTypeButton.setTitleColor(Asset.tangerine.color, for: .normal)
            voteTypeButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var voteGenderButton: UIButton! {
        didSet {
            voteGenderButton.customCornerRadius = 10.0

            voteGenderButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            voteGenderButton.setBackgroundColor(Asset.lightTangerine.color, for: .normal)
            voteGenderButton.setTitleColor(Asset.tangerine.color, for: .normal)
            voteGenderButton.setTitleColor(Asset.tangerine.color, for: .selected)

        }
    }

    @IBOutlet weak var favTopButton: UIButton!
    // 광고보고 무료 충전
    @IBOutlet weak var adFreePointGetView: UIView! {
        didSet {
            adFreePointGetView.layer.borderWidth = 1
            adFreePointGetView.layer.borderColor = Asset.purpley.color.cgColor
            adFreePointGetView.layer.cornerRadius = 15
        }
    }
    @IBOutlet weak var adVotePointButton: UIButton!

    var item: VoteDataListModel? {
        didSet {
            guard let cellItem = item else { return }
            guard let cellModel = cellItem.voteHeaderModel else { return }
            favTopButton.isSelected = cellModel.isSelectedFavSinger

            let voteType = Utils.getLastSavedVoteType()
            let voteGender = Utils.getLastSavedVoteGender()

            switch voteType {
            case .monthly:
                voteTypeButton.setTitle("월간", for: .normal)
            case .weekly:
                voteTypeButton.setTitle("주간", for: .normal)
            case .normally:
                voteTypeButton.setTitle("누적", for: .normal)
            default:
                break;
            }
            switch voteGender {
            case .both:
                voteGenderButton.setTitle("전체성별", for: .normal)
            case .female:
                voteGenderButton.setTitle("여자", for: .normal)
            case .male:
                voteGenderButton.setTitle("남자", for: .normal)
            }

        }
    }
    
    func initializeEvent() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.MyInfo.self, sticky: true).subscribe { [weak self] event in
            let voteCount = "\(User.shared.myInfo?.vote_cnt ?? 0)"
            let pointCount = "\(Utils.roundByPointIntValue(input: User.shared.myInfo?.point ?? 0, someBelow: 2) )"
            self?.voteDescLabel.text = "투표권 : \(voteCount.insertComma)장 | 포인트 : \(pointCount.insertComma) P"

        }.disposed(by: disposeBag)

        favTopButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                let isSelect = self?.favTopButton.isSelected ?? false
                let artist_no = User.shared.myInfo?.artist_no ?? 0
                if artist_no == 0 || artist_no == -1 {
                    // 아무 변화 없습니다!
                } else {
                    self?.item?.voteHeaderModel?.isSelectedFavSinger = !isSelect
                }
                self?.delegate?.checkFavTopSinger(isCheck: !isSelect)
            }).disposed(by: disposeBag)
        voteGenderButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.voteHeaderModel?.viewListType = VoteListType.weekly.rawValue
                self?.delegate?.clickVoteGender()
            }).disposed(by: disposeBag)
        voteTypeButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.voteHeaderModel?.viewListType = VoteListType.monthly.rawValue
                self?.delegate?.clickVoteType()
            }).disposed(by: disposeBag)
        adVotePointButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.delegate?.adverClick()
            }).disposed(by: disposeBag)

    }
}
