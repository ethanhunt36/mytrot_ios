//
//  CheerMainTotalCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/22.
//

import Foundation
import UIKit
import Kingfisher

class CheerMainTotalCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }

    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var myCheerLabel: PaddingLabel!{
        didSet {
            myCheerLabel.layer.cornerRadius = Define.cornerRadius
            myCheerLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var cheerLabel: PaddingLabel!{
        didSet {
            cheerLabel.layer.cornerRadius = Define.cornerRadius
            cheerLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var myLikeLabel: PaddingLabel!{
        didSet {
            myLikeLabel.layer.cornerRadius = Define.cornerRadius
            myLikeLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var likeLabel: PaddingLabel!{
        didSet {
            likeLabel.layer.cornerRadius = Define.cornerRadius
            likeLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var singerImageView: UIImageView!{
        didSet {
            singerImageView.layer.cornerRadius = 25.0
            singerImageView.layer.masksToBounds = true
        }
    }

    @IBOutlet weak var singerLabel: UILabel!


    var item: CheerTotalDataListModel? {
        didSet {
            guard let cellItem = item else { return }
            myLikeLabel.text = "\(cellItem.my_like_cnt.commaString)"
            myCheerLabel.text = "\(cellItem.my_bbs_cnt.commaString)"
            likeLabel.text = "\(cellItem.like_cnt.commaString)"
            cheerLabel.text = "\(cellItem.bbs_cnt.commaString)"
            singerLabel.text = cellItem.name
            singerImageView.kf.setImage(with: URL(string: cellItem.pic.validateHostImageUrl))
        }
    }
}
