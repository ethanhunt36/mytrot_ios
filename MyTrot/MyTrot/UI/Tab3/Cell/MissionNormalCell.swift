//
//  MissionNormalCell.swift
//  MyTrot
//
//  Created by hclim on 2021/04/05.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
protocol MissionNormalCellDelegate: AnyObject {
    func moveToMission(code: String, humorUrl: String)
}
class MissionNormalCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    var humorUrl: String = ""
    weak var delegate: MissionNormalCellDelegate?
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLebel: PaddingLabel! {
        didSet {
            rightLebel.layer.cornerRadius = 8.0
            rightLebel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var coverView: UIView! {
        didSet {
            coverView.layer.borderWidth = 1
            coverView.layer.cornerRadius = 20.0
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {
        actionButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let sendItem = self?.item else { return }
                HomeMainPagerViewReactor.event.onNext(.missionRowClick(code: sendItem.code, url: self?.humorUrl ?? ""))

                self?.delegate?.moveToMission(code: sendItem.code, humorUrl: self?.humorUrl ?? "")
            }).disposed(by: disposeBag)

    }

    var item: MissionDataListModel? {
        didSet {
            leftLabel.text = "\(item?.name ?? "") - \(item?.cnt ?? 0)\(item?.unit ?? "")"
            let completeYN = item?.complete_yn ?? "N"
            if completeYN == "Y" {
                rightLebel.text = "완료"
                rightLebel.backgroundColor = Asset.lightPurpley.color
                rightLebel.textColor = Asset.purpley.color
                coverView.layer.borderColor = Asset.brownishGrey.color.cgColor

            } else {
                rightLebel.text = "\(item?.stat ?? 0) / \(item?.cnt ?? 0)"
                rightLebel.backgroundColor = Asset.lightTangerine.color
                rightLebel.textColor = Asset.tangerine.color
                coverView.layer.borderColor = Asset.tangerine.color.cgColor

            }
            let code = item?.code ?? ""
            actionButton.isHidden = false
            switch code {
            case MissionType.READ_AD_MOVIE.rawValue:
                actionButton.isHidden = false
            case MissionType.READ_PODONG.rawValue:
                actionButton.isHidden = false
            case MissionType.READ_LUCKY.rawValue:
                actionButton.isHidden = false
            case MissionType.PPOBKKI.rawValue:
                actionButton.isHidden = false
            default:
                break;
            }
//            contentLabel.text = item?.cont.htmlToString ?? ""
        }
    }
}
