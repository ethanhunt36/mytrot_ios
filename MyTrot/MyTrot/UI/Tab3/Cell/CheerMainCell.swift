//
//  CheerMainCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/22.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
protocol CheerMainCellDelegate: AnyObject {
    func like(model: CheerDataListModel)
    func unLike(model: CheerDataListModel)
    func block(boardNo: Int64)
}
class CheerMainCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()
    weak var delegate: CheerMainCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var writerLabel: UILabel!
    @IBOutlet weak var regDateLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var replyCountLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!

    @IBOutlet weak var singerImageView: UIImageView!{
        didSet {
            singerImageView.layer.borderWidth = 1.0
            singerImageView.layer.borderColor = Asset.normalGrey.color.cgColor
            singerImageView.layer.cornerRadius = 35.0
            singerImageView.layer.masksToBounds = true
        }
    }

    var item: CheerDataListModel? {
        didSet {
            descLabel.text = item?.cont.htmlToString
            writerLabel.text = "\(item?.nick ?? "") Lv.\(item?.lv ?? 0)"
            regDateLabel.text = item?.reg_dttm ?? ""
            if item?.my_like_cnt ?? 0 == 0 {
                likeButton.setImage(Asset.btnHeartOff.image, for: .normal)
            } else {
                likeButton.setImage(Asset.btnHeartOn.image, for: .normal)
            }
            likeCountLabel.text = "\(item?.like_cnt.commaString ?? "")"
            replyCountLabel.text = "댓글 \(item?.comment_cnt.commaString ?? "")"
            guard let pic = item?.artist_pic else { return }
            singerImageView.kf.setImage(with: URL(string: pic.validateHostImageUrl))

        }
    }
    func initializeEvent() {
        let longpressgesture = UILongPressGestureRecognizer(target: self, action: #selector(showResetMenu))
        longpressgesture.isEnabled = true
        longpressgesture.minimumPressDuration = 2.0
        self.addGestureRecognizer(longpressgesture)
        likeButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let model = self?.item else { return }
                let isLike = model.my_like_cnt > 0
                if isLike == true {
                    self?.delegate?.unLike(model: model)
                } else {
                    self?.delegate?.like(model: model)
                }
                
            }).disposed(by: disposeBag)

    }
    @IBAction func showResetMenu(sender: UILongPressGestureRecognizer) {
        guard let model = self.item else { return }

        self.delegate?.block(boardNo: model.no)

        print("showResetMenu")
    }
}
