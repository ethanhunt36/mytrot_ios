//
//  CheerDetailReplyCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/30.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

protocol CheerDetailReplyCellDelegate: AnyObject {
    func replyDeleteButton(item: BoardDetailCommentDataListModel)
}
class CheerDetailReplyCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    weak var delegate: CheerDetailReplyCellDelegate?
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var writerLabel: UILabel!
    @IBOutlet weak var regDateLabel: UILabel!
    @IBOutlet weak var modifyButton: UIButton!


    var item: BoardDetailCommentDataListModel? {
        didSet {
            contentLabel.text = item?.cont?.htmlToString ?? ""
            writerLabel.text = "\(item?.member_info?.nick ?? "") Lv. \(item?.member_info?.lv ?? 0)"
            let date = Utils.changeDateFormat(dateString: item?.reg_dttm ?? "")
            regDateLabel.text = date
            
            if let member_no = item?.member_info?.member_no?.value as? String , member_no == User.shared.myInfo?.member_no {
                modifyButton.isHidden = false

            } else {
                modifyButton.isHidden = true

            }

//            if item?.del_yn == "Y" {
//                modifyButton.isHidden = false
//            } else {
//                modifyButton.isHidden = true
//            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {
        modifyButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let sendItem = self?.item else { return }
                self?.delegate?.replyDeleteButton(item: sendItem)
            }).disposed(by: disposeBag)

    }
}
