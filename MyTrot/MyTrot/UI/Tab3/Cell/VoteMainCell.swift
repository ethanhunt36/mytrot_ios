//
//  VoteMainCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/16.
//

import Foundation
import UIKit
import Kingfisher

class VoteMainCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }

    @IBOutlet weak var myVoteLabel: PaddingLabel!{
        didSet {
            myVoteLabel.layer.cornerRadius = Define.cornerRadius
            myVoteLabel.layer.masksToBounds = true
        }
    }

    @IBOutlet weak var voteLabel: PaddingLabel!{
        didSet {
            voteLabel.layer.cornerRadius = Define.cornerRadius
            voteLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var singerImageView: UIImageView!{
        didSet {
            singerImageView.layer.borderWidth = 1.0
            singerImageView.layer.borderColor = Asset.normalGrey.color.cgColor
            singerImageView.layer.cornerRadius = 35.0
            singerImageView.layer.masksToBounds = true
        }
    }

    @IBOutlet weak var singerLabel: UILabel!

    var item: VoteDataListModel? {
        didSet {
            guard let cellItem = item else { return }
            guard let type = cellItem.viewListType else { return }
            switch type {
            case VoteListType.normally.rawValue:
                voteLabel.text = "\(cellItem.vote_cnt.commaString)"
            case VoteListType.monthly.rawValue:
                voteLabel.text = "\(cellItem.vote_cnt_month.commaString)"
            case VoteListType.weekly.rawValue:
                voteLabel.text = "\(cellItem.vote_cnt_week.commaString)"

            default:
                break;
            }
            myVoteLabel.text = "\(cellItem.my_vote_cnt.insertComma)"

            singerLabel.text = cellItem.name
            singerImageView.kf.setImage(with: URL(string: cellItem.pic.validateHostImageUrl))
        }
    }
}
