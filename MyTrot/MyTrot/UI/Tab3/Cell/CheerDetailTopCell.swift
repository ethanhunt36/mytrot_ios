//
//  CheerDetailTopCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/30.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
protocol CheerDetailTopCellDelegate: AnyObject {
    func likeButton(isLikeOn: Bool)
}
class CheerDetailTopCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()

    weak var delegate: CheerDetailTopCellDelegate?

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var writerLabel: UILabel!
    @IBOutlet weak var regDateLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var readCountLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }
    func initializeEvent() {
        likeButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let isLike = self?.isLike else {
                    self?.delegate?.likeButton(isLikeOn: true)
                    return
                }
                self?.delegate?.likeButton(isLikeOn: isLike == "N")
            }).disposed(by: disposeBag)

    }

    var isLike: String? {
        didSet {
            if isLike == "Y" {
                likeImageView.image = Asset.btnHeartOn.image
            } else {
                likeImageView.image = Asset.btnHeartOff.image

            }
        }
    }
    var item: BoardDetailViewDataModel? {
        didSet {
            contentLabel.text = item?.cont.htmlToString ?? ""
            writerLabel.text = "\(item?.member_info.nick ?? "") Lv. \(item?.member_info.lv ?? 0)"
            regDateLabel.text = item?.reg_dttm ?? ""
            likeCountLabel.text = "\(item?.like_cnt.commaString ?? "")명 좋아요"
            readCountLabel.text = "조회 \(item?.read_cnt.commaString ?? "")"
            isLike = item?.is_like
        }
    }
}
