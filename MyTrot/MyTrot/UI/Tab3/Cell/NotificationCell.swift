//
//  NotificationCell.swift
//  MyTrot
//
//  Created by hclim on 2021/07/05.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
class NotificationCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var regDateLabel: UILabel!


    var item: NotificationDataListModel? {
        didSet {
            descLabel.text = item?.cont
            regDateLabel.text = item?.reg_dttm ?? ""
        }
    }
}
