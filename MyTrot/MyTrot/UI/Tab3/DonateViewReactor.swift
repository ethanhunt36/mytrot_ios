//
//  DonateViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class DonateViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        
        case clickMaxDonate
        case clickConfirm
        case changeDonateValue(String)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setIsMaxDonate(Bool)
        case setDonateCountValue(String)
        case setDonateComplete(CommonResponse)
        case empty

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var dataModel: DonateDataListModel
        var isMaxDonate: Bool
        var maxDonateCount: Double
        var donateCountValue: String
        var artistName: String
        var completeMessage: String?
        var donateGoalCount: Int64
        var donateCurrentCount: Int64
        var currentTime: Int
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(withModel model: DonateDataListModel) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            dataModel: model,
            isMaxDonate: false ,
            maxDonateCount: User.shared.myInfo?.point ?? 0,
            donateCountValue: "",
            artistName: model.name,
            donateGoalCount: model.donate_goal_point ?? 0,
            donateCurrentCount: model.donate_cur_point,
            currentTime: model.donate_nth ?? 0

        )
    }
    
    func mutate(action: DonateViewReactor.Action) -> Observable<DonateViewReactor.Mutation> {
        switch action {
        case .clickMaxDonate:
            if currentState.isMaxDonate == true {
                return .concat([
                    .just(.setDonateCountValue("")),
                    .just(.setIsMaxDonate(false))
                ])

            } else {
                let ceilPoint = Int(ceil(currentState.maxDonateCount))
                return .concat([
                    .just(.setDonateCountValue("\(ceilPoint)")),
                    .just(.setIsMaxDonate(true))
                ])

            }
        case .clickConfirm:
            if preferencesService.getIsUserAgreeTerms() == false {
                Utils.AlertShow(msg: Constants.String.AlertMessage.agreementDeniedMessage)
                
                return .empty()
            }

            print("clickConfirm : \(currentState.dataModel.artist_no), \(currentState.donateCountValue)")
            if let donateCount = Int(currentState.donateCountValue) {
                if donateCount < 10 {
                    RxBus.shared.post(event: Events.AlertShow(message: "10P 부터 기부할 수 있습니다."))
                    return .empty()
                }
                return networkService.uploadDonate(artist_no: currentState.dataModel.artist_no, donate_cnt: donateCount).asObservable().map { Mutation.setDonateComplete($0) }
            } else {
                return .empty()
            }
        case .changeDonateValue(let value):
            return .just(.setDonateCountValue(value))

        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: DonateViewReactor.State, mutation: DonateViewReactor.Mutation) -> DonateViewReactor.State {
        var state = state
        switch mutation {
        case .setIsMaxDonate(let flag):
            state.isMaxDonate = flag
        case .setDonateCountValue(let value):
            state.donateCountValue = value
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty:
            return state
        case .setDonateComplete(let response):
            if response.isResponseStatusSuccessful() {
                let message = "\(currentState.artistName) 님의 이름으로\n \(currentState.donateCountValue)P 기부 참여 되었습니다."
                state.completeMessage = message
                RxBus.shared.post(event: Events.RefreshMyInfo(), sticky: true)
                RxBus.shared.post(event: Events.DonateFinish(
                                    id: state.dataModel.artist_no,
                                    mypoint: Int(state.dataModel.donate_my_point) ?? 0,
                                    totalpoint: Int64(state.dataModel.donate_cur_point),
                                    givePoint: Int(state.donateCountValue) ?? 0), sticky: true)

            } else {
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)
            }
        }
        return state
    }
}
