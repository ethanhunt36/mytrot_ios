//
//  HomeMainPagerViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
import XLPagerTabStrip
import ReactorKit
import Reusable
import GoogleMobileAds
import UnityAds

protocol HomeMainPagerDelegate: AnyObject {
    func moveToVoteMainPage(model: VoteDataListModel)
    func moveToDonateMainPage(model: DonateDataListModel)
    func moveToCheerDetailPage(board_no: Int64)
    func moveToCheerUploadPage()
    func moveToCheerArtistSelectPage()
    func moveToDonateDonePage(model: DonateDataListModel)
    func moveToDonateResultPage(model: DonateDataListModel)
    func moveToMissionAd()
    func moveToMissionPodong(url: String)
    func moveToVoteHeaderAd()
    func moveToMissionLuckyNumber()
    func moveToMissionPpobki()
    func moveToBlog()
}

class HomeMainPagerViewController: ButtonBarPagerTabStripViewController, StoryboardView , StoryboardBased{
    
    var disposeBag = DisposeBag()

    @IBOutlet weak var notificationButton: UIButton!{
        didSet {
            notificationButton.layer.cornerRadius = 5.0
            notificationButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var checkInButton: UIButton! {
        didSet {
            checkInButton.layer.cornerRadius = 5.0
            checkInButton.layer.masksToBounds = true
        }
    }

    @IBOutlet weak var labelNew: UILabel! {
        didSet {
            labelNew.layer.cornerRadius = 10.0
            labelNew.layer.masksToBounds = true
        }
    }

    func bind(reactor: HomeMainPagerViewReactor) {
        bindAction(reactor)
        bindState(reactor)

    }
    
    var _isViewIntroAd = false
    
    // 배너
    var caulyView: CaulyAdView? = nil
    
    // 전면
    var _interstitialAd:CaulyInterstitialAd? = nil


    override func viewDidLoad() {
        // change selected bar color
        print("HomeMainPagerViewController viewDidLoad")
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = Asset.custard.color
        settings.style.buttonBarItemFont =  UIFont.boldSystemFont(ofSize: 16)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true

        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = Asset.bottomTabDisabled.color
            newCell?.label.textColor = Asset.black.color
        }
        super.viewDidLoad()
        initialize()
    }

    // MARK: - PagerTabStripDataSource

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let voteList = HomeVoteListViewController.instantiate()
        let voteReactor = HomeVoteListViewReactor()
        voteList.reactor = voteReactor
        voteList.pagerDelegate = self 
        voteList.itemInfo = IndicatorInfo(title: "투표")
        let donateList = HomeDonateListViewController.instantiate()
        let donateReactor = HomeDonateListViewReactor()
        donateList.reactor = donateReactor
        donateList.pagerDelegate = self
        donateList.itemInfo = IndicatorInfo(title: "기부")
        
        let homeCheerListViewController = HomeCheerListViewController.instantiate()
        let homeCheerListViewReactor = HomeCheerListViewReactor()
        homeCheerListViewController.reactor = homeCheerListViewReactor
        homeCheerListViewController.itemInfo = IndicatorInfo(title: "응원")
        homeCheerListViewController.pagerDelegate = self

        let homeMissionListViewController = HomeMissionListViewController.instantiate()
        let homeMissionListViewReactor = HomeMissionListViewReactor()
        homeMissionListViewController.reactor = homeMissionListViewReactor
        homeMissionListViewController.itemInfo = IndicatorInfo(title: "미션")
        homeMissionListViewController.pagerDelegate = self

        return [voteList, donateList, homeCheerListViewController, homeMissionListViewController]
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)        
        if Utils.needToCheckAttendance() == true {
            print("체크인 해야합니다!")
            let controller = UIAlertController.init(title: "", message: "출석 체크 하시겠습니까?", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "확인", style: .default, handler: { [weak self] _ in
                guard let reactor = self?.reactor else { return }
                reactor.action.onNext(.checkInAttendance)
            })
            let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)

            controller.addAction(defaultAction)
            controller.addAction(cancel)

            self.present(controller, animated: true)

        } else {
            print("오늘은 체크인 했어요!")
        }

    }
    // MARK: - Actions

}

// MARK: -
// MARK: bindAction For Reactor
private extension HomeMainPagerViewController {
    func bindAction(_ reactor: HomeMainPagerViewReactor) {
        //        backButton.rx.tap
        //            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
        //            .map { Reactor.Action.dismiss }
        //            .bind(to: reactor.action)
        //            .disposed(by: disposeBag)
        notificationButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { [weak self] _ in
                self?.labelNew.isHidden = true 
                return Reactor.Action.moveToStepScreen(.notificationPage) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        // 출석부 이동!! 기존
        checkInButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                if Utils.needToCheckAttendance() == true {
                    self.attandenceAlert()
                } else {
                    reactor.action.onNext(.moveToStepScreen(.checkInPage))
                }
            }).disposed(by: disposeBag)


        
    }
}

// MARK: bindState For Reactor
private extension HomeMainPagerViewController {
    func bindState(_ reactor: HomeMainPagerViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension HomeMainPagerViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        
        // 유니티 광고 초기화
        initializeUnityAds()

        // 카울리 광고 초기화 (배너)
        initiaizeCaulyBanner()

        // 카울리 광고 초기화 (전면)
        initializeCaulyInterstitialAd()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.RefreshMyInfo.self, sticky: true).subscribe {[weak self] event in
            print("RefreshMyInfo!!!")
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.refreshMyInfo)
        }.disposed(by: disposeBag)
        
        bus.asObservable(event: Events.DisplayNotificationNew.self, sticky: true).subscribe { [weak self] event in
            
            if let unreadCount = event.element?.unreadCount {
                
                self?.labelNew.isHidden = unreadCount == 0
            }
            

        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.HomeMainPagerTopTabSelectedAction.self, sticky: true).subscribe { [weak self] event in
            
            if let selectedIndex = event.element?.selectedIndex {
                self?.moveToViewController(at: selectedIndex)
            }
            

        }.disposed(by: disposeBag)

        
    }
    
    /// UnityAd 관련 초기화
    func initializeUnityAds() {
        UnityAds.initialize(Constants.Ad.unity_game_id)
        UnityAds.add(self)
        
    }
    
    func initiaizeCaulyBanner() {
        let caulySetting = CaulyAdSetting.global();
        CaulyAdSetting.setLogLevel(CaulyLogLevelDebug)  //  Cauly Log 레벨
        caulySetting?.appCode = Constants.Ad.cauly_app_code               //  발급ID 입력
        caulySetting?.animType = CaulyAnimNone          //  화면 전환 효과
        
        caulySetting?.closeOnLanding=true               // app으로 이동할 때 webview popup창을 자동으로 닫아줍니다. 기본값은 false입니다.

    }
    
    func initializeCaulyInterstitialAd() {
        self._interstitialAd=CaulyInterstitialAd.init()
        _interstitialAd?.delegate = self;    //  전면 delegate 설정
        _interstitialAd?.startRequest();     //  전면광고 요청
    }

}
extension HomeMainPagerViewController {
    func attandenceAlert() {
        let controller = UIAlertController.init(title: "", message: "광고 시청 후 출석 처리 됩니다.\n\n출석 하시겠습니까?", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "예", style: .default, handler: { [weak self] _ in
            guard let reactor = self?.reactor else { return }
            #warning("TODO : AD")
            reactor.action.onNext(.checkInAttendance)
        })
        let cancel = UIAlertAction.init(title: "아니오", style: .cancel, handler: { [weak self] _ in
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.moveToStepScreen(.checkInPage))

        })

        controller.addAction(defaultAction)
        controller.addAction(cancel)

        self.present(controller, animated: true)

    }
    func commandReactorActionVote(model: VoteDataListModel) {
        guard let reactor = reactor else { return }

        Observable.just(Reactor.Action.moveToStepScreen(CommonStep.voteUploadPage(model: model)))
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
    }

    func commandReactorActionDonate(model: DonateDataListModel) {
        guard let reactor = reactor else { return }

        Observable.just(Reactor.Action.moveToStepScreen(CommonStep.donateUploadPage(model: model)))
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
    }
    func commandReactorActionCheerDetail(board_no: Int64) {
        guard let reactor = reactor else { return }

        Observable.just(Reactor.Action.moveToStepScreen(CommonStep.cheerDetailPage(board_no: board_no)))
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
    }
    func commandReactorActionCheerUpload() {
        guard let reactor = reactor else { return }

        Observable.just(Reactor.Action.moveToStepScreen(CommonStep.cheerUploadPage))
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
    }

}
extension HomeMainPagerViewController: HomeMainPagerDelegate {
    func moveToBlog() {
        guard let reactor = reactor else { return }
        reactor.action.onNext(.moveToStepScreen(CommonStep.missionBlog(type: .blog, url: User.shared.blog_url)))

    }
    func moveToVoteHeaderAd() {
        print("moveToVoteHeaderAd")

    }
    func moveToMissionAd() {
        print("moveToMissionAd")
    }
    func moveToMissionLuckyNumber() {
        print("moveToMissionLuckyNumber")
        guard let reactor = reactor else { return }
        reactor.action.onNext(.moveToStepScreen(.missionLuckyNumberPage))
    }
    func moveToMissionPpobki() {
        print("moveToMissionPpobki")
        guard let reactor = reactor else { return }
        reactor.action.onNext(.moveToStepScreen(.ppobKiPage))
    }
    
    func moveToMissionPodong(url: String) {
        print("moveToMissionPodong")
        guard let reactor = reactor else { return }
        reactor.action.onNext(.callPodongApi(url))
    }
    
    func moveToVoteMainPage(model: VoteDataListModel) {
        commandReactorActionVote(model: model)
    }
    
    func moveToDonateMainPage(model: DonateDataListModel) {
        commandReactorActionDonate(model: model)

    }
    func moveToCheerDetailPage(board_no: Int64) {
        commandReactorActionCheerDetail(board_no: board_no)
    }

    func moveToCheerUploadPage() {
        commandReactorActionCheerUpload()
    }
    
    func moveToCheerArtistSelectPage() {
        guard let reactor = reactor else { return }
        reactor.action.onNext(.moveToStepScreen(.favSingerSelectPage(type: .choiceSinger)))
    }
    
    func moveToDonateDonePage(model: DonateDataListModel) {
        guard let reactor = reactor else { return }
        let img = model.img01 ?? ""
        guard let donate_no = model.donate_no else { return }
        let title = "\(model.name) 님의 \(model.donate_nth ?? 0)차 기부 회원"
        reactor.action.onNext(.moveToStepScreen(.donateDonePage(url: img, donate_no: donate_no, artist_no: model.artist_no, title: title)))

    }
    func moveToDonateResultPage(model: DonateDataListModel) {
        guard let reactor = reactor else { return }
        let donate_no = -1
        if model.counts ?? 0 < 2 {
            let title = "\(model.name) 님의 \(model.counts ?? 0)차 기부 회원"
            reactor.action.onNext(.moveToStepScreen(.donateResultPage(donate_no: donate_no, artist_no: model.artist_no, title: title)))

        } else {
            let title = "\(model.name) 님의 1차 ~ \(model.counts ?? 0)차 기부 회원"
            reactor.action.onNext(.moveToStepScreen(.donateResultPage(donate_no: donate_no, artist_no: model.artist_no, title: title)))

        }

    }
    
}

extension HomeMainPagerViewController: GADFullScreenContentDelegate {
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        print("Ad did fail to present full screen content. \(ad), error : \(error)")
    }

    /// Tells the delegate that the ad presented full screen content.
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Ad did present full screen content. \(ad)")
    }

    /// Tells the delegate that the ad dismissed full screen content.
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Ad did dismiss full screen content. \(ad)")
    }

}


// 유니티 광고 Delegate
extension HomeMainPagerViewController: UnityAdsDelegate {
    func unityAdsReady(_ placementId: String) {
        print("UnityAdsDelegate HOME Ready placementId : \(placementId)")
        
        if(_isViewIntroAd == false) {
            _isViewIntroAd = true
            UnityAds.show(self, placementId: "video", showDelegate: nil)
        }
    }
    
    func unityAdsDidStart(_ placementId: String) {
        print("UnityAdsDelegate HOME DidStart placementId : \(placementId)")

    }
    func unityAdsDidError(_ error: UnityAdsError, withMessage message: String) {
        print("UnityAdsDelegate HOME DidError withMessage : \(message)")

    }
    func unityAdsDidFinish(_ placementId: String, with state: UnityAdsFinishState) {
        print("UnityAdsDelegate HOME DidFinish placementId : \(placementId)")
    }
}


// Cauly 광고 Delegate (전면)
extension HomeMainPagerViewController: CaulyInterstitialAdDelegate {
    // 광고 정보 수신 성공
    func didReceive(_ interstitialAd: CaulyInterstitialAd!, isChargeableAd: Bool) {
        NSLog("Cauly Interstitial Recevie intersitial");
        
        if(_isViewIntroAd == false) {
            _isViewIntroAd = true
            _interstitialAd?.show(withParentViewController: self)
        }
    }

    func didFail(toReceive interstitialAd: CaulyInterstitialAd!, errorCode: Int32, errorMsg: String!) {
        print("Cauly Interstitial Recevie fail intersitial errorCode:\(errorCode) errorMsg:\(errorMsg!)");
        _interstitialAd = nil
    }
    //Interstitial 형태의 광고가 보여지기 직전
    func willShow(_ interstitialAd: CaulyInterstitialAd!) {
        print("Cauly Interstitial willShow")
    }
    // Interstitial 형태의 광고가 닫혔을 때
    func didClose(_ interstitialAd: CaulyInterstitialAd!) {
        print("Cauly Interstitial didClose")
        _interstitialAd=nil
    }

}
// Cauly 광고 Delegate (배너)
extension HomeMainPagerViewController: CaulyAdViewDelegate {
    //광고 정보 수신 성공
    func didReceiveAd(_ adView: CaulyAdView!, isChargeableAd: Bool) {
        print("Cauly Banner Loaded didReceiveAd callback")
        adView?.show(withParentViewController: self, target: self.view)
        
    }
    //광고 정보 수신 실패
    func didFail(toReceiveAd adView: CaulyAdView!, errorCode: Int32, errorMsg: String!) {
         print("Cauly Banner didFailToReceiveAd: \(errorCode)(\(errorMsg!))");
    }
    // 랜딩 화면 표시
    func willShowLanding(_ adView: CaulyAdView!) {
        print("Cauly Banner willShowLanding")
    }
    // 랜딩 화면이 닫혔을 때
    func didCloseLanding(_ adView: CaulyAdView!) {
        print("Cauly Banner didCloseLanding")
    }

}
