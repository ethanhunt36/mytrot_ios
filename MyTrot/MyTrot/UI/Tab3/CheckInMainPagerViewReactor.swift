//
//  CheckInMainPagerViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class CheckInMainPagerViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case checkInAttendance

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case empty
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false
        )
    }
    
    func mutate(action: CheckInMainPagerViewReactor.Action) -> Observable<CheckInMainPagerViewReactor.Mutation> {
        switch action {
        case .checkInAttendance:
            return networkService.attendanceCheckIn().asObservable().map {
                res in
                if res.isResponseStatusSuccessful() {
                    // 출석 완료
                    print("출석 완료")
                    Utils.updateAttendance()
                    RxBus.shared.post(event: Events.AttendanceCheckFinish())

                }
                return .empty
            }
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: CheckInMainPagerViewReactor.State, mutation: CheckInMainPagerViewReactor.Mutation) -> CheckInMainPagerViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty: return state
        }
        return state
    }
}
