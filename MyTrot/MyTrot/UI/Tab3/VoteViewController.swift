//
//  VoteViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit

final class VoteViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var voteAvailableCountLabel: UILabel!
    @IBOutlet weak var voteTextField: UITextField!
    @IBOutlet weak var maxVoteButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var completeMessageLabel: UILabel!
    @IBOutlet weak var completeView: UIView!
    @IBOutlet weak var completeMessageView: UIView! {
        didSet {
            completeMessageView.layer.cornerRadius = 10.0
            completeMessageView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var completeMessageTopView: UIView! {
        didSet {
            completeMessageTopView.setGradient(color1: UIColor.blue, color2: Asset.purpley.color)
        }
    }

    @IBOutlet weak var completeButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: VoteViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension VoteViewController {
    func bindAction(_ reactor: VoteViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        cancelButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        completeButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        confirmButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.clickConfirm }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        maxVoteButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.clickMaxVote }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        voteTextField.rx
            .text
            .orEmpty
            .map { Reactor.Action.changeVoteValue($0) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        
    }
}

// MARK: bindState For Reactor
private extension VoteViewController {
    func bindState(_ reactor: VoteViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.artistName }
            .bind(to: artistNameLabel.rx.text)
            .disposed(by: disposeBag)
        reactor.state.map { $0.maxVoteCount }
            .map{ "\($0)장"}
            .bind(to: voteAvailableCountLabel.rx.text)
            .disposed(by: disposeBag)
        reactor.state.map { $0.isMaxVote }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isSelected in
                self?.maxVoteButton.isSelected = isSelected
                if isSelected == true {
                    self?.voteTextField.text = "\(reactor.currentState.maxVoteCount)"
                    Observable.just(Reactor.Action.changeVoteValue("\(reactor.currentState.maxVoteCount)"))
                        .observeOn(MainScheduler.asyncInstance)
                        .bind(to: reactor.action)
                        .disposed(by: self?.disposeBag ?? DisposeBag())

                } else {
                    self?.voteTextField.text = ""

                    Observable.just(Reactor.Action.changeVoteValue(""))
                        .observeOn(MainScheduler.asyncInstance)
                        .bind(to: reactor.action)
                        .disposed(by: self?.disposeBag ?? DisposeBag())
                }
            })
            .disposed(by: disposeBag)
        reactor.state.map { $0.completeMessage }
            .filterNil()
            .take(1)
            .subscribe(onNext: { [weak self] message in
                self?.completeMessageLabel.text = message
                self?.completeView.isHidden = false
            }).disposed(by: disposeBag)
    }
}

// MARK: -
// MARK: private initialize
private extension VoteViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureScrollView))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(singleTapGestureRecognizer)

    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
    @objc func tapGestureScrollView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

}


// MARK: - Reactor Action
private extension VoteViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
//            Observable.just(Reactor.Action.<# 액션명 #>)
//                .observeOn(MainScheduler.asyncInstance)
//                .bind(to: reactor.action)
//                .disposed(by: disposeBag)
    //    }
}

// MARK: -

extension UIView{
    func setGradient(color1:UIColor,color2:UIColor){
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [color1.cgColor,color2.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = bounds
        layer.addSublayer(gradient)
    }
}
