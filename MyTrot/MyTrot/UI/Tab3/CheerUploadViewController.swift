//
//  CheerUploadViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit
import RxOptional

final class CheerUploadViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var singerLabel: UILabel!
    @IBOutlet weak var nickNameLabel: UILabel!{
        didSet {
            nickNameLabel.text = User.shared.myInfo?.nick ?? ""
        }
    }
    @IBOutlet weak var singerSelectButton: UIButton!

    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var uploadTextView: UITextView!
    @IBOutlet weak var uploadSendButton: UIButton!
    @IBOutlet weak var uploadCancelButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: CheerUploadViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension CheerUploadViewController {
    func bindAction(_ reactor: CheerUploadViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        uploadCancelButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        uploadSendButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { [weak self] _ in
                Reactor.Action.uploadBoard(cont: self?.uploadTextView.text ?? "") }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        singerSelectButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.moveToStepScreen(.favSingerSelectPage(type: .choiceUploadSinger)) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

    }
}

// MARK: bindState For Reactor
private extension CheerUploadViewController {
    func bindState(_ reactor: CheerUploadViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        
        reactor.state.map { $0.singerName}
            .bind(to: singerLabel.rx.text)
            .disposed(by: disposeBag)
        
        reactor.state.map { $0.alertBackMessage}
            .distinctUntilChanged()
            .filterNil()
            .subscribe(onNext: { [weak self] alertBackMessage in
                self?.subscribeOnNextBaseAlertPushBack(alertData: alertBackMessage)
                
            }).disposed(by: disposeBag)


    }
}

// MARK: -
// MARK: private initialize
private extension CheerUploadViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureScrollView))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(singleTapGestureRecognizer)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }

    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.UploadSelectArtistInfo.self, sticky: true).subscribe { [weak self] event in
            
            if let artist_no = event.element?.artist_no , let artist_name = event.element?.artist_name {
                guard let reactor = self?.reactor else { return }
                reactor.action.onNext(.setSingerName(artist_name))
                reactor.action.onNext(.setSingerNo(artist_no))
            }
            

        }.disposed(by: disposeBag)
    }
}
// MARK: Private Function
private extension CheerUploadViewController {
    @objc func tapGestureScrollView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        bottomAnchorConstraint.constant = ((view.convert(keyboardFrame.cgRectValue, from: nil).size.height) - view.safeAreaInsets.bottom - 80 )
        print("keyboard height : \((view.convert(keyboardFrame.cgRectValue, from: nil).size.height))")
        print("view.safeAreaInsets.bottom : \(view.safeAreaInsets.bottom)")

//        commentView.isHidden = false
//
//        print("view.safeAreaInsets.top  :  \(view.safeAreaInsets.top )")
//        print("view.safeAreaInsets.bottom  :  \(view.safeAreaInsets.bottom )")
//        UIView.animate(withDuration: 0.30) { [weak self] in
//            self?.view.layoutIfNeeded()
//        }
        placeHolderLabel.isHidden = true
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        bottomAnchorConstraint.constant = 0
        if uploadTextView.hasText == false {
            placeHolderLabel.isHidden = false
        }
    }

}


// MARK: - Reactor Action
private extension CheerUploadViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
