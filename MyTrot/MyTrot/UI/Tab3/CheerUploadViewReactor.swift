//
//  CheerUploadViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class CheerUploadViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case setSingerName(String)
        case setSingerNo(Int64)
        case setUploadStatus(UploadStatus)

        case uploadBoard(cont: String)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setSingerName(String)
        case setSingerNo(Int64)
        case setUploadStatus(UploadStatus)
        case setResponse(CommonResponse)
        case empty

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var uploadStatus: UploadStatus
        var singerName: String
        var singerNo: Int64
        var alertBackMessage: String?
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            uploadStatus: .none,
            singerName: User.shared.myInfo?.artist_name ?? "" ,
            singerNo: User.shared.myInfo?.artist_no ?? 0

        )
    }
    
    func mutate(action: CheerUploadViewReactor.Action) -> Observable<CheerUploadViewReactor.Mutation> {
        switch action {
        case .uploadBoard(let cont):
            if preferencesService.getIsUserAgreeTerms() == false {
                Utils.AlertShow(msg: Constants.String.AlertMessage.agreementDeniedMessage)
                
                return .empty()
            }
            return networkService.uploadBoard(artist_no: currentState.singerNo, name: User.shared.myInfo?.nick ?? "", cont: cont).asObservable().map { Mutation.setResponse($0)}
        case .setSingerName(let name):
            return .just(.setSingerName(name))

        case .setSingerNo(let no):
            return .just(.setSingerNo(no))

        case .setUploadStatus(let status):
            return .just(.setUploadStatus(status))
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: CheerUploadViewReactor.State, mutation: CheerUploadViewReactor.Mutation) -> CheerUploadViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setSingerName(let name):
            state.singerName = name
        case .setSingerNo(let no):
            state.singerNo = no
        case .setUploadStatus(let status):
            state.uploadStatus = status
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.alertBackMessage = response.msg?.replaceSlashString ?? ""
                
                RxBus.shared.post(event: Events.CheerUploadFinish())

            } else {
                RxBus.shared.post(event: Events.AlertShow(message: response.msg ?? ""))
            }
        case .empty:
            return state
        }
        return state
    }
}
