//
//  HomeMainPagerViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class HomeMainPagerViewReactor: BaseReactor, EventType {
    enum Event {
        case missionRowClick(code: String, url: String)
    }
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        
        case moveToStepScreen(CommonStep)

        case checkInAttendance
        
        case refreshMyInfo
        
        case callPodongApi(String)
        
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case empty
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
    }
    
    /// 초기 상태값
    let initialState: State
    let networkService = MyTrotService()
    var preferencesService: PreferencesService = PreferencesService()

    init() {
        self.initialState = State(
            isDismiss: false
        )
    }
    
    func mutate(action: HomeMainPagerViewReactor.Action) -> Observable<HomeMainPagerViewReactor.Mutation> {
        switch action {
        case .callPodongApi(let url):
            return networkService.podongCheck().asObservable().map {
                [weak self] res in
                if res.isResponseStatusSuccessful() {
                    // 미션성공
                    print("포동 체크 완료 ")
                }
                self?.navigate(step: CommonStep.missionHumorPage(url: url ))
                return .empty
            }
        case .refreshMyInfo:
            return networkService.getMyInfoByNo().asObservable().map {
                res in
                if res.isResponseStatusSuccessful() {
                    User.shared.updateMyInformation(data: res.data)
                }
                return .empty
            }
        case .checkInAttendance:
            return networkService.attendanceCheckIn().asObservable().map {
                res in
                if res.isResponseStatusSuccessful() {
                    // 출석 완료
                    print("출석 완료")
                    
                    Toast(text: res.msg, duration: 0.9).show()
                    
                    Utils.updateAttendance()
                }
                return .empty
            }
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
            
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: HomeMainPagerViewReactor.State, mutation: HomeMainPagerViewReactor.Mutation) -> HomeMainPagerViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty: return state
        }
        return state
    }
    
    func transform(mutation: Observable<Mutation>) -> Observable<Mutation> {
        let eventMutation = HomeMainPagerViewReactor.event.flatMap { event -> Observable<Mutation> in
            switch event {
            case .missionRowClick(let code, let url):
                /**
                 GET_AD_BONUS : 광고보고 - 더보기
                 VOTE : 투표하기 - 투표 탭으로 이동
                 DONATION : 기부참여 - 기부 탭으로 이동
                 CHEERUP_WRTE : 응원글 작성 - 응원탭으로 이동
                 */
                print("missionRowClick code: \(code), url: \(url)")
                return .empty()
            }
        }
        return Observable.merge(mutation, eventMutation)
    }
}
