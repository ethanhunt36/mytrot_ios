//
//  DonateViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit

final class DonateViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var donateMyHavingCountLabel: UILabel!
    @IBOutlet weak var donateGoalAvailableCountLabel: UILabel!
    @IBOutlet weak var donateCurrentCountLabel: UILabel!
    @IBOutlet weak var donateTimeCountLabel: UILabel!
    @IBOutlet weak var donateTextField: UITextField!
    @IBOutlet weak var maxDonateButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var completeMessageLabel: UILabel!
    @IBOutlet weak var completeView: UIView!
    @IBOutlet weak var completeMessageView: UIView! {
        didSet {
            completeMessageView.layer.cornerRadius = 10.0
            completeMessageView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var completeMessageTopView: UIView! {
        didSet {
            completeMessageTopView.setGradient(color1: UIColor.blue, color2: Asset.purpley.color)
        }
    }

    @IBOutlet weak var completeButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: DonateViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension DonateViewController {
    func bindAction(_ reactor: DonateViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        cancelButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        completeButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        confirmButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.clickConfirm }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        maxDonateButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.clickMaxDonate }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        donateTextField.rx
            .text
            .orEmpty
            .map { Reactor.Action.changeDonateValue($0) }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        
    }
}

// MARK: bindState For Reactor
private extension DonateViewController {
    func bindState(_ reactor: DonateViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.artistName }
            .map { "\($0)님의 이름으로\n기부할 포인트를 입력해주세요. "}
            .bind(to: artistNameLabel.rx.text)
            .disposed(by: disposeBag)
        reactor.state.map { $0.maxDonateCount }
            .map{"\($0)".insertComma}
            .map{ "\($0) P"}
            .bind(to: donateMyHavingCountLabel.rx.text)
            .disposed(by: disposeBag)
        reactor.state.map { $0.donateGoalCount }
            .map{"\($0)".insertComma}
            .map{ "\($0) P"}
            .bind(to: donateGoalAvailableCountLabel.rx.text)
            .disposed(by: disposeBag)
        reactor.state.map { $0.donateCurrentCount }
            .map{"\($0)".insertComma}
            .map{ "\($0) P"}
            .bind(to: donateCurrentCountLabel.rx.text)
            .disposed(by: disposeBag)
        reactor.state.map { $0.currentTime }
            .map{ "\($0)차 목표"}
            .bind(to: donateTimeCountLabel.rx.text)
            .disposed(by: disposeBag)
        reactor.state.map { $0.isMaxDonate }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isSelected in
                self?.maxDonateButton.isSelected = isSelected
                if isSelected == true {
                    self?.donateTextField.text = "\(Int(reactor.currentState.maxDonateCount))"
                    Observable.just(Reactor.Action.changeDonateValue("\(Int(reactor.currentState.maxDonateCount))"))
                        .observeOn(MainScheduler.asyncInstance)
                        .bind(to: reactor.action)
                        .disposed(by: self?.disposeBag ?? DisposeBag())

                } else {
                    self?.donateTextField.text = ""

                    Observable.just(Reactor.Action.changeDonateValue(""))
                        .observeOn(MainScheduler.asyncInstance)
                        .bind(to: reactor.action)
                        .disposed(by: self?.disposeBag ?? DisposeBag())
                }
            })
            .disposed(by: disposeBag)
        reactor.state.map { $0.completeMessage }
            .filterNil()
            .take(1)
            .subscribe(onNext: { [weak self] message in
                self?.completeMessageLabel.text = message
                self?.completeView.isHidden = false
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension DonateViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureScrollView))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(singleTapGestureRecognizer)
    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
    @objc func tapGestureScrollView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

}


// MARK: - Reactor Action
private extension DonateViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
