//
//  HomeDonateListViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class HomeDonateListViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case getList(DonateListType)
        case setMyFavTop(Bool, isUpdate: Bool)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setItems(ResponseDonateList)
        case setType(DonateListType)
        case setMyFavTop(Bool, isUpdate: Bool)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var type: DonateListType
        var items: [DonateDataListModel]
        var headerModel: DonateHeaderDataModel
        var isMyFavTop: Bool

    }
    
    /// 초기 상태값
    let initialState: State
    let networkService = MyTrotService()
    let preference = PreferencesService()

    init() {
        self.initialState = State(
            isDismiss: false,
            type: .doing,
            items: [],
            headerModel: DonateHeaderDataModel(isSelectedFavSinger: preference.getIsSelectdMyFavSinger(), viewListType: DonateListType.doing.rawValue),
            isMyFavTop : false

        )
    }
    
    func mutate(action: HomeDonateListViewReactor.Action) -> Observable<HomeDonateListViewReactor.Mutation> {
        switch action {
        case .setMyFavTop(let flag, let isUpdate):
            return .just(.setMyFavTop(flag, isUpdate: isUpdate))

        case .getList(let type):
            return .concat([
                .just(.setType(type)),
                self.networkService.donateList(type: type).asObservable().map{ Mutation.setItems($0)}
            ])
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: HomeDonateListViewReactor.State, mutation: HomeDonateListViewReactor.Mutation) -> HomeDonateListViewReactor.State {
        var state = state
        switch mutation {
        case .setItems(let response):
            if response.isResponseStatusSuccessful() == true {
                // alert
                var sectionList: [DonateDataListModel] = []
                state.headerModel.done_data = response.data?.done_data
                if let data: DonateDataModel = response.data {
                    for var listOne in data.list_data {
                        listOne.donateType = currentState.type
                        if sectionList.count == 0 {
                            listOne.donateHeaderModel = state.headerModel
                            sectionList.append(listOne)
                        }
                        sectionList.append(listOne)
                    }
                }

                state.items = sectionList
                
                let flag = preference.getIsSelectdMyFavSinger()
                state = arrangeList(state: state, flag: flag)

            }

        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setType(let type):
            state.headerModel.viewListType = type.rawValue

            state.type = type
        case .setMyFavTop(let flag, let isUpdate):
            // 최애 탑 설정
            if isUpdate == true {
                preference.setIsSelectdMyFavSinger(isSelectdMyFavSinger: flag)
            }
            state = arrangeList(state: state, flag: flag)

        }
        return state
    }
    
    func arrangeList(state: HomeDonateListViewReactor.State, flag: Bool) -> HomeDonateListViewReactor.State {
        var state = state

        if flag == true {
            state.headerModel.isSelectedFavSinger = true
            var tempList: [DonateDataListModel] = []
            tempList.append(contentsOf: state.items)
            state.items.removeAll()
            let myArtistNo = User.shared.myInfo?.artist_no ?? 0
            for var listOne in tempList {
                if state.items.count == 0 {
                    state.headerModel.isSelectedFavSinger = true
                    listOne.donateHeaderModel = state.headerModel
                    state.items.append(listOne)
                } else if myArtistNo == listOne.artist_no {
                    state.items.insert(listOne, at: 1)
                } else {
                    state.items.append(listOne)
                }
                
            }

        } else {
            state.headerModel.isSelectedFavSinger = false
        }
        state.isMyFavTop = flag

        return state
    }
}
