//
//  CheckInRankViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class CheckInRankViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        
        case getList

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(ResponseAttendanceRankList)
        case empty

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var items: [AttendanceRankDataListModel]

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            items: []
        )
    }
    
    func mutate(action: CheckInRankViewReactor.Action) -> Observable<CheckInRankViewReactor.Mutation> {
        switch action {
        case .getList:
            return networkService.attendanceRankList().asObservable().map { Mutation.setResponse($0) }
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: CheckInRankViewReactor.State, mutation: CheckInRankViewReactor.Mutation) -> CheckInRankViewReactor.State {
        var state = state
        switch mutation {
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                let listData = response.data?.list_data ?? []
                if state.items.count == 0, let firstObj = listData.first {
                    let headerData = AttendanceRankDataListModel(nick: "", message: "연속 출석 기록 TOP100을 소개합니다.")
                    
                    state.items.append(headerData)
                    state.items.append(contentsOf: listData)
                }
            } else {
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)

            }

        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty: return state
        }
        return state
    }
}
