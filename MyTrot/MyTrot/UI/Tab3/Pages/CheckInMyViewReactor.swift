//
//  CheckInMyViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class CheckInMyViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        
        case getList
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(ResponseAttendanceMyList)
        case setLastNo(String)
        case empty

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var last_no: String
        var items: [AttendanceMyDataListModel]
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            last_no: "",
            items: []

        )
    }
    
    func mutate(action: CheckInMyViewReactor.Action) -> Observable<CheckInMyViewReactor.Mutation> {
        switch action {
        case .getList:
            return networkService.attendanceMyList(lastNo: currentState.last_no).asObservable().map { Mutation.setResponse($0) }
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: CheckInMyViewReactor.State, mutation: CheckInMyViewReactor.Mutation) -> CheckInMyViewReactor.State {
        var state = state
        switch mutation {
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.last_no = response.data?.last_no ?? ""
                let listData = response.data?.list_data ?? []
                if state.items.count == 0, let firstObj = listData.first {
                    let headerData = AttendanceMyDataListModel(message: firstObj.message, point: firstObj.point, reg_dttm: firstObj.reg_dttm, combo: firstObj.combo, bonus: response.data?.bonus ?? 0)
                    
                    state.items.append(headerData)
                    state.items.append(contentsOf: listData)
                } else {
                    state.items = listData
                }
            } else {
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)

            }

        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setLastNo(let lastNo):
            state.last_no = lastNo
        case .empty: return state
        }
        return state
    }
}
