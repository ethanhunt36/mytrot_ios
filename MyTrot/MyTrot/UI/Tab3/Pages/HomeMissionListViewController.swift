//
//  HomeMissionListViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation

import UIKit
import ReactorKit
import XLPagerTabStrip
enum MissionType: String{
    case READ_AD_MOVIE = "READ_AD_MOVIE"
    case READ_PODONG = "READ_PODONG"
    case READ_LUCKY = "READ_LUCKY"
    case PPOBKKI = "PPOBKKI"
}
final class HomeMissionListViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    var itemInfo = IndicatorInfo(title: "View")
    weak var pagerDelegate: HomeMainPagerDelegate?

    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var tableViewList: [MissionDataListModel] = []
    
    var isCompleteMission: Bool = false

    @IBOutlet weak var completeMissionButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    
    }
    
    func bind(reactor: HomeMissionListViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let reactor = reactor else { return }
        reactor.action.onNext(.getMissionList)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension HomeMissionListViewController {
    func bindAction(_ reactor: HomeMissionListViewReactor) {
        completeMissionButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.completeMission }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
    }
}

// MARK: bindState For Reactor
private extension HomeMissionListViewController {
    func bindState(_ reactor: HomeMissionListViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.responseMission }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] model in
                guard let list = model?.data?.list else { return }
                guard var headerObject = list.first else { return }
                headerObject.voteCount = model?.data?.reward?.reward_vote ?? 0
                headerObject.completeCount = model?.data?.success_cnt ?? 0
                headerObject.pointCount = model?.data?.reward?.reward_point ?? 0
                self?.tableViewList.removeAll()
                self?.tableViewList.append(headerObject)
                self?.tableViewList.append(contentsOf: list)
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension HomeMissionListViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()

    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension HomeMissionListViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
// MARK: - IndicatorInfoProvider
extension HomeMissionListViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}
extension HomeMissionListViewController {
    func initializeTableView() {
        self.tableView.register(cellType: MissionTopCell.self)
        self.tableView.register(cellType: MissionNormalCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension HomeMissionListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        if indexPath.row == 0 { return }
    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 85

        } else {
            return 50

        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(for: indexPath) as MissionTopCell
            cell.item = self.tableViewList[indexPath.row]
            
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(for: indexPath) as MissionNormalCell
            cell.item = self.tableViewList[indexPath.row]
            
            cell.delegate = self
            guard let reactor = reactor else { return cell }
            cell.humorUrl = reactor.currentState.humorUrl
            return cell

        }

    }
}
extension HomeMissionListViewController: MissionNormalCellDelegate {
    func moveToMission(code: String, humorUrl: String) {
        print("mission code : \(code)")
        switch code {
            
        case MissionType.READ_PODONG.rawValue:
            if let delegate = pagerDelegate {
                delegate.moveToMissionPodong(url: humorUrl)
            }
        case MissionType.READ_AD_MOVIE.rawValue:
            if let delegate = pagerDelegate {
                delegate.moveToMissionAd()
            }
        case MissionType.READ_LUCKY.rawValue:
            if let delegate = pagerDelegate {
                delegate.moveToMissionLuckyNumber()
            }
        case MissionType.PPOBKKI.rawValue:
            if let delegate = pagerDelegate {
                delegate.moveToMissionPpobki()
            }
        case "VOTE":
            RxBus.shared.post(event: Events.HomeMainPagerTopTabSelectedAction(selectedIndex: 0))
        case "DONATION":
            RxBus.shared.post(event: Events.HomeMainPagerTopTabSelectedAction(selectedIndex: 1))
        case "GET_AD_BONUS":
            RxBus.shared.post(event: Events.MainTabSelectedAction(selectedIndex: 4))

        case "CHEER":
            RxBus.shared.post(event: Events.HomeMainPagerTopTabSelectedAction(selectedIndex: 2))
        case "BLOG":
            print("blog")
            if let delegate = pagerDelegate {
                delegate.moveToBlog()
            }

        default:
            break;
        }
    }
    
}
