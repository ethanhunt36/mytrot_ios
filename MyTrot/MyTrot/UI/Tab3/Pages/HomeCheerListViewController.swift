//
//  HomeCheerListViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation

import UIKit
import ReactorKit
import XLPagerTabStrip

final class HomeCheerListViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    var itemInfo = IndicatorInfo(title: "View")
    weak var pagerDelegate: HomeMainPagerDelegate?

    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var isListLoadFinish: Bool = false
    var tableViewList: [CheerDataListModel] = []
    var tableViewTotalList: [CheerTotalDataListModel] = []
    
    @IBOutlet weak var uploadButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: HomeCheerListViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("HomeCheerListViewController viewDidAppear")
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension HomeCheerListViewController {
    func bindAction(_ reactor: HomeCheerListViewReactor) {
        uploadButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.pagerDelegate?.moveToCheerUploadPage()
            }).disposed(by: disposeBag)

        reactor.action.onNext(.getCheerApiByType(.recent))
    }
}

// MARK: bindState For Reactor
private extension HomeCheerListViewController {
    func bindState(_ reactor: HomeCheerListViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.cheerModel }
            .distinctUntilChanged()
//            .filter{ $0?.list_data.count ?? 0 > 0 }
            .subscribe(onNext: { [weak self] model in
                if reactor.currentState.isAddtionalLoading == true {
                    print("isAddtionalLoading == true")
                    self?.isListLoadFinish = true
                    self?.tableViewList.append(contentsOf: model?.list_data ?? [])
                } else {
                    print("isAddtionalLoading == false")
                    self?.isListLoadFinish = true
                    self?.tableViewList = [CheerDataListModel(no: 0, artist_pic: "", artist_name: "", my_like_cnt: 0, cont: "", reg_dttm: "", nick: "", lv: 0, like_cnt: 0, comment_cnt: 0, del_yn: "", cheerHeaderModel: reactor.currentState.headerModel, cheerListType: reactor.currentState.type)]
                    self?.tableViewList.append(contentsOf: model?.list_data ?? [])

                }
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)
        reactor.state.map { $0.cheerTotalModel }
            .filter{ $0?.list_data.count ?? 0 > 0 }
            .subscribe(onNext: { [weak self] model in
//                self?.tableViewTotalList = [CheerTotalDataListModel(no: 0, name: "", pic: "", rank1: 0, bbs_cnt: 0, my_bbs_cnt: 0, like_cnt: 0, my_like_cnt: 0, cheerHeaderModel: reactor.currentState.headerModel, cheerListType: reactor.currentState.type)]
                print("reactor.currentState.headerModel :: \(reactor.currentState.headerModel.viewListType)")
                print("reactor.currentState.type :: \(reactor.currentState.type)")
                self?.tableViewTotalList = [CheerTotalDataListModel(no: 0, name: "", pic: "", rank1: 0, bbs_cnt: 0, my_bbs_cnt: 0, like_cnt: 0, my_like_cnt: 0, cheerHeaderModel: reactor.currentState.headerModel, cheerListType: reactor.currentState.type)]

                self?.tableViewTotalList.append(contentsOf: model?.list_data ?? [])
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)

//        reactor.state.map { $0.artist_name }
//            .distinctUntilChanged()
//            .subscribe(onNext: { [weak self] name in
//                guard var headerModel = self?.tableViewList.first else { return }
//                if name == "" {
//                    headerModel.cheerHeaderModel?.isSelectedFavSinger = false
//                }
//                headerModel.cheerHeaderModel?.favSingerName = name
//                self?.tableViewList.removeFirst()
//                self?.tableViewList.insert(headerModel, at: 0)
//                self?.tableView.reloadData()
//                
//            }).disposed(by: disposeBag)
    }
}

// MARK: -
// MARK: private initialize
private extension HomeCheerListViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.SelectArtistInfo.self, sticky: true).subscribe { [weak self] event in
            
            if let artist_no = event.element?.artist_no , let artist_name = event.element?.artist_name {
                guard let reactor = self?.reactor else { return }
                Observable.just(Reactor.Action.cheerListByArtistNo(artist_no, artist_name, .recent))
                    .observeOn(MainScheduler.asyncInstance)
                    .bind(to: reactor.action)
                    .disposed(by: self?.disposeBag ?? DisposeBag())

            }
            

        }.disposed(by: disposeBag)
        
        bus.asObservable(event: Events.CheerFinish.self, sticky: true).subscribe { [weak self] event in
            guard let id = event.element?.id else { return }
            guard let likeCount = event.element?.likeCount else { return }
            guard let commentCount = event.element?.commentCount else { return }
            guard let isLike = event.element?.isLike else { return }
            let tempList = self?.tableViewList ?? []
            var cheerList: [CheerDataListModel] = []
            for item in tempList {
                if item.no == id {
                    var changeCheer = item
                    if isLike {
                        changeCheer.my_like_cnt = 1
                    } else {
                        changeCheer.my_like_cnt = 0
                    }
                    changeCheer.like_cnt = item.like_cnt + likeCount
                    changeCheer.comment_cnt = item.comment_cnt + commentCount
                    cheerList.append(changeCheer)
                } else {
                    cheerList.append(item)
                }
                
            }
            self?.tableViewList.removeAll()
            self?.tableViewList.append(contentsOf: cheerList)
            self?.tableView.reloadData()
            
        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.CheerUploadFinish.self, sticky: true).subscribe { [weak self] _ in
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.getCheerApiByDefault)

        }.disposed(by: disposeBag)

    }
}


// MARK: - Reactor Action
private extension HomeCheerListViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction 액션명 () {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
// MARK: - IndicatorInfoProvider
extension HomeCheerListViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}

extension HomeCheerListViewController {
    func initializeTableView() {
        self.tableView.register(cellType: CheerMainCell.self)
        self.tableView.register(cellType: CheerMainHeaderCell.self)
        self.tableView.register(cellType: CheerMainTotalCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension HomeCheerListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        if indexPath.row == 0 { return }

        guard let reactor = reactor else { return }
        let type = reactor.currentState.type
        if type == CheerListType.total {
            //
            let name = tableViewTotalList[indexPath.row].name
            let artistNo = tableViewTotalList[indexPath.row].no
            // name 으로 검색 ->
            // cheerListByArtistNo
            Observable.just(Reactor.Action.cheerListByArtistNo(artistNo, name, .recent))
                .observeOn(MainScheduler.asyncInstance)
                .bind(to: reactor.action)
                .disposed(by: disposeBag)

        } else {
            // 응원 상세 페이지 이동 delegate ->
            let board_no = tableViewList[indexPath.row].no
            pagerDelegate?.moveToCheerDetailPage(board_no: board_no)
        }

    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        guard let reactor = reactor else { return 0 }
        let type = reactor.currentState.type
        if type == CheerListType.total {
            return self.tableViewTotalList.count

        } else {
            return self.tableViewList.count

        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let reactor = reactor else { return 0 }
        if indexPath.row == 0 {
            return 52
        }
        let type = reactor.currentState.type
        if type == CheerListType.total {
            return 100

        } else {
            return UITableView.automaticDimension

        }


    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let reactor = reactor else { return UITableViewCell() }
        let type = reactor.currentState.type

        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(for: indexPath) as CheerMainHeaderCell
            if type == CheerListType.total {
                cell.item = CheerDataListModel(no: 0, artist_pic: "", artist_name: "", my_like_cnt: 0, cont: "", reg_dttm: "", nick: "", lv: 0, like_cnt: 0, comment_cnt: 0, del_yn: "", cheerHeaderModel: reactor.currentState.headerModel, cheerListType: reactor.currentState.type)
            } else {
                cell.item = self.tableViewList[indexPath.row]
            }
            cell.delegate = self
            return cell

        } else {
            if type == CheerListType.total {
                
                let cell = tableView.dequeueReusableCell(for: indexPath) as CheerMainTotalCell
                
                cell.rankLabel.text = "\(indexPath.row)"
                cell.item = self.tableViewTotalList[indexPath.row]
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(for: indexPath) as CheerMainCell
                cell.item = self.tableViewList[indexPath.row]
                cell.delegate = self
                return cell

            }

        }

    }
}
extension HomeCheerListViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.height

        // 스크롤이 테이블 뷰 Offset의 끝에 가게 되면 다음 페이지를 호출
        let didScrollGoToEndOfTableView: Bool = offsetY > (contentHeight - height)
        if didScrollGoToEndOfTableView, isListLoadFinish == true {
            isListLoadFinish = false
            print("호출")
            guard let reactor = reactor else { return }
            if reactor.currentState.last_no != "" {
                reactor.action.onNext(.getCheerApi)
            }
        }
    }
}

extension HomeCheerListViewController: CheerMainHeaderCellDelegate {
    func sortList(type: CheerListType) {
        print("sortList : \(type)")
        guard let reactor = reactor else { return }

        if type == .total {
            Observable.just(Reactor.Action.cheerTotalList)
                .observeOn(MainScheduler.asyncInstance)
                .bind(to: reactor.action)
                .disposed(by: disposeBag)

        } else {
            Observable.just(Reactor.Action.getCheerApiByType(type))
                .observeOn(MainScheduler.asyncInstance)
                .bind(to: reactor.action)
                .disposed(by: disposeBag)

        }

    }
    
    func checkFavTopSinger(isCheck: Bool) {
        print("checkFavTopSinger : \(isCheck)")
        guard let reactor = reactor else { return }
        reactor.action.onNext(.cheerListByArtistNo(-1, "", reactor.currentState.type))

    }
    func checkTotalSinger(isCheck: Bool) {
        print("checkTotalSinger : \(isCheck)")
        if let delegate = pagerDelegate {
            delegate.moveToCheerArtistSelectPage()
        }
    }
    
}

extension HomeCheerListViewController: CheerMainCellDelegate {
    func like(model: CheerDataListModel) {
        guard let reactor = reactor else { return }
        reactor.action.onNext(.like(boardNo: model.no))
    }
    func unLike(model: CheerDataListModel) {
        guard let reactor = reactor else { return }
        reactor.action.onNext(.unLike(boardNo: model.no))

    }
    func block(boardNo: Int64) {
        openDeleteAlert(boardNo: boardNo)
    }
    func openDeleteAlert(boardNo: Int64) {
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        let deleteArticle =  UIAlertAction(title: "블라인드", style: .default) { [weak self] (action) in
            print("블라인드!!")
            let controller = UIAlertController.init(title: "", message: "해당 글을 블라인드 처리 하시겠습니까?", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "확인", style: .default, handler: { [weak self] _ in
                guard let reactor = self?.reactor else { return }
                reactor.action.onNext(.reloadWithoutNo(boardNo: boardNo))
            })
            let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)

            controller.addAction(defaultAction)
            controller.addAction(cancel)

            self?.present(controller, animated: true)

        }

        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        alert.addAction(deleteArticle)

        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)


    }

}
