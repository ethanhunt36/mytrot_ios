//
//  HomeCheerListViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class HomeCheerListViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        
        case getCheerApi
        
        case getCheerApiByDefault
        case getCheerApiByType(CheerListType)
        
        case cheerTotalList
        case cheerListByArtistNo(Int64, String, CheerListType)
        case like(boardNo: Int64)
        case unLike(boardNo: Int64)
        
        case reloadWithoutNo(boardNo: Int64)
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponseCheerList(ResponseCheerList)
        case setResponseCheerTotalList(ResponseCheerTotalList)
        case setAddtionalLoading(Bool)
        case setCheerType(CheerListType)
        case setLastNo(String)
        case setArtistName(String)
        case setArtistNo(Int64)
        case setLike(Int64, Bool)
        case setBlockNo(Int64)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var cheerModel: CheerDataModel?
        var type: CheerListType
        var last_no: String
        var isAddtionalLoading: Bool
        var headerModel: CheerHeaderDataModel
        var cheerTotalModel: CheerTotalDataModel?
        var artist_no : Int64
        var artist_name: String
        var blockArticles: [Int64]
    }
    
    /// 초기 상태값
    let initialState: State
    let networkService = MyTrotService()

    init() {
        self.initialState = State(
            isDismiss: false,
            type: CheerListType.recent,
            last_no: "",
            isAddtionalLoading: false,
            headerModel: CheerHeaderDataModel(isSelectedFavSinger: false, favSingerName: "", viewListType: CheerListType.popular.rawValue),
            artist_no: -1,
            artist_name: "",
            blockArticles: []
        )
    }
    
    func mutate(action: HomeCheerListViewReactor.Action) -> Observable<HomeCheerListViewReactor.Mutation> {
        switch action {
        case .reloadWithoutNo(let boardNo):
            return .concat([
                .just(.setAddtionalLoading(currentState.isAddtionalLoading)),
                .just(.setCheerType(currentState.type)),
                .just(.setLastNo(currentState.last_no)),
                .just(.setArtistName(currentState.artist_name)),
                .just(.setArtistNo(currentState.artist_no)),
                .just(.setBlockNo(boardNo)),
                networkService.cheerListByArtistNo(type: currentState.type, lastNo: currentState.last_no, artist_no: currentState.artist_no).asObservable().map{ Mutation.setResponseCheerList($0)}

            ])
        case .like(let boardNo):
            return networkService.boardLike(board_no: boardNo).asObservable().map { _ in Mutation.setLike(boardNo, true)}
        case .unLike(let boardNo):
            return networkService.boardUnLike(board_no: boardNo).asObservable().map { _ in Mutation.setLike(boardNo, false)}

        case .cheerListByArtistNo(let artist_no, let artist_name, let type):
            return .concat([
                .just(.setAddtionalLoading(false)),
                .just(.setCheerType(type)),
                .just(.setLastNo("")),
                .just(.setArtistName(artist_name)),
                .just(.setArtistNo(artist_no)),
                networkService.cheerListByArtistNo(type: type, lastNo: "", artist_no: artist_no).asObservable().map{ Mutation.setResponseCheerList($0)}
            ])

        case .cheerTotalList:
            return .concat([
                .just(.setAddtionalLoading(false)),
                .just(.setCheerType(.total)),
                .just(.setLastNo("")),
                networkService.cheerTotalList().asObservable().map{ Mutation.setResponseCheerTotalList($0)}
            ])

        case .getCheerApiByType(let type):
            return .concat([
                .just(.setAddtionalLoading(false)),
                .just(.setCheerType(type)),
                .just(.setLastNo("")),
                networkService.cheerListByArtistNo(type: type, lastNo: "", artist_no: currentState.artist_no).asObservable().map{ Mutation.setResponseCheerList($0)}
            ])
        case .getCheerApiByDefault:
            return .concat([
                .just(.setAddtionalLoading(false)),
                .just(.setCheerType(.recent)),
                .just(.setLastNo("")),
                .just(.setArtistName("")),
                .just(.setArtistNo(-1)),
                networkService.cheerListByArtistNo(type: .recent, lastNo: "", artist_no: -1).asObservable().map{ Mutation.setResponseCheerList($0)}
            ])

        case .getCheerApi:
            return .concat([
                .just(.setAddtionalLoading(true)),
                networkService.cheerListByArtistNo(type: currentState.type, lastNo: currentState.last_no, artist_no: currentState.artist_no).asObservable().map{ Mutation.setResponseCheerList($0)}
            ])
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: HomeCheerListViewReactor.State, mutation: HomeCheerListViewReactor.Mutation) -> HomeCheerListViewReactor.State {
        var state = state
        switch mutation {
        case .setLike(let no, let isLike):
            guard let tempModel = state.cheerModel else { return state }
            var tempList: [CheerDataListModel] = []
            let optionalList = tempModel.list_data ?? []
            for var item in optionalList {
                if no == item.no {
                    if isLike == true {
                        item.like_cnt = item.like_cnt + 1
                    } else {
                        item.like_cnt = item.like_cnt - 1
                    }
                    
                    item.my_like_cnt = isLike ? 1 : 0
                }
                tempList.append(item)
            }
            var model = CheerDataModel(err: 0, msg: "")
            model.last_no = tempModel.last_no
            model.list_data = tempList
//            let model = CheerDataModel(last_no: tempModel.last_no, list_data: tempList)
            state.cheerModel = model


        case .setArtistNo(let no):
            state.artist_no = no
        case .setArtistName(let name):
            state.artist_name = name
            state.headerModel.favSingerName = name
            if name == "" {
                state.headerModel.isSelectedFavSinger = false
            } else {
                state.headerModel.isSelectedFavSinger = true
            }
        case .setResponseCheerTotalList(let response):
            if response.isResponseStatusSuccessful() {
                state.headerModel.viewListType = CheerListType.total.rawValue
                state.cheerTotalModel = response.data

            }
        case .setResponseCheerList(let response):
            if response.isResponseStatusSuccessful() {
//                state.cheerModel = response.data
                state.cheerModel = self.blockArrangeModel(targetModel: response.data)
                state.last_no = response.data?.last_no ?? ""

            }
        case .setBlockNo(let boardNo):
            state.blockArticles.append(boardNo)
            
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setAddtionalLoading(let flag):
            state.isAddtionalLoading = flag
        case .setCheerType(let type):
            state.headerModel.viewListType = type.rawValue
            state.type = type
        case .setLastNo(let lastNo):
            state.last_no = lastNo
        }
        return state
    }
    
    func blockArrangeModel(targetModel: CheerDataModel?) -> CheerDataModel? {
        guard var tempModel = targetModel else { return nil }
        let list = tempModel.list_data?.filter({ model -> Bool in
            return currentState.blockArticles.contains(model.no) == false
        })
        tempModel.list_data = list
        return tempModel

    }
}
