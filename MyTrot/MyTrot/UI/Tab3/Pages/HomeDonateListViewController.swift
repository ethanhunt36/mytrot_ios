//
//  HomeDonateListViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation

import UIKit
import ReactorKit
import XLPagerTabStrip

final class HomeDonateListViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    var itemInfo = IndicatorInfo(title: "View")
    weak var pagerDelegate: HomeMainPagerDelegate?

    @IBOutlet weak var tableView: UITableView!
    var tableViewList: [DonateDataListModel] = []

    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: HomeDonateListViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension HomeDonateListViewController {
    func bindAction(_ reactor: HomeDonateListViewReactor) {
        //        backButton.rx.tap
        //            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
        //            .map { Reactor.Action.dismiss }
        //            .bind(to: reactor.action)
        //            .disposed(by: disposeBag)
        reactor.action.onNext(.getList(DonateListType.doing))


        
    }
}

// MARK: bindState For Reactor
private extension HomeDonateListViewController {
    func bindState(_ reactor: HomeDonateListViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .subscribe(onNext: { [weak self] items in
                self?.tableViewList.removeAll()
                self?.tableViewList.append(contentsOf: items)
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension HomeDonateListViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.DonateFinish.self, sticky: true).subscribe { [weak self] event in
            guard let id = event.element?.id else { return }
            guard let mypoint = event.element?.mypoint else { return }
            guard let totalpoint = event.element?.totalpoint else { return }
            guard let givePoint = event.element?.givePoint else { return }
            let tempList = self?.tableViewList ?? []
            var voteList: [DonateDataListModel] = []
            for item in tempList {
                if item.artist_no == id {
                    var changeVote = item
                    changeVote.donate_my_point = "\(mypoint + givePoint)"
                    changeVote.donate_cur_point = totalpoint + Int64(givePoint)
                    voteList.append(changeVote)
                } else {
                    voteList.append(item)
                }
                
            }
            self?.tableViewList.removeAll()
            self?.tableViewList.append(contentsOf: voteList)
            self?.tableView.reloadData()
            
        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.SetMyFavSingerFinish.self, sticky: true).subscribe { [weak self] _ in
            guard let reactor = self?.reactor else { return }
            let preference = PreferencesService()
            let isSelectdMyFavSinger = preference.getIsSelectdMyFavSinger()
            reactor.action.onNext(.setMyFavTop(isSelectdMyFavSinger, isUpdate: false))

            if isSelectdMyFavSinger == false {
                reactor.action.onNext(.getList(reactor.currentState.type))
            }

        }.disposed(by: disposeBag)

    }
}


// MARK: - Reactor Action
private extension HomeDonateListViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
// MARK: - IndicatorInfoProvider
extension HomeDonateListViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}

extension HomeDonateListViewController {
    func initializeTableView() {
        self.tableView.register(cellType: DonateMainCell.self)
        self.tableView.register(cellType: DonateMainHeaderCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension HomeDonateListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        if indexPath.row == 0 { return }
        guard let reactor = reactor else { return }
        switch reactor.currentState.type {
        case .doing:
            if let delegate = pagerDelegate {
                delegate.moveToDonateMainPage(model: tableViewList[indexPath.row])
            }
        case .done:
            if let delegate = pagerDelegate {
                delegate.moveToDonateDonePage(model: tableViewList[indexPath.row])
            }
        case .total:
            if let delegate = pagerDelegate {
                delegate.moveToDonateResultPage(model: tableViewList[indexPath.row])
            }
        }

    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 120

        } else {
            return 145

        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(for: indexPath) as DonateMainHeaderCell
            cell.item = self.tableViewList[indexPath.row]
            cell.delegate = self
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(for: indexPath) as DonateMainCell
            cell.item = self.tableViewList[indexPath.row]
            return cell
        }

    }
}
extension HomeDonateListViewController: DonateMainHeaderCellDelegate {
    func sortList(type: DonateListType) {
        guard let reactor = reactor else { return }
        Observable.just(Reactor.Action.getList(type))
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

    }
    
    func checkFavTopSinger(isCheck: Bool) {
        // move to singer select
        print("move to singer select")
        print("move to singer select : \(isCheck)")
        let artist_no = User.shared.myInfo?.artist_no ?? 0
        if artist_no == 0 || artist_no == -1 {
            Utils.AlertShow(msg: "더보기 화면에서 최애 가수를 설정해주세요.")
            return
        }
        guard let reactor = reactor else { return }
        if isCheck == true {
            reactor.action.onNext(.setMyFavTop(true, isUpdate: true))
        } else {
            reactor.action.onNext(.setMyFavTop(false, isUpdate: true))
            reactor.action.onNext(.getList(reactor.currentState.type))
        }

    }
    
    
}
