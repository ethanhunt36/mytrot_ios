//
//  HomeVoteListViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class HomeVoteListViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)

        case getList(VoteListType)
        case setMyFavTop(Bool, isUpdate: Bool)
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setItems(ResponseVoteList)
        case setSort(VoteListType)
        case setMyFavTop(Bool, isUpdate: Bool)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var sort: VoteListType
        var items: [VoteDataListModel]
        var headerModel: VoteHeaderDataModel
        var isMyFavTop: Bool
    }
    
    /// 초기 상태값
    let initialState: State
    let networkService = MyTrotService()
    let preference = PreferencesService()

    init() {
        self.initialState = State(
            isDismiss: false,
            sort: .monthly,
            items: [],
            headerModel: VoteHeaderDataModel(isSelectedFavSinger: preference.getIsSelectdMyFavSinger(), viewListType: VoteListType.monthly.rawValue),
            isMyFavTop: false
        )
    }
    
    func mutate(action: HomeVoteListViewReactor.Action) -> Observable<HomeVoteListViewReactor.Mutation> {
        switch action {
        case .setMyFavTop(let flag, let isUpdate):
            return .just(.setMyFavTop(flag, isUpdate: isUpdate))
        case .getList(let sort):
            return .concat([
                .just(.setSort(sort)),
                self.networkService.voteList(sort: sort).asObservable().map{ Mutation.setItems($0)}
            ])
            
            

        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        }
    }
    
    func reduce(state: HomeVoteListViewReactor.State, mutation: HomeVoteListViewReactor.Mutation) -> HomeVoteListViewReactor.State {
        var state = state
        switch mutation {
        case .setItems(let response):
            if response.isResponseStatusSuccessful() == true {
                let push_unread_cnt = response.data?.push_unread_cnt ?? 0
                RxBus.shared.post(event: Events.DisplayNotificationNew(unreadCount: push_unread_cnt))
                
                // alert
                var sectionList: [VoteDataListModel] = []

                if let data: VoteDataModel = response.data {
                    for var listOne in data.list_data {
                        if sectionList.count == 0 {
                            listOne.voteHeaderModel = currentState.headerModel
                            sectionList.append(listOne)
                        }
                        listOne.viewListType = state.sort.rawValue
                        sectionList.append(listOne)
                    }
                }
                User.shared.updatePointAmount(point_amount: response.data?.point_amount ?? 0)
                User.shared.updateAmountTicketAd(amount_ticket_ad: response.data?.amount_ticket_ad ?? "")
                state.items = sectionList
                
                let flag = preference.getIsSelectdMyFavSinger()
                state = arrangeList(state: state, flag: flag)

            }
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setSort(let sort):
            state.headerModel.viewListType = sort.rawValue
            state.sort = sort
        case .setMyFavTop(let flag, let isUpdate):
            // 최애 탑 설정
            if isUpdate == true {
                preference.setIsSelectdMyFavSinger(isSelectdMyFavSinger: flag)
            }
            state = arrangeList(state: state, flag: flag)
        }
        return state
    }
    
    func arrangeList(state: HomeVoteListViewReactor.State, flag: Bool) -> HomeVoteListViewReactor.State {
        var state = state
        if flag == true {
            state.headerModel.isSelectedFavSinger = true
            var tempList: [VoteDataListModel] = []
            tempList.append(contentsOf: state.items)
            state.items.removeAll()
            let myArtistNo = User.shared.myInfo?.artist_no ?? 0
            for var listOne in tempList {
                if state.items.count == 0 {
                    state.headerModel.isSelectedFavSinger = true
                    listOne.voteHeaderModel = state.headerModel
                    state.items.append(listOne)
                } else if myArtistNo == listOne.no {
                    state.items.insert(listOne, at: 1)
                } else {
                    state.items.append(listOne)
                }
                
            }

        } else {
            state.headerModel.isSelectedFavSinger = false
        }
        state.isMyFavTop = flag

        return state
    }
}
