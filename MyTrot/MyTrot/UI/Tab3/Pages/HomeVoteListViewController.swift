//
//  HomeVoteListViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import UIKit
import ReactorKit
import XLPagerTabStrip

final class HomeVoteListViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    var itemInfo = IndicatorInfo(title: "View")

    weak var pagerDelegate: HomeMainPagerDelegate?

    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var tableViewList: [VoteDataListModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: HomeVoteListViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension HomeVoteListViewController {
    func bindAction(_ reactor: HomeVoteListViewReactor) {
        //        backButton.rx.tap
        //            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
        //            .map { Reactor.Action.dismiss }
        //            .bind(to: reactor.action)
        //            .disposed(by: disposeBag)
        
        reactor.action.onNext(.getList(VoteListType.monthly))
    }
}

// MARK: bindState For Reactor
private extension HomeVoteListViewController {
    func bindState(_ reactor: HomeVoteListViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .subscribe(onNext: { [weak self] items in
                self?.tableViewList.removeAll()
                self?.tableViewList.append(contentsOf: items)
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension HomeVoteListViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.VoteFinish.self, sticky: true).subscribe { [weak self] event in
            guard let id = event.element?.id else { return }
            guard let mypoint = event.element?.mypoint else { return }
            guard let totalpoint = event.element?.totalpoint else { return }
            guard let givePoint = event.element?.givePoint else { return }
            let tempList = self?.tableViewList ?? []
            var voteList: [VoteDataListModel] = []
            for item in tempList {
                if item.no == id {
                    var changeVote = item
                    changeVote.my_vote_cnt = "\(mypoint + givePoint)"
                    changeVote.vote_cnt = totalpoint + Int64(givePoint)
                    voteList.append(changeVote)
                } else {
                    voteList.append(item)
                }
                
            }
            self?.tableViewList.removeAll()
            self?.tableViewList.append(contentsOf: voteList)
            self?.tableView.reloadData()
            
        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.MyInfo.self, sticky: true).subscribe { [weak self] _ in
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.SetMyFavSingerFinish.self, sticky: true).subscribe { [weak self] _ in
            guard let reactor = self?.reactor else { return }
            let preference = PreferencesService()
            let isSelectdMyFavSinger = preference.getIsSelectdMyFavSinger()
            reactor.action.onNext(.setMyFavTop(isSelectdMyFavSinger, isUpdate: false))

            if isSelectdMyFavSinger == false {
                reactor.action.onNext(.getList(reactor.currentState.sort))
            }
        }.disposed(by: disposeBag)
    }
}


// MARK: -
// MARK: - IndicatorInfoProvider
extension HomeVoteListViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}

extension HomeVoteListViewController {
    func initializeTableView() {
        self.tableView.register(cellType: VoteMainCell.self)
        self.tableView.register(cellType: VoteMainHeaderCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension HomeVoteListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        if indexPath.row == 0 { return }
        if let delegate = pagerDelegate {
            delegate.moveToVoteMainPage(model: tableViewList[indexPath.row])
        }
    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            //return 144
            return 75
        } else {
            return 115

        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(for: indexPath) as VoteMainHeaderCell
            cell.item = self.tableViewList[indexPath.row]
            cell.delegate = self
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(for: indexPath) as VoteMainCell
            cell.item = self.tableViewList[indexPath.row]
            return cell

        }

        /* // 페이징 예제
        if indexPath.row == tableViewList.count - 1 {
            print("row : \(indexPath.row)")
            let rowLastNo = self.tableViewList[indexPath.row].no
            guard let reactor = reactor else { return cell }
            print("rowLastNo ::: \(rowLastNo)")
            print("lastIndex ::: \(reactor.currentState.lastNo)")

            if reactor.currentState.lastNo != rowLastNo {
                reactor.action.onNext(.getList)
            }
            
        }
        */
    }
}

extension HomeVoteListViewController: VoteHeaderDelegate {
    func clickVoteGender() {
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        let both =  UIAlertAction(title: "전체성별", style: .default) { [weak self] (action) in
            print("신고하기")
            Utils.saveVoteGender(type: .both)
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.getList(Utils.getLastSavedVoteType()))
        }
        let male =  UIAlertAction(title: "남자", style: .default) { [weak self] (action) in
            print("신고하기")
            Utils.saveVoteGender(type: .male)
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.getList(Utils.getLastSavedVoteType()))
        }
        let female =  UIAlertAction(title: "여자", style: .default) { [weak self] (action) in
            print("신고하기")
            Utils.saveVoteGender(type: .female)
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.getList(Utils.getLastSavedVoteType()))

        }


        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        alert.addAction(both)
        alert.addAction(male)
        alert.addAction(female)
        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)

    }
    
    func clickVoteType() {
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        let week =  UIAlertAction(title: "주간", style: .default) { [weak self] (action) in
            print("신고하기")
            Utils.saveVoteType(type: .weekly)
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.getList(.weekly))
        }
        let month =  UIAlertAction(title: "월간", style: .default) { [weak self] (action) in
            print("신고하기")
            Utils.saveVoteType(type: .monthly)
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.getList(.monthly))
        }
        let normal =  UIAlertAction(title: "누적", style: .default) { [weak self] (action) in
            print("신고하기")
            Utils.saveVoteType(type: .normally)
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.getList(.normally))

        }


        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        alert.addAction(week)
        alert.addAction(month)
        alert.addAction(normal)
        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)

    }
    
    func adverClick() {
        print("adverClick Action!!")
        if let delegate = pagerDelegate {
            delegate.moveToVoteHeaderAd()
        }

    }
    
    func sortList(type: VoteListType) {
        guard let reactor = reactor else { return }

        Observable.just(Reactor.Action.getList(type))
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

    }
    
    func checkFavTopSinger(isCheck: Bool) {
        // move to singer select
        print("move to singer select : \(isCheck)")
        let artist_no = User.shared.myInfo?.artist_no ?? 0
        if artist_no == 0 || artist_no == -1 {
            Utils.AlertShow(msg: "더보기 화면에서 최애 가수를 설정해주세요.")
            return
        }
        guard let reactor = reactor else { return }
        if isCheck == true {
            reactor.action.onNext(.setMyFavTop(true, isUpdate: true))
        } else {
            reactor.action.onNext(.setMyFavTop(false, isUpdate: true))
            reactor.action.onNext(.getList(reactor.currentState.sort))
        }
        
        
        
    }
    
    
}
