//
//  CheckInRankViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit
import XLPagerTabStrip

final class CheckInRankViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    var itemInfo = IndicatorInfo(title: "View")

    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    var tableViewList: [AttendanceRankDataListModel] = []
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: CheckInRankViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension CheckInRankViewController {
    func bindAction(_ reactor: CheckInRankViewReactor) {
        //        backButton.rx.tap
        //            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
        //            .map { Reactor.Action.dismiss }
        //            .bind(to: reactor.action)
        //            .disposed(by: disposeBag)
        
        reactor.action.onNext(.getList)

    }
}

// MARK: bindState For Reactor
private extension CheckInRankViewController {
    func bindState(_ reactor: CheckInRankViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .filter{ $0.count > 0}
            .subscribe(onNext: { [weak self] items in
                self?.tableViewList.append(contentsOf: items)
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension CheckInRankViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()

    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension CheckInRankViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
// MARK: - IndicatorInfoProvider
extension CheckInRankViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}
// MARK: -
extension CheckInRankViewController {
    func initializeTableView() {
        self.tableView.register(cellType: AttendanceCell.self)
        self.tableView.register(cellType: AttendanceHeaderCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension CheckInRankViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 60
        }
        return 40
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(for: indexPath) as AttendanceHeaderCell
            cell.rankItem = self.tableViewList[indexPath.row]
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(for: indexPath) as AttendanceCell
            cell.rankItem = self.tableViewList[indexPath.row]
            return cell

        }

    }
}

