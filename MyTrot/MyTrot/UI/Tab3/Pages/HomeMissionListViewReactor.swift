//
//  HomeMissionListViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class HomeMissionListViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        
        case getMissionList
        case completeMission
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(ResponseMissionList)
        case empty
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var responseMission: ResponseMissionList?
        var humorUrl: String
    }
    
    /// 초기 상태값
    let initialState: State
    let networkService = MyTrotService()

    init() {
        self.initialState = State(
            isDismiss: false,
            humorUrl: ""
        )
    }
    
    func mutate(action: HomeMissionListViewReactor.Action) -> Observable<HomeMissionListViewReactor.Mutation> {
        switch action {
        case .completeMission:
            return networkService.completeMission().asObservable().map {
                res in
                if res.isResponseStatusSuccessful() {
                    // 미션성공
                    print("미션성공")
                    RxBus.shared.post(event: Events.RefreshMyInfo(), sticky: true)
                }
                Utils.AlertShow(msg: res.msg ?? Constants.Network.ErrorMessage.networkError)
                return .empty
            }
        case .getMissionList:
            return networkService.missionList().asObservable().map{ Mutation.setResponse($0)}
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: HomeMissionListViewReactor.State, mutation: HomeMissionListViewReactor.Mutation) -> HomeMissionListViewReactor.State {
        var state = state
        switch mutation {
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.responseMission = response
                state.humorUrl = response.data?.podong_url ?? ""
            } else {
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)
            }
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty: return state
        }
        return state
    }
}
