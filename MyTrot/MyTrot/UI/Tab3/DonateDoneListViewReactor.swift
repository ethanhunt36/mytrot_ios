//
//  DonateDoneListViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/04/04.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class DonateDoneListViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case getList
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(ResponseDonateMemberList)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var imgUrl: String
        var donate_no: Int
        var artist_no: Int64
        var items: [DonateMemberDataListModel]
        var titleString: String
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(imgurl url: String, donate_no: Int, artist_no: Int64, title: String) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            imgUrl: url,
            donate_no: donate_no,
            artist_no: artist_no,
            items: [],
            titleString: title
        )
    }
    
    func mutate(action: DonateDoneListViewReactor.Action) -> Observable<DonateDoneListViewReactor.Mutation> {
        switch action {
        case .getList:
            return networkService.donateMemberList(donate_goal_no: currentState.donate_no, artist_no: currentState.artist_no).asObservable().map { Mutation.setResponse($0)}
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: DonateDoneListViewReactor.State, mutation: DonateDoneListViewReactor.Mutation) -> DonateDoneListViewReactor.State {
        var state = state
        switch mutation {
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                let baseRow = DonateMemberDataListModel(member_nick: "", sum_point: "", imgUrl: currentState.imgUrl, search_cnt: response.data?.search_cnt)
                state.items.append(baseRow)
                guard let list = response.data?.list_data else { return state}
                state.items.append(contentsOf: list)
            }
        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
