//
//  ChartFlow.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
import RxFlow
import RxRelay

final class Tab3Flow: Flow {
    var root: Presentable {
        return self.rootViewController
    }

    private lazy var rootViewController: BaseNavigationController = {
        let viewController = BaseNavigationController()
        viewController.setNavigationBarHidden(true, animated: false)
        return viewController
    }()

    private let myStepper: Tab3Stepper

    init(withStepper stepper: Tab3Stepper) {
        self.myStepper = stepper
    }

    deinit { print("\(type(of: self)): \(#function)") }

    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? CommonStep else { return FlowContributors.none }

        switch step {
        case .tab3Main:
            return navigateToMainScreen()
        /// 알림
        case .notificationPage:
            return navigateToNotificationScreen()
        /// 출석부
        case .checkInPage:
            return navigateToCheckInScreen()
        /// 투표 등록 페이지
        case .voteUploadPage(let model):
            return voteUploadPage(model: model)
        /// 기부 등록 페이지
        case .donateUploadPage(let model):
            return donateUploadPage(model: model)
        /// 응원 등록 페이지
        case .cheerUploadPage:
            return cheerUploadPage()
        /// 응원 상세 페이지
        case .cheerDetailPage(let board_no):
            return cheerDetailPage(board_no: board_no)
        /// 미션 - 행운의 번호
        case .missionLuckyNumberPage:
            return missionLuckyNumberPage()
        /// 미션 - 유머글
        case .missionHumorPage(let humorUrl):
            return missionHumorPage(url: humorUrl)
        /// 미션 - 광고 시청 (미정)
        
        /// 신고하기
        case .reportPage(let board_no):
            return reportPage(boardNo: board_no)
        case .videoSearchResultPage(let keyword):
            return videoSearchResultPage(keyword: keyword)

        case .favSingerSelectPage(let type):
            return favSingerSelectPage(type: type)
            
        case .donateDonePage(let url, let donate_no, let artist_no, let title):
            return donateDonePage(url: url, donate_no: donate_no, artist_no: artist_no, title: title)
        case .donateResultPage(let donate_no, let artist_no, let title):
            return donateResultPage(donate_no: donate_no, artist_no: artist_no, title: title)
        case .modalDismiss(let animated):
            return dismissScreen(type: .modal, animated: animated)
        case .pushDismiss(let animated):
            return dismissScreen(type: .push, animated: animated)
        case .ppobKiPage:
            return ppobKiPage()
        case .missionBlog(let type, let url):
            return missionBlog(type: type, url: url)
        default: return .none
        }
    }
}

// MARK: -
// MARK: 이동 (navigate)
// MARK: -
extension Tab3Flow {
    private func missionBlog(type: WebType, url: String) -> FlowContributors {
        let controller = CommonWebViewController.instantiate()
        let reactor = CommonWebViewReactor(withWebType: type, url: url)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }

    /// 메인화면 이동
    private func navigateToMainScreen() -> FlowContributors {
        let controller = HomeMainPagerViewController.instantiate()
        let reactor = HomeMainPagerViewReactor()
        controller.reactor = reactor

        self.rootViewController.setViewControllers([controller], animated: false)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))
    }
    private func navigateToNotificationScreen() -> FlowContributors {
        let controller = HomeNotificationListViewController.instantiate()
        let reactor = HomeNotificationListViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func navigateToCheckInScreen() -> FlowContributors {
        let controller = CheckInMainPagerViewController.instantiate()
        let reactor = CheckInMainPagerViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func voteUploadPage(model: VoteDataListModel) -> FlowContributors {
        let controller = VoteViewController.instantiate()
        let reactor = VoteViewReactor(withModel: model)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    
    private func donateUploadPage(model: DonateDataListModel) -> FlowContributors {
        let controller = DonateViewController.instantiate()
        let reactor = DonateViewReactor(withModel: model)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))
    }
    
    private func cheerUploadPage() -> FlowContributors {
        let controller = CheerUploadViewController.instantiate()
        let reactor = CheerUploadViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func cheerDetailPage(board_no: Int64) -> FlowContributors {
        let controller = CheerDetailViewController.instantiate()
        let reactor = CheerDetailViewReactor(no: board_no)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func missionLuckyNumberPage() -> FlowContributors {
        let controller = CommonLuckyNumberViewController.instantiate()
        let reactor = CommonLuckyNumberViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func missionHumorPage(url: String) -> FlowContributors {
        let controller = CommonWebViewController.instantiate()
        let reactor = CommonWebViewReactor(withWebType: .podong, url: url)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func reportPage(boardNo: Int64) -> FlowContributors {
        let controller = CommonReportViewController.instantiate()
        let reactor = CommonReportViewReactor(withBoardNo: boardNo)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func favSingerSelectPage(type: FavSingerScreenType) -> FlowContributors {
        let controller = FavSingerSelectViewController.instantiate()
        let reactor = FavSingerSelectViewReactor(screenType: type)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))


    }
    
    private func donateDonePage(url: String, donate_no: Int, artist_no: Int64, title: String) -> FlowContributors {
        let controller = DonateDoneListViewController.instantiate()
        let reactor = DonateDoneListViewReactor(imgurl: url, donate_no: donate_no, artist_no: artist_no, title: title)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func donateResultPage(donate_no: Int, artist_no: Int64, title: String) -> FlowContributors {
        let controller = DonateResultListViewController.instantiate()
        let reactor = DonateResultListViewReactor(donate_no: donate_no, artist_no: artist_no, title: title)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func videoSearchResultPage(keyword: String) ->FlowContributors {
        let controller = CommonVideoListViewController.instantiate()
        let reactor = CommonVideoListViewReactor(keyword: keyword)
        controller.reactor = reactor
        
        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func ppobKiPage() ->FlowContributors {
        let controller = PpobKiViewController.instantiate()
        let reactor = PpobKiViewReactor()
        controller.reactor = reactor
        
        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }

}

// MARK: 닫기 (dismiss, pop)
private extension Tab3Flow {
    func dismissScreen(type: NavigationType = .push, animated: Bool = true) -> FlowContributors {
        switch type {
        case .push:
            if self.rootViewController.viewControllers.count == 1 {
                return dismissScreen(type: .modal, animated: true)
            }
            self.rootViewController.popViewController(animated: animated)
        case .modal:
            self.rootViewController.dismiss(animated: animated, completion: {
            })
        }
        return .none
    }
}

class Tab3Stepper: Stepper {
    var steps = PublishRelay<Step>()
    var step: CommonStep?
    convenience init(step: CommonStep) {
        self.init()
        self.step = step
    }
    var initialStep: Step {
        if let step = self.step {
            return step
        }
        return CommonStep.tab3Main
    }
}

