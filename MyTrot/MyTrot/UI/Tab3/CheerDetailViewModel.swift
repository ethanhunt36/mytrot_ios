//
//  CheerDetailViewModel.swift
//  MyTrot
//
//  Created by hclim on 2021/03/30.
//

import Foundation
struct BoardViewModel: Hashable {
    static func == (lhs: BoardViewModel, rhs: BoardViewModel) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(createdTime)
    }

    // 모델 생성시간, hashValue 생성을 위한 값으로 사용
    let createdTime: Double = Date().timeIntervalSince1970

    init(_ items: [BoardDetailCellItem] = []) {
        self.items = items
    }

    // 리스트 데이터
    let items: [BoardDetailCellItem]

    // item 갯수
    var count: Int { items.count }

    // item 요소에 접근 정의
    subscript(index: Int) -> BoardDetailCellItem { return items[index] }
}


protocol BoardDetailCellItem {
    // 타입
    var type: BoardDetailType { get }
}

enum BoardDetailType {
    /// 전문가상담게시판 상세
    case top
    case image
    case reply

}
extension BoardDetailCellItem where Self: BoardTopItem {
    var type: BoardDetailType { return .top }
}
// 공통 컨텐츠 상단 아이템 =================================
final class BoardTopItem: BoardDetailCellItem {
    let response: BoardDetailViewDataModel
    let url: String
    init(withResponse response: BoardDetailViewDataModel) {
        self.response = response
        self.url = ""
    }
    init(withResponse response: BoardDetailViewDataModel, imgUrl: String) {
        self.response = response
        self.url = imgUrl
    }

}
extension BoardDetailCellItem where Self: BoardReplyItem {
    var type: BoardDetailType { return .reply }
}
// 공통 컨텐츠 댓글 =================================
final class BoardReplyItem: BoardDetailCellItem {
    let response: BoardDetailCommentDataListModel
    init(withResponse response: BoardDetailCommentDataListModel) {
        self.response = response
    }

}
extension BoardDetailCellItem where Self: BoardImageItem {
    var type: BoardDetailType { return .image }
}
// 공통 컨텐츠 댓글 =================================
final class BoardImageItem: BoardDetailCellItem {
    let url: String
    init(withResponse url: String) {
        self.url = url
    }

}
