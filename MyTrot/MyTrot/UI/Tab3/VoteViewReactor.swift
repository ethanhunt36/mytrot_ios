//
//  VoteViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class VoteViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        
        case clickMaxVote
        case clickConfirm
        case changeVoteValue(String)
        
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setIsMaxVote(Bool)
        case setVoteCountValue(String)
        case setVoteComplete(CommonResponse)
        case empty
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var dataModel: VoteDataListModel
        var isMaxVote: Bool
        var maxVoteCount: Int64
        var voteCountValue: String
        var artistName: String
        var completeMessage: String?
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(withModel model: VoteDataListModel) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            dataModel: model,
            isMaxVote: false ,
            maxVoteCount: User.shared.myInfo?.vote_cnt ?? 0,
            voteCountValue: "",
            artistName: model.name
        )
    }
    
    func mutate(action: VoteViewReactor.Action) -> Observable<VoteViewReactor.Mutation> {
        switch action {
        case .clickMaxVote:
            if currentState.isMaxVote == true {
                return .concat([
                    .just(.setVoteCountValue("")),
                    .just(.setIsMaxVote(false))
                ])

            } else {
                return .concat([
                    .just(.setVoteCountValue("\(currentState.maxVoteCount)")),
                    .just(.setIsMaxVote(true))
                ])

            }
        case .clickConfirm:
            if preferencesService.getIsUserAgreeTerms() == false {
                Utils.AlertShow(msg: Constants.String.AlertMessage.agreementDeniedMessage)
                
                return .empty()
            }

            print("clickConfirm : \(currentState.dataModel.no), \(currentState.voteCountValue)")
            if let voteCount = Int(currentState.voteCountValue) {
                return networkService.uploadVote(artist_no: currentState.dataModel.no, vote_cnt: voteCount).asObservable().map { Mutation.setVoteComplete($0) }
            } else {
                return .empty()
            }
        case .changeVoteValue(let value):
            return .just(.setVoteCountValue(value))
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: VoteViewReactor.State, mutation: VoteViewReactor.Mutation) -> VoteViewReactor.State {
        var state = state
        switch mutation {
        case .setIsMaxVote(let flag):
            state.isMaxVote = flag
        case .setVoteCountValue(let value):
            state.voteCountValue = value
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty:
            return state
        case .setVoteComplete(let response):
            if response.isResponseStatusSuccessful() {
                let message = "\(currentState.artistName) 님에게\n 투표권 \(currentState.voteCountValue)장이 투표되었습니다."
                state.completeMessage = message
                RxBus.shared.post(event: Events.RefreshMyInfo(), sticky: true)
                RxBus.shared.post(event: Events.VoteFinish(
                                    id: state.dataModel.no,
                                    mypoint: Int(state.dataModel.my_vote_cnt) ?? 0,
                                    totalpoint: Int64(state.dataModel.vote_cnt),
                                    givePoint: Int(state.voteCountValue) ?? 0), sticky: true)
            } else {
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)
            }

        }
        return state
    }
    
}
