//
//  HomeNotificationListViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class HomeNotificationListViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        
        /// 알림 리스트
        case getNotificationList(Int64)
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponseNotificationList(ResponseNotificationList)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var listModel: [NotificationDataListModel]
        var last_no: Int64
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType

    init() {
        self.networkService = MyTrotService()

        self.initialState = State(
            isDismiss: false,
            listModel: [],
            last_no: 0
        )
    }
    
    func mutate(action: HomeNotificationListViewReactor.Action) -> Observable<HomeNotificationListViewReactor.Mutation> {
        switch action {
        case .getNotificationList(let no):
            return networkService.notificationList(lastNo: no).asObservable().map { Mutation.setResponseNotificationList($0)}
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: HomeNotificationListViewReactor.State, mutation: HomeNotificationListViewReactor.Mutation) -> HomeNotificationListViewReactor.State {
        var state = state
        switch mutation {
        case .setResponseNotificationList(let response):
            if response.isResponseStatusSuccessful() == true {
                let list = response.data?.list_data ?? []
                state.listModel = list
                
                let lastNo = response.data?.last_no ?? 0
                state.last_no = lastNo
            }
        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}


