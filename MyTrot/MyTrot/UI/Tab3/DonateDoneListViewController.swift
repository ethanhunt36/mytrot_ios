//
//  DonateDoneListViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/04/04.
//

import Foundation

import UIKit
import ReactorKit

final class DonateDoneListViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var tableViewList: [DonateMemberDataListModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: DonateDoneListViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension DonateDoneListViewController {
    func bindAction(_ reactor: DonateDoneListViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        reactor.action.onNext(.getList)
        
    }
}

// MARK: bindState For Reactor
private extension DonateDoneListViewController {
    func bindState(_ reactor: DonateDoneListViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .subscribe(onNext: { [weak self] items in
                self?.tableViewList.removeAll()
                self?.tableViewList.append(contentsOf: items)
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)
        
        reactor.state.map { $0.titleString}
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
    }
}

// MARK: -
// MARK: private initialize
private extension DonateDoneListViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension DonateDoneListViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
extension DonateDoneListViewController {
    func initializeTableView() {
        self.tableView.register(cellType: DonateMemberCell.self)
        self.tableView.register(cellType: DonateMemberTopCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension DonateDoneListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            guard let reactor = reactor else { return 500 }
            if reactor.currentState.imgUrl == "" {
                return 80
            }
            return 500

        } else {
            return 55

        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(for: indexPath) as DonateMemberTopCell
            cell.item = self.tableViewList[indexPath.row]
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(for: indexPath) as DonateMemberCell
            cell.item = self.tableViewList[indexPath.row]
            cell.rowIndex = indexPath.row
            return cell

        }

    }
}
