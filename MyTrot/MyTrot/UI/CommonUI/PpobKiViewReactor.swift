//
//  PpobKiViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/06/11.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class PpobKiViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        /// 뽑기 API
        case getPpobkiInfo
        case playPpobki(Int)
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponsePpobkiInfo(ResponsePpobkiInfo)
        case setResponsePpobkiResultInfo(ResponsePpobkiResultInfo)
        case empty
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var last_apply_dttm: String
        var ppobkiResultInfo: ResponsePpobkiResultInfo?
        var ppobkiPlayComplete: Bool
        var ppobkiViewModel: PpobKiViewModel?
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            last_apply_dttm: "",
            ppobkiPlayComplete: false
        )
    }
    
    func mutate(action: PpobKiViewReactor.Action) -> Observable<PpobKiViewReactor.Mutation> {
        switch action {
        case .playPpobki(let no ):
            return networkService.playPpobkki(no: no).asObservable().map { res in
                print("ppob ki result : \(res)")
                return Mutation.setResponsePpobkiResultInfo(res)
            }
        case .getPpobkiInfo:
            return networkService.getPpobkkiList().asObservable().map { Mutation.setResponsePpobkiInfo($0) }
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: PpobKiViewReactor.State, mutation: PpobKiViewReactor.Mutation) -> PpobKiViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setResponsePpobkiInfo(let info):
            guard let bonus: BonusNumberInfoModel = info.data?.bonus else { return state }
            var section: [PpobKiCollectionItem] = []
            section.append(PpobKiCollectionTopItem(withInfo: bonus.one, numberPrize: 1))
            section.append(PpobKiCollectionTopItem(withInfo: bonus.two, numberPrize: 2))
            section.append(PpobKiCollectionTopItem(withInfo: bonus.three, numberPrize: 3))
            section.append(PpobKiCollectionTopItem(withInfo: bonus.four, numberPrize: 4))
            section.append(PpobKiCollectionTopItem(withInfo: bonus.five, numberPrize: 5))
            guard let list = info.data?.list else { return state }
            for item in list {
                section.append(PpobKiCollectionListItem(withInfo: item))
            }
            let ppobkiViewModel = PpobKiViewModel(section)
            state.ppobkiViewModel = ppobkiViewModel
        case .setResponsePpobkiResultInfo(let response):
            if response.isResponseStatusSuccessful() {
                state.ppobkiResultInfo = response
                state.ppobkiPlayComplete = true
                
            } else if response.err == 1 {
                state.ppobkiPlayComplete = false
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)

            } else if response.err == 2 {
                state.ppobkiPlayComplete = false
                Utils.AlertShow(msg: response.msg ?? Constants.Network.ErrorMessage.networkError)
                print("뽑기 리프레쉬2")
                
                RxBus.shared.post(event: Events.PpobKiNeedToRefresh())
            }
            
        case .empty: return state
        }
        return state
    }
}

