//
//  CommonLuckyNumberViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit

final class CommonLuckyNumberViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    var data = LuckyNumberViewModel() {
        didSet {
            tableView.reloadData()
        }
    }
    // 배너
    var _caulyView: CaulyAdView? = nil
    // 카울리 전면
    var _caulyInterstitialAd:CaulyInterstitialAd? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: CommonLuckyNumberViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension CommonLuckyNumberViewController {
    func bindAction(_ reactor: CommonLuckyNumberViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        
    }
}

// MARK: bindState For Reactor
private extension CommonLuckyNumberViewController {
    func bindState(_ reactor: CommonLuckyNumberViewReactor) {
        reactor.action.onNext(.getLuckyNumber)
        
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        
        reactor.state.map { $0.luckyNumberViewModel }
            .filterNil()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] model in
                self?.data = model
            }).disposed(by: disposeBag)

        
    }
    
    func getImageString(num: Int) -> String? {
        if num < 10 {
            return ""
        }
        return nil
    }
}

// MARK: -
// MARK: private initialize
private extension CommonLuckyNumberViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
        initAd_Cauly()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
    
    func initAd_Cauly(){
        self._caulyView=CaulyAdView.init()
        self._caulyView?.delegate = self                       //  delegate 설정

        //let frame = CGRect.init(x: 0, y: strongSelf.view.bounds.height - (85 + (strongSelf.tabBarController?.tabBar.frame.size.height ?? 40)), width: strongSelf.view.frame.width, height: 100)
        //self.caulyView?.bounds = frame
        
                
    }

}


// MARK: - Reactor Action
private extension CommonLuckyNumberViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
extension CommonLuckyNumberViewController {
    func initializeTableView() {
        self.tableView.register(cellType: LuckyNumberHeaderCell.self)
        self.tableView.register(cellType: LuckyNumberDupleCell.self)
        self.tableView.register(cellType: LuckyNumberAdvCell.self)
        self.tableView.register(cellType: LuckyNumberNormalCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension CommonLuckyNumberViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let type = data[indexPath.row].type
        switch type {
        case .header:
            return 150
        case .duplicate:
            return 145
        case .adv:
            return 60
        case .normal:
            return 135
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = data[indexPath.row].type
        switch type {
        case .header:
            let cell = tableView.dequeueReusableCell(for: indexPath) as LuckyNumberHeaderCell
            cell.item = data[indexPath.row] as? LuckyNumberCellHeaderItem
            return cell
        case .duplicate:
            let cell = tableView.dequeueReusableCell(for: indexPath) as LuckyNumberDupleCell
            cell.item = data[indexPath.row] as? LuckyNumberCellDuplicateItem
            return cell
        case .adv:
            let cell = tableView.dequeueReusableCell(for: indexPath) as LuckyNumberAdvCell
            cell.item = data[indexPath.row] as? LuckyNumberCellAdvItem
//            cell.adView.addSubview(self._caulyView!)
            cell.adInsideView = _caulyView
            self._caulyView?.startBannerAdRequest()                //배너광고요청
            return cell
        case .normal:
            let cell = tableView.dequeueReusableCell(for: indexPath) as LuckyNumberNormalCell
            cell.item = data[indexPath.row] as? LuckyNumberCellNormalItem
            return cell 
        }
    }

}

// Cauly 광고 Delegate (배너)
extension CommonLuckyNumberViewController: CaulyAdViewDelegate {
    //광고 정보 수신 성공
    func didReceiveAd(_ adView: CaulyAdView!, isChargeableAd: Bool) {
        print("Cauly Banner Loaded didReceiveAd callback")
        adView?.show(withParentViewController: self, target: self.view)
        
    }
    //광고 정보 수신 실패
    func didFail(toReceiveAd adView: CaulyAdView!, errorCode: Int32, errorMsg: String!) {
         print("Cauly Banner didFailToReceiveAd: \(errorCode)(\(errorMsg!))");
    }
    // 랜딩 화면 표시
    func willShowLanding(_ adView: CaulyAdView!) {
        print("Cauly Banner willShowLanding")
    }
    // 랜딩 화면이 닫혔을 때
    func didCloseLanding(_ adView: CaulyAdView!) {
        print("Cauly Banner didCloseLanding")
    }

}
