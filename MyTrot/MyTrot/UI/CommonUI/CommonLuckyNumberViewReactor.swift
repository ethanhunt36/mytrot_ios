//
//  CommonLuckyNumberViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class CommonLuckyNumberViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(Step)
        
        case getLuckyNumber
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponseLuckyNumberList(ResponseLuckyNumberList)
        case empty
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var luckyNumberViewModel: LuckyNumberViewModel?

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false
        )
    }
    
    func mutate(action: CommonLuckyNumberViewReactor.Action) -> Observable<CommonLuckyNumberViewReactor.Mutation> {
        switch action {
        case .getLuckyNumber:
            return networkService.luckyNumberList().asObservable().map { Mutation.setResponseLuckyNumberList($0)}
            
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))

            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: CommonLuckyNumberViewReactor.State, mutation: CommonLuckyNumberViewReactor.Mutation) -> CommonLuckyNumberViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty: return state
        case .setResponseLuckyNumberList(let response):
            print("response : \(response)")
            var sectionList: [LuckyNumberCellItem] = []
            let duplicateInfo = response.data?.duplicate_list2 ?? []
            if duplicateInfo.count >= 12 {
                let headerItem: LuckyNumberCellHeaderItem = LuckyNumberCellHeaderItem(withDuplicateable: true)
                sectionList.append(headerItem)
                let duplicateItem: LuckyNumberCellDuplicateItem = LuckyNumberCellDuplicateItem(withInfo: duplicateInfo)
                sectionList.append(duplicateItem)

            } else {
                let headerItem: LuckyNumberCellHeaderItem = LuckyNumberCellHeaderItem(withDuplicateable: false)
                sectionList.append(headerItem)

            }
            let advItem: LuckyNumberCellAdvItem = LuckyNumberCellAdvItem()
            sectionList.append(advItem)
            let normalList = response.data?.list ?? []
            for item in normalList {
                let normalItem: LuckyNumberCellNormalItem = LuckyNumberCellNormalItem(withInfo: item)
                sectionList.append(normalItem)
            }
            let mainModel = LuckyNumberViewModel(sectionList)
            state.luckyNumberViewModel = mainModel
        }
        return state
    }
}
