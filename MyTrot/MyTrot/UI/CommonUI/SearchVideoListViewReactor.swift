//
//  SearchVideoListViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/11.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class SearchVideoListViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(Step)

        case empty
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
    }
    
    /// 초기 상태값
    let initialState: State
    
    init() {
        self.initialState = State(
            isDismiss: false
        )
    }
    
    func mutate(action: SearchVideoListViewReactor.Action) -> Observable<SearchVideoListViewReactor.Mutation> {
        switch action {
        case .empty:
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return .empty()
//            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        }
    }
    
    func reduce(state: SearchVideoListViewReactor.State, mutation: SearchVideoListViewReactor.Mutation) -> SearchVideoListViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
