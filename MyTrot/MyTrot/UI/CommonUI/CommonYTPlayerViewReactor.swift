//
//  CommonYTPlayerViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/09/01.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class CommonYTPlayerViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)
        case addBookmark(artist_no: Int64, vod_no: Int64)
        case removeBookmark(artist_no: Int64, vod_no: Int64)

        case changeYoutubeId(String)
        case chenagNextVideo
        
        case playApi
        case finishApi(String)
        
        case finishApiAndBack(String)
        
        case currentPlayTime(Float)
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setAddBookMarkingStatus(BookMarkingStatus, vod_no: Int64)
        case setRemoveBookMarkingStatus(BookMarkingStatus, vod_no: Int64)
        case setYoutubeId(String)
        case setCurrentPlayTime(Float)
        case empty
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var link: String
        var items: [ArtistVideoListModel]
        var bookmarkingStatus: BookMarkingStatus

        var totalPlayTime: Int
        var playItemIndex: Int
        
        var currentPlayTime: Float
    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(withLink link: String) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            link: link,
            items: [],
            bookmarkingStatus: .none,
            totalPlayTime: 0,
            playItemIndex: 0,
            currentPlayTime: 0
        )
    }
    init(withLink link: String, list: [ArtistVideoListModel]) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            link: link,
            items: list,
            bookmarkingStatus: .none,
            totalPlayTime: Utils.getTotalPlayTime(items: list, link: link),
            playItemIndex: Utils.getPlayItemIndex(items: list, link: link),
            currentPlayTime: 0
        )
    }

    func mutate(action: CommonYTPlayerViewReactor.Action) -> Observable<CommonYTPlayerViewReactor.Mutation> {
        switch action {
        case .currentPlayTime(let time):
            return .just(.setCurrentPlayTime(time))
        case .finishApiAndBack(let playId):
            return networkService.finishYoutube(artist_vod_no: Utils.getPlayItemNo(items: currentState.items, link: playId)).asObservable().map { [weak self] res in
                if self?.currentState.currentPlayTime ?? 0 > 60 {
                    RxBus.shared.post(event: Events.YoutubeMyCountUpdate(model: Utils.getPlayItem(items: self?.currentState.items ?? [], link: playId)) )
                }

                self?.navigate(step: CommonStep.pushDismiss(animated: true))
                return .empty
            }

        case .playApi:
            return networkService.playYoutube(artist_vod_no: Utils.getPlayItemNo(items: currentState.items, link: currentState.link)).asObservable().map { res in
                return .empty
            }
        case .finishApi(let playId):
            return networkService.finishYoutube(artist_vod_no: Utils.getPlayItemNo(items: currentState.items, link: playId)).asObservable().map { [weak self] res in
                if self?.currentState.currentPlayTime ?? 0 > 60 {
                    RxBus.shared.post(event: Events.YoutubeMyCountUpdate(model: Utils.getPlayItem(items: self?.currentState.items ?? [], link: playId)) )
                }

                return .empty
            }

        case .chenagNextVideo:
            
            if currentState.items.count > currentState.playItemIndex + 1{
                let nextItem: ArtistVideoListModel = currentState.items[currentState.playItemIndex + 1]
                return .just(.setYoutubeId(nextItem.youtube_id))

            } else {
                print("다음 플레이 리스트 없음")
                return .empty()
            }
        case .changeYoutubeId(let youtubeId):
            return .just(.setYoutubeId(youtubeId))
        case .removeBookmark(let artist_no, let vod_no):
            if artist_no == 0, vod_no == 0 {
                return .just(.setRemoveBookMarkingStatus(.none, vod_no: 0))
            }
            return .concat([
                .just(.setRemoveBookMarkingStatus(.processing, vod_no: 0)),
                networkService.removeBookmark(artist_no: artist_no, vodNo: vod_no).asObservable().map { response in
                    if response.isResponseStatusSuccessful() {
                        RxBus.shared.post(event: Events.RemoveBookMark(no: vod_no), sticky: true)

                        return .setRemoveBookMarkingStatus(.doneSuccess, vod_no: vod_no)
                    } else {
                        return .setRemoveBookMarkingStatus(.doneFail, vod_no: 0)
                    }
                    
                }
            ])

        case .addBookmark(let artist_no, let vod_no):
            if artist_no == 0, vod_no == 0 {
                return .just(.setAddBookMarkingStatus(.none, vod_no: 0))
            }
            return .concat([
                .just(.setAddBookMarkingStatus(.processing, vod_no: 0)),
                networkService.addBookmark(artist_no: artist_no, vodNo: vod_no).asObservable().map { response in
                    if response.isResponseStatusSuccessful() {
                        RxBus.shared.post(event: Events.AddBookMark(no: vod_no), sticky: true)

                        return .setAddBookMarkingStatus(.doneSuccess, vod_no: vod_no)
                    } else {
                        return .setAddBookMarkingStatus(.doneFail, vod_no: 0)
                    }
                    
                }
            ])

        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: CommonYTPlayerViewReactor.State, mutation: CommonYTPlayerViewReactor.Mutation) -> CommonYTPlayerViewReactor.State {
        var state = state
        switch mutation {
        case .setRemoveBookMarkingStatus(let status, let vod_no):
            var tempList: [ArtistVideoListModel] = []
            tempList.append(contentsOf: state.items)
            state.items.removeAll()
            for var item in tempList {
                if vod_no == item.no {
                    
                    item.is_bookmark = 0
                }
                state.items.append(item)
            }
            
            state.bookmarkingStatus = status

        case .setAddBookMarkingStatus(let status, let vod_no):
            
            var tempList: [ArtistVideoListModel] = []
            tempList.append(contentsOf: state.items)
            state.items.removeAll()
            for var item in tempList {
                if vod_no == item.no {
                    
                    item.is_bookmark = 1
                }
                state.items.append(item)
            }
            
            state.bookmarkingStatus = status

        case .setDismiss(let flag):
            state.isDismiss = flag
        case .setYoutubeId(let youtubeId):
            state.totalPlayTime = Utils.getTotalPlayTime(items: currentState.items, link: youtubeId)
            state.playItemIndex = Utils.getPlayItemIndex(items: currentState.items, link: youtubeId)
            state.link = youtubeId
        case .setCurrentPlayTime(let time):
            state.currentPlayTime = time
        case .empty: return state
        }
        return state
    }
}
