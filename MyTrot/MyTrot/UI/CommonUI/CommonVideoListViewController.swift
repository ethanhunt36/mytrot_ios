//
//  CommonVideoListViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit

final class CommonVideoListViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    var tableViewList: [ArtistVideoListModel] = []
    @IBOutlet weak var tableView: UITableView!
    var isListLoadFinish: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: CommonVideoListViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension CommonVideoListViewController {
    func bindAction(_ reactor: CommonVideoListViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        reactor.action.onNext(.getVideoList(type: .recent, last_no: ""))

    }
}

// MARK: bindState For Reactor
private extension CommonVideoListViewController {
    func bindState(_ reactor: CommonVideoListViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .distinctUntilChanged({ (resList1, resList2) -> Bool in
                guard let firstObj1 = resList1.first else { return false }
                guard let firstObj2 = resList2.first else { return false }
                return firstObj1.no == firstObj2.no
            })
            .subscribe(onNext: { [weak self] items in
                if reactor.currentState.isAddtionalList == true {
                    self?.tableViewList.append(contentsOf: items)
                    self?.isListLoadFinish = true
                } else {
                    self?.isListLoadFinish = true
                    self?.tableViewList.removeAll()
                    self?.tableViewList.append(contentsOf: items)
                }
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)
        
        reactor.state.map { $0.artist_name }
            .filter{ $0 != "" }
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
        reactor.state.map { $0.keyword }
            .filter{ $0 != "" }
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)


    }
}

// MARK: -
// MARK: private initialize
private extension CommonVideoListViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.YoutubeMyCountUpdate.self, sticky: true).subscribe { [weak self] event in
            guard let model = event.element?.model else { return }
            var tempList: [ArtistVideoListModel] = []
            for var item in self?.tableViewList ?? [] {
                if item.youtube_id == model.youtube_id {
                    print("match! YoutubeMyCountUpdate 0")
                    print("match! YoutubeMyCountUpdate 1: \(item.my_play_cnt)")
                    item.my_play_cnt = item.my_play_cnt + 1
                    print("match! YoutubeMyCountUpdate 2: \(item.my_play_cnt)")

                }
                tempList.append(item)

            }
            self?.tableViewList = tempList
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.AddBookMark.self, sticky: true).subscribe { [weak self] event in
            let strongList = self?.tableViewList ?? []
            var tempTableList: [ArtistVideoListModel] = []
            for var item in strongList {
                if item.no == event.element?.no {
                    item.is_bookmark = 1
                }
                tempTableList.append(item)
            }
            self?.tableViewList = tempTableList
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.RemoveBookMark.self, sticky: true).subscribe { [weak self] event in
            let strongList = self?.tableViewList ?? []
            var tempTableList: [ArtistVideoListModel] = []
            for var item in strongList {
                if item.no == event.element?.no {
                    item.is_bookmark = 0
                }
                tempTableList.append(item)
            }
            self?.tableViewList = tempTableList
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)

    }
}


// MARK: - Reactor Action
private extension CommonVideoListViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
extension CommonVideoListViewController {
    func initializeTableView() {
        self.tableView.register(cellType: VideoMainCell.self)
        self.tableView.register(cellType: VideoMainHeaderCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension CommonVideoListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        if indexPath.row == 0 { return }
        guard let reactor = reactor else { return }

        let link = self.tableViewList[indexPath.row].youtube_id
        var temp = tableViewList
        temp.removeFirst()
        reactor.action.onNext(.moveToStepScreen(CommonStep.playYoutube(link: link, list: temp)))

    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 50

        } else {
            return 162

        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(for: indexPath) as VideoMainHeaderCell
            cell.item = self.tableViewList[indexPath.row]
            cell.delegate = self
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(for: indexPath) as VideoMainCell
            cell.item = self.tableViewList[indexPath.row]
            cell.delegate = self

            return cell

        }
        
    }

}
extension CommonVideoListViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.height

        // 스크롤이 테이블 뷰 Offset의 끝에 가게 되면 다음 페이지를 호출
        let didScrollGoToEndOfTableView: Bool = offsetY > (contentHeight - height)
        if didScrollGoToEndOfTableView, isListLoadFinish == true {
            isListLoadFinish = false
            print("호출")
            guard let reactor = reactor else { return }
            if reactor.currentState.last_no != "" {
                reactor.action.onNext(.getVideoList(type: reactor.currentState.type, last_no: reactor.currentState.last_no))
            }
        }
    }
}

extension CommonVideoListViewController: VideoMainHeaderCellDelegate{
    func sortList(type: VideoListType) {
        print("sortList")
        guard let reactor = reactor else { return }
        Observable.just(Reactor.Action.getVideoList(type: type, last_no: ""))
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

    }
    
    
}
extension CommonVideoListViewController: VideoMainCellDelegate{
    func moveToYoutube(link: String) {

        guard let urlLink = URL(string: "https://www.youtube.com/watch?v=\(link)") else { return}
        UIApplication.shared.open(urlLink)
    }
    
    func bookMarking(model: ArtistVideoListModel) {
        //
        print("bookMarking")
        guard let reactor = reactor else { return }
        let isBookmarking = model.is_bookmark

        if isBookmarking == 0 {
            reactor.action.onNext(.addBookmark(artist_no: model.artist_no, vod_no: model.no))
        } else {
            reactor.action.onNext(.removeBookmark(artist_no: model.artist_no, vod_no: model.no))

        }

    }
    
}
