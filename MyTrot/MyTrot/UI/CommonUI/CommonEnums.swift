//
//  CommonEnums.swift
//  MyTrot
//
//  Created by hclim on 2021/03/11.
//

import Foundation

enum SelectedTabType: String {
    case tab1 = "tab1"
    case tab2 = "tab2"
    case tab3 = "tab3"
    case tab4 = "tab4"
    case tab5 = "tab5"
}
