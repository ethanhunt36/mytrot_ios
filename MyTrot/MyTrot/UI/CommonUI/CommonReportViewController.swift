//
//  CommonReportViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit

final class CommonReportViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var reportTextView: UITextView!
    @IBOutlet weak var reportSendButton: UIButton!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var placeHolderLabel: UILabel!
    
    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: CommonReportViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension CommonReportViewController {
    func bindAction(_ reactor: CommonReportViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        categoryButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.openAlert()
            }).disposed(by: disposeBag)
        reportSendButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { [weak self] _ in
                return Reactor.Action.sendReport(self?.reportTextView.text ?? "")
            }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

    }
}

// MARK: bindState For Reactor
private extension CommonReportViewController {
    func bindState(_ reactor: CommonReportViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.alertBackMessage}
            .distinctUntilChanged()
            .filterNil()
            .subscribe(onNext: { [weak self] alertBackMessage in
                self?.subscribeOnNextBaseAlertPushBack(alertData: alertBackMessage)
                
            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension CommonReportViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureScrollView))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(singleTapGestureRecognizer)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
    
    @objc func tapGestureScrollView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        bottomAnchorConstraint.constant = -1 * ((view.convert(keyboardFrame.cgRectValue, from: nil).size.height) - view.safeAreaInsets.bottom )
        placeHolderLabel.isHidden = true
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        bottomAnchorConstraint.constant = 0
        if reportTextView.hasText == false {
            placeHolderLabel.isHidden = false
        }
    }

}


// MARK: - Reactor Action
private extension CommonReportViewController {
    func openAlert() {
        let alert =  UIAlertController(title: "선택해주세요", message: "", preferredStyle: .actionSheet)

        let reportNotGood =  UIAlertAction(title: "사진/글 불량", style: .default) { [weak self] (action) in
            print("신고하기")
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.setReportType(.reportBoardIsNotGood))
            self?.categoryLabel.text = "사진/글 불량"
        }
        let reportIncludeAD =  UIAlertAction(title: "사진/글 광고", style: .default) { [weak self] (action) in
            print("신고하기")
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.setReportType(.reportBoardIsIncludingAd))
            self?.categoryLabel.text = "사진/글 광고"
        }
        let reportIncludeEtc =  UIAlertAction(title: "기타", style: .default) { [weak self] (action) in
            print("신고하기")
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.setReportType(.reportEtc))
            self?.categoryLabel.text = "기타"

        }


        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        alert.addAction(reportNotGood)
        alert.addAction(reportIncludeAD)
        alert.addAction(reportIncludeEtc)
        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)

    }
}

// MARK: -
