//
//  PpobKiViewModel.swift
//  MyTrot
//
//  Created by hclim on 2021/06/11.
//

import Foundation
struct PpobKiViewModel: Hashable {
    static func == (lhs: PpobKiViewModel, rhs: PpobKiViewModel) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(createdTime)
    }

    // 모델 생성시간, hashValue 생성을 위한 값으로 사용
    let createdTime: Double = Date().timeIntervalSince1970

    init(_ items: [PpobKiCollectionItem] = []) {
        self.items = items
    }

    // 리스트 데이터
    let items: [PpobKiCollectionItem]

    // item 갯수
    var count: Int { items.count }

    // item 요소에 접근 정의
    subscript(index: Int) -> PpobKiCollectionItem { return items[index] }
}


enum PpobKiType {
    case top
    case normal
}
protocol PpobKiCollectionItem {
    // 타입
    var type: PpobKiType { get }
}
extension PpobKiCollectionItem where Self: PpobKiCollectionTopItem {
    var type: PpobKiType { return .top }
}
extension PpobKiCollectionItem where Self: PpobKiCollectionListItem {
    var type: PpobKiType { return .normal }
}
final class PpobKiCollectionTopItem: PpobKiCollectionItem {

    var info: BonusNumberDataModel
    var prize: Int
    
    init(withInfo info: BonusNumberDataModel, numberPrize: Int) {
        self.info = info
        self.prize = numberPrize
    }
}
final class PpobKiCollectionListItem: PpobKiCollectionItem {
    var info: PpobkiDataInfoModel
    init(withInfo info: PpobkiDataInfoModel) {
        self.info = info
    }

}
