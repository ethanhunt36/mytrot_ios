//
//  SearchVideoListViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/11.
//

import Foundation

import UIKit
import ReactorKit

final class SearchVideoListViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    var tableViewList: [KeywordListDataModel] = SearchKeyword.shared.keywords ?? []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchKeywordField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: SearchVideoListViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension SearchVideoListViewController {
    func bindAction(_ reactor: SearchVideoListViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        
        searchButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { [weak self] _ in
                let keyword = self?.searchKeywordField.text ?? ""
                if keyword == "" {
                    return Reactor.Action.empty
                } else {
                    return Reactor.Action.moveToStepScreen(CommonStep.videoSearchResultPage(keyword: keyword)) }

                }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        
    }
}

// MARK: bindState For Reactor
private extension SearchVideoListViewController {
    func bindState(_ reactor: SearchVideoListViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        
    }
}

// MARK: -
// MARK: private initialize
private extension SearchVideoListViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()

    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureScrollView))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(singleTapGestureRecognizer)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension SearchVideoListViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
    
    @objc func tapGestureScrollView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        bottomAnchorConstraint.constant = ((view.convert(keyboardFrame.cgRectValue, from: nil).size.height)  )
        print("keyboard height : \((view.convert(keyboardFrame.cgRectValue, from: nil).size.height))")
        print("view.safeAreaInsets.bottom : \(view.safeAreaInsets.bottom)")

    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        bottomAnchorConstraint.constant = 0
    }

}

// MARK: -
extension SearchVideoListViewController {
    func initializeTableView() {
        self.tableView.register(cellType: SearchVideoCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension SearchVideoListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as SearchVideoCell
        cell.item = self.tableViewList[indexPath.row]
        cell.delegate = self
        return cell

    }

}

extension SearchVideoListViewController: SearchVideoCellDelegate {
    func moveToSearchResult(keyword: String) {
        print("moveToSearchResult : \(keyword)")
        guard let reactor = reactor else { return }
        reactor.action.onNext(.moveToStepScreen(CommonStep.videoSearchResultPage(keyword: keyword)))
    }
    
}
