//
//  CommonYTPlayerViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/09/01.
//

import Foundation

import UIKit
import ReactorKit
import YoutubePlayer_in_WKWebView

final class CommonYTPlayerViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var playerView: WKYTPlayerView!
    
    var tableViewList: [ArtistVideoListModel] = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var agreementPopupView: UIView!
    @IBOutlet weak var agreementButton: UIButton!
    @IBOutlet weak var agreementMessageView: UIView! {
        didSet {
            agreementMessageView.layer.cornerRadius = 10.0
            agreementMessageView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var agreementMessageTopView: UIView! {
        didSet {
            agreementMessageTopView.setGradient(color1: UIColor.blue, color2: Asset.purpley.color)
        }
    }
    
    var currentPlayTime = 0
    var totalPlayTime = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: CommonYTPlayerViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension CommonYTPlayerViewController {
    func bindAction(_ reactor: CommonYTPlayerViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let playId = reactor.currentState.link
                reactor.action.onNext(.finishApiAndBack(playId))
            }).disposed(by: disposeBag)
        agreementButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.agreementPopupView.isHidden = true
                Utils.updateYoutubeAgreeDate()
            }).disposed(by: disposeBag)
        
    }
}

// MARK: bindState For Reactor
private extension CommonYTPlayerViewController {
    func bindState(_ reactor: CommonYTPlayerViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        
        reactor.state.map { $0.link }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] playerId in
                self?.playerView.load(withVideoId: playerId)
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .distinctUntilChanged({ (resList1, resList2) -> Bool in
                guard let firstObj1 = resList1.first else { return false }
                guard let firstObj2 = resList2.first else { return false }
                return firstObj1.no == firstObj2.no
            })
            .subscribe(onNext: { [weak self] items in
                self?.tableViewList.append(contentsOf: items)
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)

        reactor.state.map { $0.totalPlayTime }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] time in
                self?.totalPlayTime = time
            }).disposed(by: disposeBag)


    }
}

// MARK: -
// MARK: private initialize
private extension CommonYTPlayerViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
        if Utils.checkYoutubeAgreeDate() == false {
            agreementPopupView.isHidden = false
        }
        playerView.delegate = self
    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.YoutubeMyCountUpdate.self, sticky: true).subscribe { [weak self] event in
            guard let model = event.element?.model else { return }
            var tempList: [ArtistVideoListModel] = []
            for var item in self?.tableViewList ?? [] {
                if item.youtube_id == model.youtube_id {
                    print("match! YoutubeMyCountUpdate 0")
                    print("match! YoutubeMyCountUpdate 1: \(item.my_play_cnt)")
                    item.my_play_cnt = item.my_play_cnt + 1
                    print("match! YoutubeMyCountUpdate 2: \(item.my_play_cnt)")

                }
                tempList.append(item)

            }
            self?.tableViewList = tempList
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.AddBookMark.self, sticky: false).subscribe { [weak self] event in
            let strongList = self?.tableViewList ?? []
            var tempTableList: [ArtistVideoListModel] = []
            for var item in strongList {
                if item.no == event.element?.no {
                    item.is_bookmark = 1
                }
                tempTableList.append(item)
            }
            self?.tableViewList = tempTableList
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)
        bus.asObservable(event: Events.RemoveBookMark.self, sticky: false).subscribe { [weak self] event in
            let strongList = self?.tableViewList ?? []
            var tempTableList: [ArtistVideoListModel] = []
            for var item in strongList {
                if item.no == event.element?.no {
                    item.is_bookmark = 0
                }
                tempTableList.append(item)
            }
            self?.tableViewList = tempTableList
            self?.tableView.reloadData()
        }.disposed(by: disposeBag)

    }
}


// MARK: - Reactor Action
private extension CommonYTPlayerViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
extension CommonYTPlayerViewController {
    func initializeTableView() {
        self.tableView.register(cellType: VideoMainCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension CommonYTPlayerViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        let link = self.tableViewList[indexPath.row].youtube_id
        guard let reactor = reactor else { return }

        reactor.action.onNext(.changeYoutubeId(link))

    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as VideoMainCell
        cell.item = self.tableViewList[indexPath.row]
        cell.delegate = self

        return cell

    }

}
extension CommonYTPlayerViewController: VideoMainCellDelegate{
    func moveToYoutube(link: String) {
        // https://www.youtube.com/watch?v=p2IrYhWuXqM
        guard let urlLink = URL(string: "https://www.youtube.com/watch?v=\(link)") else { return}
        UIApplication.shared.open(urlLink)
    }
    
    func bookMarking(model: ArtistVideoListModel) {
        //
        print("bookMarking")
        guard let reactor = reactor else { return }
        let isBookmarking = model.is_bookmark ?? 0

        if isBookmarking == 0 {
            reactor.action.onNext(.addBookmark(artist_no: model.artist_no, vod_no: model.no))
        } else {
            reactor.action.onNext(.removeBookmark(artist_no: model.artist_no, vod_no: model.no))

        }

    }
    
}
extension CommonYTPlayerViewController: WKYTPlayerViewDelegate{
    func playerView(_ playerView: WKYTPlayerView, didPlayTime playTime: Float) {
        print("playerView didPlayTime::\(playTime)")
        guard let reactor = reactor else { return }
        reactor.action.onNext(.currentPlayTime(playTime))

    }
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        print("playerView playerViewDidBecomeReady:: API 호출")

        playerView.playVideo()
        guard let reactor = reactor else { return }
        reactor.action.onNext(.playApi)
    }
    
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState) {
        print("playerView didChangeTo state:: \(state.rawValue)")

        if state == WKYTPlayerState.ended {
            print("next Play")
            print("next Play")
            guard let reactor = reactor else { return }
            let playId = reactor.currentState.link
            reactor.action.onNext(.finishApi(playId))
            reactor.action.onNext(.chenagNextVideo)

        }
    }
}
