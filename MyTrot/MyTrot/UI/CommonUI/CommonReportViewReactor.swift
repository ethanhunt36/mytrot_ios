//
//  CommonReportViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class CommonReportViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(Step)
        
        case setReportType(ReportCategoryType)
        case sendReport(String)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setReportType(ReportCategoryType)
        case empty
        case setResponse(CommonResponse)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var board_no: Int64
        var reportType: ReportCategoryType
        var alertBackMessage: String?

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init(withBoardNo board_no: Int64) {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            board_no: board_no,
            reportType: .nonSelected
        )
    }
    
    func mutate(action: CommonReportViewReactor.Action) -> Observable<CommonReportViewReactor.Mutation> {
        switch action {
        case .sendReport(let cont):
            if currentState.reportType == .nonSelected {
                Utils.AlertShow(msg: "신고사유를 선택해 주세요.")
                return .empty()
            }
            if cont.count == 0 {
                Utils.AlertShow(msg: "신고내용을 입력해 주세요.")
                return .empty()
            }
            return networkService.reportBoard(board_no: currentState.board_no, type: currentState.reportType.rawValue, reason: cont).asObservable().map { Mutation.setResponse($0)}
        case .setReportType(let type):
            return .just(.setReportType(type))
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            navigate(step: CommonStep.pushDismiss(animated: true))

            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: CommonReportViewReactor.State, mutation: CommonReportViewReactor.Mutation) -> CommonReportViewReactor.State {
        var state = state
        switch mutation {
        case .setReportType(let type):
            state.reportType = type
        case .setDismiss(let flag):
            state.isDismiss = flag
        case .empty: return state
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.alertBackMessage = response.msg?.replaceSlashString ?? "신고 접수가 완료되었습니다.\n최대한 빨리 확인/처리해서 회신 드리겠습니다."
                
            } else {
                RxBus.shared.post(event: Events.AlertShow(message: response.msg ?? ""))
            }

        }
        return state
    }
}
