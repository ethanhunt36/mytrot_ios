//
//  CommonWebViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/12.
//

import Foundation

import UIKit
import ReactorKit
import WebKit
/**
 5-6-2 유머글 확인 
 7-11 공지사항 상세
 7-12 FAQ
 7-13 이용약관 상세
 
 */
final class CommonWebViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var agreeTermsButton: UIButton! {
        didSet {
            agreeTermsButton.customCornerRadius = 10.0
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: CommonWebViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension CommonWebViewController {
    func bindAction(_ reactor: CommonWebViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        agreeTermsButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                Utils.AlertShow(msg: "동의되었습니다.\n\n이제 마이트롯의 모든 기능을 사용하세요!")
                
                let pref = PreferencesService()
                pref.setIsUserAgreeTerms(isUserAgreeTerms: true)
                self?.agreeTermsButton.isHidden = true 
                
            }).disposed(by: disposeBag)

        
    }
}

// MARK: bindState For Reactor
private extension CommonWebViewController {
    func bindState(_ reactor: CommonWebViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        
        reactor.state.map { $0.type}
            .subscribe(onNext: { [weak self] type in
                switch type {
                case.blog:
                    self?.topTitleLabel.text = "블로그"
                case .agreement:
                    self?.topTitleLabel.text = "약관"
                    let pref = PreferencesService()
                    if pref.getIsUserAgreeTerms() == false {
                        self?.agreeTermsButton.isHidden = false
                    } else {
                        self?.agreeTermsButton.isHidden = true
                    }
                case .podong:
                    self?.topTitleLabel.text = "유머글"
                case .notice:
                    self?.topTitleLabel.text = "공지사항"
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.url}
            .take(1)
            .subscribe(onNext: {[weak self] url in
                self?.webView.load(URLRequest.init(url: URL(string: url)!))
            }).disposed(by: disposeBag)
    }
}

// MARK: -
// MARK: private initialize
private extension CommonWebViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension CommonWebViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
