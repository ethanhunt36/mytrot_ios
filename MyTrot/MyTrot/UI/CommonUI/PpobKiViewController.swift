//
//  PpobKiViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/06/11.
//

import Foundation

import UIKit
import ReactorKit

final class PpobKiViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    enum Define {
        static let screenWidth: CGFloat = UIScreen.main.bounds.width
    }
    
    @IBOutlet weak var completeMessageLabel: UILabel!
    @IBOutlet weak var completeView: UIView!
    @IBOutlet weak var completeRankImageView: UIImageView!
    @IBOutlet weak var completeMessageView: UIView! {
        didSet {
            completeMessageView.layer.cornerRadius = 10.0
            completeMessageView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var completeMessageTopView: UIView! {
        didSet {
            completeMessageTopView.setGradient(color1: UIColor.blue, color2: Asset.purpley.color)
        }
    }

    @IBOutlet weak var completeButton: UIButton!
    var data = PpobKiViewModel() {
        didSet {
            collectionView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: PpobKiViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension PpobKiViewController {
    func bindAction(_ reactor: PpobKiViewReactor) {
        backButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        completeButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.dismiss }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        
    }
}

// MARK: bindState For Reactor
private extension PpobKiViewController {
    func bindState(_ reactor: PpobKiViewReactor) {
        reactor.action.onNext(.getPpobkiInfo)
        
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.ppobkiViewModel }
            .filterNil()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] model in
                self?.data = model
            }).disposed(by: disposeBag)


        reactor.state.map { $0.ppobkiResultInfo}
            .filterNil()
            .map { $0.data }
            .debug()
            .filterNil()
            .debug()
            .map{ $0.bonus_num }
            .subscribe(onNext: { [weak self] bonus in
                switch bonus {
                case 1:
                    self?.completeRankImageView.image = Asset.icDrawRank1.image
                case 2:
                    self?.completeRankImageView.image = Asset.icDrawRank2.image
                case 3:
                    self?.completeRankImageView.image = Asset.icDrawRank3.image
                case 4:
                    self?.completeRankImageView.image = Asset.icDrawRank4.image
                case 5:
                    self?.completeRankImageView.image = Asset.icDrawRank5.image
                default: break;
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.ppobkiResultInfo}
            .filterNil()
            .debug()
            .map { $0.data }
            .filterNil()
            .map{ $0.msg}
            .bind(to: completeMessageLabel.rx.text)
            .disposed(by: disposeBag)
        reactor.state.map { $0.ppobkiPlayComplete }
            .map { !$0 }
            .bind(to: completeView.rx.isHidden)
            .disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension PpobKiViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeCollectionLayout()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.PpobKiNeedToRefresh.self, sticky: true).subscribe { [weak self] _ in
            guard let reactor = self?.reactor else { return }
            reactor.action.onNext(.getPpobkiInfo)

        }.disposed(by: disposeBag)
    }
    
    func initializeCollectionLayout() {
        // ios 10
//        let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
//
//        let collectionViewCellwidth: CGFloat = (Define.screenWidth / 6)
//        let collectionViewCellheight: CGFloat = 110.0
//
//        print("collectionViewCellwidth : \(collectionViewCellwidth)")
//        print("Define.screenWidth : \(Define.screenWidth)")
//        print("collectionViewCellheight : \(collectionViewCellheight)")
//
//        // 고정된 크기
//
//        flowLayout?.estimatedItemSize = CGSize(width: collectionViewCellwidth, height: collectionViewCellheight)
        

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(cellType: PpobKiTopCell.self)
        collectionView.register(cellType: PpobKiListCell.self)

        

    }

}


// MARK: - Reactor Action
private extension PpobKiViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
extension PpobKiViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //위아래 라인 간격
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

    // 옆 간격
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print("cell width: \(collectionView.frame.size.width)")
        print("cell width /5: \(collectionView.frame.size.width/6)")
        let width = (collectionView.frame.size.width / 5 - 1)
        let item = data[indexPath.row]
        switch item.type {
        case .top:

            return CGSize(width: width, height: 120)
        case .normal:
            return CGSize(width: width, height: width)

        }

    }

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//
//        return 1
//
//    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let item = ppobkiList[indexPath.row]
        let item = data[indexPath.row]
        switch item.type {
        case .top:
            let cell = collectionView.dequeueReusableCell(for: indexPath) as PpobKiTopCell
            cell.item = item as? PpobKiCollectionTopItem
            return cell
        case .normal:
            let cell = collectionView.dequeueReusableCell(for: indexPath) as PpobKiListCell
            cell.item = item as? PpobKiCollectionListItem
            cell.delegate = self
            return cell

        }
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }

}

extension PpobKiViewController: PpobKiListCellDelegate {
    func ppobkiAction(item: PpobKiCollectionListItem) {
        print("ppobkiAction: \(item.info)")

        if item.info.use_yn == "Y" {
            Utils.AlertShow(msg: "다른 분이 이미 뽑았습니다.\n\n다른 뽑기를 선택해주세요.")
            return
        }
        
        
        guard let reactor = reactor else { return }

        reactor.action.onNext(.playPpobki(item.info.no))
    }
    
    
}
