//
//  LuckyNumberNormalCell.swift
//  MyTrot
//
//  Created by hclim on 2021/06/15.
//

import Foundation
import RxSwift

class LuckyNumberNormalCell: BaseTableViewCell {
    var disposeBag = DisposeBag()

    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    @IBOutlet weak var outView: UIView! {
        didSet {
            outView.layer.borderColor = Asset.darkGrey.color.cgColor
            outView.layer.cornerRadius = 20.0
            outView.layer.masksToBounds = true
            outView.layer.borderWidth = 1.0
        }
    }

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var number1row1ImageView: UIImageView!
    @IBOutlet weak var number1row2ImageView: UIImageView!
    @IBOutlet weak var number1row3ImageView: UIImageView!
    @IBOutlet weak var number1row4ImageView: UIImageView!
    @IBOutlet weak var number1row5ImageView: UIImageView!
    @IBOutlet weak var number1row6ImageView: UIImageView!

    @IBOutlet weak var number2row1ImageView: UIImageView!
    @IBOutlet weak var number2row2ImageView: UIImageView!
    @IBOutlet weak var number2row3ImageView: UIImageView!
    @IBOutlet weak var number2row4ImageView: UIImageView!
    @IBOutlet weak var number2row5ImageView: UIImageView!
    @IBOutlet weak var number2row6ImageView: UIImageView!

    @IBOutlet weak var number1row1Label: UILabel!
    @IBOutlet weak var number1row2Label: UILabel!
    @IBOutlet weak var number1row3Label: UILabel!
    @IBOutlet weak var number1row4Label: UILabel!
    @IBOutlet weak var number1row5Label: UILabel!
    @IBOutlet weak var number1row6Label: UILabel!

    @IBOutlet weak var number2row1Label: UILabel!
    @IBOutlet weak var number2row2Label: UILabel!
    @IBOutlet weak var number2row3Label: UILabel!
    @IBOutlet weak var number2row4Label: UILabel!
    @IBOutlet weak var number2row5Label: UILabel!
    @IBOutlet weak var number2row6Label: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    var item: LuckyNumberCellNormalItem?{
        didSet {
            guard let numberFirstObject = item?.info else { return }
            number1row1Label.text = "\(numberFirstObject.num01)"
            number1row2Label.text = "\(numberFirstObject.num02)"
            number1row3Label.text = "\(numberFirstObject.num03)"
            number1row4Label.text = "\(numberFirstObject.num04)"
            number1row5Label.text = "\(numberFirstObject.num05)"
            number1row6Label.text = "\(numberFirstObject.num06)"
            number2row1Label.text = "\(numberFirstObject.num07)"
            number2row2Label.text = "\(numberFirstObject.num08)"
            number2row3Label.text = "\(numberFirstObject.num09)"
            number2row4Label.text = "\(numberFirstObject.num10)"
            number2row5Label.text = "\(numberFirstObject.num11)"
            number2row6Label.text = "\(numberFirstObject.num12)"
            dateLabel.text = numberFirstObject.dates
            Utils.displayNumberBackground(imageView: number1row1ImageView)
            Utils.displayNumberBackground(imageView: number1row2ImageView)
            Utils.displayNumberBackground(imageView: number1row3ImageView)
            Utils.displayNumberBackground(imageView: number1row4ImageView)
            Utils.displayNumberBackground(imageView: number1row5ImageView)
            Utils.displayNumberBackground(imageView: number1row6ImageView)
            Utils.displayNumberBackground(imageView: number2row1ImageView)
            Utils.displayNumberBackground(imageView: number2row2ImageView)
            Utils.displayNumberBackground(imageView: number2row3ImageView)
            Utils.displayNumberBackground(imageView: number2row4ImageView)
            Utils.displayNumberBackground(imageView: number2row5ImageView)
            Utils.displayNumberBackground(imageView: number2row6ImageView)
            
            
            number1row1ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num01)
            number1row2ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num02)
            number1row3ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num03)
            number1row4ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num04)
            number1row5ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num05)
            number1row6ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num06)
            number2row1ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num07)
            number2row2ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num08)
            number2row3ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num09)
            number2row4ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num10)
            number2row5ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num11)
            number2row6ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject.num01)
        }
        
    }
}
