//
//  LuckyNumberAdvCell.swift
//  MyTrot
//
//  Created by hclim on 2021/06/15.
//

import Foundation
import RxSwift
class LuckyNumberAdvCell: BaseTableViewCell {
    var disposeBag = DisposeBag()

    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    
    @IBOutlet weak var adView: UIView!
    var adInsideView: CaulyAdView? = nil {
        didSet {
            guard let strongADView = adInsideView else { return }
            adView.addSubview(strongADView)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
    var item: LuckyNumberCellAdvItem? {
        didSet {
        }
    }
}
