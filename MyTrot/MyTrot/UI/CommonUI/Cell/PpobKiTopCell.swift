//
//  PpobKiTopCell.swift
//  MyTrot
//
//  Created by hclim on 2021/06/11.
//

import Foundation
import Reusable
import Kingfisher

class PpobKiTopCell: UICollectionViewCell, NibReusable {
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var existCountLabel: UILabel!
    @IBOutlet weak var backgroundImageTop: UIImageView!
    @IBOutlet weak var backgroundImageBottom: UIImageView!
    var item: PpobKiCollectionTopItem? {
        didSet {
            descLabel.text = "투표권\n\(item?.info.vote_cnt ?? 0)장\n포인트\n\(item?.info.point ?? 0)P"
            existCountLabel.text = "\(item?.info.cnt ?? 0)장"
            switch item?.prize {
            case 1:
                backgroundImageTop.image = Asset.bgPrize1Top.image
                backgroundImageBottom.image = Asset.bgPrize1Foot.image
            case 2:
                backgroundImageTop.image = Asset.bgPrize2Top.image
                backgroundImageBottom.image = Asset.bgPrize2Foot.image
                descLabel.textColor = .orange
                existCountLabel.textColor = .orange
            case 3:
                backgroundImageTop.image = Asset.bgPrize3Top.image
                backgroundImageBottom.image = Asset.bgPrize3Foot.image
                descLabel.textColor = .blue
                existCountLabel.textColor = .blue
            case 4:
                backgroundImageTop.image = Asset.bgPrize4Top.image
                backgroundImageBottom.image = Asset.bgPrize4Foot.image
                descLabel.textColor = .gray
                existCountLabel.textColor = .gray

            case 5:
                backgroundImageTop.image = Asset.bgPrize5Top.image
                backgroundImageBottom.image = Asset.bgPrize5Foot.image
                descLabel.textColor = .gray
                existCountLabel.textColor = .gray

            default:
                break;
            }

        }
    }
}
