//
//  LuckyNumberDupleCell.swift
//  MyTrot
//
//  Created by hclim on 2021/06/15.
//

import Foundation
import RxSwift

class LuckyNumberDupleCell: BaseTableViewCell {
    var disposeBag = DisposeBag()

    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }

    @IBOutlet weak var number1row1ImageView: UIImageView!
    @IBOutlet weak var number1row2ImageView: UIImageView!
    @IBOutlet weak var number1row3ImageView: UIImageView!
    @IBOutlet weak var number1row4ImageView: UIImageView!
    @IBOutlet weak var number1row5ImageView: UIImageView!
    @IBOutlet weak var number1row6ImageView: UIImageView!

    @IBOutlet weak var number2row1ImageView: UIImageView!
    @IBOutlet weak var number2row2ImageView: UIImageView!
    @IBOutlet weak var number2row3ImageView: UIImageView!
    @IBOutlet weak var number2row4ImageView: UIImageView!
    @IBOutlet weak var number2row5ImageView: UIImageView!
    @IBOutlet weak var number2row6ImageView: UIImageView!

    @IBOutlet weak var number1row1Label: UILabel!
    @IBOutlet weak var number1row2Label: UILabel!
    @IBOutlet weak var number1row3Label: UILabel!
    @IBOutlet weak var number1row4Label: UILabel!
    @IBOutlet weak var number1row5Label: UILabel!
    @IBOutlet weak var number1row6Label: UILabel!

    @IBOutlet weak var number2row1Label: UILabel!
    @IBOutlet weak var number2row2Label: UILabel!
    @IBOutlet weak var number2row3Label: UILabel!
    @IBOutlet weak var number2row4Label: UILabel!
    @IBOutlet weak var number2row5Label: UILabel!
    @IBOutlet weak var number2row6Label: UILabel!

    
    @IBOutlet weak var count1row1Label: UILabel!
    @IBOutlet weak var count1row2Label: UILabel!
    @IBOutlet weak var count1row3Label: UILabel!
    @IBOutlet weak var count1row4Label: UILabel!
    @IBOutlet weak var count1row5Label: UILabel!
    @IBOutlet weak var count1row6Label: UILabel!

    @IBOutlet weak var count2row1Label: UILabel!
    @IBOutlet weak var count2row2Label: UILabel!
    @IBOutlet weak var count2row3Label: UILabel!
    @IBOutlet weak var count2row4Label: UILabel!
    @IBOutlet weak var count2row5Label: UILabel!
    @IBOutlet weak var count2row6Label: UILabel!
    @IBOutlet weak var outView: UIView! {
        didSet {
            outView.layer.borderColor = Asset.tangerine.color.cgColor
            outView.layer.cornerRadius = 20.0
            outView.layer.masksToBounds = true
            outView.layer.borderWidth = 1.0
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    var item: LuckyNumberCellDuplicateItem?{
        didSet {
            guard let numberFirstObject = item?.info else { return }
            if numberFirstObject.count >= 12 {
                number1row1Label.text = "\(numberFirstObject[0].num)"
                number1row2Label.text = "\(numberFirstObject[1].num)"
                number1row3Label.text = "\(numberFirstObject[2].num)"
                number1row4Label.text = "\(numberFirstObject[3].num)"
                number1row5Label.text = "\(numberFirstObject[4].num)"
                number1row6Label.text = "\(numberFirstObject[5].num)"
                number2row1Label.text = "\(numberFirstObject[6].num)"
                number2row2Label.text = "\(numberFirstObject[7].num)"
                number2row3Label.text = "\(numberFirstObject[8].num)"
                number2row4Label.text = "\(numberFirstObject[9].num)"
                number2row5Label.text = "\(numberFirstObject[10].num)"
                number2row6Label.text = "\(numberFirstObject[11].num)"
                
                count1row1Label.text = "X\(numberFirstObject[0].count)"
                count1row2Label.text = "X\(numberFirstObject[1].count)"
                count1row3Label.text = "X\(numberFirstObject[2].count)"
                count1row4Label.text = "X\(numberFirstObject[3].count)"
                count1row5Label.text = "X\(numberFirstObject[4].count)"
                count1row6Label.text = "X\(numberFirstObject[5].count)"
                count2row1Label.text = "X\(numberFirstObject[6].count)"
                count2row2Label.text = "X\(numberFirstObject[7].count)"
                count2row3Label.text = "X\(numberFirstObject[8].count)"
                count2row4Label.text = "X\(numberFirstObject[9].count)"
                count2row5Label.text = "X\(numberFirstObject[10].count)"
                count2row6Label.text = "X\(numberFirstObject[11].count)"

                number1row1ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[0].num)
                number1row2ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[1].num)
                number1row3ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[2].num)
                number1row4ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[3].num)
                number1row5ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[4].num)
                number1row6ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[5].num)
                number2row1ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[6].num)
                number2row2ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[7].num)
                number2row3ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[8].num)
                number2row4ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[9].num)
                number2row5ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[10].num)
                number2row6ImageView.backgroundColor = Utils.getLuckyNumberBackgroundColor(num: numberFirstObject[11].num)
                Utils.displayNumberBackground(imageView: number1row1ImageView)
                Utils.displayNumberBackground(imageView: number1row2ImageView)
                Utils.displayNumberBackground(imageView: number1row3ImageView)
                Utils.displayNumberBackground(imageView: number1row4ImageView)
                Utils.displayNumberBackground(imageView: number1row5ImageView)
                Utils.displayNumberBackground(imageView: number1row6ImageView)
                Utils.displayNumberBackground(imageView: number2row1ImageView)
                Utils.displayNumberBackground(imageView: number2row2ImageView)
                Utils.displayNumberBackground(imageView: number2row3ImageView)
                Utils.displayNumberBackground(imageView: number2row4ImageView)
                Utils.displayNumberBackground(imageView: number2row5ImageView)
                Utils.displayNumberBackground(imageView: number2row6ImageView)

            }

        }
        
    }
}
