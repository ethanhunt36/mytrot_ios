//
//  SearchVideoCell.swift
//  MyTrot
//
//  Created by hclim on 2021/04/30.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

protocol SearchVideoCellDelegate: AnyObject {
    func moveToSearchResult(keyword: String)
}
class SearchVideoCell: BaseTableViewCell {
    var disposeBag = DisposeBag()

    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    weak var delegate: SearchVideoCellDelegate?
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    func initializeEvent() {
        firstButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let item = self?.item else { return }

                self?.delegate?.moveToSearchResult(keyword: item.key01)
            }).disposed(by: disposeBag)
        secondButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let item = self?.item else { return }
                self?.delegate?.moveToSearchResult(keyword: item.key02)
            }).disposed(by: disposeBag)

    }
    var item: KeywordListDataModel?{
        didSet {
            firstLabel.text = item?.key01
            secondLabel.text = item?.key02
        }
        
    }
}
