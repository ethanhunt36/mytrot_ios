//
//  LuckyNumberHeaderCell.swift
//  MyTrot
//
//  Created by hclim on 2021/06/15.
//

import Foundation
import RxSwift
class LuckyNumberHeaderCell: BaseTableViewCell {
    var disposeBag = DisposeBag()

    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    
    @IBOutlet weak var duplicateLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        duplicateLabel.isHidden = true
    }
    var item: LuckyNumberCellHeaderItem? {
        didSet {
            if item?.hasDuplicate ?? false == true {
                duplicateLabel.isHidden = false
            }
        }
    }
}
