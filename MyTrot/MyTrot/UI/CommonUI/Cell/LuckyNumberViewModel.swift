//
//  LuckyNumberViewModel.swift
//  MyTrot
//
//  Created by hclim on 2021/06/15.
//

import Foundation
/// 행운의 번호에서 사용되는 데이터 모델
struct LuckyNumberViewModel: Hashable {
    static func == (lhs: LuckyNumberViewModel, rhs: LuckyNumberViewModel) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(createdTime)
    }

    // 모델 생성시간, hashValue 생성을 위한 값으로 사용
    let createdTime: Double = Date().timeIntervalSince1970

    init(_ items: [LuckyNumberCellItem] = []) {
        self.items = items
    }

    // 리스트 데이터
    let items: [LuckyNumberCellItem]

    // item 갯수
    var count: Int { items.count }

    // item 요소에 접근 정의
    subscript(index: Int) -> LuckyNumberCellItem { return items[index] }
}

enum LuckyNumberType {
    case header
    case duplicate
    case adv
    case normal
}
protocol LuckyNumberCellItem {
    // 타입
    var type: LuckyNumberType { get }
}
extension LuckyNumberCellItem where Self: LuckyNumberCellHeaderItem {
    var type: LuckyNumberType { return .header }
}
extension LuckyNumberCellItem where Self: LuckyNumberCellDuplicateItem {
    var type: LuckyNumberType { return .duplicate }
}
extension LuckyNumberCellItem where Self: LuckyNumberCellAdvItem {
    var type: LuckyNumberType { return .adv }
}
extension LuckyNumberCellItem where Self: LuckyNumberCellNormalItem {
    var type: LuckyNumberType { return .normal }
}

final class LuckyNumberCellHeaderItem: LuckyNumberCellItem {

    var hasDuplicate: Bool
    
    init(withDuplicateable hasDuplicate: Bool) {
        self.hasDuplicate = hasDuplicate
    }
}
final class LuckyNumberCellDuplicateItem: LuckyNumberCellItem {

    var info: [Duplicate2ListModel]
    
    init(withInfo info: [Duplicate2ListModel]) {
        self.info = info
    }
}

// 중간 광고 리스트
final class LuckyNumberCellAdvItem: LuckyNumberCellItem {

}
final class LuckyNumberCellNormalItem: LuckyNumberCellItem {

    var info: LuckyNumberListModel
    
    init(withInfo info: LuckyNumberListModel) {
        self.info = info
    }
}
