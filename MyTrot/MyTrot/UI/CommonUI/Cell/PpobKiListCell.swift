//
//  PpobKiListCell.swift
//  MyTrot
//
//  Created by hclim on 2021/06/11.
//

import Foundation
import RxSwift
import Reusable
import Kingfisher
protocol PpobKiListCellDelegate: AnyObject {
    func ppobkiAction(item: PpobKiCollectionListItem)
}

class PpobKiListCell: UICollectionViewCell, NibReusable {
    var disposeBag = DisposeBag()

    @IBOutlet weak var ppobkiImageView: UIImageView!
    @IBOutlet weak var actionButton: UIButton!
    weak var delegate: PpobKiListCellDelegate?
    override func prepareForReuse() {
        ppobkiImageView.image = Asset.icDraw.image
        super.prepareForReuse()

    }
    var item: PpobKiCollectionListItem? {
        didSet {
            let useYn = item?.info.use_yn ?? "N"
            if useYn == "Y" {
                ppobkiImageView.image = Asset.icDrawWin.image
            } else {
                ppobkiImageView.image = Asset.icDraw.image
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }


    func initializeEvent() {
        actionButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let item = self?.item else { return }
                self?.delegate?.ppobkiAction(item: item)
            }).disposed(by: disposeBag)

    }

}
