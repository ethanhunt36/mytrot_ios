//
//  ChartMainViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
import ReactorKit
import RxOptional

final class ChartMainViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton! {
        didSet {
            searchButton.layer.cornerRadius = 5.0
            searchButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var tableView: UITableView!

    var tableViewList: [ArtistRankDataListModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
    func bind(reactor: ChartMainViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Utils.needToCheckChartFirstPopupDate() == true {
            print("팝업이 노출되어야 해야합니다!")
            let msg = "트롯차트의 점수 집계 방식에 대해\n안내 말씀 드립니다.\n\n\n 총점 = 재생X4 + 투표X3 + 응원글X2 + 좋아요\n\n\n좋아하는 가수의 노래를 많이 듣고,\n투표하고, 응원글도 남기고,\n응원글에 '좋아요' 많이 눌러주세요~"
            let controller = UIAlertController.init(title: "", message: msg, preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "확인", style: .default, handler: { _ in
                Utils.updateChartFirstPopupDate()
            })

            controller.addAction(defaultAction)

            self.present(controller, animated: true)


        } else {
            print("오늘은 팝업이 노출 되었어요!")
        }
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension ChartMainViewController {
    func bindAction(_ reactor: ChartMainViewReactor) {
        searchButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.moveToSearch }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
        

        reactor.action.onNext(.getList)
    }
}

// MARK: bindState For Reactor
private extension ChartMainViewController {
    func bindState(_ reactor: ChartMainViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .subscribe(onNext: { [weak self] items in
                self?.tableViewList = items
//                self?.tableViewList.append(contentsOf: items)
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension ChartMainViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}


// MARK: - Reactor Action
private extension ChartMainViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

extension ChartMainViewController {
    func initializeTableView() {
        self.tableView.register(cellType: ChartCell.self)
        self.tableView.register(cellType: ChartHeaderCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension ChartMainViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        if indexPath.row == 0 { return }
        let cellItem = tableViewList[indexPath.row]
        guard let reactor = reactor else { return }
        reactor.action.onNext(.moveToStepScreen(.videoListPage(keyword: cellItem.name)))
    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 { return 50 }

        return 190
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(for: indexPath) as ChartHeaderCell
            cell.item = self.tableViewList[indexPath.row]
            cell.delegate = self
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(for: indexPath) as ChartCell
            cell.rowNum = indexPath.row 
            cell.item = self.tableViewList[indexPath.row]

            return cell
        }

    }
}

extension ChartMainViewController: ChartHeaderCellDelegate {
    func sortList(type: GenderType) {
        guard let reactor = reactor else { return }
        reactor.action.onNext(.getListByGender(type))
    }
    
    
}
