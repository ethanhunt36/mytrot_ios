//
//  ChartCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/14.
//

import Foundation
import UIKit
import Kingfisher

class ChartCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }

    @IBOutlet weak var myPlayLabel: PaddingLabel!{
        didSet {
            myPlayLabel.layer.cornerRadius = Define.cornerRadius
            myPlayLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var myVoteLabel: PaddingLabel!{
        didSet {
            myVoteLabel.layer.cornerRadius = Define.cornerRadius
            myVoteLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var myLikeLabel: PaddingLabel!{
        didSet {
            myLikeLabel.layer.cornerRadius = Define.cornerRadius
            myLikeLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var myCheerLabel: PaddingLabel!{
        didSet {
            myCheerLabel.layer.cornerRadius = Define.cornerRadius
            myCheerLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var playLabel: PaddingLabel!{
        didSet {
            playLabel.layer.cornerRadius = Define.cornerRadius
            playLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var voteLabel: PaddingLabel!{
        didSet {
            voteLabel.layer.cornerRadius = Define.cornerRadius
            voteLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var likeLabel: PaddingLabel!{
        didSet {
            likeLabel.layer.cornerRadius = Define.cornerRadius
            likeLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var cheerLabel: PaddingLabel!{
        didSet {
            cheerLabel.layer.cornerRadius = Define.cornerRadius
            cheerLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var singerImageView: UIImageView!{
        didSet {
            singerImageView.layer.cornerRadius = 35.0
            singerImageView.layer.masksToBounds = true
        }
    }

    @IBOutlet weak var singerLabel: UILabel!

    @IBOutlet weak var totalLabel: UILabel!

    var rowNum: Int = 0
    var item: ArtistRankDataListModel? {
        didSet {
            guard let cellItem = item else { return }
            
            let attrString = "\(rowNum.commaString).  \(cellItem.name)"
            let attributedString = NSMutableAttributedString(string: attrString, attributes: [
                .font: UIFont.systemFont(ofSize: 20.0, weight: .semibold),
                .foregroundColor: Asset.black.color,
              .kern: 0.0
            ])
            attributedString.addAttribute(.foregroundColor, value: UIColor.purple, range: NSRange(location: 0, length: rowNum.commaString.count + 1))
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 23.0, weight: .heavy), range: NSRange(location: 0, length: rowNum.commaString.count + 1))

            singerLabel.attributedText = attributedString

            myPlayLabel.text = "\(cellItem.my_play_cnt.insertComma)"
            myVoteLabel.text = "\(cellItem.my_vote_cnt.insertComma)"
            myLikeLabel.text = "\(cellItem.my_like_cnt.commaString)"
            myCheerLabel.text = "\(cellItem.my_bbs_cnt.commaString)"
            playLabel.text = "\(cellItem.play_cnt.commaString)"
            voteLabel.text = "\(cellItem.vote_cnt.commaString)"
            likeLabel.text = "\(cellItem.like_cnt.commaString)"
            cheerLabel.text = "\(cellItem.bbs_cnt.commaString)"
            totalLabel.text = "\(cellItem.rank1_score.commaString)"
            singerImageView.kf.setImage(with: URL(string: cellItem.pic.validateHostImageUrl))
        }
    }
}
