//
//  ChartHeaderCell.swift
//  MyTrot
//
//  Created by hclim on 2021/10/09.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
protocol ChartHeaderCellDelegate: AnyObject {
    func sortList(type: GenderType)
}
class ChartHeaderCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()
    weak var delegate: ChartHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    @IBOutlet weak var totalButton: UIButton! {
        didSet {
            totalButton.customCornerRadius = 10.0
            totalButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            totalButton.setBackgroundColor(UIColor.clear, for: .normal)
            totalButton.setTitleColor(Asset.tangerine.color, for: .normal)
            totalButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var maleButton: UIButton! {
        didSet {
            maleButton.customCornerRadius = 10.0

            maleButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            maleButton.setBackgroundColor(UIColor.clear, for: .normal)
            maleButton.setTitleColor(Asset.tangerine.color, for: .normal)
            maleButton.setTitleColor(Asset.tangerine.color, for: .selected)

        }
    }
    @IBOutlet weak var femaleButton: UIButton! {
        didSet {
            femaleButton.customCornerRadius = 10.0

            femaleButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            femaleButton.setBackgroundColor(UIColor.clear, for: .normal)
            femaleButton.setTitleColor(Asset.tangerine.color, for: .normal)
            femaleButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }

    var item: ArtistRankDataListModel? {
        didSet {
//            guard let cellItem = item else { return }
//            
//            guard let type = cellItem.chartType else { return }
            let type = Utils.getLastSavedChartGender()
            totalButton.isSelected = false
            maleButton.isSelected = false
            femaleButton.isSelected = false
            switch type {
            case .both:
                totalButton.isSelected = true
            case .male:
                maleButton.isSelected = true
            case .female:
                femaleButton.isSelected = true
            default:
                break;
            }
        }
    }
    
    func initializeEvent() {
        totalButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.chartType = .both
                self?.delegate?.sortList(type: .both)
            }).disposed(by: disposeBag)
        maleButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.chartType = .male
                self?.delegate?.sortList(type: .male)
            }).disposed(by: disposeBag)
        femaleButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.chartType = .female
                self?.delegate?.sortList(type: .female)
            }).disposed(by: disposeBag)

    }
}
