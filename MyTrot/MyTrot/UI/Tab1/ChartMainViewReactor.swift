//
//  ChartMainViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class ChartMainViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToSearch
        case moveToStepScreen(CommonStep)

        case getList
        case getListByGender(GenderType)
    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setItems(ResponseArtistRankList)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        
        var items: [ArtistRankDataListModel]
        var header: ArtistRankDataListModel
    }
    
    /// 초기 상태값
    let initialState: State
    let networkService = MyTrotService()
    init() {
        self.initialState = State(
            isDismiss: false,
            items: [],
            header: ArtistRankDataListModel(rank1: 0, rank1_score: 0, name: "", my_play_cnt: "", my_bbs_cnt: 0, my_vote_cnt: "", my_like_cnt: 0, play_cnt: 0, bbs_cnt: 0, vote_cnt: 0, like_cnt: 0, pic: "", no: 0, chartType: Utils.getLastSavedChartGender())
        )
    }
    
    func mutate(action: ChartMainViewReactor.Action) -> Observable<ChartMainViewReactor.Mutation> {
        switch action {
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()

        case .moveToSearch:
            navigate(step: CommonStep.searchPage)
            return .empty()
        case .getListByGender(let gender):
            Utils.saveChartGender(type: gender)
            return self.networkService.chartList().asObservable().map{ Mutation.setItems($0)}

        case .getList:
            return self.networkService.chartList().asObservable().map{ Mutation.setItems($0)}

        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: ChartMainViewReactor.State, mutation: ChartMainViewReactor.Mutation) -> ChartMainViewReactor.State {
        var state = state
        switch mutation {
        case .setItems(let response):
            if response.isResponseStatusSuccessful() == false {
                // alert
                RxBus.shared.post(event: Events.AlertShow(message: response.msg ?? Constants.Network.ErrorMessage.networkError))
                return state
            }
            var sectionList: [ArtistRankDataListModel] = []
            sectionList.append(currentState.header)
            if let data: ArtistRankDataModel = response.data {
                for listOne in data.list_data {
                    sectionList.append(listOne)
                }
            }
            state.items = sectionList
        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
    
}
