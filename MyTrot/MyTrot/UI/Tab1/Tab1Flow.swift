//
//  ChartFlow.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
import RxFlow
import RxRelay

final class Tab1Flow: Flow {
    var root: Presentable {
        return self.rootViewController
    }

    private lazy var rootViewController: BaseNavigationController = {
        let viewController = BaseNavigationController()
        viewController.setNavigationBarHidden(true, animated: false)
        return viewController
    }()

    private let myStepper: Tab1Stepper

    init(withStepper stepper: Tab1Stepper) {
        self.myStepper = stepper
    }

    deinit { print("\(type(of: self)): \(#function)") }

    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? CommonStep else { return FlowContributors.none }

        switch step {
        case .tab1Main:
            return navigateToMainScreen()
        case .searchPage:
            return navigateToSearchScreen()
        case .videoListPage(let keyword):
            return videoListPage(artist_name: keyword)
        case .playYoutube(let link, let list):
            return playYoutube(link: link, list: list)
        case .videoSearchResultPage(let keyword):
            return videoSearchResultPage(keyword: keyword)
        case .modalDismiss(let animated):
            return dismissScreen(type: .modal, animated: animated)
        case .pushDismiss(let animated):
            return dismissScreen(type: .push, animated: animated)
        default: return .none
        }
    }
}

// MARK: -
// MARK: 이동 (navigate)
// MARK: -
extension Tab1Flow {
    /// 메인화면 이동
    private func navigateToMainScreen() -> FlowContributors {
        let controller = ChartMainViewController.instantiate()
        let reactor = ChartMainViewReactor()
        controller.reactor = reactor

        self.rootViewController.setViewControllers([controller], animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))
    }
    private func playYoutube(link: String, list: [ArtistVideoListModel]) -> FlowContributors {
        let controller = CommonYTPlayerViewController.instantiate()
        let reactor = CommonYTPlayerViewReactor(withLink: link, list: list)
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func navigateToSearchScreen() -> FlowContributors {
        let controller = SearchVideoListViewController.instantiate()
        let reactor = SearchVideoListViewReactor()
        controller.reactor = reactor

        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func videoListPage(artist_name: String) -> FlowContributors {
        let controller = CommonVideoListViewController.instantiate()
        let reactor = CommonVideoListViewReactor(name: artist_name)
        controller.reactor = reactor
        
        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }
    private func videoSearchResultPage(keyword: String) ->FlowContributors {
        let controller = CommonVideoListViewController.instantiate()
        let reactor = CommonVideoListViewReactor(keyword: keyword)
        controller.reactor = reactor
        
        self.rootViewController.pushViewController(controller, animated: true)
        return .one(flowContributor: .contribute(withNextPresentable: controller, withNextStepper: reactor))

    }

}

// MARK: 닫기 (dismiss, pop)
private extension Tab1Flow {
    func dismissScreen(type: NavigationType = .push, animated: Bool = true) -> FlowContributors {
        switch type {
        case .push:
            if self.rootViewController.viewControllers.count == 1 {
                return dismissScreen(type: .modal, animated: true)
            }
            self.rootViewController.popViewController(animated: animated)
        case .modal:
            self.rootViewController.dismiss(animated: animated, completion: {
            })
        }
        return .none
    }
}

class Tab1Stepper: Stepper {
    var steps = PublishRelay<Step>()
    var step: CommonStep?
    convenience init(step: CommonStep) {
        self.init()
        self.step = step
    }
    var initialStep: Step {
        if let step = self.step {
            return step
        }
        return CommonStep.tab1Main
    }
}

