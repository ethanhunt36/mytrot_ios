//
//  VideoMainPagerViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
import XLPagerTabStrip
import ReactorKit
import Reusable
protocol VideoMainPagerDelegate: AnyObject {
    func moveToCommonVideoList(artist_name: String)
    func moveToYoutubePlayerPage(link: String, list: [ArtistVideoListModel])

}
class VideoMainPagerViewController: ButtonBarPagerTabStripViewController, StoryboardView , StoryboardBased, HasPreferencesService{
    deinit { print("deinit \(type(of: self)): \(#function)") }

    var disposeBag = DisposeBag()
    var preferencesService = PreferencesService()

    @IBOutlet weak var searchButton: UIButton! {
        didSet {
            searchButton.layer.cornerRadius = 5.0
            searchButton.layer.masksToBounds = true
        }
    }

    func bind(reactor: VideoMainPagerViewReactor) {
        bindAction(reactor)
        bindState(reactor)

    }
    

    lazy var viewHasSinger: Bool = false
    
    
    override func viewDidLoad() {
        // change selected bar color
        print("HomeMainPagerViewController viewDidLoad")
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = Asset.custard.color
        settings.style.buttonBarItemFont =  UIFont.boldSystemFont(ofSize: 16)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true

        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = Asset.bottomTabDisabled.color
            newCell?.label.textColor = Asset.black.color
        }
        super.viewDidLoad()
        initialize()
    }

    // MARK: - PagerTabStripDataSource

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        print("pagerTabStripController")
        if let reactor = reactor {
            let hasSinger: Bool = reactor.currentState.favoriteSingerName != ""
            viewHasSinger = hasSinger
            if hasSinger == true {
                let videoMySingerListViewController = VideoMySingerListViewController.instantiate()
                let videoMySingerListViewReactor = VideoMySingerListViewReactor()
                videoMySingerListViewController.reactor = videoMySingerListViewReactor
                videoMySingerListViewController.itemInfo = IndicatorInfo(title: "최애 영상")
                videoMySingerListViewController.pagerDelegate = self

                let videoListViewController = VideoListViewController.instantiate()
                let videoListViewReactor = VideoListViewReactor()
                videoListViewController.reactor = videoListViewReactor
                videoListViewController.itemInfo = IndicatorInfo(title: "전체 영상")
                videoListViewController.pagerDelegate = self

                let videoSingerListViewController = VideoSingerListViewController.instantiate()
                let videoSingerListViewReactor = VideoSingerListViewReactor()
                videoSingerListViewController.reactor = videoSingerListViewReactor
                videoSingerListViewController.itemInfo = IndicatorInfo(title: "가수별 영상")
                videoSingerListViewController.pagerDelegate = self
                return [videoMySingerListViewController, videoListViewController, videoSingerListViewController]

            } else {
                let videoListViewController = VideoListViewController.instantiate()
                let videoListViewReactor = VideoListViewReactor()
                videoListViewController.reactor = videoListViewReactor
                videoListViewController.itemInfo = IndicatorInfo(title: "전체 영상")
                videoListViewController.pagerDelegate = self

                let videoSingerListViewController = VideoSingerListViewController.instantiate()
                let videoSingerListViewReactor = VideoSingerListViewReactor()
                videoSingerListViewController.reactor = videoSingerListViewReactor
                videoSingerListViewController.itemInfo = IndicatorInfo(title: "가수별 영상")
                videoSingerListViewController.pagerDelegate = self
                return [videoListViewController, videoSingerListViewController]

            }
        } else {
            let videoListViewController = VideoListViewController.instantiate()
            let videoListViewReactor = VideoListViewReactor()
            videoListViewController.reactor = videoListViewReactor
            videoListViewController.itemInfo = IndicatorInfo(title: "전체 영상")
            videoListViewController.pagerDelegate = self

            let videoSingerListViewController = VideoSingerListViewController.instantiate()
            let videoSingerListViewReactor = VideoSingerListViewReactor()
            videoSingerListViewController.reactor = videoSingerListViewReactor
            videoSingerListViewController.itemInfo = IndicatorInfo(title: "가수별 영상")
            videoSingerListViewController.pagerDelegate = self
            return [videoListViewController, videoSingerListViewController]

        }
        
    }

    // MARK: - Actions

}

// MARK: -
// MARK: bindAction For Reactor
private extension VideoMainPagerViewController {
    func bindAction(_ reactor: VideoMainPagerViewReactor) {

        searchButton.rx.tap
            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .map { Reactor.Action.moveToSearch }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        
    }
}

// MARK: bindState For Reactor
private extension VideoMainPagerViewController {
    func bindState(_ reactor: VideoMainPagerViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        
    }
}

// MARK: -
// MARK: private initialize
private extension VideoMainPagerViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {}
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.VideoTabRefresh.self, sticky: true).subscribe { [weak self] event in

            let hasSinger: Bool = User.shared.myInfo?.artist_name ?? "" != ""
            if self?.viewHasSinger != hasSinger {
                delay(delay: 0.01, closure: { [weak self] in
                    self?.commandReactorActionPage()
                })
            }

        }.disposed(by: disposeBag)

    }
    
    func commandReactorActionDismiss() {
        print("commandReactorActionDismiss")
        guard let reactor = reactor else { return }

        Observable.just(Reactor.Action.dismiss)
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
    }
    func commandReactorActionPage() {
        print("commandReactorActionTwoPage")
        guard let reactor = reactor else { return }

        Observable.just(Reactor.Action.moveToPager)
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)
    }

}

extension VideoMainPagerViewController: VideoMainPagerDelegate {
    func moveToCommonVideoList(artist_name: String) {
        guard let reactor = reactor else { return }
        reactor.action.onNext(.moveToStepScreen(.videoListPage(keyword: artist_name)))
    }
    
    func moveToYoutubePlayerPage(link: String, list: [ArtistVideoListModel]) {
        guard let reactor = reactor else { return }

        reactor.action.onNext(.moveToStepScreen(CommonStep.playYoutube(link: link, list: list)))

    }

}
