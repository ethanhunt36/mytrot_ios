//
//  VideoMySingerListViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class VideoMySingerListViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(Step)

        case getVideoList(type: VideoListType, last_no: String)
        case addBookmark(artist_no: Int64, vod_no: Int64)
        case removeBookmark(artist_no: Int64, vod_no: Int64)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponse(ResponseArtistVideoList)
        case setType(VideoListType)
        case setLastNo(String)
        case setArtistNo(Int64)
        case setAddBookMarkingStatus(BookMarkingStatus, vod_no: Int64)
        case setRemoveBookMarkingStatus(BookMarkingStatus, vod_no: Int64)
        case setAddtionalList(Bool)

    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var type: VideoListType
        var last_no: String
        var artist_no: Int64
        var items: [ArtistVideoListModel]
        var headerModel: VideoHeaderDataModel
        var bookmarkingStatus: BookMarkingStatus
        var isAddtionalList: Bool
        var artist_name: String

    }
    
    /// 초기 상태값
    let initialState: State
    private let networkService: MyTrotServiceType
    var preferencesService: PreferencesService
    
    init() {
        self.networkService = MyTrotService()
        self.preferencesService = PreferencesService()
        self.initialState = State(
            isDismiss: false,
            type: .recent,
            last_no: "",
            artist_no: User.shared.myInfo?.artist_no ?? 0,
            items: [],
            headerModel: VideoHeaderDataModel(viewListType: VideoListType.recent.rawValue),
            bookmarkingStatus: .none,
            isAddtionalList: false,
            artist_name: User.shared.myInfo?.artist_name ?? ""

        )

    }

    func mutate(action: VideoMySingerListViewReactor.Action) -> Observable<VideoMySingerListViewReactor.Mutation> {
        switch action {
        case .removeBookmark(let artist_no, let vod_no):
            if artist_no == 0, vod_no == 0 {
                return .just(.setRemoveBookMarkingStatus(.none, vod_no: 0))
            }
            return .concat([
                .just(.setRemoveBookMarkingStatus(.processing, vod_no: 0)),
                networkService.removeBookmark(artist_no: artist_no, vodNo: vod_no).asObservable().map { response in
                    if response.isResponseStatusSuccessful() {
                        RxBus.shared.post(event: Events.RemoveBookMark(no: vod_no), sticky: true)

                        return .setRemoveBookMarkingStatus(.doneSuccess, vod_no: vod_no)
                    } else {
                        return .setRemoveBookMarkingStatus(.doneFail, vod_no: 0)
                    }
                    
                }
            ])

        case .addBookmark(let artist_no, let vod_no):
            if artist_no == 0, vod_no == 0 {
                return .just(.setAddBookMarkingStatus(.none, vod_no: 0))
            }
            return .concat([
                .just(.setAddBookMarkingStatus(.processing, vod_no: 0)),
                networkService.addBookmark(artist_no: artist_no, vodNo: vod_no).asObservable().map { response in
                    if response.isResponseStatusSuccessful() {
                        RxBus.shared.post(event: Events.AddBookMark(no: vod_no), sticky: true)
                        return .setAddBookMarkingStatus(.doneSuccess, vod_no: vod_no)
                    } else {
                        return .setAddBookMarkingStatus(.doneFail, vod_no: 0)
                    }
                    
                }
            ])

        case .getVideoList(let type, let last_no):
            // isAddtionalList
            if last_no == "" {
                return .concat([
                    .just(.setType(type)),
                    .just(.setLastNo(last_no)),
                    .just(.setAddtionalList(false)),
                    networkService.vodList(type: type, last_no: last_no, artist_no: currentState.artist_no, artist_name: currentState.artist_name, keyword: "").asObservable().map{ Mutation.setResponse($0)}
                ])

            } else {
                return .concat([
                    .just(.setType(type)),
                    .just(.setLastNo(last_no)),
                    .just(.setAddtionalList(true)),
                    networkService.vodList(type: type, last_no: last_no, artist_no: currentState.artist_no, artist_name: currentState.artist_name, keyword: "").asObservable().map{ Mutation.setResponse($0)}
                ])

            }
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: VideoMySingerListViewReactor.State, mutation: VideoMySingerListViewReactor.Mutation) -> VideoMySingerListViewReactor.State {
        var state = state
        switch mutation {
        case .setResponse(let response):
            if response.isResponseStatusSuccessful() {
                state.items = response.data?.list_data ?? []
                
                // alert
                var sectionList: [ArtistVideoListModel] = []

                if let data: ArtistVideoModel = response.data {
                    if let listdata = data.list_data {
                        for var listOne in listdata {
                            if sectionList.count == 0 {
                                listOne.videoHeaderModel = currentState.headerModel
                                sectionList.append(listOne)
                            }
                            sectionList.append(listOne)
                        }

                    }
                }
                state.last_no = response.data?.last_no ?? ""
                state.items = sectionList

            }
        case .setType(let type):
            state.headerModel.viewListType = type.rawValue
            state.type = type
        case .setLastNo(let lastNo):
            state.last_no = lastNo
        case .setArtistNo(let artistNo):
            state.artist_no = artistNo
        case .setRemoveBookMarkingStatus(let status, let vod_no):
            var tempList: [ArtistVideoListModel] = []
            tempList.append(contentsOf: state.items)
            state.items.removeAll()
            for var item in tempList {
                if vod_no == item.no {
                    
                    item.is_bookmark = 0
                }
                state.items.append(item)
            }
            
            state.bookmarkingStatus = status

        case .setAddBookMarkingStatus(let status, let vod_no):
            
            var tempList: [ArtistVideoListModel] = []
            tempList.append(contentsOf: state.items)
            state.items.removeAll()
            for var item in tempList {
                if vod_no == item.no {
                    
                    item.is_bookmark = 1
                }
                state.items.append(item)
            }
            
            state.bookmarkingStatus = status
        case .setAddtionalList(let flag):
            state.isAddtionalList = flag

        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
