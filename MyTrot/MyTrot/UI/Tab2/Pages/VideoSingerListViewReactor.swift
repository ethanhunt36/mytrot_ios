//
//  VideoSingerListViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class VideoSingerListViewReactor: BaseReactor {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        
        case getSingerList
        case moveToStepScreen(CommonStep)

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
        case setResponseArtistVideoListBySinger(ResponseArtistVideoListBySinger)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var items: [ArtistVideoBySingerDataListModel]
    }
    
    /// 초기 상태값
    let initialState: State
    let networkService = MyTrotService()

    init() {
        self.initialState = State(
            isDismiss: false,
            items: []
        )
    }
    
    func mutate(action: VideoSingerListViewReactor.Action) -> Observable<VideoSingerListViewReactor.Mutation> {
        switch action {
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()

        case .getSingerList:
            return networkService.artistVideoListBySinger().asObservable().map { Mutation.setResponseArtistVideoListBySinger($0)}
        case .dismiss:
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: VideoSingerListViewReactor.State, mutation: VideoSingerListViewReactor.Mutation) -> VideoSingerListViewReactor.State {
        var state = state
        switch mutation {
        case .setResponseArtistVideoListBySinger(let response):
            if response.isResponseStatusSuccessful() {
                state.items = response.data?.list_data ?? []
            }
        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
