//
//  VideoSingerListViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation

import UIKit
import ReactorKit
import XLPagerTabStrip

final class VideoSingerListViewController: BaseViewController, StoryboardView {
    deinit { print("\(type(of: self)): \(#function)") }
    var disposeBag = DisposeBag()
    var itemInfo = IndicatorInfo(title: "View")

    weak var pagerDelegate: VideoMainPagerDelegate?
    //    /// 뒤로가기 (푸시)
    //    @IBOutlet weak var backButton: UIButton!
    //    /// 뒤로가기 (모달)
    //    @IBOutlet weak var backModalButton: UIButton!
    //
    //    /// 상단 타이틀 라벨
    //    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var tableViewList: [ArtistVideoBySingerDataListModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ViewController 초기 설정
        initialize()
    }
    
//    lazy var headerView: UIView = UIView(frame: .zero)
    func bind(reactor: VideoSingerListViewReactor) {
        bindAction(reactor)
        bindState(reactor)
    }
}

// MARK: -
// MARK: bindAction For Reactor
private extension VideoSingerListViewController {
    func bindAction(_ reactor: VideoSingerListViewReactor) {
        //        backButton.rx.tap
        //            .throttle(RxTimeInterval.seconds(1), latest: false, scheduler: MainScheduler.instance)
        //            .map { Reactor.Action.dismiss }
        //            .bind(to: reactor.action)
        //            .disposed(by: disposeBag)
        
        reactor.action.onNext(.getSingerList)
    }
}

// MARK: bindState For Reactor
private extension VideoSingerListViewController {
    func bindState(_ reactor: VideoSingerListViewReactor) {
        reactor.state.map { $0.isDismiss }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isDismiss in
                guard let self = self else { return }
                if isDismiss == true {
                    self.disposeBag = DisposeBag()
                }
            }).disposed(by: disposeBag)
        reactor.state.map { $0.items }
            .subscribe(onNext: { [weak self] items in
                self?.tableViewList.removeAll()
                self?.tableViewList.append(contentsOf: items)
                self?.tableView.reloadData()

            }).disposed(by: disposeBag)

    }
}

// MARK: -
// MARK: private initialize
private extension VideoSingerListViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
        initializeTableView()
    }
    
    /// 레이아웃 초기 설정
    func initializeLayout() {
//        let etcLabel = UILabel()
//        let trotLabel = UILabel()
//        let entLabel = UILabel()
//        let imgLine = UIImageView()
//        headerView.addSubview(etcLabel)
//        headerView.addSubview(entLabel)
//        headerView.addSubview(trotLabel)
//        headerView.addSubview(imgLine)
//        imgLine.translatesAutoresizingMaskIntoConstraints = false
//        etcLabel.translatesAutoresizingMaskIntoConstraints = false
//        trotLabel.translatesAutoresizingMaskIntoConstraints = false
//        entLabel.translatesAutoresizingMaskIntoConstraints = false
//        headerView.backgroundColor = UIColor.white
////        headerView.alignInsideSuperview(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), edges: [.all])
//        headerView.widthAnchor.constraint(equalToConstant: tableView.frame.width).isActive = true
//
//        headerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
//        etcLabel.alignInsideSuperview(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10), edges: [.top,.bottom,.right])
//        trotLabel.alignInsideSuperview(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 120), edges: [.top,.bottom,.right])
//        entLabel.alignInsideSuperview(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 65), edges: [.top,.bottom,.right])
//        imgLine.alignInsideSuperview(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 65), edges: [.left,.bottom,.right])
//        imgLine.do {
//            $0.backgroundColor = Asset.normalGrey.color
//            $0.heightAnchor.constraint(equalToConstant: 1).isActive = true
//        }
//        etcLabel.text = "기타"
//        etcLabel.do {
//            $0.text = "기타"
//            $0.font = UIFont.systemFont(ofSize: 16)
//            $0.textColor = Asset.black.color
//            $0.widthAnchor.constraint(equalToConstant: 50).isActive = true
//            $0.textAlignment = .center
//        }
//        trotLabel.do {
//            $0.text = "노래"
//            $0.font = UIFont.systemFont(ofSize: 16)
//            $0.textColor = Asset.black.color
//            $0.widthAnchor.constraint(equalToConstant: 50).isActive = true
//            $0.textAlignment = .center
//        }
//        entLabel.do {
//            $0.text = "예능"
//            $0.font = UIFont.systemFont(ofSize: 16)
//            $0.textColor = Asset.black.color
//            $0.widthAnchor.constraint(equalToConstant: 50).isActive = true
//            $0.textAlignment = .center
//        }

    }
    
    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {
    }
}


// MARK: - Reactor Action
private extension VideoSingerListViewController {
    //    /// Reactor Action, <# 액션명 #>
    //    func commandReactorAction<# 액션명 #>() {
    //        guard let reactor = reactor else { return }
    //
    //        Observable.just(Reactor.Action.<# 액션명 #>)
    //            .observeOn(MainScheduler.asyncInstance)
    //            .bind(to: reactor.action)
    //            .disposed(by: disposeBag)
    //    }
}

// MARK: -
// MARK: - IndicatorInfoProvider
extension VideoSingerListViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}

extension VideoSingerListViewController {
    func initializeTableView() {
        
        self.tableView.register(cellType: VideoSingerCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
}
// MARK: -
// MARK: UITableViewDelegate
extension VideoSingerListViewController: UITableViewDataSource, UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
//    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
//        return 0
//    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView: VideoSingerHeaderCell = VideoSingerHeaderCell.loadFromNib()
            return headerView
        }
        return UIView()
        
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 0
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("didSelectRowAt")
        let cellItem = tableViewList[indexPath.row]
        if let delegate = pagerDelegate {
            delegate.moveToCommonVideoList(artist_name: cellItem.name)
        }

    }

    func tableView(_ tableView: UITableView,
                    numberOfRowsInSection section: Int) -> Int {
        return self.tableViewList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as VideoSingerCell
        cell.item = self.tableViewList[indexPath.row]
        return cell
    }
}
