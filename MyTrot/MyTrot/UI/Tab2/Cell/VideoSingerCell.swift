//
//  VideoSingerCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/25.
//

import Foundation
import UIKit
import Kingfisher
class VideoSingerCell: BaseTableViewCell {

    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    @IBOutlet weak var singerLabel: UILabel!
    @IBOutlet weak var songCountLabel: UILabel!
    @IBOutlet weak var entCountLabel: UILabel!
    @IBOutlet weak var etcCountLabel: UILabel!
    @IBOutlet weak var singerImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    var item: ArtistVideoBySingerDataListModel? {
        didSet {
            guard let cellItem = item else { return }
            singerLabel.text = cellItem.name
            songCountLabel.text = "\(cellItem.vod_tro_cnt.commaString)"
            entCountLabel.text = "\(cellItem.vod_ent_cnt.commaString)"
            etcCountLabel.text = "\(cellItem.vod_etc_cnt.commaString)"
            singerImageView.kf.setImage(with: URL(string: cellItem.pic.validateHostImageUrl))

        }
    }
}
