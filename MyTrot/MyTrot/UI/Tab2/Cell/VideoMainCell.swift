//
//  VideoMainCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/23.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift

protocol VideoMainCellDelegate: AnyObject {
    func moveToYoutube(link: String)
    func bookMarking(model: ArtistVideoListModel)
}
class VideoMainCell: BaseTableViewCell {
    var disposeBag = DisposeBag()

    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    weak var delegate: VideoMainCellDelegate?
    @IBOutlet weak var categoryLabel: UILabel!{
        didSet {
            categoryLabel.layer.cornerRadius = Define.cornerRadius
            categoryLabel.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playCountLabel: UILabel!
    @IBOutlet weak var myPlayCountLabel: UILabel!
    @IBOutlet weak var myFavButton: UIButton!
    @IBOutlet weak var youtubeButton: UIButton! {
        didSet {
            youtubeButton.layer.borderWidth = 1.0
            youtubeButton.layer.borderColor = Asset.black.color.cgColor
        }
    }
    @IBOutlet weak var youtubeImageView: UIImageView!
    @IBOutlet weak var youtubeTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    var myPlayCount: Int? {
        didSet {
            myPlayCountLabel.text = "My 재생 : \(myPlayCount?.commaString ?? "0")"

        }
    }
    func initializeEvent() {

        youtubeButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.delegate?.moveToYoutube(link: self?.item?.youtube_id ?? "")
            }).disposed(by: disposeBag)
        myFavButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let item = self?.item else { return }
                self?.delegate?.bookMarking(model: item)
            }).disposed(by: disposeBag)

    }
    var item: ArtistVideoListModel? {
        didSet {
            guard let cellItem = item else { return }
            titleLabel.text = cellItem.title.htmlToString
            playCountLabel.text = "총 재생 : \(cellItem.play_cnt.commaString)"
            myPlayCountLabel.text = "My 재생 : \(cellItem.my_play_cnt.commaString)"
            switch cellItem.gb {
            case "TRO":
                categoryLabel.text = "트롯"
                categoryLabel.textColor = Asset.purpley.color
                categoryLabel.backgroundColor = Asset.lightPurpley.color
            default:
                categoryLabel.text = "예능"
                categoryLabel.textColor = Asset.tangerine.color
                categoryLabel.backgroundColor = Asset.lightTangerine.color
            }
            let vodMin = cellItem.vod_min < 10 ? "0\(cellItem.vod_min)" : "\(cellItem.vod_min)"
            let vodSec = cellItem.vod_sec < 10 ? "0\(cellItem.vod_sec)" : "\(cellItem.vod_sec)"

            youtubeTimeLabel.text = "\(vodMin):\(vodSec)"
            youtubeImageView.kf.setImage(with: URL(string: cellItem.img01.validateHostImageUrl))
            print("ArtistVideoListModel title: \(cellItem.title)")
            print("ArtistVideoListModel is_bookmark: \(cellItem.is_bookmark)")
            print("ArtistVideoListModel no: \(cellItem.no)")
            if cellItem.is_bookmark == 0 {
                myFavButton.setImage(Asset.btnHeartOff.image, for: .normal)
            } else {
                myFavButton.setImage(Asset.btnHeartOn.image, for: .normal)
            }
        }
    }
}
