//
//  VideoMainHeaderCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/23.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
protocol VideoMainHeaderCellDelegate: AnyObject {
    func sortList(type: VideoListType)
}
class VideoMainHeaderCell: BaseTableViewCell {
    enum Define {
        static let cornerRadius: CGFloat = 5.0
    }
    var disposeBag = DisposeBag()
    weak var delegate: VideoMainHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeEvent()
    }

    @IBOutlet weak var recentButton: UIButton! {
        didSet {
            recentButton.customCornerRadius = 10.0
            recentButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            recentButton.setBackgroundColor(UIColor.clear, for: .normal)
            recentButton.setTitleColor(Asset.tangerine.color, for: .normal)
            recentButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var titleButton: UIButton! {
        didSet {
            titleButton.customCornerRadius = 10.0

            titleButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            titleButton.setBackgroundColor(UIColor.clear, for: .normal)
            titleButton.setTitleColor(Asset.tangerine.color, for: .normal)
            titleButton.setTitleColor(Asset.tangerine.color, for: .selected)

        }
    }
    @IBOutlet weak var playButton: UIButton! {
        didSet {
            playButton.customCornerRadius = 10.0

            playButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            playButton.setBackgroundColor(UIColor.clear, for: .normal)
            playButton.setTitleColor(Asset.tangerine.color, for: .normal)
            playButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }
    @IBOutlet weak var recommandButton: UIButton! {
        didSet {
            recommandButton.customCornerRadius = 10.0

            recommandButton.setBackgroundColor(Asset.lightTangerine.color, for: .selected)
            recommandButton.setBackgroundColor(UIColor.clear, for: .normal)
            recommandButton.setTitleColor(Asset.tangerine.color, for: .normal)
            recommandButton.setTitleColor(Asset.tangerine.color, for: .selected)
        }
    }

    var item: ArtistVideoListModel? {
        didSet {
            guard let cellItem = item else { return }
            guard let cellModel = cellItem.videoHeaderModel else { return }
            titleButton.isSelected = false
            recentButton.isSelected = false
            playButton.isSelected = false
            recommandButton.isSelected = false
            switch cellModel.viewListType {
            case VideoListType.title.rawValue:
                titleButton.isSelected = true
            case VideoListType.recent.rawValue:
                recentButton.isSelected = true
            case VideoListType.play.rawValue:
                playButton.isSelected = true
            case VideoListType.editor_score.rawValue:
                recommandButton.isSelected = true
            default:
                break;
            }
        }
    }
    
    func initializeEvent() {
        recentButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.videoHeaderModel?.viewListType = VideoListType.recent.rawValue
                self?.delegate?.sortList(type: .recent)
            }).disposed(by: disposeBag)
        titleButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.videoHeaderModel?.viewListType = VideoListType.title.rawValue
                self?.delegate?.sortList(type: .title)
            }).disposed(by: disposeBag)
        playButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.videoHeaderModel?.viewListType = VideoListType.play.rawValue
                self?.delegate?.sortList(type: .play)
            }).disposed(by: disposeBag)
        recommandButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.item?.videoHeaderModel?.viewListType = VideoListType.editor_score.rawValue
                self?.delegate?.sortList(type: .editor_score)
            }).disposed(by: disposeBag)

    }
}
