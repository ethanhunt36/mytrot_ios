//
//  VideoMainPagerViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
import RxRelay
import RxFlow
import RxSwift

final class VideoMainPagerViewReactor: BaseReactor, HasPreferencesService {
    /// RxFlow Step
    var steps = PublishRelay<Step>()
    var preferencesService = PreferencesService()
    
    /// action은 사용자의 동작을 나타냅니다.
    enum Action {
        /// 닫기, 뒤로가기
        case dismiss
        /// 다른 블록 화면으로 이동하기
        case moveToScreen(AppStep)
        case moveToStepScreen(CommonStep)

        case moveToPager
        case moveToSearch

    }
    
    /// mutation은 상태 변화를 나타냅니다.
    enum Mutation {
        /// 닫기, 뒤로가기
        case setDismiss(Bool)
    }
    
    /// 상태 스트림 입니다. 이 상태를 사용하여 상태 변경을 관찰합니다.
    struct State {
        var isDismiss: Bool
        var favoriteSingerName: String
    }
    
    /// 초기 상태값
    let initialState: State
    
    init() {
        self.initialState = State(
            isDismiss: false,
            favoriteSingerName: User.shared.myInfo?.artist_name ?? ""
        )
    }
    
    func mutate(action: VideoMainPagerViewReactor.Action) -> Observable<VideoMainPagerViewReactor.Mutation> {
        switch action {
        case .moveToStepScreen(let step):
            navigate(step: step)
            return .empty()

        case .moveToSearch:
            navigate(step: CommonStep.searchPage)
            return .empty()

        case .moveToPager:
            print("moveToPager")
            self.navigate(step: CommonStep.mainWithSinger)
            return .empty()

        case .dismiss:
            self.navigate(step: CommonStep.pushDismiss(animated: false))
            return Observable.just(Mutation.setDismiss(true))
        case .moveToScreen(let step):
            AppStepper.navigate(to: step)
            return .empty()
        }
    }
    
    func reduce(state: VideoMainPagerViewReactor.State, mutation: VideoMainPagerViewReactor.Mutation) -> VideoMainPagerViewReactor.State {
        var state = state
        switch mutation {
        case .setDismiss(let flag):
            state.isDismiss = flag
        }
        return state
    }
}
