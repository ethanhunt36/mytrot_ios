//
//  AppFlow.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
import RxFlow
class AppFlow: Flow {
    var root: Presentable {
        return self.rootWindow
    }

    private let rootWindow: UIWindow
    private var currentPresentedViewController: UIViewController? {
        if let presentedController = self.rootWindow.rootViewController {
            return topViewController(base: presentedController)
        } else {
            return self.rootWindow.rootViewController
        }
    }

    private func topViewController(base: UIViewController?) -> UIViewController? {
        if let navigationController = base as? UINavigationController {
            return topViewController(base: navigationController.visibleViewController)
        }

        if let tabBarController = base as? UITabBarController, let selectedController = tabBarController.selectedViewController {
            return topViewController(base: selectedController)
        }

        if let presentedController = base?.presentedViewController {
            return topViewController(base: presentedController)
        }
        return base
    }

    init(withWindow window: UIWindow) {
        self.rootWindow = window
    }
    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? AppStep else { return .none }
        switch step {
        case .splashScreen:
            return navigationToSplashScreen()
        case .mainScreen:
            return navigateToMainScreen()
        }
    }
    private func navigationToSplashScreen() -> FlowContributors {
        let stepper = SplashStepper()
        let flow = SplashFlow(withStepper: stepper)
        let isNotMakedKeyAndVisible = self.rootWindow.rootViewController == nil
        
        Flows.use(flow, when: .ready) { [unowned self] root in            DispatchQueue.main.async {
                self.rootWindow.rootViewController = root
                if isNotMakedKeyAndVisible {
                    self.rootWindow.makeKeyAndVisible()
                }
            }
        }
        return .one(flowContributor: .contribute(withNextPresentable: flow, withNextStepper: stepper))

    }
    private func navigateToMainScreen() -> FlowContributors {
        let mainboardStepper = MyTrotStepper()
        let mainboardFlow = MyTrotFlow(withStepper: mainboardStepper)
        let isNotMakedKeyAndVisible = self.rootWindow.rootViewController == nil

        Flows.use(mainboardFlow, when: .ready) { [unowned self] root in            self.rootWindow.rootViewController = root
            if isNotMakedKeyAndVisible {
                self.rootWindow.makeKeyAndVisible()
                
            }
            
        }
        return .one(flowContributor: .contribute(withNextPresentable: mainboardFlow,                                            withNextStepper: mainboardStepper))

    }

}
