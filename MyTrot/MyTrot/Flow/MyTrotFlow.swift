//
//  MyTrotFlow.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import RxFlow
import RxSwift
import RxRelay
import UIKit

final class MyTrotFlow: Flow {
    var root: Presentable {
        return self.rootViewController
    }
    let disposeBag = DisposeBag()
    let rootViewController = BaseTabBarController()
    
    private let mainboardStepper: MyTrotStepper

    init(withStepper stepper: MyTrotStepper) {
        self.mainboardStepper = stepper
    }

    deinit { print("\(type(of: self)): \(#function)") }

    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? MyTrotStep else { return FlowContributors.none }

        switch step {
        case .mainScreen:
            return navigateToMainScreen()
        }
    }

    private func navigateToMainScreen() -> FlowContributors {
        print("\(type(of: self)): \(#function)")

        let tab1Stepper = Tab1Stepper()
        let tab1Flow = Tab1Flow(withStepper: tab1Stepper)

        let tab2Stepper = Tab2Stepper()
        let tab2Flow = Tab2Flow(withStepper: tab2Stepper)

        let tab3Stepper = Tab3Stepper()
        let tab3Flow = Tab3Flow(withStepper: tab3Stepper)

        let tab4Stepper = Tab4Stepper()
        let tab4Flow = Tab4Flow(withStepper: tab4Stepper)

        let tab5Stepper = Tab5Stepper()
        let tab5Flow = Tab5Flow(withStepper: tab5Stepper)

        // TEXT 색상 값 설정
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16), .foregroundColor: Asset.bottomTabDisabled.color], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16), .foregroundColor: Asset.purpley.color], for: .selected)

        
        Flows.use(tab1Flow, tab2Flow, tab3Flow, tab4Flow, tab5Flow, when: .ready) { [weak self] (root1: UINavigationController, root2: UINavigationController, root3: UINavigationController, root4: UINavigationController, root5: UINavigationController) in            guard let self = self else { return }

            
            let tabBarItem1 = UITabBarItem(title: "차트", image: nil, selectedImage: nil)
            let tabBarItem2 = UITabBarItem(title: "영상", image: nil, selectedImage: nil)
            let tabBarItem3 = UITabBarItem(title: "HOME", image: nil, selectedImage: nil)
            let tabBarItem4 = UITabBarItem(title: "갤러리", image: nil, selectedImage: nil)
            let tabBarItem5 = UITabBarItem(title: "더보기", image: nil, selectedImage: nil)

            root1.tabBarItem = tabBarItem1
            root2.tabBarItem = tabBarItem2
            root3.tabBarItem = tabBarItem3
            root4.tabBarItem = tabBarItem4
            root5.tabBarItem = tabBarItem5


            // navigationBar Hidden
            root1.navigationBar.isHidden = true
            root2.navigationBar.isHidden = true
            root3.navigationBar.isHidden = true
            root4.navigationBar.isHidden = true
            root5.navigationBar.isHidden = true

            self.rootViewController.setViewControllers([root1, root2, root3, root4, root5], animated: false)

            // tabBar appearance setting

            // icon 선택 색상
            self.rootViewController.tabBar.tintColor = UIColor.purple
            // tabBar 배경 색상
            self.rootViewController.tabBar.barTintColor = .white

            
            // Title 위치 조정 (임의 조정)
            UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -10)
            self.rootViewController.selectedIndex = 2
            // tabBarController delegate
            self.rootViewController.rx.didSelect.subscribe({ [weak self] _ in
                guard let self = self else { return }
                print("TabBar SelectedIndex : \(self.rootViewController.selectedIndex)")
                let tabIndex = self.rootViewController.selectedIndex
                let preference = PreferencesService()
                switch tabIndex {
                case 0:
                    preference.setSelectedTab(tab: .tab1)
                case 1:
                    RxBus.shared.post(event: Events.VideoTabRefresh())
                    preference.setSelectedTab(tab: .tab2)
                case 2:
                    preference.setSelectedTab(tab: .tab3)
                case 3:
                    preference.setSelectedTab(tab: .tab4)
                case 4:
                    preference.setSelectedTab(tab: .tab5)
                default: break;
                }
            }).disposed(by: self.disposeBag)
            let bus = RxBus.shared

            bus.asObservable(event: Events.MainTabSelectedAction.self, sticky: true).subscribe { [weak self] event in
                guard let self = self else { return }

                if let selectedIndex = event.element?.selectedIndex {
                    self.rootViewController.selectedIndex = selectedIndex
                }
                

            }.disposed(by: self.disposeBag)

        }

        return .multiple(flowContributors: [
            .contribute(withNextPresentable: tab1Flow,
                        withNextStepper: OneStepper(withSingleStep: CommonStep.tab1Main)),
            .contribute(withNextPresentable: tab2Flow,
                        withNextStepper: OneStepper(withSingleStep: CommonStep.tab2Main)),
            .contribute(withNextPresentable: tab3Flow,
                        withNextStepper: OneStepper(withSingleStep: CommonStep.tab3Main)),
            .contribute(withNextPresentable: tab4Flow,
                        withNextStepper: OneStepper(withSingleStep: CommonStep.tab4Main)),
            .contribute(withNextPresentable: tab5Flow,
                        withNextStepper: OneStepper(withSingleStep: CommonStep.tab5Main))
        ])

    }
}

class MyTrotStepper: Stepper {
    var steps = PublishRelay<Step>()
    var step: MyTrotStep?

    convenience init(step: MyTrotStep) {
        self.init()
        self.step = step
    }

    var initialStep: Step {
        if let step = self.step {
            return step
        }
        return MyTrotStep.mainScreen
    }
}
