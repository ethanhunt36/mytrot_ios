//
//  CommonStep.swift
//  MyTrot
//
//  Created by hclim on 2021/03/18.
//

import Foundation
import RxFlow

enum CommonStep: Step {
    
    case modalDismiss(animated: Bool)
    case pushDismiss(animated: Bool)

    case popDismissRoot(animated: Bool)
    // Tab1
    /// 차트 메인
    case tab1Main
    
    /// 검색 페이지
    case searchPage
    /// 검색 결과 리스트 페이지 (영상)
    case videoSearchResultPage(keyword: String)
    

    // Tab2
    /// 영상 메인 페이지
    case tab2Main
    
    /// 최애 가수 있는경우 3개 페이지 없으면 2개
    case mainWithSinger

    case videoListPage(keyword: String)
    case videoListByArtistNo(artist_no: Int64, artist_name: String)
    // Tab3
    case tab3Main
    
    /// 알림
    case notificationPage
    /// 출석부
    case checkInPage
    /// 투표 등록 페이지
    case voteUploadPage(model: VoteDataListModel)
    /// 기부 등록 페이지
    case donateUploadPage(model: DonateDataListModel)
    /// 기부 완료 페이지
    case donateDonePage(url: String, donate_no: Int, artist_no: Int64, title: String)
    /// 기부 결과 페이지
    case donateResultPage(donate_no: Int, artist_no: Int64, title: String)

    /// 응원 등록 페이지
    case cheerUploadPage
    /// 응원 상세 페이지
    case cheerDetailPage(board_no: Int64)
    /// 미션 - 행운의 번호
    case missionLuckyNumberPage
    /// 미션 - 유머글
    case missionHumorPage(url: String)
    /// 미션 - 광고 시청 (미정)
    
    /// 신고하기
    case reportPage(boardNo: Int64)
    
    /// 블로그 이동
    case missionBlog(type: WebType, url: String)

    // Tab4
    case tab4Main
    
    /// 갤러리 등록
    case galleryUploadPage
    /// 갤러리 상세
    case galleryDetailPage(boardNo: Int64)
    /// 신고하기
//    case reportPage

    // Tab5
    case tab5Main
    
    /// 닉네임 변경
    case nickChangePage
    /// 핸드폰 인증
    case phoneAuthPage
    /// 최애가서 선택
    case favSingerSelectPage(type: FavSingerScreenType)
    /// 영상 추가요청
    case askAdditionalVideoPage
    /// 영상 문의
    case askQuestionPage
    /// 공지사항 리스트
    case noticeListPage
    /// 공지사항 리스트
    case noticeDetailPage(model: NoticeDataListModel)
    case faqDetailPage(model: FAQDataListModel)

    /// 공지사항 웹이동
    case noticeWebDetailPage(type: WebType, url: String)
    /// FAQ
    case faqPage
    /// 이용약관
    case termsPage(type: WebType, url: String)
    /// 영상보관함
    case videoStoragePage
    /// 포인트 순위
    case pointRankPage
    /// 투표권 순위
    case voteRankPage
    /// 투표권 내역 리스트
    case voteHistoryPage
    /// 포인트 내역 리스트
    case pointHistoryPage

    /// 마이트롯 인증서
    case activityCertPage(artistModel: ArtistSelectDataListModel, isYesterDay: Bool)
    
    /// ppobKi 이동
    case ppobKiPage
    
    /// 홍보 인증 리스트
    case promotionListPage
    
    /// 홍보 인증 등록
    case promotionUploadPage
    
    /// 레벨 경험치 리스트
    case levelRankListPage
    
    /// 결제 화면
    case iapPage
    
    /// Youtobe Player화면
    case playYoutube(link: String, list: [ArtistVideoListModel])
}

