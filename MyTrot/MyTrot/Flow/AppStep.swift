//
//  AppStep.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
import RxFlow
import RxRelay
import RxSwift
enum NavigationType {
    case push
    case modal
}

enum AppStep: Step {

    case splashScreen
    case mainScreen
}
class AppStepper: Stepper {
    let steps = PublishRelay<Step>()
    private let disposeBag = DisposeBag()

    var initialStep: Step {
        return AppStep.splashScreen
    }

    func navigate(step: AppStep) {
        self.steps.accept(step)
    }

    static func navigate(to step: AppStep) {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else { return }
        if delegate.appStepper != nil {
            delegate.appStepper.navigate(step: step)
        }
    }
}
