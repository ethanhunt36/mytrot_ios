//
//  MyTrotStep.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import RxFlow

enum MyTrotStep: Step {
    // MARK: 메인 화면 진입
    case mainScreen
}
