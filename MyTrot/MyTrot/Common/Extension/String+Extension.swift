//
//  String+Extension.swift
//  MyTrot
//
//  Created by hclim on 2021/03/16.
//

import Foundation
extension String {
    /** 숫자형 문자열에 3자리수 마다 콤마 넣기 Double형으로 형변환 되지 않으면 원본을 유지한다.
     ```swift let stringValue = "10005000.123456789" print(stringValue.insertComma)
     // 결과 : "10,005,000.123456789" ```
     */
    var insertComma: String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal

        // 소수점이 있는 경우 처리
        if let _ = self.range(of: ".") {
            let numberArray = self.components(separatedBy: ".")
            if numberArray.count == 1 {
                var numberString = numberArray[0]
                if numberString.isEmpty { numberString = "0" }
                guard let doubleValue = Double(numberString) else { return self }
                return numberFormatter.string(from: NSNumber(value: doubleValue)) ?? self
            } else if numberArray.count == 2 { var numberString = numberArray[0]
                if numberString.isEmpty { numberString = "0" }
                guard let doubleValue = Double(numberString) else { return self }
                return (numberFormatter.string(from: NSNumber(value: doubleValue)) ?? numberString) + ".\(numberArray[1])" }
        } else {
            guard let doubleValue = Double(self) else { return self }
            return numberFormatter.string(from: NSNumber(value: doubleValue)) ?? self }
        return self
    }
    
    var removeDateFormatStyle: String {
        if self.count > 10 {
            return String(self.prefix(10))
        }
        return self
    }

    var validateHostImageUrl: String {
        if self.hasPrefix("http") == true {
            return self
        } else {
            return "https://mytrot.s3.ap-northeast-2.amazonaws.com\(self)"
        }
    }
    
//    var removeTimeFormat: String {
//        if self.count > 10 {
//            return self.ind
//        }
//        return self
//    }
    
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    var replaceSlashString: String {
        return self.replacingOccurrences(of: "\\n", with: "\n")
    }
    var youTubeId: String? {
        let typePattern = "(?:(?:\\.be\\/|embed\\/|v\\/|\\?v=|\\&v=|\\/videos\\/)|(?:[\\w+]+#\\w\\/\\w(?:\\/[\\w]+)?\\/\\w\\/))([\\w-_]+)"
        let regex = try? NSRegularExpression(pattern: typePattern, options: .caseInsensitive)
        return regex
            .flatMap { $0.firstMatch(in: self, range: NSMakeRange(0, self.count)) }
            .flatMap { Range($0.range(at: 1), in: self) }
            .map { String(self[$0]) }
    }

}
