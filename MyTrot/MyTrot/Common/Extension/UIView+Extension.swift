//
//  UIView+Extension.swift
//  MyTrot
//
//  Created by hclim on 2021/03/21.
//

import UIKit
extension UIView {
    @IBInspectable var customCornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
        get {
            return layer.cornerRadius
        }
    }
}
extension UIView {
    @discardableResult
    final func alignInsideSuperview(insets: UIEdgeInsets = .zero, edges: UIRectEdge = .all) -> [NSLayoutConstraint] {
        guard let superview = self.superview else { return [] }
        self.translatesAutoresizingMaskIntoConstraints = false
        var constraints: [NSLayoutConstraint] = []
        if edges.contains(.top) {
            constraints.append(topAnchor.constraint(equalTo: superview.topAnchor, constant: insets.top))
        }
        if edges.contains(.left) {
            constraints.append(leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: insets.left))
        }
        if edges.contains(.right) {
            constraints.append(superview.trailingAnchor.constraint(equalTo: trailingAnchor, constant: insets.right))
        }
        if edges.contains(.bottom) {
            constraints.append(superview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: insets.bottom))
        }
        NSLayoutConstraint.activate(constraints)
        return constraints
    }
}
