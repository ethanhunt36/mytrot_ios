//
//  Double+Extension.swift
//  MyTrot
//
//  Created by hclim on 2021/07/12.
//

import Foundation
extension Double {
    var commaString: String {
        return String(self).insertComma
    }
}
