//
//  Int+Extension.swift
//  MyTrot
//
//  Created by hclim on 2021/03/16.
//

import Foundation
extension Int {
    var commaString: String {
        return String(self).insertComma
    }
}
extension Int64 {
    var commaString: String {
        return String(self).insertComma
    }
}
