//
//  Constants.swift
//  MyTrot
//
//  Created by hclim on 2021/03/15.
//

import Foundation
enum Constants {

    enum Network { }
    enum Ad { }
    
    enum AdType {}
    enum AdNextAction {}
    enum String {}
}

extension Constants.Ad {
    static let google_opening       = "ca-app-pub-9706798414570356/8557685018"
    
    static let google_interstitial  = "ca-app-pub-9706798414570356/1377188289"  
    
    static let google_reward        = "ca-app-pub-9706798414570356/5230732411"
    static let google_reward_full   = "ca-app-pub-9706798414570356/8978405739"
    
    static let unity_game_id = "3821704"
    static let cauly_app_code = "aLpq8wur"
}

extension Constants.AdType {
    static let g_full_screen  = "g_full_screen"
    static let g_reward       = "g_reward"
    static let g_reward_full  = "g_reward_full"
    static let cauly_full     = "cauly_full"
    static let wad_full       = "wad_full"
    static let UnityAds       = "UnityAds"
    static let VungleFull     = "VungleFull"
    static let VungleReward   = "VungleReward"
}

extension Constants.AdNextAction {
    static let for_default          = "for_default"
    static let for_move_ppobkki     = "for_move_ppobkki"
}

extension Constants.String {
    enum AlertMessage {
        static let agreementDeniedMessage = "약관에 미동의 하여서, 일부기능이 제한됩니다.\n 메뉴의 \"더보기\" - \"약관동의하기\" 메뉴에서 동의가 가능합니다."
        static let blockAllUserSuccess = "차단 완료되었습니다.\n더이상 상대방의 게시글이 보이지 않습니다."
        static let updateRequiredMessage = "앱스토어 필수 업데이트가 있습니다."
        static let updateSelectionMessage = "앱스토어 선택 업데이트가 있습니다."
        static let promotionValidateMessage = "내용을 필수로 입력해 주세요."
    }
    enum AppStore{
        static let appstoreUrl = "itms-apps://itunes.apple.com/kr/app/1560666425"
    }
}
extension Constants.Network {
    /// 서버환경
    enum Environment {
        static let backEndURL = "https://www.mytrot.co.kr"
        static let backDevEndURL = "http://dev.mytrot.co.kr"

    }
    enum ErrorMessage {
        static let networkError = "네트워크 연결 중 문제가 발생하였습니다\n잠시 후 다시 시도해 주세요"
        // 통신은 성공, Json 에러
        static let networkResponseError = "네트워크 연결 중 문제가 발생하였습니다\n잠시 후 다시 시도해 주세요(A99)"
        // 통신은 성공, Moya 에러
        static let networkMoyaError = "네트워크 연결 중 문제가 발생하였습니다\n잠시 후 다시 시도해 주세요(A98)"
        // 통신에러 (200이외)
        static let networkStatusCodeError = "네트워크 연결 중 문제가 발생하였습니다\n잠시 후 다시 시도해 주세요(A97)"
    }
}

