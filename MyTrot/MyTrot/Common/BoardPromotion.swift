//
//  BoardPromotion.swift
//  MyTrot
//
//  Created by hclim on 2021/07/12.
//

import Foundation
class BoardPromotion {
    static let shared = BoardPromotion()

    var promotionInfo: BoardPromoInfoDataModel? = nil {
        didSet {
            print("🤗=================================================")
            print("🤗== 공통으로 사용하는 BoardPromotion 메모리(Volatile Memory) 데이터에 BoardPromoInfoDataModel List 정보가 업데이트 되었습니다. ")
            print("🤗=================================================")

        }
    }
}
extension BoardPromotion {
    func updatePromotionInfo(data: BoardPromoInfoDataModel?) {
        // 사용자 정보 업데이트
        self.promotionInfo = data
    }
}
