//
//  SearchKeyword.swift
//  MyTrot
//
//  Created by hclim on 2021/04/30.
//

import Foundation
class SearchKeyword {
    static let shared = SearchKeyword()

    var keywords: [KeywordListDataModel]? = nil {
        didSet {
            print("🤗=================================================")
            print("🤗== 공통으로 사용하는 SearchKeyword 메모리(Volatile Memory) 데이터에 KeywordListDataModel List 정보가 업데이트 되었습니다. ")
            print("🤗=================================================")

        }
    }
}
extension SearchKeyword {
    func updateKeyword(data: [KeywordListDataModel]?) {
        // 사용자 정보 업데이트
        self.keywords = data
    }
}
