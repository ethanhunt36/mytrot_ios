//
//  InAppProducts.swift
//  MyTrot
//
//  Created by hclim on 2021/08/09.
//

import Foundation
import StoreKit

public struct IAPProducts {
    public static let store = IAPHelper(productIds: InAppProducts.shared.productIdentifiers)
}
public class InAppProducts {

    static let shared = InAppProducts()

    var productIdentifiers: Set<String> = Set<String>()
    
//    var products: Dictionary<String, SKProduct> = Dictionary< String, SKProduct>()
    var products: [SKProduct] = []

}
extension InAppProducts {
    func store() -> IAPHelper {
        return IAPHelper.init(productIds: InAppProducts.shared.productIdentifiers)
    }
    
    func addProducts(productIds: [String]) {
        for productId in productIds {
            InAppProducts.shared.productIdentifiers.insert(productId)
        }
    }
    
    func addSKProducts(sendProducts: [SKProduct]) {
        products = sendProducts
    }
}
