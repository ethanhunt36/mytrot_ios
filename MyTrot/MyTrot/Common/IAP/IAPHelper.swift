//
//  IAPHelper.swift
//  MyTrot
//
//  Created by hclim on 2021/08/09.
//

import Foundation
import StoreKit

public typealias ProductsRequestCompletionHandler = (_ success: Bool, _ products: [SKProduct]?) -> Void
public typealias ProductsPaymentCompletionHandler = (_ success: Bool, _ transaction: SKPaymentTransaction) -> Void

public class IAPHelper: NSObject  {

    private let productIdentifiers: Set<String>
    private var productsRequest: SKProductsRequest?
    private var productsRequestCompletionHandler: ProductsRequestCompletionHandler?
    private var productsPaymentCompletionHandler: ProductsPaymentCompletionHandler?

    public init(productIds: Set<String>) {
        productIdentifiers = productIds
        super.init()
        SKPaymentQueue.default().add(self)  // App Store와 지불정보를 동기화하기 위한 Observer 추가
    }
  
    // App Store Connect에서 등록한 인앱결제 상품들을 가져올 때
    public func requestProducts(_ completionHandler: @escaping ProductsRequestCompletionHandler) {
        productsRequest?.cancel()
        productsRequestCompletionHandler = completionHandler
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest!.delegate = self    // 추후 delegate 추가
        productsRequest!.start()
    }
    
    // 인앱결제 상품을 구입할 때
//    public func buyProduct(_ product: SKProduct, _ completionHandler: @escaping ProductsPaymentCompletionHandler) {
//        productsPaymentCompletionHandler = completionHandler
//        let payment = SKPayment(product: product)
//        SKPaymentQueue.default().add(payment)
//    }
    public func buyProductWithOrderId(_ product: SKProduct, order_id: String, _ completionHandler: @escaping ProductsPaymentCompletionHandler) {
        productsPaymentCompletionHandler = completionHandler
        let payment = SKMutablePayment(product: product)
        payment.applicationUsername = order_id
        SKPaymentQueue.default().add(payment)
    }

    // 구입 내역을 복원할 때
    public func restorePurchases() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}
extension IAPHelper: SKProductsRequestDelegate {
    // 인앱결제 상품 리스트를 가져온다
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("productsRequest")
        let products = response.products
        productsRequestCompletionHandler?(true, products)
        clearRequestAndHandler()
    }
    
    // 상품 리스트 가져오기 실패할 경우
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        print("request didFailWithError : \(error)")
        productsRequestCompletionHandler?(false, nil)
        clearRequestAndHandler()
    }
    
    private func clearRequestAndHandler() {
        productsRequest = nil
        productsRequestCompletionHandler = nil
    }
}

extension IAPHelper: SKPaymentTransactionObserver {
  
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                complete(transaction: transaction)
                break
            case .failed:
                fail(transaction: transaction)
                break
            case .restored:
                restore(transaction: transaction)
                break
            case .deferred, .purchasing:
                break
            @unknown default:
                fatalError()
            }
        }
    }
    
    // 구입 성공
    private func complete(transaction: SKPaymentTransaction) {
        deliverPurchaseNotificationFor(transaction: transaction)
    }
    
    // 복원 성공
    private func restore(transaction: SKPaymentTransaction) {
        deliverPurchaseNotificationFor(transaction: transaction)
    }
    
    // 구매 실패
    private func fail(transaction: SKPaymentTransaction) {
        if let transactionError = transaction.error as NSError?,
            let localizedDescription = transaction.error?.localizedDescription,
            transactionError.code != SKError.paymentCancelled.rawValue {
            print("Transaction Error: \(localizedDescription)")
        }
        SKPaymentQueue.default().finishTransaction(transaction)

        productsPaymentCompletionHandler?(false, transaction)

    }
    
    // 구매한 인앱 상품 키를 UserDefaults로 로컬에 저장
    private func deliverPurchaseNotificationFor(transaction: SKPaymentTransaction) {

        productsPaymentCompletionHandler?(true, transaction)
    }
}

extension IAPHelper {
  
    // 구매이력 영수증 가져오기 - 검증용
    public func getReceiptData() -> String? {
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                let receiptString = receiptData.base64EncodedString(options: [])
                return receiptString
            }
            catch {
                print("Couldn't read receipt data with error: " + error.localizedDescription)
                return nil
            }
        }
        return nil
    }
}
