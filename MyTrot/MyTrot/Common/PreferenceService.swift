//
//  PreferenceService.swift
//  MyTrot
//
//  Created by hclim on 2021/03/11.
//

import Foundation
protocol HasPreferencesService {
    var preferencesService: PreferencesService { get }
}

enum Keys {
    enum UserDefaults: String {
        case favSinger = "favSinger"
        case selectedTab = "selectedTab"
        case favSingerNo = "favSingerNo"
        case myTrotUUID = "myTrotUUID"
        case member_no = "member_no"
        
        case favSingerModel = "favSingerModel"
        /// 최애 가수 갤러리 캐쉬 날짜
        case singerModelCachedDate = "singerModelCachedDate"
        /// 최초 약관 동의 여부
        case isUserAgreeTerms = "isUserAgreeTerms"
        
        /// 출석 날짜 저장
        case attendanceDate = "attendanceDate"
        
        /// 차트 팝업 날짜 저장
        case chartFirstPopupDate = "chartFirstPopupDate"
        
        /// 최애 체크여부 내부 저장
        case isSelectdMyFavSinger = "isSelectdMyFavSinger"
        
        /// FCM Key
        case fcmToken = "fcmToken"
        
        /// IAP 결제 내역
        case inAppPurchaseProducts = "inAppPurchaseProducts"
        
        /// youtube정책 팝업 동의 날짜
        case youtubeAgreementDate = "youtubeAgreementDate"

        case chartGender = "chartGender"
        case voteGender = "voteGender"
        case voteType = "voteType"
    }
}
class ProductInfo: NSObject ,NSCoding {
    let receipt: String
    let mytrotOrderId: String
    let appleTransactionId: String
    init(receipt: String, mytrotOrderId: String, appleTransactionId: String ) {
        self.receipt = receipt
        self.mytrotOrderId = mytrotOrderId
        self.appleTransactionId = appleTransactionId
    }
    required init?(coder: NSCoder) {
        self.receipt = coder.decodeObject(forKey: "receipt") as! String
        self.mytrotOrderId = coder.decodeObject(forKey: "mytrotOrderId") as! String
        self.appleTransactionId = coder.decodeObject(forKey: "appleTransactionId") as! String

    }
    func encode(with coder: NSCoder) {
        coder.encode(self.receipt, forKey: "receipt")
        coder.encode(self.mytrotOrderId, forKey: "mytrotOrderId")
        coder.encode(self.appleTransactionId, forKey: "appleTransactionId")

    }

    
}
final class PreferencesService {

    private var fcmToken: String {
        set {
            _ = UserConfig.setObjectFor(key: .fcmToken, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .fcmToken) as? String ?? ""
        }
    }
    private var youtubeAgreementDate: String {
        set {
            _ = UserConfig.setObjectFor(key: .youtubeAgreementDate, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .youtubeAgreementDate) as? String ?? ""
        }
    }

    private var isSelectdMyFavSinger: Bool {
        set {
            _ = UserConfig.setObjectFor(key: .isSelectdMyFavSinger, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .isSelectdMyFavSinger) as? Bool ?? false
        }
    }

    private var isUserAgreeTerms: Bool {
        set {
            _ = UserConfig.setObjectFor(key: .isUserAgreeTerms, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .isUserAgreeTerms) as? Bool ?? false
        }
    }
    
    private var chartFirstPopupDate: String {
        set {
            _ = UserConfig.setObjectFor(key: .chartFirstPopupDate, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .chartFirstPopupDate) as? String ?? ""
        }

    }
    private var attendanceDate: String {
        set {
            _ = UserConfig.setObjectFor(key: .attendanceDate, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .attendanceDate) as? String ?? ""
        }
    }

    private var singerModelCachedDate: Int {
        set {
            _ = UserConfig.setObjectFor(key: .singerModelCachedDate, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .singerModelCachedDate) as? Int ?? 0
        }
    }

    private var favSingerModel: Data {
        set {
            _ = UserConfig.setObjectFor(key: .favSingerModel, object: newValue)
        }
        get {

            return UserConfig.objectFor(key: .favSingerModel) as? Data ?? .empty
        }
    }

    private var member_no: String {
        set {
            _ = UserConfig.setObjectFor(key: .member_no, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .member_no) as? String ?? ""
        }
    }


    private var favSingerNo: Int {
        set {
            _ = UserConfig.setObjectFor(key: .favSingerNo, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .favSingerNo) as? Int ?? 0
        }
    }
    private var myTrotUUID: String {
        set {
            _ = UserConfig.setObjectFor(key: .myTrotUUID, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .myTrotUUID) as? String ?? ""
        }
    }

    private var favSinger: String {
        set {
            _ = UserConfig.setObjectFor(key: .favSinger, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .favSinger) as? String ?? ""
        }
    }
    private var selectedTab: String {
        set {
            _ = UserConfig.setObjectFor(key: .selectedTab, object: newValue)
        }
        get {
            return UserConfig.objectFor(key: .selectedTab) as? String ?? SelectedTabType.tab3.rawValue
        }
    }

}

extension PreferencesService {
    func finishIapProduct(productInfo: ProductInfo) {
        if let achievedData = UserConfig.objectFor(key: .inAppPurchaseProducts), ((achievedData as? Data) != nil) {
            do {
                let products = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(achievedData as! Data) as! [ProductInfo]
                var newValue: [ProductInfo] = []
                for product in products {
                    if product.appleTransactionId != productInfo.appleTransactionId {
                        newValue.append(product)
                    }
                }
                let mePropertyListData = try NSKeyedArchiver.archivedData(withRootObject: newValue, requiringSecureCoding: false)
                _ = UserConfig.setObjectFor(key: .inAppPurchaseProducts, object: mePropertyListData)

            } catch {
                print(error)
            }
        }
    }
    // 결제 완료 되었지만, 아이템 미지급 된 리스트 저장 (백업용)
    func setUnPaidIAPProduct(productInfo: ProductInfo) {
        if let achievedData = UserConfig.objectFor(key: .inAppPurchaseProducts), ((achievedData as? Data) != nil) {
            do {
                var products = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(achievedData as! Data) as! [ProductInfo]
                print("setIAPProduct products :: \(products)")
                for p in products {
                    print("appleTransactionId : \(p.appleTransactionId)")
                    print("mytrotOrderId : \(p.mytrotOrderId)")
                }
                products.append(productInfo)
                
                let mePropertyListData = try NSKeyedArchiver.archivedData(withRootObject: products, requiringSecureCoding: false)
                _ = UserConfig.setObjectFor(key: .inAppPurchaseProducts, object: mePropertyListData)
            }
            catch {
                print(error)
            }

        } else {
            do {
                let mePropertyListData = try NSKeyedArchiver.archivedData(withRootObject: [productInfo], requiringSecureCoding: false)
                _ = UserConfig.setObjectFor(key: .inAppPurchaseProducts, object: mePropertyListData)
            } catch { print(error) }
        }
        
    }
    
    func getIAPProducts() -> [ProductInfo] {
        if let achievedData = UserConfig.objectFor(key: .inAppPurchaseProducts), ((achievedData as? Data) != nil) {
            do {
                let products = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(achievedData as! Data) as! [ProductInfo]
                print("getIAPProducts products :: \(products)")
                for p in products {
                    print("appleTransactionId : \(p.appleTransactionId)")
                    print("mytrotOrderId : \(p.mytrotOrderId)")
                }

                return products
            }
            catch {
                print(error)
            }

        }


        return []
    }
    func initializeUUID() {
        if getMyTrotUUID() == "" {
            self.myTrotUUID = UUID().uuidString
        }
        print("self.myTrotUUID : \(self.myTrotUUID)")
    }
    
    func getFavSingerNo() -> Int {
        return self.favSingerNo
    }
    func setFavSingerNo(singerNo: Int) {
        self.favSingerNo = singerNo
    }
    func getMyTrotUUID() -> String {
        // TEST!
        return self.myTrotUUID
    }

    func getFavSinger() -> String {
        return self.favSinger
    }
    func setFavSinger(singer: String) {
        self.favSinger = singer
    }
    func getFcmToken() -> String {
        return self.fcmToken
    }
    func setFcmToken(token: String) {
        self.fcmToken = token
    }

    func getAttendanceDate() -> String {
        return self.attendanceDate
    }
    func setAttendanceDate(attendanceDate: String) {
        self.attendanceDate = attendanceDate
    }
    func getChartFirstPopupDate() -> String {
        return self.chartFirstPopupDate
    }
    func setChartFirstPopupDate(chartFirstPopupDate: String) {
        self.chartFirstPopupDate = chartFirstPopupDate
    }

    
    func getMember_no() -> String {
        return self.member_no
    }
    func setMember_no(member_no: String) {
        self.member_no = member_no
    }
    //
    func getSelectedTab() -> SelectedTabType {
        switch self.selectedTab {
        case SelectedTabType.tab1.rawValue:
            return .tab1
        case SelectedTabType.tab2.rawValue:
            return .tab2
        case SelectedTabType.tab3.rawValue:
            return .tab3
        case SelectedTabType.tab4.rawValue:
            return .tab4
        case SelectedTabType.tab5.rawValue:
            return .tab5
        default:
            return .tab3
        }
    }
    func setSelectedTab(tab: SelectedTabType) {
        return self.selectedTab = tab.rawValue

    }

    func getFavSingerModel() -> ArtistSelectDataModel {
        // self.favSingerModel
        do {
            return try JSONDecoder().decode(ResponseArtistSelectList.self, from: self.favSingerModel).data ?? ArtistSelectDataModel(list_data: [])
            
        } catch { print("getFavSingerModel error ::: \(error)"); return ArtistSelectDataModel(list_data: []) }

    }
    func setFavSingerModel(model: Data) {
        let today = Date().timeIntervalSince1970
        self.singerModelCachedDate = Int(today)
        self.favSingerModel = model
    }
    
    func getSingerModelCachedDate() -> Int {
        
        return self.singerModelCachedDate
    }

    func getIsUserAgreeTerms() -> Bool {
        return self.isUserAgreeTerms
    }
    func setIsUserAgreeTerms(isUserAgreeTerms: Bool) {
        self.isUserAgreeTerms = isUserAgreeTerms
    }
    func getYoutubeAgreementDate() -> String {
        return self.youtubeAgreementDate
    }
    func setYoutubeAgreementDate(date: String) {
        self.youtubeAgreementDate = date
    }

    func getIsSelectdMyFavSinger() -> Bool {
        return self.isSelectdMyFavSinger
    }
    func setIsSelectdMyFavSinger(isSelectdMyFavSinger: Bool) {
        self.isSelectdMyFavSinger = isSelectdMyFavSinger
        let bus = RxBus.shared
        bus.post(event: Events.SetMyFavSingerFinish())

    }

    func removeAllSavedData() {

        // UserDefaults 삭제
        PreferencesService().removeUserDefaultsData()

    }
    /// UserDefaults 에서 초기화 해도 되는 데이터 초기화
    func removeUserDefaultsData() {
        self.favSinger = ""
        self.selectedTab = SelectedTabType.tab3.rawValue
        self.favSingerNo = 0
        self.myTrotUUID = ""
        self.member_no = ""
        
        self.favSingerModel = .empty
        /// 최애 가수 갤러리 캐쉬 날짜
        self.singerModelCachedDate = 0
        /// 최초 약관 동의 여부
        self.isUserAgreeTerms = false
        
        /// 출석 날짜 저장
        self.attendanceDate = ""
        
        /// 차트 첫팝업 날짜 저장
        self.chartFirstPopupDate = ""
        /// 최애 체크여부 내부 저장
        self.isSelectdMyFavSinger = false
        
//        /// FCM Key
//        self.fcmToken = ""
        
        /// IAP 결제 내역
//        self.inAppPurchaseProducts = "inAppPurchaseProducts"
        _ = UserConfig.setObjectFor(key: .inAppPurchaseProducts, object: .none)

        /// youtube정책 팝업 동의 날짜
        self.youtubeAgreementDate = ""

        Utils.saveVoteType(type: .weekly)
        Utils.saveVoteGender(type: .both)
        Utils.saveChartGender(type: .both)
        
    }
}
