// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal static let appleGreen = ColorAsset(name: "apple_green")
  internal static let black = ColorAsset(name: "black")
  internal static let bottomTabDisabled = ColorAsset(name: "bottom_tab_disabled")
  internal static let brownishGrey = ColorAsset(name: "brownish_grey")
  internal static let custard = ColorAsset(name: "custard")
  internal static let dark = ColorAsset(name: "dark")
  internal static let darkDeepBlue = ColorAsset(name: "dark_deep_blue")
  internal static let darkGrey = ColorAsset(name: "dark_grey")
  internal static let darkSkyBlue = ColorAsset(name: "dark_sky_blue")
  internal static let fadedRed = ColorAsset(name: "faded_red")
  internal static let fadedRedTwo = ColorAsset(name: "faded_red_two")
  internal static let indigo = ColorAsset(name: "indigo")
  internal static let lightAppleGreen = ColorAsset(name: "light_apple_green")
  internal static let lightNormalGrey = ColorAsset(name: "light_normal_grey")
  internal static let lightPink = ColorAsset(name: "light_pink")
  internal static let lightPurpley = ColorAsset(name: "light_purpley")
  internal static let lightTangerine = ColorAsset(name: "light_tangerine")
  internal static let normalGrey = ColorAsset(name: "normal_grey")
  internal static let numberBlow10 = ColorAsset(name: "numberBlow10")
  internal static let numberBlow20 = ColorAsset(name: "numberBlow20")
  internal static let numberBlow30 = ColorAsset(name: "numberBlow30")
  internal static let numberBlow40 = ColorAsset(name: "numberBlow40")
  internal static let numberBlow50 = ColorAsset(name: "numberBlow50")
  internal static let paleGrey = ColorAsset(name: "pale_grey")
  internal static let purpley = ColorAsset(name: "purpley")
  internal static let royalBlue = ColorAsset(name: "royal_blue")
  internal static let squash = ColorAsset(name: "squash")
  internal static let tangerine = ColorAsset(name: "tangerine")
  internal static let violetBlue = ColorAsset(name: "violet_blue")
  internal static let arrowNext = ImageAsset(name: "arrowNext")
  internal static let bgAppbar = ImageAsset(name: "bgAppbar")
  internal static let bgCertName = ImageAsset(name: "bgCertName")
  internal static let bgCertOutline = ImageAsset(name: "bgCertOutline")
  internal static let bgPop1 = ImageAsset(name: "bgPop1")
  internal static let bgPopFoot = ImageAsset(name: "bgPopFoot")
  internal static let bgPopTop = ImageAsset(name: "bgPopTop")
  internal static let bgPrize1Foot = ImageAsset(name: "bgPrize1Foot")
  internal static let bgPrize1Top = ImageAsset(name: "bgPrize1Top")
  internal static let bgPrize2Foot = ImageAsset(name: "bgPrize2Foot")
  internal static let bgPrize2Top = ImageAsset(name: "bgPrize2Top")
  internal static let bgPrize3Foot = ImageAsset(name: "bgPrize3Foot")
  internal static let bgPrize3Top = ImageAsset(name: "bgPrize3Top")
  internal static let bgPrize4Foot = ImageAsset(name: "bgPrize4Foot")
  internal static let bgPrize4Top = ImageAsset(name: "bgPrize4Top")
  internal static let bgPrize5Foot = ImageAsset(name: "bgPrize5Foot")
  internal static let bgPrize5Top = ImageAsset(name: "bgPrize5Top")
  internal static let bgSponsorboxBtm = ImageAsset(name: "bgSponsorboxBtm")
  internal static let bgSponsorboxTop = ImageAsset(name: "bgSponsorboxTop")
  internal static let bgStorage = ImageAsset(name: "bgStorage")
  internal static let bgTradebar = ImageAsset(name: "bgTradebar")
  internal static let bgTradebarOn = ImageAsset(name: "bgTradebarOn")
  internal static let bgTypeEnt = ImageAsset(name: "bgTypeEnt")
  internal static let bgTypeEtc = ImageAsset(name: "bgTypeEtc")
  internal static let bgTypeSing = ImageAsset(name: "bgTypeSing")
  internal static let bgTypeboxBasic = ImageAsset(name: "bgTypeboxBasic")
  internal static let bgTypeboxEnt = ImageAsset(name: "bgTypeboxEnt")
  internal static let bgTypeboxEtc = ImageAsset(name: "bgTypeboxEtc")
  internal static let bgTypeboxSing = ImageAsset(name: "bgTypeboxSing")
  internal static let btCameraSmall = ImageAsset(name: "btCameraSmall")
  internal static let btTopAlimOver = ImageAsset(name: "btTopAlimOver")
  internal static let btnCert = ImageAsset(name: "btnCert")
  internal static let btnFloating1 = ImageAsset(name: "btnFloating1")
  internal static let btnFloating2 = ImageAsset(name: "btnFloating2")
  internal static let btnFloating3 = ImageAsset(name: "btnFloating3")
  internal static let btnFloating4 = ImageAsset(name: "btnFloating4")
  internal static let btnFloating5 = ImageAsset(name: "btnFloating5")
  internal static let btnFloatingClose = ImageAsset(name: "btnFloatingClose")
  internal static let btnFloatingPlus = ImageAsset(name: "btnFloatingPlus")
  internal static let btnGray = ImageAsset(name: "btnGray")
  internal static let btnGrayOver = ImageAsset(name: "btnGrayOver")
  internal static let btnGraybox = ImageAsset(name: "btnGraybox")
  internal static let btnGrayboxOver = ImageAsset(name: "btnGrayboxOver")
  internal static let btnLogout = ImageAsset(name: "btnLogout")
  internal static let btnLogoutOver = ImageAsset(name: "btnLogoutOver")
  internal static let btnOrange = ImageAsset(name: "btnOrange")
  internal static let btnOrangeOver = ImageAsset(name: "btnOrangeOver")
  internal static let btnPause = ImageAsset(name: "btnPause")
  internal static let btnPauseOver = ImageAsset(name: "btnPauseOver")
  internal static let btnPlay = ImageAsset(name: "btnPlay")
  internal static let btnPlay1Track = ImageAsset(name: "btnPlay1Track")
  internal static let btnPlay1TrackOver = ImageAsset(name: "btnPlay1TrackOver")
  internal static let btnPlayAll = ImageAsset(name: "btnPlayAll")
  internal static let btnPlayAllOver = ImageAsset(name: "btnPlayAllOver")
  internal static let btnPlayForward = ImageAsset(name: "btnPlayForward")
  internal static let btnPlayForwardOver = ImageAsset(name: "btnPlayForwardOver")
  internal static let btnPlayFull = ImageAsset(name: "btnPlayFull")
  internal static let btnPlayFullOver = ImageAsset(name: "btnPlayFullOver")
  internal static let btnPlayNorandom = ImageAsset(name: "btnPlayNorandom")
  internal static let btnPlayNorandomOver = ImageAsset(name: "btnPlayNorandomOver")
  internal static let btnPlayNore = ImageAsset(name: "btnPlayNore")
  internal static let btnPlayNoreOver = ImageAsset(name: "btnPlayNoreOver")
  internal static let btnPlayOver = ImageAsset(name: "btnPlayOver")
  internal static let btnPlayRandom = ImageAsset(name: "btnPlayRandom")
  internal static let btnPlayRandomOver = ImageAsset(name: "btnPlayRandomOver")
  internal static let btnPlayRewind = ImageAsset(name: "btnPlayRewind")
  internal static let btnPlayRewindOver = ImageAsset(name: "btnPlayRewindOver")
  internal static let btnPlayWin = ImageAsset(name: "btnPlayWin")
  internal static let btnPlayWinOver = ImageAsset(name: "btnPlayWinOver")
  internal static let btnPopLeft = ImageAsset(name: "btnPopLeft")
  internal static let btnPopLeftOver = ImageAsset(name: "btnPopLeftOver")
  internal static let btnPopPlay = ImageAsset(name: "btnPopPlay")
  internal static let btnPopPlayOver = ImageAsset(name: "btnPopPlayOver")
  internal static let btnPopRight = ImageAsset(name: "btnPopRight")
  internal static let btnPopRightOver = ImageAsset(name: "btnPopRightOver")
  internal static let btnTradebar = ImageAsset(name: "btnTradebar")
  internal static let btnTradebarOver = ImageAsset(name: "btnTradebarOver")
  internal static let btnViolet = ImageAsset(name: "btnViolet")
  internal static let btnVioletOver = ImageAsset(name: "btnVioletOver")
  internal static let btnVioletOver2 = ImageAsset(name: "btnVioletOver_2")
  internal static let btnViolet2 = ImageAsset(name: "btnViolet_2")
  internal static let btnX = ImageAsset(name: "btnX")
  internal static let btnXOver = ImageAsset(name: "btnXOver")
  internal static let btnHeartOff = ImageAsset(name: "btn_heart_off")
  internal static let btnHeartOn = ImageAsset(name: "btn_heart_on")
  internal static let frameCertPhoto = ImageAsset(name: "frameCertPhoto")
  internal static let icAd = ImageAsset(name: "icAd")
  internal static let icAdNo = ImageAsset(name: "icAdNo")
  internal static let icCertSepstar = ImageAsset(name: "icCertSepstar")
  internal static let icCertStamp = ImageAsset(name: "icCertStamp")
  internal static let icDraw = ImageAsset(name: "icDraw")
  internal static let icDrawRank1 = ImageAsset(name: "icDrawRank1")
  internal static let icDrawRank2 = ImageAsset(name: "icDrawRank2")
  internal static let icDrawRank3 = ImageAsset(name: "icDrawRank3")
  internal static let icDrawRank4 = ImageAsset(name: "icDrawRank4")
  internal static let icDrawRank5 = ImageAsset(name: "icDrawRank5")
  internal static let icDrawWin = ImageAsset(name: "icDrawWin")
  internal static let icGood10 = ImageAsset(name: "icGood10")
  internal static let icGood100 = ImageAsset(name: "icGood100")
  internal static let icGood20 = ImageAsset(name: "icGood20")
  internal static let icGood30 = ImageAsset(name: "icGood30")
  internal static let icGood50 = ImageAsset(name: "icGood50")
  internal static let icKtalk = ImageAsset(name: "icKtalk")
  internal static let icNaver = ImageAsset(name: "icNaver")
  internal static let icScore = ImageAsset(name: "icScore")
  internal static let icShare = ImageAsset(name: "icShare")
  internal static let icSponsor = ImageAsset(name: "icSponsor")
  internal static let icSponsor2 = ImageAsset(name: "icSponsor_2")
  internal static let icTrade = ImageAsset(name: "icTrade")
  internal static let icTradeArrow = ImageAsset(name: "icTradeArrow")
  internal static let icTradeP = ImageAsset(name: "icTradeP")
  internal static let icTradeVote = ImageAsset(name: "icTradeVote")
  internal static let icVideoAdd = ImageAsset(name: "icVideoAdd")
  internal static let icoMainMoreB = ImageAsset(name: "icoMainMoreB")
  internal static let iconCommentMore = ImageAsset(name: "iconCommentMore")
  internal static let iconImgTopArrowWhite = ImageAsset(name: "iconImgTopArrowWhite")
  internal static let imgNoimg = ImageAsset(name: "imgNoimg")
  internal static let imgPopDonate1 = ImageAsset(name: "imgPopDonate1")
  internal static let imgPopDonate2 = ImageAsset(name: "imgPopDonate2")
  internal static let imgPopPoint1 = ImageAsset(name: "imgPopPoint1")
  internal static let imgPopPoint2 = ImageAsset(name: "imgPopPoint2")
  internal static let imgPopVote1 = ImageAsset(name: "imgPopVote1")
  internal static let imgPopVote2 = ImageAsset(name: "imgPopVote2")
  internal static let lineCert = ImageAsset(name: "lineCert")
  internal static let lineImg = ImageAsset(name: "lineImg")
  internal static let mytrotIntro = ImageAsset(name: "mytrotIntro")
  internal static let popLogo = ImageAsset(name: "popLogo")
  internal static let popLogoUtube = ImageAsset(name: "popLogoUtube")
  internal static let splash = ImageAsset(name: "splash")
  internal static let titCert = ImageAsset(name: "titCert")
  internal static let txtDraw = ImageAsset(name: "txtDraw")
  internal static let frameLoading01 = ImageAsset(name: "frame_loading_01")
  internal static let frameLoading02 = ImageAsset(name: "frame_loading_02")
  internal static let frameLoading03 = ImageAsset(name: "frame_loading_03")
  internal static let frameLoading04 = ImageAsset(name: "frame_loading_04")
  internal static let frameLoading05 = ImageAsset(name: "frame_loading_05")
  internal static let frameLoading06 = ImageAsset(name: "frame_loading_06")
  internal static let frameLoading07 = ImageAsset(name: "frame_loading_07")
  internal static let frameLoading08 = ImageAsset(name: "frame_loading_08")
  internal static let frameLoading09 = ImageAsset(name: "frame_loading_09")
  internal static let frameLoading10 = ImageAsset(name: "frame_loading_10")
  internal static let frameLoading11 = ImageAsset(name: "frame_loading_11")
  internal static let frameLoading12 = ImageAsset(name: "frame_loading_12")
  internal static let frameLoading13 = ImageAsset(name: "frame_loading_13")
  internal static let frameLoading14 = ImageAsset(name: "frame_loading_14")
  internal static let frameLoading15 = ImageAsset(name: "frame_loading_15")
  internal static let frameLoading16 = ImageAsset(name: "frame_loading_16")
  internal static let frameLoading17 = ImageAsset(name: "frame_loading_17")
  internal static let frameLoading18 = ImageAsset(name: "frame_loading_18")
  internal static let frameLoading19 = ImageAsset(name: "frame_loading_19")
  internal static let frameLoading20 = ImageAsset(name: "frame_loading_20")
  internal static let frameLoading21 = ImageAsset(name: "frame_loading_21")
  internal static let frameLoading22 = ImageAsset(name: "frame_loading_22")
  internal static let frameLoading23 = ImageAsset(name: "frame_loading_23")
  internal static let frameLoading24 = ImageAsset(name: "frame_loading_24")
  internal static let frameLoading25 = ImageAsset(name: "frame_loading_25")
  internal static let frameLoading26 = ImageAsset(name: "frame_loading_26")
  internal static let frameLoading27 = ImageAsset(name: "frame_loading_27")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = Color(asset: self)

  #if os(iOS) || os(tvOS)
  @available(iOS 11.0, tvOS 11.0, *)
  internal func color(compatibleWith traitCollection: UITraitCollection) -> Color {
    let bundle = BundleToken.bundle
    guard let color = Color(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }
  #endif

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, macOS 10.7, *)
  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }

  #if os(iOS) || os(tvOS)
  @available(iOS 8.0, tvOS 9.0, *)
  internal func image(compatibleWith traitCollection: UITraitCollection) -> Image {
    let bundle = BundleToken.bundle
    guard let result = Image(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
  #endif
}

internal extension ImageAsset.Image {
  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, *)
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
