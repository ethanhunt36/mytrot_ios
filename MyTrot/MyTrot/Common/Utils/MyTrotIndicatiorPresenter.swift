//
//  MyTrotIndicatiorPresenter.swift
//  MyTrot
//
//  Created by hclim on 2021/03/15.
//

import Foundation
import UIKit
/// 로딩 표시를 하는 클래스
/// MyTrotIndicatiorPresenter.shared.startAnimating()
/// MyTrotIndicatiorPresenter.shared.startAnimating("메시지를 표시할 경우 이렇게~")
final class MyTrotIndicatiorPresenter {
    private let restorationIdentifier = "AptnerIndicatorPresenter"

    static let shared = MyTrotIndicatiorPresenter()

    enum Config {
        /// 로딩 이미지 가로 길이
        static let imageViewSizeWidth: CGFloat = 43.0
        /// 로딩 이미지 세로 길이
        static let imageViewSizeHeight: CGFloat = 43.0
        /// 로딩 이미지와 텍스트 사이 간격
        static let messageSpacing: CGFloat = 10.0
        /// 애니메이션을 지연시켜 보여주도록 하는 시간
        static let lateOnScreenDuration = 0.7
    }

    /// 에니메이션 상태
    var isAnimating: Bool = false

    /// 애니메이션을 지연시켜 보여주도록 하는 옵션
    /// true 는 짧은 것을 안보여 줍니다.
    /// false 는 짧은 애니메션도 모두 보여줍니다.
    var lateOnScreen: Bool = true

    /// 로딩 이미지 아래에 표시 될 텍스트
    private let messageLabel: UILabel = {
        let label = UILabel()

        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false

        return label
    }()
}

extension MyTrotIndicatiorPresenter {
    func startAnimating(_ message: String = "") {
        guard Thread.isMainThread else {
          DispatchQueue.main.async { [weak self] in
            self?.startAnimating()
          }
          return
        }

        if isAnimating {
            self.show(message: message)
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.show(message: message)
            }
        }
//        self.show(message: message)
    }

    func stopAnimating() {
        guard Thread.isMainThread else {
          DispatchQueue.main.async { [weak self] in
            self?.stopAnimating()
          }
          return
        }

        if isAnimating {
            DispatchQueue.main.async { [weak self] in
              self?.hide()
            }
        } else {
            self.hide()
        }
    }
}

extension MyTrotIndicatiorPresenter {
    fileprivate func show(message: String) {
        print("\(#function)")
        guard let keyWindow = UIApplication.shared.keyWindow, Thread.isMainThread, self.isAnimating == false else { return }
        self.isAnimating = true

        // 로딩이미지를 담는 그릇이 될 뷰
        let containerView = UIView(frame: UIScreen.main.bounds)

        containerView.backgroundColor = Asset.paleGrey.color
        containerView.alpha = 0.2
        containerView.restorationIdentifier = restorationIdentifier
        containerView.translatesAutoresizingMaskIntoConstraints = false
        keyWindow.addSubview(containerView)

        // Add constraints for `containerView`.
        ({
            let leadingConstraint = NSLayoutConstraint(item: keyWindow, attribute: .leading, relatedBy: .equal, toItem: containerView, attribute: .leading, multiplier: 1, constant: 0)
            let trailingConstraint = NSLayoutConstraint(item: keyWindow, attribute: .trailing, relatedBy: .equal, toItem: containerView, attribute: .trailing, multiplier: 1, constant: 0)
            let topConstraint = NSLayoutConstraint(item: keyWindow, attribute: .top, relatedBy: .equal, toItem: containerView, attribute: .top, multiplier: 1, constant: 0)
            let bottomConstraint = NSLayoutConstraint(item: keyWindow, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1, constant: 0)

            keyWindow.addConstraints([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
            }())

        /// 로딩 이미지
        let imageView = UIImageView()
        imageView.frame = CGRect.zero

        var imageArray = [UIImage]()
        imageArray.append(Asset.frameLoading01.image)
        imageArray.append(Asset.frameLoading02.image)
        imageArray.append(Asset.frameLoading03.image)
        imageArray.append(Asset.frameLoading04.image)
        imageArray.append(Asset.frameLoading05.image)
        imageArray.append(Asset.frameLoading06.image)
        imageArray.append(Asset.frameLoading07.image)
        imageArray.append(Asset.frameLoading08.image)
        imageArray.append(Asset.frameLoading09.image)
        imageArray.append(Asset.frameLoading10.image)
        imageArray.append(Asset.frameLoading11.image)
        imageArray.append(Asset.frameLoading12.image)
        imageArray.append(Asset.frameLoading13.image)
        imageArray.append(Asset.frameLoading14.image)
        imageArray.append(Asset.frameLoading16.image)
        imageArray.append(Asset.frameLoading17.image)
        imageArray.append(Asset.frameLoading18.image)
        imageArray.append(Asset.frameLoading19.image)
        imageArray.append(Asset.frameLoading20.image)
        imageArray.append(Asset.frameLoading21.image)
        imageArray.append(Asset.frameLoading21.image)
        imageArray.append(Asset.frameLoading22.image)
        imageArray.append(Asset.frameLoading23.image)
        imageArray.append(Asset.frameLoading24.image)
        imageArray.append(Asset.frameLoading25.image)
        imageArray.append(Asset.frameLoading26.image)
        imageArray.append(Asset.frameLoading27.image)
        
        imageView.animationImages = imageArray
        imageView.animationDuration = 2.4
        imageView.animationRepeatCount = 0
        containerView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false

        imageView.widthAnchor.constraint(equalToConstant: Config.imageViewSizeWidth).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: Config.imageViewSizeHeight).isActive = true
        imageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true

        imageView.startAnimating()

        /// 로딩 이미지 아래에 텍스트
        if message.count > 0 {
            messageLabel.font = UIFont.systemFont(ofSize: 20)
            messageLabel.textColor = .white
            messageLabel.text = message
            containerView.addSubview(messageLabel)

            // Add constraints for `messageLabel`.
            ({
                let leadingConstraint = NSLayoutConstraint(item: containerView, attribute: .leading, relatedBy: .equal, toItem: messageLabel, attribute: .leading, multiplier: 1, constant: -8)
                let trailingConstraint = NSLayoutConstraint(item: containerView, attribute: .trailing, relatedBy: .equal, toItem: messageLabel, attribute: .trailing, multiplier: 1, constant: 8)

                containerView.addConstraints([leadingConstraint, trailingConstraint])

                let spacingConstraint = NSLayoutConstraint(item: messageLabel, attribute: .top, relatedBy: .equal, toItem: imageView, attribute: .bottom, multiplier: 1, constant: Config.messageSpacing)

                containerView.addConstraint(spacingConstraint)
                }())
        }

        if lateOnScreen {
            containerView.alpha = 0.0
            UIView.animate(withDuration: Config.lateOnScreenDuration) {
                containerView.alpha = 0.2
            }
        }
    }

    fileprivate func hide() {
        print("\(#function)")
        guard Thread.isMainThread else { return }
        self.isAnimating = false
        for window in UIApplication.shared.windows {
            for item in window.subviews where item.restorationIdentifier == restorationIdentifier {
                for subItem in item.subviews {
                    if let imageView = subItem as? UIImageView, imageView.isAnimating == true {
                        imageView.stopAnimating()
                    }
                    subItem.layer.removeAllAnimations()
                    subItem.removeFromSuperview()
                }
                item.layer.removeAllAnimations()
                item.removeFromSuperview()
            }
        }
    }
}
