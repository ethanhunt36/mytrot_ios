//
//  EasyUnityAdable.swift
//  MyTrot
//
//  Created by hclim on 2021/05/21.
//

import Foundation
import UnityAds

protocol EasyUnityAdable : UnityAdsDelegate{
    var targetViewController: EasyUnityAds { get }
    func initializeUnityAd(unityAdId: String)
}
final class EasyUnityAds: UIViewController {
    
}
extension EasyUnityAdable{
    func initializeUnityAd(unityAdId: String) {
//        UnityAds.initialize(unityAdId, initializationDelegate: targetViewController)
    }

    
    func unityAdsReady(_ placementId: String) {
        print("unityAdsReady placementId : \(placementId)")
    }
    
    func unityAdsDidStart(_ placementId: String) {
        print("unityAdsDidStart placementId : \(placementId)")

    }
    func unityAdsDidError(_ error: UnityAdsError, withMessage message: String) {
        print("unityAdsDidError withMessage : \(message)")

    }
    func unityAdsDidFinish(_ placementId: String, with state: UnityAdsFinishState) {
        print("unityAdsDidFinish placementId : \(placementId)")

    }
}
