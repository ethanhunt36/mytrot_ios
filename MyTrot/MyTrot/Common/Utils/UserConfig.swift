//
//  UserConfig.swift
//  MyTrot
//
//  Created by hclim on 2021/03/11.
//

import Foundation
final class UserConfig {
    static func objectFor(key: Keys.UserDefaults) -> AnyObject? {
        return UserDefaults.standard.object(forKey: key.rawValue) as AnyObject
    }

    static func setObjectFor(key: Keys.UserDefaults, object: Any?) -> Bool {
        if let object = object {
            UserDefaults.standard.set(object, forKey: key.rawValue)
        } else {
            UserDefaults.standard.removeObject(forKey: key.rawValue)
        }
        return UserDefaults.standard.synchronize()
    }
}
