//
//  EventBus.swift
//  MyTrot
//
//  Created by hclim on 2021/03/10.
//

import Foundation
struct Events {
    /// 글로벌
    struct VideoTabRefresh: BusEvent { }
    struct AlertShow: BusEvent { let message: String }
    struct MyInfo: BusEvent { let info: MyInfoDataModel }
    
    // 광고 완료 후 Res 에 따른 광고 포인트, 적립이 변경되었을때 자동UI업데이트를 위한 Event
    struct RefreshAdInfo: BusEvent { }

    struct RefreshMyInfo: BusEvent { }
    
    struct AddBookMark: BusEvent {
        let no: Int64
    }
    struct RemoveBookMark: BusEvent {
        let no: Int64
    }
    struct SelectArtistInfo: BusEvent {
        let artist_no: Int64
        let artist_name: String
    }
    struct UploadSelectArtistInfo: BusEvent {
        let artist_no: Int64
        let artist_name: String
    }

    struct VoteFinish: BusEvent {
        let id: Int64
        let mypoint: Int
        let totalpoint: Int64
        let givePoint: Int
    }
    struct DonateFinish: BusEvent {
        let id: Int64
        let mypoint: Int
        let totalpoint: Int64
        let givePoint: Int
    }
    
    struct CheerFinish: BusEvent {
        let id: Int64
        let likeCount: Int64?
        let commentCount: Int64?
        let isLike: Bool?
    }
    
    struct CheerUploadFinish: BusEvent {}
    struct GalleryUploadFinish: BusEvent {}
    struct AttendanceCheckFinish: BusEvent {}
    struct SetMyFavSingerFinish: BusEvent {}
    struct PpobKiNeedToRefresh: BusEvent {}
    
    struct DisplayNotificationNew: BusEvent { let unreadCount: Int }
    
    struct YoutubeMyCountUpdate: BusEvent { let model: ArtistVideoListModel?}

    struct HomeMainPagerTopTabSelectedAction: BusEvent {
        let selectedIndex: Int
    }
    
    struct MainTabSelectedAction: BusEvent {
        let selectedIndex: Int
    }

}
