//
//  Utils.swift
//  MyTrot
//
//  Created by hclim on 2021/03/21.
//

import Foundation
import GoogleMobileAds

class Utils {
    // someBelow 밑으로 까지 소수점
    // ex)
    // input : 3.33333 , someBelow : 2
    // -> 3.33
    static func roundByPointIntValue(input: Double, someBelow: Int) -> Double{
        var mathValue: Double = 1
        
        for _ in 0..<someBelow {
            mathValue = mathValue * 10
        }

        return round(input*mathValue)/mathValue
    }
    static func hasArtistList() -> Bool {
        let preference = PreferencesService()
        let today = (Int)(Date().timeIntervalSince1970)
        
        if preference.getSingerModelCachedDate() == 0 || today > preference.getSingerModelCachedDate() + 86400 {
            print("hasArtistList false")
            return false
        }
        if preference.getFavSingerModel().list_data.count > 0 {
            print("hasArtistList true")
            return true
        } else {
            print("hasArtistList false")
            return false 
        }
    }
    static func changeDateFormat(dateString: String) -> String {
        guard dateString.count > 9 else { return dateString }

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date = dateFormatter.date(from: dateString) else { return dateString }
        
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let dateComponets = calendar.dateComponents([.year,.hour,.minute,.month, .day], from: date)
        let todayDateComponets = calendar.dateComponents([.year,.hour,.minute,.month, .day], from: Date())

        let timeinterval = date.timeIntervalSince1970
        let todayTimeInterval = Date().timeIntervalSince1970
        
        // 86400 초 = 1일
        // 오늘 이전
        if todayTimeInterval < timeinterval + 86400 {
            // 3분 이내
            if todayTimeInterval < timeinterval + 180 {
                return "방금전"
            }
            // 1시간 이내
            else if todayTimeInterval < timeinterval + 3600 {
                return "\(Int((todayTimeInterval - timeinterval)/60))분전"
            }
            // 어제
            else if let day = dateComponets.day, let todayDay = todayDateComponets.day, day != todayDay {
                return "어제"

            }
            // 시간전
            else if let hour = dateComponets.hour, let todayHour = todayDateComponets.hour {
                return "\(todayHour - hour)시간전"
            }

        }
        // 어제
        else if todayTimeInterval < timeinterval + (86400 * 2 ) {
            return "어제"
        }
        // 같은년도
        else if let year = dateComponets.year, let todayYear = todayDateComponets.year, year == todayYear{

            if let month = dateComponets.month,
               let day = dateComponets.day {
                let returnValue = String(format: "%02d.%02d", month, day)
                return returnValue
            }

        }
        if let year = dateComponets.year,
           let month = dateComponets.month,
           let day = dateComponets.day {
            let returnValue = String(format: "%d.%02d.%02d",year, month, day)
            return returnValue
        }


        return dateString
    }

    static func AlertShow(msg: String) {
        RxBus.shared.post(event: Events.AlertShow(message: msg))
    }
    
    static func getFavSingerTitle(screenType: FavSingerScreenType) -> String {
        switch screenType {
        case .choiceFavSinger:
            return "최애 가수 선택"
        case .choiceSinger:
            return "응원할 가수를 선택하세요"
        case .choiceUploadSinger:
            return "응원할 가수를 선택하세요"
        case .activityCertification:
            return "인증서 발급할 가수를 선택해주세요"
        }
    }
    static func converSecToTimeString(time: Int64) -> String {
        // 305 - 5시간5분
        let totalMin = time/60
        let hour = totalMin/60
        let min = totalMin - ( hour * 60 )
        return String(format: "%02d시간%02d분", hour, min)
    }

    static func needToCheckAttendance() -> Bool {
        let pref = PreferencesService()
        let savedAttendanceDate = pref.getAttendanceDate()
        if savedAttendanceDate == "" {
            return true
        }
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let stringTodayDate = dateFormatter.string(from: todayDate)
        if savedAttendanceDate != stringTodayDate {
            return true
        }
        return false
    }
    static func needToCheckChartFirstPopupDate() -> Bool {
        let pref = PreferencesService()
        let savedChartDate = pref.getChartFirstPopupDate()
        if savedChartDate == "" {
            return true
        }
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let stringTodayDate = dateFormatter.string(from: todayDate)
        if savedChartDate != stringTodayDate {
            return true
        }
        return false
    }

    static func getTodayString() -> String {
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let stringTodayDate = dateFormatter.string(from: todayDate)

        return stringTodayDate
    }
    static func getTodayStringWithFormatter(format: String) -> String {
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let stringTodayDate = dateFormatter.string(from: todayDate)

        return stringTodayDate
    }
    // 앞에 시간(compare1)이 현재 시간을 넘었는지 확인 (yyyy-MM-dd HH:mm:ss)
    static func isAvailablePpobKi(compareTime: String) -> Bool {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        format.timeZone = .current

        guard let startTime = format.date(from: compareTime) else {return false}
        let today = Date()
        let stringTodayDate = format.string(from: today)
        guard let endTime = format.date(from: stringTodayDate) else {return false}

        let useTime = Int(startTime.timeIntervalSince(endTime))
        print("startTime :: \(startTime)")
        print("endTime :: \(endTime)")
        print("useTime :: \(useTime)")
        return useTime < 0
    }
    static func getLeftPpobkiTime(compareTime: String) -> Double {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        format.timeZone = .current

        guard let startTime = format.date(from: compareTime) else {return 0}
        let today = Date()
        let stringTodayDate = format.string(from: today)
        guard let endTime = format.date(from: stringTodayDate) else {return 0}

        let useTime = Int(startTime.timeIntervalSince(endTime))

        return Double(useTime)
    }
    static func calculateLeftPpobkiTimeFromDate(compareTime: String) -> String {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        format.timeZone = .current

        guard let startTime = format.date(from: compareTime) else {return ""}
        let today = Date()
        let stringTodayDate = format.string(from: today)
        guard let endTime = format.date(from: stringTodayDate) else {return ""}

        let useTime = Int(startTime.timeIntervalSince(endTime))
        print("startTime :: \(startTime)")
        print("endTime :: \(endTime)")
        print("useTime :: \(useTime)")

        return Utils.calculateLeftPpobkiTime(time: Double(useTime))
    }
    static func secondsToHoursMinutesSeconds (seconds : Double) -> (Int, Int, Int) {
        let (hr,  minf) = modf (seconds / 3600)
        let (min, secf) = modf (60 * minf)
        return (Int(hr), Int(min), Int( 60 * secf))
    }

    static func calculateLeftPpobkiTime(time: Double) -> String {
                
        print("calculateLeftPpobkiTime : \(time)")
        let changedTime = Utils.secondsToHoursMinutesSeconds(seconds: time)
        let dateStartString = String(format: "%02d:%02d:%02d", changedTime.0, changedTime.1, changedTime.2)
        return dateStartString

    }
    static func updateAttendance() {
        let pref = PreferencesService()
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let stringTodayDate = dateFormatter.string(from: todayDate)
        pref.setAttendanceDate(attendanceDate: stringTodayDate)
    }
    
    static func updateChartFirstPopupDate() {
        let pref = PreferencesService()
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let stringTodayDate = dateFormatter.string(from: todayDate)
        pref.setChartFirstPopupDate(chartFirstPopupDate: stringTodayDate)
    }

    static func updateYoutubeAgreeDate() {
        let pref = PreferencesService()
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let stringTodayDate = dateFormatter.string(from: todayDate)
        pref.setYoutubeAgreementDate(date: stringTodayDate)
    }
    static func checkYoutubeAgreeDate() -> Bool {
        let pref = PreferencesService()

        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let stringTodayDate = dateFormatter.string(from: todayDate)

        let savedDate = pref.getYoutubeAgreementDate()
        if savedDate == "" || savedDate != stringTodayDate {
            return false
        }
        return true
    }
    static func getMyFavType() -> FavSingerMyArtistType {
        let preferencesService = PreferencesService() 
        if User.shared.myInfo?.artist_no ?? 0 < 1 {
            return .nothing
        }
        else if preferencesService.getIsSelectdMyFavSinger() {
            return .topCheck
        } else {
            return .nonCheck
        }
    }
    
    static func getPpobKiCount(list: [PpobkiDataInfoModel]) -> Int {
        
        var resultCount = 0
        for item in list {
            let useYn = item.use_yn
            if useYn == "N" {
                resultCount += 1
            }
        }
        return resultCount
    }

    static func getLuckyNumberBackgroundColor(num: Int) -> UIColor {
        if num < 11 {
            return Asset.numberBlow10.color
        } else if num < 21 {
            return Asset.numberBlow20.color
        } else if num < 31 {
            return Asset.numberBlow30.color
        } else if num < 41 {
            return Asset.numberBlow40.color
        }
        return Asset.numberBlow50.color
    }
    
    static func displayNumberBackground(imageView: UIImageView) {
        imageView.layer.cornerRadius = 15.0
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = Asset.black.color.cgColor
    }
    
    static func getTotalPlayTime(items: [ArtistVideoListModel], link: String) -> Int {
        var playMin = 0
        var playSec = 0
        for item in items {
            if item.youtube_id == link {
                playMin = item.vod_min
                playSec = item.vod_sec

            }
        }
        return playMin * 60 + playSec
    }
    static func getPlayItemIndex(items: [ArtistVideoListModel], link: String) -> Int {
        var index = 0
        for item in items {
            if item.youtube_id == link {
                return index
            }
            index += 1
        }
        return 0
    }
    static func getPlayItemNo(items: [ArtistVideoListModel], link: String) -> Int {
        var itemNo: Int64 = 0
        for item in items {
            if item.youtube_id == link {
                itemNo = item.no
            }
        }
        return Int(itemNo)
    }
    static func getPlayItem(items: [ArtistVideoListModel], link: String) -> ArtistVideoListModel? {
        var itemModel: ArtistVideoListModel?
        for item in items {
            if item.youtube_id == link {
                itemModel = item
            }
        }
        return itemModel
    }
    
    static func getLastSavedChartGender() -> GenderType {
        let gender = UserConfig.objectFor(key: .chartGender) as? String ?? ""
        if gender == "M" {
            return .male
        } else if gender == "F" {
            return .female
        }
        return .both
    }
    static func saveChartGender(type: GenderType) {
        _ = UserConfig.setObjectFor(key: .chartGender, object: type.rawValue)
    }
    static func getLastSavedVoteGender() -> GenderType {
        let gender = UserConfig.objectFor(key: .voteGender) as? String ?? ""
        if gender == "M" {
            return .male
        } else if gender == "F" {
            return .female
        }
        return .both
    }
    static func saveVoteGender(type: GenderType) {
        _ = UserConfig.setObjectFor(key: .voteGender, object: type.rawValue)

    }
    
    static func getLastSavedVoteType() -> VoteListType {
        let voteType = UserConfig.objectFor(key: .voteType) as? String ?? ""
        if voteType == "vote_cnt_week" {
            return .weekly
        } else if voteType == "vote_cnt_month" {
            return .monthly
        }
        return .normally
    }
    static func saveVoteType(type: VoteListType) {
        _ = UserConfig.setObjectFor(key: .voteType, object: type.rawValue)

    }

}
