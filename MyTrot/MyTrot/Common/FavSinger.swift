//
//  FavSinger.swift
//  MyTrot
//
//  Created by hclim on 2021/03/29.
//

import Foundation
class FavSinger: HasPreferencesService {
    static let shared = User()
    var preferencesService = PreferencesService()

    var favInfo: ArtistSelectDataListModel? = nil {
        didSet {
            // 확인용 로그
            print("\n\n")
            print("🤗=================================================")
            print("🤗== 공통으로 사용하는 ArtistSelectDataListModel 메모리(Volatile Memory) 데이터에 내정보(favInfo) 정보가 업데이트 되었습니다. ")
            print("🤗=================================================")
            print("\n\n")
        }
    }

}
extension FavSinger {
    func initiatizeData() {
        self.favInfo = nil
    }
}
extension FavSinger {
    func updateFavInformation(data: ArtistSelectDataListModel?) {
        guard let user = data else { return }

        // 사용자 정보 업데이트
        self.favInfo = user
    }
}
