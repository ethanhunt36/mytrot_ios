//
//  User.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
class User {
    static let shared = User()

    var amount_ticket_ad = ""
    var point_amount: Double = 0.0
    var blog_url = ""
    var myInfo: MyInfoDataModel? = nil {
        didSet {
            guard let strongMyInfo = myInfo else { return }

            // 확인용 로그
            print("\n\n")
            print("🤗=================================================")
            print("🤗== 공통으로 사용하는 User 메모리(Volatile Memory) 데이터에 내정보(myInfo) 정보가 업데이트 되었습니다. ")
            print("🤗=================================================")
            print("\n\n")
            let preferencesService = PreferencesService()

            preferencesService.setMember_no(member_no: strongMyInfo.member_no)

            let bus = RxBus.shared
            bus.post(event: Events.MyInfo(info: strongMyInfo))
        }
    }

}
extension User {
    func initiatizeData() {
        self.myInfo = nil
    }
}
extension User {
    func updateMyInformation(data: MyInfoDataModel?) {
        guard let user = data else { return }

        // 사용자 정보 업데이트
        self.myInfo = user
//        self.preferencesService.setMember_no(member_no: user.member_no)
    }
    
    func updateAmountTicketAd(amount_ticket_ad: String) {
        self.amount_ticket_ad = amount_ticket_ad
        let bus = RxBus.shared
        bus.post(event: Events.RefreshAdInfo())

    }
    func updatePointAmount(point_amount: Double) {
        self.point_amount = point_amount
        let bus = RxBus.shared
        bus.post(event: Events.RefreshAdInfo())

    }
}
