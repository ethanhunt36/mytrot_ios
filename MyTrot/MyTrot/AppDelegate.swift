//
//  AppDelegate.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
import RxFlow
import RxSwift
import GoogleMobileAds
import Firebase
import FirebaseMessaging

@main
class AppDelegate: UIResponder, UIApplicationDelegate,HasPreferencesService {

    var coordinator = FlowCoordinator()
    var appFlow: AppFlow!
    var appStepper: AppStepper!
    let disposeBag = DisposeBag()
    var window: UIWindow?
    var preferencesService = PreferencesService()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        preferencesService.initializeUUID()
        initializeSetupAppFlow()
        initializeEventBus()
        
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self

        return true
    }


    func initializeEventBus() {
        let bus = RxBus.shared
        bus.asObservable(event: Events.AlertShow.self, sticky: true).subscribe { event in
            guard let message = event.element?.message else { return }
            let controller = UIAlertController.init(title: "", message: message.replaceSlashString, preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "확인", style: .cancel)
            controller.addAction(defaultAction)
            let presentController = UIApplication.topViewController()

            presentController?.present(controller, animated: true)
        }.disposed(by: disposeBag)

    }
    func initializeSetupAppFlow() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.coordinator.rx.willNavigate.subscribe(onNext: { _, _ in
//            print("will navigate to flow=\(flow) and step=\(step)")
        }).disposed(by: self.disposeBag)

        self.coordinator.rx.didNavigate.subscribe(onNext: { _, _ in
//            print("did navigate to flow=\(flow) and step=\(step)")
        }).disposed(by: self.disposeBag)

        self.appFlow = AppFlow(withWindow: window)
        self.appStepper = AppStepper()

        coordinator.coordinate(flow: self.appFlow, with: self.appStepper)
        self.window = window

        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
    }


}

// MARK: - MessagingDelegate
extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")

        preferencesService.setFcmToken(token: fcmToken ?? "")
    }
}


