//
//  Service.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import RxSwift
import MoyaSugar

protocol MyTrotServiceType {
    func loginAfter() -> Single<CommonResponse>
    func chartList() -> Single<ResponseArtistRankList>
    func voteList(sort: VoteListType) -> Single<ResponseVoteList>
    func donateList(type: DonateListType) -> Single<ResponseDonateList>
    func cheerList(type: CheerListType, lastNo: String) -> Single<ResponseCheerList>
    func cheerListByArtistNo(type: CheerListType, lastNo: String, artist_no: Int64) -> Single<ResponseCheerList>

    func cheerTotalList() -> Single<ResponseCheerTotalList>
    func missionList() -> Single<ResponseMissionList>
    func notificationList(lastNo: Int64) -> Single<ResponseNotificationList>
    func attendanceMyList(lastNo: String) -> Single<ResponseAttendanceMyList>
    func attendanceRankList() -> Single<ResponseAttendanceRankList>
    /// 영상( 전체공통)
    func vodList(type: VideoListType, last_no: String, artist_no: Int64, artist_name: String?, keyword: String) -> Single<ResponseArtistVideoList>

    func artistVideoListWithFav(lastNo: Int64) -> Single<ResponseArtistVideoList>
    func artistVideoListWithTotal(lastNo: Int64) -> Single<ResponseArtistVideoList>
    func artistVideoListBySinger() -> Single<ResponseArtistVideoListBySinger>
    func donateMemberList(donate_goal_no: Int, artist_no: Int64) -> Single<ResponseDonateMemberList>
    func boardDetail(board_no: Int64, lastNo: Int64, category: BoardCategoryType) -> Single<ResponseBoardDetail>
    func luckyNumberList() -> Single<ResponseLuckyNumberList>
    func galleryList(sort: GalleryListType, lastNo: String, artistNo: Int64) -> Single<ResponseGalleryList>

    func getMyInfoByNo() -> Single<ResponseMyInfo>
    func getMyRCDMCode() -> Single<ResponseMyRCMDCode>
    func artistSelectList() -> Single<ResponseArtistSelectList>
    func voteHistoryList(gubun: VoteHistoryListType, lastNo: Int64) -> Single<ResponseVoteHistoryList>
    func pointHistoryList(gubun: PointHistoryListType, lastNo: Int64) -> Single<ResponsePointHistoryList>
    func noticeList(last_no: String, is_html: String) -> Single<ResponseNoticeList>
    func faqList(last_no: String, is_html: String) -> Single<ResponseFAQList>
    /// 투표권 순위 적립/사용 리스트
    func voteRankList(gubun: VoteRankListType, terms: VoteRankTermListType) -> Single<ResponseVoteRankList>
    /// 포인트 순위 적립/사용 리스트
    func pointRankList(gubun: PointRankListType, terms: PointRankTermListType ) -> Single<ResponsePointRankList>

    func videoStorageList(gb: VideoStorageListType, artist_no: Int64) -> Single<ResponseArtistVideoList>
    func videoStorageSingerList(gb: VideoStorageListType, artist_no: Int64) -> Single<ResponseVideoStorageList>
    func versionCheck() -> Single<ResponseVersionCheck>
    func addMember() -> Single<ResponseAddMember>
    func memberLogin() -> Single<ResponseMyInfo>
    func getMemberInfo() -> Single<ResponseMyInfo>
    func votePutTicketAdPoint(ad_type: String) -> Single<ResponseVotePutAd>
    func boardLike(board_no: Int64) -> Single<CommonResponse>
    func boardUnLike(board_no: Int64) -> Single<CommonResponse>
    func imageDownload(board_no: Int64) -> Single<CommonResponse>

    func reportBoard(board_no: Int64, type: Int, reason: String) -> Single<CommonResponse>

    func blockAllBoard(boardNo: Int64) -> Single<CommonResponse>

    /// 인증번호 받기
    func getAuthNumber(phoneNo: String) -> Single<CommonResponse>
    /// 인증번호 확인
    func confirmAuthNumber(phoneNo: String, pin: String) -> Single<ResponseAuthNumber>
    func noticeRead(no: Int64) -> Single<CommonResponse>

    func completeMission() -> Single<CommonResponse>
    func podongCheck() -> Single<CommonResponse>
    func attendanceCheckIn() -> Single<CommonResponse>
    func choiceMember(phoneNo: String) -> Single<CommonResponse>
    func getCertInfo(artist_no: Int64, isYesterDay: Bool) -> Single<ResponseActivityCertInfo>
    
    func checkCertInfo() -> Single<CommonResponse>
    
    func getPpobkkiList() -> Single<ResponsePpobkiInfo>
    func playPpobkki(no: Int) -> Single<ResponsePpobkiResultInfo>
    /// 홍보인증
    func getPromotionList(last_no: Int64) -> Single<ResponsePromotionList>
    func levelRankList() -> Single<ResponseLevelRankList>

    // MARK: - POST
    func uploadVote(artist_no: Int64, vote_cnt: Int) -> Single<CommonResponse>
    func addBookmark(artist_no: Int64, vodNo: Int64) -> Single<CommonResponse>
    func removeBookmark(artist_no: Int64, vodNo: Int64) -> Single<CommonResponse>

    func uploadDonate(artist_no: Int64, donate_cnt: Int) -> Single<CommonResponse>
    func uploadMyArtist(artist_no: Int64) -> Single<ResponseMyInfo>

    func uploadComment(board_no: Int64, cont: String) -> Single<CommonStringResponse>
    func uploadBoard(artist_no: Int64, name: String, cont: String) -> Single<CommonResponse>
    func uploadGalleryBoard(artist_no: Int64, name: String, cont: String, image: Data) -> Single<CommonResponse>
    func uploadPromotion(nick: String, title: String, cont: String, link: String, image1: Data, image2: Data, image3: Data) -> Single<CommonResponse>
    func updateNickname(nick: String) -> Single<CommonResponse>
    func uploadQuestion(cont: String, pics: [Data]?) -> Single<CommonResponse>
    func uploadAdditionalVideo(cont: String) -> Single<CommonResponse>
    func unregister() -> Single<CommonResponse>


    // MARK: - DEL
    func deleteBoard(board_no: Int64) -> Single<CommonResponse>
    func deleteComment(comment_no: Int64) -> Single<CommonResponse>

    // MARK: - IAP
    func getIAPList() -> Single<ResponseProductList>
    /// 상품 주문
    func addOrder(product_no: Int, price: Int) -> Single<CommonStringResponse>
    /// 상품 주문 -> 결제 완료
    func completeOrder(order_no: Int, price: Int, pay_type: String, sku: String, order_id: String, token: String) -> Single<CommonResponse>
    /// 포인트로 투표권 교환
    func exchangePoint(point_amount: Int) -> Single<CommonResponse>
    /// 내 보유 아이템 리스트 가져오기
    func getMyItemAll(product_type: String) -> Single<CommonResponse>

    /// Youtube Play
    func playYoutube(artist_vod_no: Int) -> Single<CommonResponse>
    /// Youtube finish
    func finishYoutube(artist_vod_no: Int) -> Single<CommonResponse>

    
    func cheatTimeApi() -> Single<CommonResponse>
}

final class MyTrotService: MyTrotServiceType {
    
    fileprivate let networking = CommonNetworking()
    func cheatTimeApi() -> Single<CommonResponse> {
        return self.networking
            .request(.cheatTimeApi)
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .debug()

    }
    func chartList() -> Single<ResponseArtistRankList> {
        return self.networking
            .startLoading()
            .request(.chartList)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseArtistRankList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }

    func voteList(sort: VoteListType) -> Single<ResponseVoteList> {
        return self.networking
            .startLoading()
            .request(.voteList(sort: sort))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseVoteList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()


    }
    func donateList(type: DonateListType) -> Single<ResponseDonateList> {
        return self.networking
            .startLoading()
            .request(.donateList(type: type))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseDonateList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func cheerList(type: CheerListType, lastNo: String) -> Single<ResponseCheerList> {
        return self.networking
            .startLoading()
            .request(.cheerList(type: type, lastNo: lastNo))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseCheerList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func cheerListByArtistNo(type: CheerListType, lastNo: String, artist_no: Int64) -> Single<ResponseCheerList> {
        return self.networking
            .startLoading()
            .request(.cheerListByArtistNo(type: type, lastNo: lastNo, artist_no: artist_no))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseCheerList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func cheerTotalList() -> Single<ResponseCheerTotalList> {
        return self.networking
            .startLoading()
            .request(.cheerTotalList)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseCheerTotalList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func missionList() -> Single<ResponseMissionList> {
        return self.networking
            .startLoading()
            .request(.missionList)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseMissionList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func notificationList(lastNo: Int64) -> Single<ResponseNotificationList> {
        return self.networking
            .startLoading()
            .request(.notificationList(lastNo: lastNo))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseNotificationList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }

    func attendanceMyList(lastNo: String) -> Single<ResponseAttendanceMyList> {
        return self.networking
            .startLoading()
            .request(.attendanceMyList(lastNo: lastNo))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseAttendanceMyList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func attendanceRankList() -> Single<ResponseAttendanceRankList> {
        return self.networking
            .startLoading()
            .request(.attendanceRankList)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseAttendanceRankList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()
    }
    func artistVideoListWithFav(lastNo: Int64) -> Single<ResponseArtistVideoList> {
        return self.networking
            .startLoading()
            .request(.artistVideoListWithFav(lastNo: lastNo))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseArtistVideoList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }

    func artistVideoListWithTotal(lastNo: Int64) -> Single<ResponseArtistVideoList> {
        return self.networking
            .startLoading()
            .request(.artistVideoListWithTotal(lastNo: lastNo))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseArtistVideoList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func artistVideoListBySinger() -> Single<ResponseArtistVideoListBySinger> {
        return self.networking
            .startLoading()
            .request(.artistVideoListBySinger)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseArtistVideoListBySinger.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func donateMemberList(donate_goal_no: Int, artist_no: Int64) -> Single<ResponseDonateMemberList> {
        return self.networking
            .startLoading()
            .request(.donateMemberList(donate_goal_no: donate_goal_no, artist_no: artist_no))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseDonateMemberList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }

    func boardDetail(board_no: Int64, lastNo: Int64, category: BoardCategoryType) -> Single<ResponseBoardDetail> {
        return self.networking
            .startLoading()
            .request(.boardDetail(board_no: board_no, lastNo: lastNo, category: category))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseBoardDetail.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func luckyNumberList() -> Single<ResponseLuckyNumberList> {
        return self.networking
            .startLoading()
            .request(.luckyNumberList)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseLuckyNumberList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func galleryList(sort: GalleryListType, lastNo: String, artistNo: Int64) -> Single<ResponseGalleryList> {
        return self.networking
            .startLoading()
            .request(.galleryList(sort: sort, lastNo: lastNo, artistNo: artistNo))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseGalleryList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func getMyInfoByNo() -> Single<ResponseMyInfo> {
        return self.networking
            .startLoading()
            .request(.getMyInfoByNo)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseMyInfo.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func getMyRCDMCode() -> Single<ResponseMyRCMDCode> {
        return self.networking
            .startLoading()
            .request(.getMyRCDMCode)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseMyRCMDCode.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func artistSelectList() -> Single<ResponseArtistSelectList> {
        if Utils.hasArtistList() {
            return Single.create { single -> Disposable in
                let preference = PreferencesService()
                var artistModel = ResponseArtistSelectList(err: 0, msg: "")
                artistModel.data = preference.getFavSingerModel()
                
                single(.success(artistModel))
                return Disposables.create()
            }
        }
        return self.networking
            .startLoading()
            .request(.artistSelectList)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseArtistSelectList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func voteHistoryList(gubun: VoteHistoryListType, lastNo: Int64) -> Single<ResponseVoteHistoryList> {
        return self.networking
            .startLoading()
            .request(.voteHistoryList(gubun: gubun, lastNo: lastNo))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseVoteHistoryList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func pointHistoryList(gubun: PointHistoryListType, lastNo: Int64) -> Single<ResponsePointHistoryList> {
        return self.networking
            .startLoading()
            .request(.pointHistoryList(gubun: gubun, lastNo: lastNo))
            .debug()
            .asMoyaResponseLog()
            .map(ResponsePointHistoryList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func noticeList(last_no: String, is_html: String) -> Single<ResponseNoticeList> {
        return self.networking
            .startLoading()
            .request(.noticeList(last_no: last_no, is_html: is_html))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseNoticeList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func faqList(last_no: String, is_html: String) -> Single<ResponseFAQList> {
        return self.networking
            .startLoading()
            .request(.faqList(last_no: last_no, is_html: is_html))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseFAQList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func videoStorageList(gb: VideoStorageListType, artist_no: Int64) -> Single<ResponseArtistVideoList> {
        return self.networking
            .startLoading()
            .request(.videoStorageList(gb: gb, artist_no: artist_no))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseArtistVideoList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func videoStorageSingerList(gb: VideoStorageListType, artist_no: Int64) -> Single<ResponseVideoStorageList> {
        return self.networking
            .startLoading()
            .request(.videoStorageList(gb: gb, artist_no: artist_no))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseVideoStorageList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }

    func versionCheck() -> Single<ResponseVersionCheck> {
        return self.networking
            .startLoading()
            .request(.versionCheck)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseVersionCheck.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func addMember() -> Single<ResponseAddMember> {
        return self.networking
            .startLoading()
            .request(.addMember)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseAddMember.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func memberLogin() -> Single<ResponseMyInfo> {
        return self.networking
            .startLoading()
            .request(.memberLogin)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseMyInfo.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func uploadVote(artist_no: Int64, vote_cnt: Int) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.uploadVote(artist_no: artist_no, vote_cnt: vote_cnt))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func uploadDonate(artist_no: Int64, donate_cnt: Int) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.uploadDonate(artist_no: artist_no, donate_cnt: donate_cnt))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func removeBookmark(artist_no: Int64, vodNo: Int64) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.removeBookmark(artist_no: artist_no, vodNo: vodNo))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func addBookmark(artist_no: Int64, vodNo: Int64) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.addBookmark(artist_no: artist_no, vodNo: vodNo))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func getMemberInfo() -> Single<ResponseMyInfo> {
        return self.networking
            .startLoading()
            .request(.getMemberInfo)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseMyInfo.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func vodList(type: VideoListType, last_no: String, artist_no: Int64, artist_name: String?, keyword: String) -> Single<ResponseArtistVideoList> {
        return self.networking
            .startLoading()
            .request(.vodList(type: type, last_no: last_no, artist_no: artist_no, artist_name: artist_name ?? "", keyword: keyword))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseArtistVideoList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func uploadMyArtist(artist_no: Int64) -> Single<ResponseMyInfo> {
        return self.networking
            .startLoading()
            .request(.uploadMyArtist(artist_no: artist_no))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseMyInfo.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func uploadComment(board_no: Int64, cont: String) -> Single<CommonStringResponse> {
        return self.networking
            .startLoading()
            .request(.uploadComment(board_no: board_no, cont: cont))
            .debug()
            .asMoyaResponseLog()
            .map(CommonStringResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func uploadBoard(artist_no: Int64, name: String, cont: String) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.uploadBoard(artist_no: artist_no, name: name, cont: cont))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func uploadGalleryBoard(artist_no: Int64, name: String, cont: String, image: Data) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.uploadGalleryBoard(artist_no: artist_no, name: name, cont: cont, image: image))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func boardLike(board_no: Int64) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.boardLike(board_no: board_no))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func boardUnLike(board_no: Int64) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.boardUnLike(board_no: board_no))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }

    func deleteBoard(board_no: Int64) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.deleteBoard(board_no: board_no))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func deleteComment(comment_no: Int64) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.deleteComment(comment_no: comment_no))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func imageDownload(board_no: Int64) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.imageDownload(board_no: board_no))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func updateNickname(nick: String) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.updateNickname(nick: nick))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func votePutTicketAdPoint(ad_type: String) -> Single<ResponseVotePutAd> {
        return self.networking
            //.startLoading()
            .request(.votePutTicketAdPoint(ad_type: ad_type))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseVotePutAd.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            //.stopLoading()
            .debug()

    }
    /// 인증번호 받기
    func getAuthNumber(phoneNo: String) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.getAuthNumber(phoneNo: phoneNo))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    /// 인증번호 확인
    func confirmAuthNumber(phoneNo: String, pin: String) -> Single<ResponseAuthNumber> {
        return self.networking
            .startLoading()
            .request(.confirmAuthNumber(phoneNo: phoneNo, pin: pin))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseAuthNumber.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    
    func noticeRead(no: Int64) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.noticeRead(no: no))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func loginAfter() -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.loginAfter)
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    
    func reportBoard(board_no: Int64, type: Int, reason: String) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.reportBoard(board_no: board_no, type: type, reason: reason))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    
    func blockAllBoard(boardNo: Int64) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.blockAllBoard(boardNo: boardNo))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func completeMission() -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.completeMission)
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func podongCheck() -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.podongCheck)
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func attendanceCheckIn() -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.attendanceCheckIn)
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func choiceMember(phoneNo: String) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.choiceMember(phoneNo: phoneNo))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    
    func uploadQuestion(cont: String, pics: [Data]?) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.uploadQuestion(cont: cont, pics: pics))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func uploadAdditionalVideo(cont: String) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.uploadAdditionalVideo(cont: cont))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    
    func getCertInfo(artist_no: Int64, isYesterDay: Bool) -> Single<ResponseActivityCertInfo> {
        return self.networking
            .startLoading()
            .request(.getCertInfo(artist_no: artist_no, isYesterDay: isYesterDay))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseActivityCertInfo.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func checkCertInfo() -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.checkCertInfo)
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()
    }
    
    func getPpobkkiList() -> Single<ResponsePpobkiInfo> {
        return self.networking
            .startLoading()
            .request(.getPpobkkiList)
            .debug()
            .asMoyaResponseLog()
            .map(ResponsePpobkiInfo.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    
    func playPpobkki(no: Int) -> Single<ResponsePpobkiResultInfo> {
        return self.networking
            .startLoading()
            .request(.playPpobkki(no: no))
            .debug()
            .asMoyaResponseLog()
            .map(ResponsePpobkiResultInfo.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    
    func getPromotionList(last_no: Int64) -> Single<ResponsePromotionList> {
        return self.networking
            .startLoading()
            .request(.getPromotionList(last_no: last_no))
            .debug()
            .asMoyaResponseLog()
            .map(ResponsePromotionList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func uploadPromotion(nick: String, title: String, cont: String, link: String, image1: Data, image2: Data, image3: Data) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.uploadPromotion(nick: nick, title: title, cont: cont, link: link, image1: image1, image2: image2, image3: image3))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }

    func pointRankList(gubun: PointRankListType, terms: PointRankTermListType) -> Single<ResponsePointRankList> {
        return self.networking
            .startLoading()
            .request(.pointRankList(gubun: gubun, terms: terms))
            .debug()
            .asMoyaResponseLog()
            .map(ResponsePointRankList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func voteRankList(gubun: VoteRankListType, terms: VoteRankTermListType) -> Single<ResponseVoteRankList>{
        return self.networking
            .startLoading()
            .request(.voteRankList(gubun: gubun, terms: terms))
            .debug()
            .asMoyaResponseLog()
            .map(ResponseVoteRankList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }

    func levelRankList() -> Single<ResponseLevelRankList> {
        return self.networking
            .startLoading()
            .request(.levelRankList)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseLevelRankList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    func getIAPList() -> Single<ResponseProductList> {
        return self.networking
            .startLoading()
            .request(.getIAPList)
            .debug()
            .asMoyaResponseLog()
            .map(ResponseProductList.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }

    /// 상품 주문
    func addOrder(product_no: Int, price: Int) -> Single<CommonStringResponse> {
        return self.networking
            .startLoading()
            .request(.addOrder(product_no: product_no, price: price))
            .debug()
            .asMoyaResponseLog()
            .map(CommonStringResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    /// 상품 주문 -> 결제 완료
    func completeOrder(order_no: Int, price: Int, pay_type: String, sku: String, order_id: String, token: String) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.completeOrder(order_no: order_no, price: price, pay_type: pay_type, sku: sku, order_id: order_id, token: token))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    /// 포인트로 투표권 교환
    func exchangePoint(point_amount: Int) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.exchangePoint(point_amount: point_amount))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }
    /// 내 보유 아이템 리스트 가져오기
    func getMyItemAll(product_type: String) -> Single<CommonResponse> {
        return self.networking
            .startLoading()
            .request(.getMyItemAll(product_type: product_type))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .stopLoading()
            .debug()

    }

    /// Youtube Play
    func playYoutube(artist_vod_no: Int) -> Single<CommonResponse> {
        return self.networking
            .request(.playYoutube(artist_vod_no: artist_vod_no))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .debug()

    }
    /// Youtube finish
    func finishYoutube(artist_vod_no: Int) -> Single<CommonResponse> {
        return self.networking
            .request(.finishYoutube(artist_vod_no: artist_vod_no))
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .debug()

    }

    func unregister() -> Single<CommonResponse> {
        return self.networking
            .request(.unregister)
            .debug()
            .asMoyaResponseLog()
            .map(CommonResponse.self)
            .debug()
            .catchError { CommonNetworking.catchError($0) }
            .debug()

    }
}
