//
//  ResponseGalleryList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseGalleryList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseGalleryList, rhs: ResponseGalleryList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: GalleryDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct GalleryDataModel: Codable {
    var last_no: String
    var list_data: [GalleryDataListModel]
}

struct GalleryDataListModel: Codable {
    var no: Int64
    var img01: String
    var like_cnt: Int64
    var comment_cnt: Int64
    var read_cnt: Int64
    
    var galleryHeaderModel: GalleryHeaderDataModel?
    var galleryListType: GalleryListType?

}

struct GalleryHeaderDataModel: Codable {
    var isSelectedFavSinger: Bool
    var favSingerName: String
    var viewListType: String
}
