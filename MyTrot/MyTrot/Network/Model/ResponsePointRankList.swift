//
//  ResponsePointRankList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponsePointRankList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponsePointRankList, rhs: ResponsePointRankList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: PointRankDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct PointRankDataModel: Codable {
    var list: [PointRankDataListModel]
    var my_ranking: AnyCodable
}

struct PointRankDataListModel: Codable {
    var rank: Int64
    var nick: String
    var point_sum: Double
    var reg_dttm: String

}
struct PointRankDataMyModel: Codable {
    var rank: Int
    var nick: String
    var date: String
}

struct MyRankModel: Codable {
    var rank: Int
}
