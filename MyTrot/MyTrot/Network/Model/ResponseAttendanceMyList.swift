//
//  ResponseAttendanceMyList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseAttendanceMyList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseAttendanceMyList, rhs: ResponseAttendanceMyList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: AttendanceMyDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct AttendanceMyDataModel: Codable {
    var last_no: String
    var list_data: [AttendanceMyDataListModel]
    var point: Int
    var bonus: Int
}

struct AttendanceMyDataListModel: Codable {
    var message: String
    var point: Int64
    var reg_dttm: String
    var combo: Int
    var bonus: Int?

}
