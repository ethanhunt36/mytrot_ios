//
//  ResponseMyInfo.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseMyInfo: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseMyInfo, rhs: ResponseMyInfo) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: MyInfoDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct MyInfoDataModel: Codable {
    var no: Int64
    var lv_pnt: Int
    var exp_end: Int?
    var lv: Int
    var nick: String
    var member_no: String
    var put_rcmd_code: String
    var email: String
    var vote_cnt: Int64
    var point_get: Double
    var point: Double
    var auth_state: Int
    var artist_name: String?
    var artist_pan_count: Int?
    var artist_no: Int64
    var phone: String?
    var vod_cnt: Int?
    var vod_time: Int64?
    var point_use: Int?
    var voted_cnt: String?
    var board_write_cnt: Int?
    var get_like_cnt: String?
    var put_like_cnt: Int?
}

