//
//  ResponsePpobkiResultInfo.swift
//  MyTrot
//
//  Created by hclim on 2021/06/11.
//

import Foundation
struct ResponsePpobkiResultInfo: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponsePpobkiResultInfo, rhs: ResponsePpobkiResultInfo) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: PpobkiResultInfoModel?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}

struct PpobkiResultInfoModel: Codable {
    var msg: String
    var bonus_num : Int

}
