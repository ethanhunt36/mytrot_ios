//
//  ResponseVersionCheck.swift
//  MyTrot
//
//  Created by hclim on 2021/03/18.
//

import Foundation
struct ResponseVersionCheck: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseVersionCheck, rhs: ResponseVersionCheck) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: VersionCheckDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct VersionCheckDataModel: Codable {
    var new_app_version: String
    var new_must_update: String
    
    var config_info: ConfigInfoDataModel?
}

struct ConfigInfoDataModel: Codable {
    var keyword_list: [KeywordListDataModel]?

    var board_promo_info: BoardPromoInfoDataModel?
    var blog_url: String?
}
struct KeywordListDataModel: Codable {
    var key01: String
    var key02: String
}

struct BoardPromoInfoDataModel: Codable {
    var reward_vote: Int
    var reward_point: Int
}
