//
//  ResponseJoinMember.swift
//  MyTrot
//
//  Created by hclim on 2021/03/18.
//

import Foundation
struct ResponseAddMember: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseAddMember, rhs: ResponseAddMember) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: String?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


