//
//  ResponsePointHistoryList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponsePointHistoryList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponsePointHistoryList, rhs: ResponsePointHistoryList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: PointHistoryDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct PointHistoryDataModel: Codable {
    var last_no: Int64
    var list: [PointHistoryDataListModel]
}

struct PointHistoryDataListModel: Codable {
    var subject: String
    var point_amount: Double
    var gubun: String
    var reg_dttm: String
}
