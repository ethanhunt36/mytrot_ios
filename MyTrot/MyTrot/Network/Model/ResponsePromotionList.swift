//
//  ResponsePromotionList.swift
//  MyTrot
//
//  Created by hclim on 2021/07/06.
//

import Foundation
struct ResponsePromotionList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponsePromotionList, rhs: ResponsePromotionList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: PromotionDataInfoModel?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}

struct PromotionDataInfoModel: Codable {
    var last_no: Int64
    var list_data : [PromotionListInfoModel]?

}
struct PromotionListInfoModel: Codable {
    var no: Int64
    var img01: String
    var read_cnt: Int
    var like_cnt: Int
    var comment_cnt: Int
    var reg_dttm: String
    var title: String
    var nick: String
}
