//
//  ResponseArtistVideoList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseArtistVideoList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseArtistVideoList, rhs: ResponseArtistVideoList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: ArtistVideoModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct ArtistVideoModel: Codable {
    var last_no: String
    var list_data: [ArtistVideoListModel]?
    var list: [ArtistVideoListModel]?
}

struct ArtistVideoListModel: Codable {
    var no: Int64
    var youtube_id: String
    var img01: String
    var artist_no: Int64
    var is_bookmark: Int?
    var title: String
    var gb: String
    var play_cnt: Int64
    var my_play_cnt: Int64
    var vod_min: Int
    var vod_sec: Int 
    var videoHeaderModel: VideoHeaderDataModel?
    var videoType: VideoListType?
}
struct VideoHeaderDataModel: Codable {
    var viewListType: String
}
