//
//  Response.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import RxCodable

struct CommonResponse: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: CommonResponse, rhs: CommonResponse) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    // PROPERTY
//    var data: Bool?

    /// fail
    var err: Int? = 200
    var msg: String?
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}

struct CommonStringResponse: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: CommonStringResponse, rhs: CommonStringResponse) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    // PROPERTY
    var data: String?

    /// fail
    var err: Int? = 200
    var msg: String?
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}

