//
//  ResponseCheerTotalList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseCheerTotalList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: ResponseCheerTotalList, rhs: ResponseCheerTotalList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    // PROPERTY
    var data: CheerTotalDataModel?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct CheerTotalDataModel: Codable {
    var list_data: [CheerTotalDataListModel]
}

struct CheerTotalDataListModel: Codable {
    var no: Int64
    var name: String
    var pic: String
    var rank1: Int64
    var bbs_cnt: Int64
    var my_bbs_cnt: Int64
    var like_cnt: Int64
    var my_like_cnt: Int64
    
    var cheerHeaderModel: CheerHeaderDataModel?
    var cheerListType: CheerListType?

}
