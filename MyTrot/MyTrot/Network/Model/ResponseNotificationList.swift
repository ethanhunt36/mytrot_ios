//
//  ResponseNotificationList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseNotificationList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: ResponseNotificationList, rhs: ResponseNotificationList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    // PROPERTY
    var data: NotificationDataModel?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct NotificationDataModel: Codable {
    var last_no: Int64
    var list_data: [NotificationDataListModel]
}

struct NotificationDataListModel: Codable {
    var cont: String
    var reg_dttm: String
}
