//
//  ResponseArtistSelectList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseArtistSelectList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseArtistSelectList, rhs: ResponseArtistSelectList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: ArtistSelectDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct ArtistSelectDataModel: Codable {
    var list_data: [ArtistSelectDataListModel]
}

struct ArtistSelectDataListModel: Codable {
    var no: Int64
    var name: String
    var pic: String
}
