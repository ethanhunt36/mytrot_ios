//
//  ResponseBoardDetail.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseBoardDetail: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseBoardDetail, rhs: ResponseBoardDetail) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: BoardDetailDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct BoardDetailDataModel: Codable {
    var is_ilike: Int?
    var view_data: BoardDetailViewDataModel
    var comment_data: BoardDetailCommentDataModel?
}
struct BoardDetailViewDataModel: Codable {
    var no: Int64
    var img01: String?
    var img02: String?
    var img03: String?
    var cont: String
    var is_like: String?
    var like_cnt: Int
    var read_cnt: Int
    var download_cnt: Int
    var member_info: BoardDetailViewMemberInfoDataModel
    var reg_dttm: String
    var del_yn: String

}
struct BoardDetailViewMemberInfoDataModel: Codable {
    var lv: Int?
    var nick: String
    var member_no: AnyCodable?

}
struct BoardDetailCommentDataModel: Codable {
    var last_no: String?
    var list_data: [BoardDetailCommentDataListModel]?

}

struct BoardDetailCommentDataListModel: Codable {
    var no: Int64?
    var cont: String?
    var member_info: BoardDetailViewMemberInfoDataModel?
    var reg_dttm: String?
    var del_yn: String?

}
