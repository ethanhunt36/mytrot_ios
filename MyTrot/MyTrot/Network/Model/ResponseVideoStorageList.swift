//
//  ResponseVideoStorageList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseVideoStorageList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseVideoStorageList, rhs: ResponseVideoStorageList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: VideoStorageDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct VideoStorageDataModel: Codable {
    var list: [VideoStorageDataListModel]
}

struct VideoStorageDataListModel: Codable {
    var no: Int64
    var gb: String
    var name: String
    var artist_pic: String?
    var vod_cnt: Int64
    var title: String?
    var play_cnt: Int64
    var my_play_cnt: Int64
    var vod_min: Int?
    var vod_sec: Int?
    var img01: String?
    var youtube_id: String?
    var is_bookmark: Int?
    var cnt: Int?
    var artist_no: Int64?
}
