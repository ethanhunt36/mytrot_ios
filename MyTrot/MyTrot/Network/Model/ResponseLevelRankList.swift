//
//  ResponseLevelRankList.swift
//  MyTrot
//
//  Created by hclim on 2021/07/12.
//

import Foundation
struct ResponseLevelRankList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseLevelRankList, rhs: ResponseLevelRankList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: LevelRankDataInfoModel?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}

struct LevelRankDataInfoModel: Codable {
    var list : [LevelRankListInfoModel]?
    var my_ranking: AnyCodable

}
struct LevelRankListInfoModel: Codable {
    var nick: String
    var lv_pnt: Int
    var lv: Int
}
