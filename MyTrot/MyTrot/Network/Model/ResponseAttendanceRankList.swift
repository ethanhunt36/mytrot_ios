//
//  ResponseAttendanceRankList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseAttendanceRankList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseAttendanceRankList, rhs: ResponseAttendanceRankList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: AttendanceRankDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct AttendanceRankDataModel: Codable {
    var list_data: [AttendanceRankDataListModel]
}

struct AttendanceRankDataListModel: Codable {
    var nick: String
    var message: String
}
