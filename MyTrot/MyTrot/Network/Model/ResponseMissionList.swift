//
//  ResponseMissionList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseMissionList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: ResponseMissionList, rhs: ResponseMissionList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    // PROPERTY
    var data: MissionDataModel?



    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct MissionDataModel: Codable {
    var list: [MissionDataListModel]
    var reward: MissionDataRewordModel?
    var reward_yn: String?
    var success_cnt: Int?
    var can_reward_yn: String?
    var podong_url: String?


}
struct MissionDataRewordModel: Codable {
    var reward_vote: Int
    var reward_point: Int

}
struct MissionDataListModel: Codable {
    var code: String
    var name: String
    var unit: String
    var stat: Int
    var cnt: Int64
    var complete_yn: String
    
    var voteCount: Int?
    var pointCount: Int?
    var completeCount: Int?
}
