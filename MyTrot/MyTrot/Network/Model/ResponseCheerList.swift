//
//  ResponseCheerList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseCheerList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: ResponseCheerList, rhs: ResponseCheerList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    // PROPERTY
    var data: CheerDataModel?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct CheerDataModel: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: CheerDataModel, rhs: CheerDataModel) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    var last_no: String?
    var list_data: [CheerDataListModel]?
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }

}

struct CheerDataListModel: Codable {
    var no: Int64
    var artist_pic: String
    var artist_name: String
    var my_like_cnt: Int64
//    var board_no: Int64
    var cont: String
    var reg_dttm: String
    var nick: String
    var lv: Int?
    var like_cnt: Int64
    var comment_cnt: Int64
    var del_yn: String
    
    var cheerHeaderModel: CheerHeaderDataModel?
    var cheerListType: CheerListType?

}

struct CheerHeaderDataModel: Codable {
    var isSelectedFavSinger: Bool
    var favSingerName: String
    var viewListType: String
}
