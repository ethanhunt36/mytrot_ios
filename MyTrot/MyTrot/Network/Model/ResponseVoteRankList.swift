//
//  ResponseVoteRankList.swift
//  MyTrot
//
//  Created by hclim on 2021/07/09.
//

import Foundation
struct ResponseVoteRankList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseVoteRankList, rhs: ResponseVoteRankList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: VoteRankDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}
struct VoteRankDataModel: Codable {
    var list: [VoteRankDataListModel]
    var my_ranking: AnyCodable
}

struct VoteRankDataListModel: Codable {
    var rank: Int
    var nick: String
    var vote_sum: Int64
    var reg_dttm: String
}
struct VoteRankDataMyModel: Codable {
    var rank: Int
    var nick: String
    var date: String
}


