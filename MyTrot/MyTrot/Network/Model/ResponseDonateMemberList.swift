//
//  ResponseDonateMemberList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseDonateMemberList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseDonateMemberList, rhs: ResponseDonateMemberList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: DonateMemberDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct DonateMemberDataModel: Codable {
    var list_data: [DonateMemberDataListModel]
    var search_cnt: Int

}

struct DonateMemberDataListModel: Codable {
    var member_nick: String?
    var sum_point: String
    var imgUrl: String?
    var search_cnt: Int?
}
