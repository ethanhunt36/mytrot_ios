//
//  ResponseArtistVideoListBySinger.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseArtistVideoListBySinger: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseArtistVideoListBySinger, rhs: ResponseArtistVideoListBySinger) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: ArtistVideoBySingerDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct ArtistVideoBySingerDataModel: Codable {
    var list_data: [ArtistVideoBySingerDataListModel]
}

struct ArtistVideoBySingerDataListModel: Codable {
    var name: String
    var vod_tro_cnt: Int64
    var vod_ent_cnt: Int64
    var vod_etc_cnt: Int64
    var pic: String
}
