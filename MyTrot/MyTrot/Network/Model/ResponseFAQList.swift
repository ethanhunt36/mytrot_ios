//
//  ResponseFAQList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseFAQList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseFAQList, rhs: ResponseFAQList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: FAQDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct FAQDataModel: Codable {
    var blog_data: [FAQDataListModel]
    var list_data: [FAQDataListModel]
    var last_no: String

}

struct FAQDataListModel: Codable {
    var no: Int64?
    var subject: String
    var cont: String?
    var url: String
    var reg_dttm: String?
    var img_path: String?
    var b_no: String?
}
