//
//  ResponseAuthNumber.swift
//  MyTrot
//
//  Created by hclim on 2021/05/09.
//

import Foundation
struct ResponseAuthNumber: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseAuthNumber, rhs: ResponseAuthNumber) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: OldMemberInfoModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}

struct OldMemberInfoModel: Codable {
    var old_member_info: MyInfoDataModel?
}
