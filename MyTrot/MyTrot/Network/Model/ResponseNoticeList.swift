//
//  ResponseNoticeList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseNoticeList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseNoticeList, rhs: ResponseNoticeList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: NoticeDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct NoticeDataModel: Codable {
    var blog_data: [NoticeDataListModel]
    var list_data: [NoticeDataListModel]
    var last_no: String

}

struct NoticeDataListModel: Codable {
    var no: Int64?
    var subject: String
    var cont: String?
    var url: String
    var reg_dttm: String?
    var img_path: String?
    var b_no: String?
}
