//
//  ResponsePpobkiInfo.swift
//  MyTrot
//
//  Created by hclim on 2021/06/08.
//

import Foundation
struct ResponsePpobkiInfo: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponsePpobkiInfo, rhs: ResponsePpobkiInfo) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: FunPlayInfoModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}

struct FunPlayInfoModel: Codable {
    var bonus: BonusNumberInfoModel?
//    var last_apply_date: Bool?
    var can_apply_dttm: String?
//    var last_apply_dttm: String?
    var list: [PpobkiDataInfoModel]?
    // 2022.04.26 수정 Y인경우, 단축 시간 API호출 
    var can_time_cheat: String?
}
struct BonusNumberInfoModel: Codable {
    var one: BonusNumberDataModel
    var two: BonusNumberDataModel
    var three: BonusNumberDataModel
    var four: BonusNumberDataModel
    var five: BonusNumberDataModel
    enum CodingKeys: String, CodingKey {
        case one  = "1"
        case two = "2"
        case three = "3"
        case four = "4"
        case five = "5"
    }

}
struct BonusNumberDataModel: Codable {
    var cnt: Int
    var vote_cnt: Int
    var point: Int
}
struct PpobkiDataInfoModel: Codable {
//    var dp_num: Int
    var use_yn: String
//    var bonus_num: Int
//    var upd_dttm: String
//    var end_yn: String
//    var end_dttm: String
    var no: Int
//    var member_no: Int
//    var reg_dttm: String
//    var cha: Int
}
