//
//  ResponseVoteList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/16.
//

import Foundation
struct ResponseVoteList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: ResponseVoteList, rhs: ResponseVoteList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    // PROPERTY
    var data: VoteDataModel?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct VoteDataModel: Codable {
    var point_amount: Double
    var amount_ticket_ad: String
    var list_data: [VoteDataListModel]
    var push_unread_cnt: Int
    
}

struct VoteDataListModel: Codable {
    var name: String
    var my_vote_cnt: String
    var vote_cnt_week: Int64
    var vote_cnt_month: Int64
    var vote_cnt: Int64
    var pic: String
    var no: Int64

    var voteHeaderModel: VoteHeaderDataModel?
    var viewListType: String?

}
struct VoteHeaderDataModel: Codable {
    var isSelectedFavSinger: Bool
    var viewListType: String
}
