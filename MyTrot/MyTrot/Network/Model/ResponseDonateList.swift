//
//  ResponseDonateList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/16.
//

import Foundation
struct ResponseDonateList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: ResponseDonateList, rhs: ResponseDonateList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    // PROPERTY
    var data: DonateDataModel?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct DonateDataModel: Codable {
    var list_data: [DonateDataListModel]
    var done_data: DonateDoneDataModel
}

struct DonateDoneDataModel: Codable {
    var donate_price: String
    var donate_count: Int

}

struct DonateDataListModel: Codable {
    var name: String
    var img01: String?
    var pic: String
    var donate_no: Int?
    var artist_no: Int64
    var donate_nth: Int?
    var counts: Int?
    var donate_cur_point: Int64
    var donate_my_point: String
    var donate_goal_point: Int64?
    var donate_reg_dttm: String
    var donate_end_dttm: String
    
    var donateHeaderModel: DonateHeaderDataModel?
    var donateType: DonateListType?
}
struct DonateHeaderDataModel: Codable {
    var isSelectedFavSinger: Bool
    var viewListType: String
    var done_data: DonateDoneDataModel?
}
