//
//  ResponseActivityCertInfo.swift
//  MyTrot
//
//  Created by hclim on 2021/06/11.
//

import Foundation
struct ResponseActivityCertInfo: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseActivityCertInfo, rhs: ResponseActivityCertInfo) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: ActivityCertInfoModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}

struct ActivityCertInfoModel: Codable {
    var pnt_board1: Int
    var pnt_board2 : Int
    var pnt_vote : String
    var pnt_play: String
    var pnt_play_c: Int
    var pnt_play_b: String
    var member_no: String
    var pnt_donate: String

}
