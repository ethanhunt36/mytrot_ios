//
//  ResponseProductList.swift
//  MyTrot
//
//  Created by hclim on 2021/08/05.
//

import Foundation
struct ResponseProductList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseProductList, rhs: ResponseProductList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: [ProductListInfoModel]?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}

struct ProductListInfoModel: Codable {
    var no: Int
    var app_type_id: Int
    var product_type: String
    var sku: String
    var name: String
    var price_i: Int
    var heart_cnt: Int
    var bonus_heart_cnt: Int
    var vote_cnt: Int
    var bonus_vote_cnt: Int
    var point: Int
    var bonus_point: Int
    var tag: String
    var description: String
    var period: Int
    var ruleset_codes: String
    var reg_date: String
}

