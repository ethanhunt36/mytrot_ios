//
//  ResponseMyRCMDCode.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseMyRCMDCode: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseMyRCMDCode, rhs: ResponseMyRCMDCode) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: MyRCMDCodeDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct MyRCMDCodeDataModel: Codable {
    var rcmd_code: String?
    var rcmd_cnt: Int?
    var ticket: Int?
    
    var point_target: Int?
    var ticket_target : Int?
    var point : Int?

}

