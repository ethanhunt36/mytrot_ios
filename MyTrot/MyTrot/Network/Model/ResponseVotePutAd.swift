//
//  ResponseVotePutAd.swift
//  MyTrot
//
//  Created by hclim on 2021/04/05.
//

import Foundation
struct ResponseVotePutAd: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseVotePutAd, rhs: ResponseVotePutAd) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: VotePutAdDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}

struct VotePutAdDataModel: Codable {
    var member : MyInfoDataModel
    var point_amount: Double
    var amount_ticket_ad: String

}
