//
//  ResponseLuckyNumberList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseLuckyNumberList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseLuckyNumberList, rhs: ResponseLuckyNumberList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: LuckyNumberDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct LuckyNumberDataModel: Codable {
    var duplicate_list2: [Duplicate2ListModel]
    var list: [LuckyNumberListModel]
}

struct Duplicate2ListModel: Codable {
    var num: Int
    var count: Int
}
struct LuckyNumberListModel: Codable {
    var num01: Int
    var num02: Int
    var num03: Int
    var num04: Int
    var num05: Int
    var num06: Int
    var num07: Int
    var num08: Int
    var num09: Int
    var num10: Int
    var num11: Int
    var num12: Int
    var dates: String
}
