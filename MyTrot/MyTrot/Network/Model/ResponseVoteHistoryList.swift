//
//  ResponseVoteHistoryList.swift
//  MyTrot
//
//  Created by hclim on 2021/03/17.
//

import Foundation
struct ResponseVoteHistoryList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970
    
    static func == (lhs: ResponseVoteHistoryList, rhs: ResponseVoteHistoryList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }
    
    enum Event {}
    
    // PROPERTY
    var data: VoteHistoryDataModel?
    
    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct VoteHistoryDataModel: Codable {
    var last_no: Int64
    var list: [VoteHistoryDataListModel]
}

struct VoteHistoryDataListModel: Codable {
    var subject: String
    var amount: Int
    var gubun: String
    var reg_dttm: String 
}
