//
//  ResponseArtistRanking.swift
//  MyTrot
//
//  Created by hclim on 2021/03/15.
//

import Foundation
struct ResponseArtistRankList: ModelType {
    let createdTime: Double = Date().timeIntervalSince1970

    static func == (lhs: ResponseArtistRankList, rhs: ResponseArtistRankList) -> Bool {
        return lhs.createdTime == rhs.createdTime
    }

    enum Event {}

    // PROPERTY
    var data: ArtistRankDataModel?

    /// fail
    var err: Int? = 200
    var msg: String? = nil
    init(err: Int?, msg: String?) {
        self.err = err
        self.msg = msg
    }
}


struct ArtistRankDataModel: Codable {
    var first_no: Int64
    var last_no: Int64
    var search_cnt: Int64
    var total_cnt: Int64
    var list_data: [ArtistRankDataListModel]
}

struct ArtistRankDataListModel: Codable {
    var rank1: Int64
    var rank1_score: Int64
    var name: String
    var my_play_cnt: String
    var my_bbs_cnt: Int64
    var my_vote_cnt: String
    var my_like_cnt: Int64
    var play_cnt: Int64
    var bbs_cnt: Int64
    var vote_cnt: Int64
    var like_cnt: Int64
    var pic: String
    var no: Int64
    var chartType: GenderType?
}

