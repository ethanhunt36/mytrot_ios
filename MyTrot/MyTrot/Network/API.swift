//
//  API.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import MoyaSugar
enum GenderType: String, Codable{
    case both = ""
    case male = "M"
    case female = "F"
}

enum VoteListType: String {
    case weekly = "vote_cnt_week"
    case monthly = "vote_cnt_month"
    case normally = "vote_cnt"
}
enum DonateListType: String, Codable {
    case doing = "donate.ing_list"
    case done = "donate.done_list"
    case total = "donate.done_st_list"
}
enum CheerListType: String, Codable {
    case recent = "recent"
    case popular = "popular"
    case total = "total"
}

enum GalleryListType: String, Codable {
    case recent = "recent"
    case popular = "popular"
    case comment = "comment"
}
enum VoteHistoryListType: String {
    case use = "USE"
    case get = "GET"
}

enum PointHistoryListType: String {
    case use = "USE"
    case get = "GET"
}
enum VideoStorageListType: String {
    case artist = "A"
    case trot = "V"

}
enum PointRankListType: String {
    case use = "USE"
    case get = "GET"
}
enum PointRankTermListType: String {
    case week = "week"
    case month = "month"
    case all = "all"
}

enum VoteRankListType: String {
    case use = "USE"
    case get = "GET"
}
enum VoteRankTermListType: String {
    case week = "week"
    case month = "month"
    case all = "all"
}

enum VideoListType: String, Codable {
    case recent = "recent"
    case title = "title"
    case play = "play"
    case editor_score = "editor_score"
}

enum BoardCategoryType: Int {
    case cheer = 1
    case gallery = 2
    case hongbo = 3
}

enum ReportCategoryType: Int {
    case nonSelected = 0
    case reportBoardIsNotGood = 80
    case reportBoardIsIncludingAd = 81
    case reportEtc = 99
}
enum MyTrotAPI {
    
    // MARK: -
    // MARK: 공통
    /// 버전체크
    case versionCheck
    
    /// 회원 추가 - 자동
    case addMember
    
    /// 회원 정보 조회
    case memberLogin
    
    /// 회원 정보 조회 (get_by_email)
    case getMemberInfo

    case loginAfter
    /// 차트
    case chartList
    /// 투표리스트
    case voteList(sort: VoteListType)
    /// 기부리스트
    case donateList(type: DonateListType)
    /// 응원리스트
    case cheerList(type: CheerListType, lastNo: String)
    /// 응원리스트 (by artist_no
    case cheerListByArtistNo(type: CheerListType, lastNo: String, artist_no: Int64)

    /// 신고하기
    case reportBoard(board_no: Int64, type: Int, reason: String)
    /// 응원통계리스트
    case cheerTotalList
    /// 미션
    case missionList
    /// 알림
    case notificationList(lastNo: Int64)
    /// 내 출석부
    case attendanceMyList(lastNo: String)
    /// 출석 랭크
    case attendanceRankList
    /// 출석 체크
    case attendanceCheckIn
    /// 영상( 전체공통)
    case vodList(type: VideoListType, last_no: String, artist_no: Int64, artist_name: String, keyword: String)
    /// 영상 (최애)
    case artistVideoListWithFav(lastNo: Int64)
    /// 영상 (전체)
    case artistVideoListWithTotal(lastNo: Int64)
    /// 영상 (가수별)
    case artistVideoListBySinger
    /// 영상 즐겨찾기
    case addBookmark(artist_no: Int64, vodNo: Int64)
    /// 영상 즐겨찾기 삭제
    case removeBookmark(artist_no: Int64, vodNo: Int64)
    /// Youtube Play
    case playYoutube(artist_vod_no: Int)
    /// Youtube finish
    case finishYoutube(artist_vod_no: Int)
    /// 기부증서 리스트
    case donateMemberList(donate_goal_no: Int, artist_no: Int64)
    /// 응원, 갤러리, 홍보 공통 상세
    case boardDetail(board_no: Int64, lastNo: Int64, category: BoardCategoryType)
    /// 응원글 좋아요
    case boardLike(board_no: Int64)
    /// 응원글 싫어요
    case boardUnLike(board_no: Int64)
    /// 행운의 번호
    case luckyNumberList
    /// 갤러리 리스트
    case galleryList(sort: GalleryListType, lastNo: String, artistNo: Int64)
    /// 내정보
    case getMyInfoByNo
    
    /// 미션 완료
    case completeMission
    /// 포동 미션 1회 체크
    case podongCheck
    
    /// 광고보고 투표권+포인트 얻기
    case votePutTicketAdPoint(ad_type: String)
    
    /// 내 추천인 코드
    case getMyRCDMCode
    /// 가수 리스트 (캐쉬처리)
    case artistSelectList
    /// 투표권 적립/사용 리스트
    case voteHistoryList(gubun: VoteHistoryListType, lastNo: Int64)
    /// 포인트 적립/사용 리스트
    case pointHistoryList(gubun: PointHistoryListType, lastNo: Int64)

    /// 투표권 순위 적립/사용 리스트
    case voteRankList(gubun: VoteRankListType, terms: VoteRankTermListType)
    /// 포인트 순위 적립/사용 리스트
    case pointRankList(gubun: PointRankListType, terms: PointRankTermListType )

    /// 공지사항
    case noticeList(last_no: String, is_html: String)
    /// 공지사항 읽음
    case noticeRead(no: Int64)
    /// FAQ
    case faqList(last_no: String, is_html: String)
    /// 영상보관함 (가수/트롯)
    case videoStorageList(gb: VideoStorageListType, artist_no: Int64)
    
    /// 이미지 다운로드
    case imageDownload(board_no: Int64)
    
    /// 인증번호 받기
    case getAuthNumber(phoneNo: String)
    /// 인증번호 확인
    case confirmAuthNumber(phoneNo: String, pin: String)
    /// 멤버 계정 선택
    case choiceMember(phoneNo: String)
    case blockAllBoard(boardNo: Int64)
    
    /// 활동 인증 조회
    case getCertInfo(artist_no: Int64, isYesterDay: Bool)
    
    /// 활동 인증 발급
    case checkCertInfo
    
    /// 뽑기 조회
    case getPpobkkiList
    case playPpobkki(no: Int)
    case cheatTimeApi
    
    /// 홍보인증
    case getPromotionList(last_no: Int64)
    case uploadPromotion(nick: String, title: String, cont: String, link: String, image1: Data, image2: Data, image3: Data)
    
    /// 레벨 경험치
    case levelRankList

    /// 탈퇴하기
    case unregister
    
    /// 추천인코드 가져오기

    // MARK: - POST
    case uploadVote(artist_no: Int64, vote_cnt: Int)
    case uploadDonate(artist_no: Int64, donate_cnt: Int)
    case uploadMyArtist(artist_no: Int64)
    case uploadComment(board_no: Int64, cont: String)
    case uploadBoard(artist_no: Int64, name: String, cont: String)
    case uploadGalleryBoard(artist_no: Int64, name: String, cont: String, image: Data)
    case updateNickname(nick: String)
    case uploadQuestion(cont: String, pics: [Data]?)
    case uploadAdditionalVideo(cont: String)

    // MARK: - DEL
    case deleteBoard(board_no: Int64)
    case deleteComment(comment_no: Int64)
    
    // MARK: - IAP
    /// 상품 리스트
    case getIAPList
    /// 상품 주문
    case addOrder(product_no: Int, price: Int)
    /// 상품 주문 -> 결제 완료
    case completeOrder(order_no: Int, price: Int, pay_type: String, sku: String, order_id: String, token: String)
    /// 포인트로 투표권 교환
    case exchangePoint(point_amount: Int)
    /// 내 보유 아이템 리스트 가져오기
    case getMyItemAll(product_type: String)

}
// MARK: - API Protocol
extension MyTrotAPI: SugarTargetType{
    var route: Route {
        switch self {
        case .cheatTimeApi,
                .unregister,
             .finishYoutube,
             .playYoutube,
             .getMyItemAll,
             .exchangePoint,
             .addOrder,
             .completeOrder,
             .getIAPList,
             .levelRankList,
             .voteRankList,
             .pointRankList,
             .uploadPromotion,
             .getPromotionList,
             .playPpobkki,
             .getPpobkkiList,
             .checkCertInfo,
             .getCertInfo,
             .uploadQuestion,
             .uploadAdditionalVideo,
             .choiceMember,
             .attendanceCheckIn,
             .podongCheck,
             .completeMission,
             .blockAllBoard,
             .reportBoard,
             .loginAfter,
             .noticeRead,
             .confirmAuthNumber,
             .getAuthNumber,
             .updateNickname,
             .imageDownload,
             .deleteComment,
             .deleteBoard,
             .uploadGalleryBoard(_,_,_,_):
            return .post("/_api/index.php")
        case .uploadVote(_,_),
             .uploadDonate(_,_),
             .uploadMyArtist(_),
             .uploadBoard(_,_,_),
             .uploadComment(_,_):
            return .get("/_api/index.php")
        case .memberLogin,
             .addMember,
             .versionCheck,
             .getMemberInfo:
            return .get("/_api/index.php")

        case .boardLike,
             .boardUnLike,
             .addBookmark,
             .removeBookmark,
             .vodList,
             .videoStorageList(_,_),
             .faqList(_,_),
             .noticeList(_,_),
             .pointHistoryList(_,_),
             .voteHistoryList(_,_),
             .artistSelectList,
             .getMyRCDMCode,
             .getMyInfoByNo,
             .galleryList(_,_,_),
             .luckyNumberList,
             .boardDetail(_,_,_),
             .donateMemberList,
             .chartList,
             .voteList(_),
             .donateList(_),
             .cheerList(_,_),
             .cheerListByArtistNo(_,_,_),
             .cheerTotalList,
             .missionList,
             .votePutTicketAdPoint(_),
             .notificationList(_),
             .attendanceMyList(_),
             .attendanceRankList,
             .artistVideoListWithFav(_),
             .artistVideoListWithTotal(_),
             .artistVideoListBySinger:
            return .get("/_api/index.php")
        }
    }
    var task: Task {
        switch self {
        case .uploadQuestion(let cont, let pics):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(cont, forKey: "cont")
            parameter.updateValue(2, forKey: "category")

            guard let picDatas = pics else {
                guard let parameters = self.parameters else { return .requestPlain }
                return .requestParameters(parameters: parameters.values, encoding: parameters.encoding)
            }
            var imgIndex = 1
            var multiPartImages: [MultipartFormData] = []
            for item in picDatas {
                let multipartImage = MultipartFormData(provider: MultipartFormData.FormDataProvider.data(item), name: "pic\(imgIndex)", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/jpeg")
                imgIndex = imgIndex + 1
                multiPartImages.append(multipartImage)
            }
            let multipartParam = Parameters.getCommonParameter(values: parameter, ssid: "shuttle.put")

            return .uploadCompositeMultipart(multiPartImages, urlParameters: multipartParam.values)

        case .uploadGalleryBoard(let artist_no, let name, let cont, let image):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_no, forKey: "artist_no")
            parameter.updateValue(name, forKey: "nick")
            parameter.updateValue(cont, forKey: "cont")
            parameter.updateValue(BoardCategoryType.gallery.rawValue, forKey: "category")
            let multipartImage = MultipartFormData(provider: MultipartFormData.FormDataProvider.data(image), name: "pic1", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/jpeg")
            let multipartParam = Parameters.getCommonParameter(values: parameter, ssid: "board.ins")
            return .uploadCompositeMultipart([multipartImage], urlParameters: multipartParam.values)
        case .uploadPromotion(let nick, let title, let cont, let link, let image1, let image2, let image3):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(nick, forKey: "nick")
            parameter.updateValue(title, forKey: "title")
            parameter.updateValue(cont, forKey: "cont")
            if link != "" {
                parameter.updateValue(link, forKey: "link")
            }
            var multipartDatas: [MultipartFormData] = []
            if image1.count > 0 {
                let multipartImage = MultipartFormData(provider: MultipartFormData.FormDataProvider.data(image1), name: "pic1", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/jpeg")
                multipartDatas.append(multipartImage)
            }
            if image2.count > 0 {
                let multipartImage = MultipartFormData(provider: MultipartFormData.FormDataProvider.data(image2), name: "pic2", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/jpeg")
                multipartDatas.append(multipartImage)
            }
            if image3.count > 0 {
                let multipartImage = MultipartFormData(provider: MultipartFormData.FormDataProvider.data(image3), name: "pic3", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/jpeg")
                multipartDatas.append(multipartImage)
            }
            let multipartParam = Parameters.getCommonParameter(values: parameter, ssid: "boardPromo.ins")
            return .uploadCompositeMultipart(multipartDatas, urlParameters: multipartParam.values)

        default:
            guard let parameters = self.parameters else { return .requestPlain }
            return .requestParameters(parameters: parameters.values, encoding: parameters.encoding)
        }
    }
    var parameters: Parameters? {
        switch self {
        case .cheatTimeApi:
            return Parameters.getCommonParameter(values: [:], ssid: "ppobkki.run_time_cheat")


        /// 탈퇴하기
        case .unregister:
            return Parameters.getCommonParameter(values: [:], ssid: "member.out")

        /// Youtube Play
        case .playYoutube(let artist_vod_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_vod_no, forKey: "artist_vod_no")
            parameter.updateValue(User.shared.myInfo?.member_no, forKey: "member_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "artistVod.play_start")

        /// Youtube finish
        case .finishYoutube(let artist_vod_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_vod_no, forKey: "artist_vod_no")
            parameter.updateValue(User.shared.myInfo?.member_no, forKey: "member_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "artistVod.play_end")


        case .getMyItemAll(let product_type):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(product_type, forKey: "product_type")

            return Parameters.getCommonParameter(values: parameter, ssid: "memberItem.get_all")

        case .exchangePoint(let point_amount):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(point_amount, forKey: "point_amount")

            return Parameters.getCommonParameter(values: parameter, ssid: "point.buy_vote_ticket")

        case .addOrder(let product_no, let price):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(product_no, forKey: "product_no")
            parameter.updateValue(price, forKey: "price")

            return Parameters.getCommonParameter(values: parameter, ssid: "pay.order_add")

        case .completeOrder(let order_no, let price, let pay_type, let sku, let order_id, let token):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(order_no, forKey: "order_no")
            parameter.updateValue(price, forKey: "price")
            parameter.updateValue(pay_type, forKey: "pay_type")
            parameter.updateValue(sku, forKey: "sku")
            parameter.updateValue(order_id, forKey: "order_id")
            parameter.updateValue(token, forKey: "token")

            return Parameters.getCommonParameter(values: parameter, ssid: "pay.sales_add")


        case .getIAPList:
            return Parameters.getCommonParameter(values: [:], ssid: "product.get_all")

        case .levelRankList:

            return Parameters.getCommonParameter(values: [:], ssid: "member.get_level_top_n")

        /// 투표권 순위 적립/사용 리스트
        case .voteRankList(let gubun, let terms):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(gubun.rawValue, forKey: "gubun")
            parameter.updateValue(terms.rawValue, forKey: "term")
            parameter.updateValue(100, forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: "vote.ranking_get_all")

        /// 포인트 순위 적립/사용 리스트
        case .pointRankList(let gubun, let terms):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(gubun.rawValue, forKey: "gubun")
            parameter.updateValue(terms.rawValue, forKey: "term")
            parameter.updateValue(100, forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: "point.ranking_get_all")


        case .getPromotionList(let last_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(last_no, forKey: "last_no")
            parameter.updateValue(30, forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: "boardPromo.listing")

        case .playPpobkki(let no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(no, forKey: "no")

            return Parameters.getCommonParameter(values: parameter, ssid: "ppobkki.play")

        case .getPpobkkiList:
            return Parameters.getCommonParameter(values: [:], ssid: "ppobkki.get_list")

        case .checkCertInfo:

            return Parameters.getCommonParameter(values: [:], ssid: "memberMission.check_mission_get_cert")

        case .getCertInfo(let artist_no, let isYesterDay):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_no, forKey: "artist_no")
//            parameter.updateValue("today", forKey: "period")
            if isYesterDay == true {
                parameter.updateValue("yesterday", forKey: "period")
            } else {
                parameter.updateValue("today", forKey: "period")
            }

            return Parameters.getCommonParameter(values: parameter, ssid: "member.get_cert_info")

        case .choiceMember(let phoneNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(phoneNo, forKey: "phone")

            return Parameters.getCommonParameter(values: parameter, ssid: "member.choice")

        case .attendanceCheckIn:
            return Parameters.getCommonParameter(values: [:], ssid: "attendance.attendance")
        case .podongCheck:
            return Parameters.getCommonParameter(values: [:], ssid: "memberMission.check_mission_podong")
        case .completeMission:
            return Parameters.getCommonParameter(values: [:], ssid: "memberMission.bonus")

        case .blockAllBoard(let boardNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(boardNo, forKey: "board_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "memberBlock.block_by_board")

        case .reportBoard(let board_no, let type, let cont):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(board_no, forKey: "board_no")
            parameter.updateValue("B", forKey: "accuse_type")
            parameter.updateValue(type, forKey: "accuse_reason")
            parameter.updateValue(cont, forKey: "cont")

            return Parameters.getCommonParameter(values: parameter, ssid: "accuse.ins")

        case .loginAfter:
            var parameter: [String : Any?] = [:]
            parameter.updateValue("2", forKey: "device_type_id")
            parameter.updateValue("MYTROT_IOS", forKey: "device_id")
            var versionName: String? {
                guard let dictionary = Bundle.main.infoDictionary,
                    let version = dictionary["CFBundleShortVersionString"] as? String else { return nil }

                return version
            }

            parameter.updateValue(versionName, forKey: "app_version")
            
            parameter.updateValue(PreferencesService().getFcmToken(), forKey: "registration_key")

            return Parameters.getCommonParameter(values: parameter, ssid: "Member.login_after")

        case .noticeRead(let no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(no, forKey: "no")

            return Parameters.getCommonParameter(values: parameter, ssid: "notice.read")

        case .confirmAuthNumber(let phoneNo, let pin):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(pin, forKey: "pin")
            parameter.updateValue(phoneNo, forKey: "phone")
            parameter.updateValue("MYTROT_IOS", forKey: "device_id")
            parameter.updateValue("member.verify_auth_pin", forKey: "ssid")
            parameter.updateValue("json", forKey: "type")

            return Parameters(encoding: URLEncoding(), values: parameter)


        case .getAuthNumber(let phoneNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(phoneNo, forKey: "phone")

            return Parameters.getCommonParameter(values: parameter, ssid: "member.send_auth_pin")

        case .updateNickname(let nick):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(nick, forKey: "nick")

            return Parameters.getCommonParameter(values: parameter, ssid: "member.set_update_nick_v2")

        case .imageDownload(let board_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(board_no, forKey: "board_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "board.inc_download")


        case .deleteComment(let comment_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(comment_no, forKey: "comment_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "board.cmt_del")

        case .deleteBoard(let board_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(board_no, forKey: "board_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "board.del")

        case .boardLike(let board_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(board_no, forKey: "board_no")
            parameter.updateValue("public", forKey: "key")

            return Parameters.getCommonParameter(values: parameter, ssid: "board.inc_like")

        case .boardUnLike(let board_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(board_no, forKey: "board_no")
            parameter.updateValue("public", forKey: "key")

            return Parameters.getCommonParameter(values: parameter, ssid: "board.inc_like")

        case .uploadGalleryBoard(let artist_no, let name, let cont,_):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_no, forKey: "artist_no")
            parameter.updateValue(name, forKey: "nick")
            parameter.updateValue(cont, forKey: "cont")
            parameter.updateValue(BoardCategoryType.gallery.rawValue, forKey: "category")
            
            return Parameters.getCommonParameter(values: parameter, ssid: "board.ins")
        case .uploadPromotion(let nick, let title, let cont, let link, _,_,_):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(nick, forKey: "nick")
            parameter.updateValue(title, forKey: "title")
            parameter.updateValue(cont, forKey: "cont")
            if link != "" {
                parameter.updateValue(link, forKey: "link")
            }

            return Parameters.getCommonParameter(values: parameter, ssid: "boardPromo.ins")

        case .uploadQuestion(let cont, _ ):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(cont, forKey: "cont")
            parameter.updateValue(2, forKey: "category")

            return Parameters.getCommonParameter(values: parameter, ssid: "shuttle.put")
        case .uploadAdditionalVideo(let cont):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(cont, forKey: "cont")
            parameter.updateValue(1, forKey: "category")

            return Parameters.getCommonParameter(values: parameter, ssid: "shuttle.put")

        case .uploadBoard(let artist_no, let name, let cont):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_no, forKey: "artist_no")
            parameter.updateValue(name, forKey: "nick")
            parameter.updateValue(cont, forKey: "cont")

            return Parameters.getCommonParameter(values: parameter, ssid: "board.ins")

        case .uploadComment(let board_no, let cont):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(board_no, forKey: "board_no")
            parameter.updateValue(cont, forKey: "cont")

            return Parameters.getCommonParameter(values: parameter, ssid: "board.cmt_ins")

        case .uploadMyArtist(let artist_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_no, forKey: "artist_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "member.update_favorite_artist")
        case .removeBookmark(let artist_no, let vodNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_no, forKey: "artist_no")
            parameter.updateValue(vodNo, forKey: "vod_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "MemberBookmark.del")

        case .addBookmark(let artist_no, let vod_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_no, forKey: "artist_no")
            parameter.updateValue(vod_no, forKey: "vod_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "MemberBookmark.set")

        case .getMemberInfo:
//            var parameter: [String : Any?] = [:]
//
//            let preference = PreferencesService()
//            parameter.updateValue(preference.getMyTrotUUID(), forKey: "email")
//            return Parameters.getCommonParameter(values: parameter, ssid: "member.get_by_email")

            return Parameters.getCommonParameter(values: [:], ssid: "member.get_by_no")
        case .uploadDonate(let artist_no, let donate_cnt):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_no, forKey: "artist_no")
            parameter.updateValue(donate_cnt, forKey: "point")

            return Parameters.getCommonParameter(values: parameter, ssid: "donate.donate")
        
        case .uploadVote(let artist_no, let vote_cnt):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(artist_no, forKey: "artist_no")
            parameter.updateValue(vote_cnt, forKey: "vote_cnt")

            return Parameters.getCommonParameter(values: parameter, ssid: "vote.do_vote")

        case .memberLogin:
            var parameter: [String : Any?] = [:]
            let preference = PreferencesService()
            parameter.updateValue(preference.getMyTrotUUID(), forKey: "email")
            parameter.updateValue(preference.getMyTrotUUID(), forKey: "sns_id")
            parameter.updateValue("", forKey: "pw")
            parameter.updateValue("I", forKey: "login_type")

            return Parameters.getCommonParameter(values: parameter, ssid: "Member.login")
        case .addMember:
            var parameter: [String : Any?] = [:]
            let preference = PreferencesService()
            parameter.updateValue(preference.getMyTrotUUID(), forKey: "email")
            parameter.updateValue("", forKey: "pw")
            parameter.updateValue("I", forKey: "login_type")
            parameter.updateValue(preference.getMyTrotUUID(), forKey: "sns_id")
            parameter.updateValue("2", forKey: "device_type_id")
            parameter.updateValue("MYTROT_IOS", forKey: "device_id")
            var versionName: String? {
                guard let dictionary = Bundle.main.infoDictionary,
                    let version = dictionary["CFBundleShortVersionString"] as? String else { return nil }

                return version
            }

            parameter.updateValue(versionName, forKey: "app_version")
            parameter.updateValue(PreferencesService().getFcmToken(), forKey: "registration_key")

            return Parameters.getCommonParameter(values: parameter, ssid: "Member.add")

        case .versionCheck:
            var parameter: [String : Any?] = [:]
            var versionName: String? {
                guard let dictionary = Bundle.main.infoDictionary,
                    let version = dictionary["CFBundleShortVersionString"] as? String else { return nil }

                return version
            }

            parameter.updateValue(versionName, forKey: "app_version")
            parameter.updateValue("MYTROT_IOS", forKey: "device_id")
            parameter.updateValue("2", forKey: "device_type_id")

            return Parameters.getCommonParameter(values: parameter, ssid: "common.check_app_version")

        case .videoStorageList(let gb, let artist_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(gb.rawValue, forKey: "gb")
            parameter.updateValue(0, forKey: "last_no")
            parameter.updateValue("99999", forKey: "record")
            if artist_no > 0 {
                parameter.updateValue(artist_no, forKey: "artist_no")
            }

            return Parameters.getCommonParameter(values: parameter, ssid: "MemberBookmark.get_all")

        case .faqList(let last_no, let is_html):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(is_html, forKey: "is_html")
            parameter.updateValue(last_no, forKey: "last_no")
            parameter.updateValue("30", forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: "faq.listing")
        case .noticeList(let last_no, let is_html):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(is_html, forKey: "is_html")
            parameter.updateValue(last_no, forKey: "last_no")
            parameter.updateValue("30", forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: "notice.listing")

        case .pointHistoryList(let gubun, let lastNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(gubun.rawValue, forKey: "gubun")
            parameter.updateValue(lastNo, forKey: "last_no")
            parameter.updateValue("30", forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: "MemberPointHistory.listing")
        case .voteHistoryList(let gubun, let lastNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(gubun.rawValue, forKey: "gubun")
            parameter.updateValue(lastNo, forKey: "last_no")
            parameter.updateValue("30", forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: "vote.get_all_history")

        case .artistSelectList:
            var parameter: [String : Any?] = [:]
            parameter.updateValue("sort", forKey: "sort")
            parameter.updateValue("99999", forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: "artist.vote_listing")

        case .getMyRCDMCode:
            return Parameters.getCommonParameter(values: [:], ssid: "member.get_rcmd_code")

        case .getMyInfoByNo:
            return Parameters.getCommonParameter(values: [:], ssid: "member.get_by_no")

        case .votePutTicketAdPoint(let ad_type):
            var parameter: [String : Any?] = [:]
            parameter.updateValue(ad_type, forKey: "ad_type")
            return Parameters.getCommonParameter(values: parameter, ssid: "vote.put_ticket_ad_point")
            
        case .galleryList(let sort, let lastNo, let artistNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue("2", forKey: "category")
            parameter.updateValue("30", forKey: "record")
            parameter.updateValue(sort, forKey: "sort")
            if lastNo == "" {
                parameter.updateValue("-1", forKey: "last_no")
            } else {
                parameter.updateValue(lastNo, forKey: "last_no")
            }
            
            if artistNo > 0 {
                parameter.updateValue(artistNo, forKey: "srch_artist_no")
            }
            

            return Parameters.getCommonParameter(values: parameter, ssid: "board.listing")

        case .luckyNumberList:

            return Parameters.getCommonParameter(values: [:], ssid: "luckyNumber.get_today_number_list")

        case .boardDetail(let board_no, let lastNo, let category):
            var parameter: [String : Any?] = [:]
            parameter.updateValue("2000", forKey: "record")
            parameter.updateValue("recent", forKey: "sort")
            parameter.updateValue(board_no, forKey: "board_no")
            parameter.updateValue(lastNo, forKey: "last_no")
            parameter.updateValue(category.rawValue, forKey: "category")
            return Parameters.getCommonParameter(values: parameter, ssid: "board.view")

        case .donateMemberList(let donate_goal_no, let artist_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue("99999", forKey: "record")
            parameter.updateValue(donate_goal_no, forKey: "donate_goal_no")
            parameter.updateValue(artist_no, forKey: "artist_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "donate.member_list")

        case .vodList(let type, let last_no, let artist_no, let artist_name, let keyword):
            var parameter: [String : Any?] = [:]
            parameter.updateValue("30", forKey: "record")
            parameter.updateValue(last_no, forKey: "last_no")
            parameter.updateValue(type.rawValue, forKey: "sort")
            parameter.updateValue(artist_no, forKey: "artist_no")
            if artist_name != "" {
                parameter.updateValue(artist_name, forKey: "srch_artist_name")
            }
            if keyword != "" {
                parameter.updateValue(keyword, forKey: "keyword")
            }
            return Parameters.getCommonParameter(values: parameter, ssid: "artistVod.listing")

        case .artistVideoListWithFav(let lastNo):
            var parameter: [String : Any?] = [:]
            let artistNo = PreferencesService().getFavSingerNo()
            parameter.updateValue("30", forKey: "record")
            parameter.updateValue(lastNo, forKey: "last_no")
            parameter.updateValue(artistNo, forKey: "artist_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "artistVod.listing")
        case .artistVideoListWithTotal(let lastNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue("30", forKey: "record")
            parameter.updateValue(lastNo, forKey: "last_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "artistVod.listing")
        case .artistVideoListBySinger:
            var parameter: [String : Any?] = [:]
            parameter.updateValue("99999", forKey: "record")
            parameter.updateValue("vod_cnt", forKey: "sort")

            return Parameters.getCommonParameter(values: parameter, ssid: "artist.vote_listing")

        case .attendanceMyList(let lastNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue("30", forKey: "record")
            parameter.updateValue(lastNo, forKey: "last_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "attendance.listing")

        case .attendanceRankList:
            var parameter: [String : Any?] = [:]
            parameter.updateValue("100", forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: "attendance.ranking")

        case .notificationList(let lastNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue("30", forKey: "record")
            parameter.updateValue(lastNo, forKey: "last_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "alram.listing")

        case .chartList:

            var parameter: [String : Any?] = [:]
            parameter.updateValue("public", forKey: "key")
            parameter.updateValue("99999", forKey: "record")
            parameter.updateValue(Utils.getLastSavedChartGender().rawValue, forKey: "gender")

            return Parameters.getCommonParameter(values: parameter, ssid: "artist.ranking1")
        case .voteList(let sort):

            var parameter: [String : Any?] = [:]
            parameter.updateValue("public", forKey: "key")
            parameter.updateValue("99999", forKey: "record")
            parameter.updateValue(Utils.getLastSavedVoteType().rawValue, forKey: "sort")
            parameter.updateValue(Utils.getLastSavedVoteGender().rawValue, forKey: "gender")

            return Parameters.getCommonParameter(values: parameter, ssid: "artist.vote_listing")
        case .donateList(let type):
            var parameter: [String : Any?] = [:]
            parameter.updateValue("public", forKey: "key")
            parameter.updateValue("99999", forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: type.rawValue)
        case .cheerList(let type, let lastNo):
            var parameter: [String : Any?] = [:]
            parameter.updateValue("30", forKey: "record")
            parameter.updateValue(type.rawValue, forKey: "sort")
            parameter.updateValue(lastNo, forKey: "last_no")

            return Parameters.getCommonParameter(values: parameter, ssid: "board.listing")
        case .cheerListByArtistNo(let type, let lastNo, let artist_no):
            var parameter: [String : Any?] = [:]
            parameter.updateValue("30", forKey: "record")
            parameter.updateValue(type.rawValue, forKey: "sort")
            parameter.updateValue(lastNo, forKey: "last_no")
            if artist_no > 0 {
                parameter.updateValue(artist_no, forKey: "srch_artist_no")
            }

            return Parameters.getCommonParameter(values: parameter, ssid: "board.listing")

        case .cheerTotalList:
            var parameter: [String : Any?] = [:]
            parameter.updateValue("99999", forKey: "record")

            return Parameters.getCommonParameter(values: parameter, ssid: "artist.stat")

        case .missionList:
            return Parameters.getCommonParameter(values: [:], ssid: "memberMission.get")

        }
    }

    var baseURL: URL {
        
        return URL(string: Constants.Network.Environment.backEndURL)!
    }

    var headers: [String: String]? {

        return MyTrotAPI.getMixWithCommonHeader(nil)
//        switch self {
//        case .main:
//            return MyTrotAPI.getMixWithCommonHeader(["User-Agent": "mytrot_ios"])
//        case .chartList:
//            return MyTrotAPI.getMixWithCommonHeader(["User-Agent": "mytrot_ios"])
//        }
    }
    var sampleData: Data {
        return Data()
    }

}
// MARK: - API Utils
extension MyTrotAPI {
    /// 해더에 공통해더 정보를 섞은 해더 정보를 리턴합니다.
    ///
    /// - parameter header: 설정할 해더정보
    /// - returns: 공통 해더 정보를 섞은 해더정보
    static func getMixWithCommonHeader(_ header: [String: String]?) -> [String: String] {
        var value: [String: String] = [:]
        // 언어 정보는 미리 넘겨 두어도 좋치 않을까요?
        value["Content-Language"] = Locale.current.languageCode
        value["User-Agent"] = "mytrot_ios"
        
        guard let `header` = header else { return value }
        _ = header.keys.map { key -> Void in
            value[key] = header[key]
        }
        return value
    }

}


extension Parameters {
    static func getCommonParameter(values: [String : Any?], ssid: String) -> Parameters {
        var commonValues = values
        commonValues.updateValue(ssid, forKey: "ssid")
        commonValues.updateValue("json", forKey: "type")

        // 안드로이드 (팬65335)
//        commonValues.updateValue("WHVXa0g1UTFsSE9hQ2lvQ0dqTWlIQT09", forKey: "member_no")
        
        let preference = PreferencesService()
        if preference.getMember_no() != "" {
            commonValues.updateValue(preference.getMember_no(), forKey: "member_no")
        }
        
        //test
//        commonValues.updateValue("WHVXa0g1UTFsSE9hQ2lvQ0dqTWlIQT09", forKey: "member_no")

        return Parameters(encoding: URLEncoding(), values: commonValues)
    }
}
