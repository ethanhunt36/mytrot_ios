//
//  Networking.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import MoyaSugar
import RxSwift



final class Networking<Target: SugarTargetType>: MoyaSugarProvider<Target> {
    init(plugins: [PluginType] = []) {
        let session = Session(startRequestsImmediately: false)
        super.init(session: session, plugins: plugins)
    }
    static func catchError<T: ModelType>(_ error: Swift.Error) -> PrimitiveSequence<SingleTrait, T> {
        Networking.loggingForError(error, model: T.self)

        if let response = (error as? MoyaError)?.response {
            if response.statusCode == 200 {
                if let jsonObject = try? response.mapJSON(failsOnEmptyData: false), let messageObject = jsonObject as? [String: Any] {
                    
                    let message = messageObject["msg"] as? String ?? ""
                    let err = messageObject["err"] as? Int ?? 0
                    return .just((T.self as T.Type).init(err: err, msg: message))
                }
                return .just((T.self as T.Type).init(err: 999, msg: Constants.Network.ErrorMessage.networkResponseError))
            }
            if response.statusCode != 200, let jsonObject = try? response.mapJSON(failsOnEmptyData: false), let messageObject = jsonObject as? [String: Any] {
                
                
                return .just((T.self as T.Type).init(err: response.statusCode, msg: Constants.Network.ErrorMessage.networkStatusCodeError))
            } else {
                print(response)
            }
        }
//        RxBus.shared.post(event: Events.AlertShow(message: Constants.Network.ErrorMessage.networkError))

        return .just((T.self as T.Type).init(err: 999, msg: Constants.Network.ErrorMessage.networkMoyaError))
    }

}
typealias CommonNetworking = Networking<MyTrotAPI>

// MARK: - loading
extension Networking {
    func startLoading() -> Networking<Target> {
//        let activityData = ActivityData(type: .aptnerBlueSmilingDongle)
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)

        MyTrotIndicatiorPresenter.shared.startAnimating("로딩중입니다...")
        return self
    }
}

extension Single {
    func stopLoading() -> PrimitiveSequence<SingleTrait, Element> {
        return self.asObservable().map { data -> Element in
            MyTrotIndicatiorPresenter.shared.stopAnimating()
//            DispatchQueue.main.async {
//                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//            }
            return data
        }.asSingle().primitiveSequence
    }
}

extension Networking {
    /// status code 미 예외 처리 (401에러가 나더라도 토큰 갱신이 없습니다.)
    func request(
        _ target: Target,
        file: StaticString = #file,
        function: StaticString = #function,
        line: UInt = #line
        ) -> Single<Response> {
        // 요청 로그 찍기
        Networking.loggingForRequest(target, isUnauthorized: true)
        
        let requestString = "\(target.method) \(target.path) "
        return self.rx.request(target)
            .filter(statusCode: 200)
            .debug()
            .do(onSuccess: { value in
                let message = "SUCCESS: \(requestString) \(value.statusCode)"
                print("\(message) ,file\(file), function: \(function), line: \(line)")
                print("request : \(String(describing: value.request?.debugDescription))")
//                let requestDesc = String(describing: value.request?.debugDescription)
                if String(describing: function) == "artistSelectList()" {
                    print("== artist.vote_listing == 응답 저장")
                    let preference = PreferencesService()
                    preference.setFavSingerModel(model: value.data)
                }
            }, onError: { error in
                // 응답 에러 로그 찍기
                Networking.loggingForError(error)
            }, onSubscribed: {
                let message = "REQUEST: \(requestString)"
                print("\(message) ,file\(file), function: \(function), line: \(line)")
            })
            .debug()
    }

}

/// 요청 카운트
var countRequestNetworking = 1

extension Networking {
    static func loggingForRequest(_ target: Target, isUnauthorized: Bool = false) {
        #if DEBUG
        // 현재 요청 카운트
        let count = countRequestNetworking
        // 요청 카운트 증가 처리
        countRequestNetworking += 1

        var emoji = "🚀"
        if isUnauthorized { emoji = "🚀📵" }


        print("\n\n")
        print("\(emoji)=================================================")
        print("\(emoji)== \(count)번째 요청, \(Date())")
        print("\(emoji)==  URL : \(target.url.absoluteString), baseURL : \(target.baseURL.absoluteString)")
        print("\(emoji)==  Method: \(target.method), Path: \(target.path)")
        print("\(emoji)=================================================")
        if let headers = target.headers {
           print("\(emoji)== Headers\n\(headers)")
        } else {
           print("\(emoji)== Headers: 🤪.. null")
        }
        print("\(emoji)=================================================")
        if let parameter = target.parameters {
           print("\(emoji)== Parameter\n\(parameter)")
        } else {
           print("\(emoji)== Parameter: 🤪.. null")
        }
        print("\(emoji)=================================================")
        print("\n\n")
        #endif
    }

    static func loggingForError(_ error: Swift.Error, model: Any? = nil, isUnauthorized: Bool = false) {
        #if DEBUG
        var emoji = "💀"
        if isUnauthorized { emoji = "💀📵" }

        let printMoyaErrorRealLog: (String, Moya.Response) -> Void = { emoji, response in
            do {
                let valueJson = try response.mapJSON()
                print("\(emoji)=================================================")
                print("\(emoji)== JSON Format 출력 (서버 데이터 확인용)")
                #if DEBUG
                let errorJson = "JSON Format error "
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: valueJson, options: .prettyPrinted)
                    print(String(bytes: jsonData, encoding: String.Encoding.utf8) ?? errorJson)
                } catch {
                    print(errorJson)
                }
                #else
                print("\(emoji)== >> DEBUG 모드에서만 로그를 찍습니다.")
                #endif
            } catch {
                print("\(emoji)=================================================")
                print("\(emoji)== >> JSON Format 출력 실패!!")
            }
            print("\(emoji)=================================================")
            print("\n\n")
        }

        if let response = (error as? MoyaError)?.response {
            print("\n\n")
            print("\(emoji)=================================================")
            print("\(emoji)== 🙅 응답 데이터 \(Date())")
            print("\(emoji)== URL: \(String(describing: response.request?.url?.absoluteString))")
            if let unwrappingModel = model {
                print("\(emoji)== 모델명: \(unwrappingModel)")
            }
            print("\(emoji)== 😱 MoyaError Description")
            print("\(emoji)== \(error.localizedDescription)")
            print("\(emoji)=================================================")
            print("\(emoji)== 상세 내용")
            print("\(emoji)== \(error)")
            printMoyaErrorRealLog(emoji, response)
//            print("\(emoji)=================================================")
//            print("\n\n")
        } else {
            print("\n\n")
            print("\(emoji)=================================================")
            print("\(emoji)== 🤦 응답 데이터 \(Date())")
            print("\(emoji)== 인터넷에 문제가 있거나, 서버에 요청이 아예 안되는 경우??")
            print("\(emoji)== 😱 Error Description")
            print("\(emoji)== \(error.localizedDescription)")
            print("\(emoji)=================================================")
            print("\(emoji)== 상세 내용")
            print("\(emoji)== \(error)")
            print("\(emoji)=================================================")
            print("\n\n")
        }
        #endif
    }
}



extension PrimitiveSequenceType where Trait == SingleTrait, Element == Moya.Response {
    func asMoyaResponseLog<T>() -> PrimitiveSequence<Trait, T> {
        return self.map {
            do {
                let valueJson = try $0.mapJSON()
                
                print("\n\n")
                print("🔥=======================================")
                print("🔥== 🤩 응답 데이터 \(Date())")
                print("🔥== URL : \(String(describing: $0.request?.url?.absoluteString))")
                print("🔥=======================================")
                print("🔥== JSON Format 출력 (모델링 하기 전, 서버 데이터 확인용)")
                let requestUrlString = String(describing: $0.request?.url?.absoluteString)
                #if DEBUG
                let errorJson = "JSON Format error "
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: valueJson, options: .prettyPrinted)
                    print(String(bytes: jsonData, encoding: String.Encoding.utf8) ?? errorJson)
                } catch {
                    print(errorJson)
                }
                // 필요시 주석 풀어서 사용해 보세요. (단, 크레쉬가 있을 수 있어요)
//                print("\(String(describing: valueJson).koLog())") // <- 이 방법은 크래쉬를 유발해서 사용하지 않음

                #else
//                debugPrint("\(valueJson)")
                #endif
//                print("\(valueJson)")
                print("🔥=======================================")
                print("\n\n")
//                print("\n\n=======================================\n== 🔥응답 데이터 \(Date())\n=======================================\n\(valueJson)\n=======================================\n")
//                dump(valueJson)
            } catch {
                print("\n\n")
                print("👻=======================================")
                print("👻== 😱 응답 데이터 \(Date())")
                print("👻== URL : \(String(describing: $0.request?.url?.absoluteString))")
                print("👻=======================================")
                print(error)
                print("👻=======================================")
                print("\n\n")
            }
            return $0 as! T
        }
    }
}

