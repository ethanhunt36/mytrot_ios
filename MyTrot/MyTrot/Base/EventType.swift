//
//  EventType.swift
//  MyTrot
//
//  Created by gaekuri on 2021/12/04.
//

import Foundation
import RxSwift
protocol EventType {
    associatedtype Event

}

private var streams: [String: Any] = [:]

extension EventType {
  static var event: PublishSubject<Event> {
    let key = String(describing: self)
    if let stream = streams[key] as? PublishSubject<Event> {
      return stream
    }
    let stream = PublishSubject<Event>()
    streams[key] = stream
    return stream
  }
}
