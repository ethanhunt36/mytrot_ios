//
//  BaseViewController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
import Reusable

class BaseViewController: UIViewController, StoryboardBased {
    override func viewDidLoad() {
        super.viewDidLoad()
//        UIApplication.shared.statusBarView?.backgroundColor = UIColor.green

        initialize()
    }

}


// MARK: -
// MARK: private initialize
private extension BaseViewController {
    /// 초기 설정
    func initialize() {
        initializeLayout()
        initializeEventBus()
    }

    /// 레이아웃 초기 설정
    func initializeLayout() {}

    /// 이벤트 받는 부분 초기 설정
    func initializeEventBus() {}
}

// MARK: -
// MARK: Reactor subscribe On Next
extension BaseViewController {
    /// subscribe onNext, 기본 공통 알림 처리
    func baseAlert(message: String) {
        let controller = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        self.present(controller, animated: true)
    }
    func subscribeOnNextBaseAlert(alertData: String) {
        let controller = UIAlertController.init(title: "", message: alertData.replaceSlashString, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "확인", style: .cancel)
        controller.addAction(defaultAction)

        self.present(controller, animated: true)
    }
    func subscribeOnNextBaseAlertPushBack(alertData: String) {
        let controller = UIAlertController.init(title: "", message: alertData.replaceSlashString, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "확인", style: .cancel, handler: { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        })
        
        controller.addAction(defaultAction)

        self.present(controller, animated: true)
    }

}


extension UIApplication {
    static func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = base as? UINavigationController {
            return topViewController(base: navigationController.visibleViewController)
        }

        if let tabBarController = base as? UITabBarController, let selectedController = tabBarController.selectedViewController {
            return topViewController(base: selectedController)
        }

        if let presentedController = base?.presentedViewController {
            return topViewController(base: presentedController)
        }
        return base
    }
}

