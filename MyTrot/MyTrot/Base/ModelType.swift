//
//  ModelType.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import Then
import RxSwift

protocol ModelType: Codable, Then, Hashable {
//    associatedtype Event

    static var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy { get }

    var err: Int? { get set }
    var msg: String? { get set }

    init(err: Int?, msg: String?)

    func isResponseStatusSuccessful() -> Bool

    /// 모델 생성시간, hashValue 생성을 위한 값으로 사용
    var createdTime: Double { get }
}

extension ModelType {
    func hash(into hasher: inout Hasher) {
        hasher.combine(createdTime)
    }
}

extension ModelType {
    static var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy {
        return .iso8601
    }

    static var decoder: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = self.dateDecodingStrategy
        return decoder
    }

    func isResponseStatusSuccessful() -> Bool {
        return self.err == nil || self.err == 0
    }
}

//private var streams: [String: Any] = [:]
//
//extension ModelType {
//  static var event: PublishSubject<Event> {
//    let key = String(describing: self)
//    if let stream = streams[key] as? PublishSubject<Event> {
//      return stream
//    }
//    let stream = PublishSubject<Event>()
//    streams[key] = stream
//    return stream
//  }
//}
