//
//  BaseTableViewCell.swift
//  MyTrot
//
//  Created by hclim on 2021/03/14.
//

import UIKit
import Reusable

class BaseTableViewCell: UITableViewCell, NibReusable {

    override func awakeFromNib() {
        super.awakeFromNib()

        initializeErrorScreen()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension BaseTableViewCell {
    func initializeErrorScreen() {
        print("\(#function)")
    }
}
