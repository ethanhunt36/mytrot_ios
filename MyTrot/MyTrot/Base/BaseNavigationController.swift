//
//  BaseNavigationController.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import UIKit
class BaseNavigationController: UINavigationController {
    // MARK: - Private Properties
    fileprivate var duringPushAnimation = false

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        interactivePopGestureRecognizer?.delegate = self

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Overrides iOS 쓸어넘기기 기능
    override func pushViewController(_ viewController: UIViewController,
                                     animated: Bool) {
        duringPushAnimation = true
        super.pushViewController(viewController, animated: animated)
    }
    deinit {
        delegate = nil
        interactivePopGestureRecognizer?.delegate = nil
    }


}

extension BaseNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              didShow viewController: UIViewController,
                              animated: Bool) {
        guard let baseNavigationController = navigationController as? BaseNavigationController else { return }
        baseNavigationController.duringPushAnimation = false
    }
}
extension BaseNavigationController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard gestureRecognizer == interactivePopGestureRecognizer else {
            return true // default value
        }

        // 뒤로 가는 Gesture 조건 ,
        // 1) viewControllers 의 갯수가 1개 이상
        // 2) pushViewController() override func으로 들어왔는지 여부
        return viewControllers.count > 1 && duringPushAnimation == false
    }
}

