//
//  BaseViewReactor.swift
//  MyTrot
//
//  Created by hclim on 2021/03/09.
//

import Foundation
import ReactorKit
import RxFlow

protocol BaseReactor: Reactor, Stepper {
    func navigate(step: Step)
}

extension BaseReactor {
    func navigate(step: Step) {
        self.steps.accept(step)
    }

    func responseErrorForDebuggingToast<T: ModelType>(_ modelTypeData: T?, isShowingToast: Bool = true) -> Bool {
        var isNotError = true
        #if DEBUG
        let duration = 1.0
        let isSuccess = modelTypeData?.err == nil || modelTypeData?.err == 0
        if isSuccess == false,
            let data = modelTypeData,
            let message = data.msg {
            print("\n🍞 ==========================================================")
            print("== status: \(String(describing: data.err)), message: \(message)")
            print("== detail ===================================================")
            print("== \(data.self)")
            print("=============================================================\n")
            if isShowingToast {
                Toast(text: "🍞 \(String(describing: data.err)): \(message)", duration: duration).show()
            }
            isNotError = false
        }
        #endif
        return isNotError
    }

    func transform(action: Observable<Action>) -> Observable<Action> {
      return action.debug("BaseReactor Debug action") // Use RxSwift's debug() operator
    }
}

